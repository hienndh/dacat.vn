<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** @var \Illuminate\Support\Facades\Route $router */
/** AUTH ADMIN VIEW */
$router->group(['prefix' => 'ta-admin'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    $router->get('/login', 'AuthController@loginView')->name('admin_auth_login_view');
    $router->get('/register', 'AuthController@registerView')->name('admin_auth_register_view');
    $router->get('/recover-password', 'AuthController@registerView')->name('admin_auth_recover_view');
    $router->get('/reset-password', 'AuthController@resetPasswordView')->name('admin_auth_reset_password_view');
});

/** AUTH ADMIN ACTION */
$router->post('/auth/login', 'AuthController@login')->name('admin_auth_login');
$router->post('/auth/register', 'AuthController@register')->name('admin_auth_register');
$router->post('/auth/recover-password', 'AuthController@recover')->name('admin_auth_recover');

$router->group(['middleware' => ['auth']], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    $router->get('/auth/logout', 'AuthController@logout')->name('admin_auth_logout');
    $router->post('/auth/reset-password', 'AuthController@resetPassword')->name('admin_auth_reset_password');
});

/** AUTH CLIENT VIEW */
$router->get('/login', 'Client\AccController@loginView')->name('login');
$router->group(['namespace' => 'Client', 'prefix' => 'account'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    $router->get('/login', 'AccController@loginView')->name('client_auth_login_view');
    $router->get('/register', 'AccController@registerView')->name('client_auth_register_view');
    $router->get('/recover-password', 'AccController@registerView')->name('client_auth_recover_view');
    $router->get('/reset-password', 'AccController@resetPasswordView')->name('client_auth_reset_password_view');
});

/** AUTH CLIENT ACTION */
$router->group(['namespace' => 'Client'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    $router->post('/acc/login', 'AccController@login')->name('client_auth_login');
    $router->post('/acc/register', 'AccController@register')->name('client_auth_register');
    $router->post('/acc/recover-password', 'AccController@recover')->name('client_auth_recover');
});

$router->group(['namespace' => 'Client', 'middleware' => ['auth']], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    $router->get('/acc/logout', 'AccController@logout')->name('client_auth_logout');
    $router->post('/acc/reset-password', 'AccController@resetPassword')->name('client_auth_reset_password');
});

/** ========================== ADMIN VIEW ========================= */
// , 'middleware' => ['auth', 'role.admin']
$router->group(['namespace' => 'Admin', 'prefix' => 'ta-admin', 'middleware' => ['role.admin']], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    /** ADMIN DASHBOARD */
    $router->get('', 'DashboardController@index')->name('admin_dashboard');

    $router->group(['prefix' => ''], function ($router) {

    });

    /** POST */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** POST VIEW */
        $router->group(['prefix' => 'posts'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'PostController@postListView')->name('admin_post_list_view');
            $router->get('/create-form', 'PostController@postFormView')->name('admin_post_new_view');
            $router->get('/update-form/{post_id}', 'PostController@postFormView')->name('admin_post_update_view');
        });
        /** POST ACTION */
        $router->group(['prefix' => 'post'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'PostController@postCreate')->name('admin_post_create');
            $router->post('/update', 'PostController@postUpdate')->name('admin_post_update');
            $router->post('/delete', 'PostController@postDelete')->name('admin_post_delete');
        });
    });

    /** ACCOUNT */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** ACCOUNT VIEW */
        $router->group(['prefix' => 'account'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'AccountController@accountListView')->name('admin_account_list_view');
            $router->get('/create-form', 'AccountController@accountFormView')->name('admin_account_new_view');
            $router->get('/update-form/{account_id}', 'AccountController@accountFormView')->name('admin_account_update_view');
        });

        /** ACCOUNT ACTION */
        $router->group(['prefix' => 'account'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/update_status', 'AccountController@updateStatus')->name('admin_account_status');
            $router->post('/create', 'AccountController@accountCreate')->name('admin_account_create');
            $router->post('/update', 'AccountController@accountUpdate')->name('admin_account_update');
            $router->post('/delete', 'AccountController@accountDelete')->name('admin_account_delete');
        });
    });

    /** CAMPAIGN */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** CAMPAIGN VIEW */
        $router->group(['prefix' => 'campaign'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'CampaignController@listCampaign')->name('admin_campaign_list_view');
            $router->get('/create-form', 'CampaignController@campaignFromView')->name('admin_campaign_new_view');
            $router->get('/update-form/{campaign_id}', 'CampaignController@campaignFromView')->name('admin_campaign_update_view');
        });

        /** CAMPAIGN ACTION */
        $router->group(['prefix' => 'campaign'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'CampaignController@campaignCreate')->name('admin_campaign_create');
            $router->post('/update', 'CampaignController@campaignUpdate')->name('admin_campaign_update');
            $router->post('/delete', 'CampaignController@campaignDelete')->name('admin_campaign_delete');
        });
    });

    /** COUPON */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** COUPON VIEW */
        $router->group(['prefix' => 'coupon'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'CouponController@listCoupon')->name('admin_coupon_list_view');
        });

        /** COUPON ACTION */
        $router->group(['prefix' => 'coupon'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'CouponController@couponCreate')->name('admin_coupon_create');
            $router->post('/delete', 'CouponController@couponDelete')->name('admin_coupon_delete');
        });
    });

    /** PAGE */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** PAGE VIEW */
        $router->group(['prefix' => 'pages'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'PageController@pageListView')->name('admin_page_list_view');
            $router->get('/create-form', 'PageController@pageFormView')->name('admin_page_new_view');
            $router->get('/update-form/{page_id}', 'PageController@pageFormView')->name('admin_page_update_view');
        });
        /** PAGE ACTION */
        $router->group(['prefix' => 'page'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'PageController@pageCreate')->name('admin_page_create');
            $router->post('/update', 'PageController@pageUpdate')->name('admin_page_update');
            $router->post('/delete', 'PageController@pageDelete')->name('admin_page_delete');
        });
    });

    /** CATEGORY */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** POST CATEGORY VIEW */
        $router->group(['prefix' => 'post-cat'], function ($router) {
            $router->get('/list', 'CategoryController@postCategoryListView')->name('admin_post_category_list_view');
            $router->get('/create-form', 'CategoryController@postCategoryFormView')->name('admin_post_category_new_view');
            $router->get('/update-form/{cat_id}', 'CategoryController@postCategoryFormView')->name('admin_post_category_update_view');
        });

        /** PRODUCT CATEGORY VIEW */
        $router->group(['prefix' => 'product-cat'], function ($router) {
            $router->get('/list', 'CategoryController@productCategoryListView')->name('admin_product_category_list_view');
            $router->get('/create-form', 'CategoryController@productCategoryFormView')->name('admin_product_category_new_view');
            $router->get('/update-form/{cat_id}', 'CategoryController@productCategoryFormView')->name('admin_product_category_update_view');
        });

        /** CATEGORY ACTION */
        $router->group(['prefix' => 'category'], function ($router) {
            $router->post('/create', 'CategoryController@categoryCreate')->name('admin_category_create');
            $router->post('/update', 'CategoryController@categoryUpdate')->name('admin_category_update');
            $router->post('/delete', 'CategoryController@categoryDelete')->name('admin_category_delete');
        });
    });

    /** ATTRIBUTE */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** PRODUCT ATTRIBUTE VIEW */
        $router->group(['prefix' => 'attributes'], function ($router) {
            $router->get('/list', 'AttributeController@attributeListView')->name('admin_attribute_list_view');
            $router->get('/create-form', 'AttributeController@attributeFormView')->name('admin_attribute_new_view');
            $router->get('/update-form/{attr_id}', 'AttributeController@attributeFormView')->name('admin_attribute_update_view');
        });

        /** ATTRIBUTE ACTION */
        $router->group(['prefix' => 'attribute'], function ($router) {
            $router->post('/create', 'AttributeController@attributeCreate')->name('admin_attribute_create');
            $router->post('/update', 'AttributeController@attributeUpdate')->name('admin_attribute_update');
            $router->post('/delete', 'AttributeController@attributeDelete')->name('admin_attribute_delete');
        });
    });

    /** MEDIA */
    $router->get('/media-library', 'DashboardController@media')->name('admin_media');

    /** PRODUCT */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** PRODUCT VIEW */
        $router->group(['prefix' => 'products'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'ProductController@productListView')->name('admin_product_list_view');
            $router->get('/create-form', 'ProductController@productFormView')->name('admin_product_new_view');
            $router->get('/update-form/{product_id}', 'ProductController@productFormView')->name('admin_product_update_view');
        });
        /** PRODUCT ACTION */
        $router->group(['prefix' => 'product'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'ProductController@productCreate')->name('admin_product_create');
            $router->post('/update', 'ProductController@productUpdate')->name('admin_product_update');
            $router->post('/delete', 'ProductController@productDelete')->name('admin_product_delete');
        });
    });

    /** REPOSITORY */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** REPOSITORY VIEW */
        $router->group(['prefix' => 'repositories'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'RepositoryController@repositoryListView')->name('admin_repository_list_view');
            $router->get('/update-form/{product_id}', 'RepositoryController@repositoryFormView')->name('admin_repository_update_view');
        });
        /** REPOSITORY ACTION */
        $router->group(['prefix' => 'repository'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/update', 'RepositoryController@repositoryUpdate')->name('admin_repository_update');
        });
    });

    /** ORDER */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** ORDER VIEW */
        $router->group(['prefix' => 'orders'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'OrderController@orderListView')->name('admin_order_list_view');
            $router->get('/create-form', 'OrderController@orderFormView')->name('admin_order_new_view');
            $router->get('/update-form/{post_id}', 'OrderController@orderFormView')->name('admin_order_update_view');
        });

        /** ORDER ACTION */
        $router->group(['prefix' => 'order'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'OrderController@orderCreate')->name('admin_order_create');
            $router->post('/update', 'OrderController@orderUpdate')->name('admin_order_update');
            $router->post('/delete', 'OrderController@orderDelete')->name('admin_order_delete');
        });

        /** REPORT VIEW */
        $router->group(['prefix' => 'report'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/status', 'ReportController@reportStatusView')->name('admin_order_report_status_view');
            $router->get('/product', 'ReportController@reportProductView')->name('admin_order_report_product_view');
            $router->get('/export-pdf', 'ReportController@ExportPDF')->name('admin_export_pdf');
        });
    });

    /** MENU */
    $router->group([], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        /** MENU VIEW */
        $router->group(['prefix' => '/ui/menu'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/', 'MenuController@menuListView')->name('admin_menu_list_view');
            $router->get('/{menu_id}/{menu_item_id?}', 'MenuController@menuFormView')->name('admin_menu_update_view')->where('menu_id', '[0-9]+')->where('menu_item_id', '[0-9]+');
        });

        /** MENU ACTION */
        $router->group(['prefix' => 'menu'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'MenuController@menuCreate')->name('admin_menu_create');
            $router->post('/update', 'MenuController@menuUpdate')->name('admin_menu_update');
            $router->post('/delete', 'MenuController@menuDelete')->name('admin_menu_delete');
        });

        /** MENU ITEM ACTION */
        $router->group(['prefix' => 'menu-item'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/create', 'MenuController@menuItemCreate')->name('admin_menu_item_create');
            $router->post('/update', 'MenuController@menuItemUpdate')->name('admin_menu_item_update');
            $router->post('/delete', 'MenuController@menuItemDelete')->name('admin_menu_item_delete');
        });
    });

    /** SETTING */
    $router->group(['prefix' => ''], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        $router->group(['prefix' => '/ui/widget'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/', 'SettingController@widgetView')->name('admin_setting_widget_view');
        });
        $router->get('/customizer', 'SettingController@customizerView')->name('admin_setting_customizer_view');
        $router->get('/ui/website-info', 'SettingController@websiteInfoView')->name('admin_setting_website_info_view');
        $router->get('/ui/homepage', 'SettingController@homePageView')->name('admin_setting_homepage_view');
        $router->get('/ui/partner-slider', 'SettingController@partnerSliderView')->name('admin_setting_partner_slider_view');

        /** SETTING ACTION */
        /** MENU ITEM ACTION */
        $router->group(['prefix' => 'setting'], function ($router) {
            $router->post('/save', 'SettingController@saveSetting')->name('admin_setting_save');
        });
    });

    /** USER */
    $router->group(['prefix' => ''], function ($router) {
        /** USER VIEW */
        $router->group(['prefix' => 'users'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->get('/list', 'UserController@userListView')->name('admin_user_list_view');
            $router->get('/create-form', 'UserController@userFormView')->name('admin_user_new_view');
            $router->get('/update-form/{user_id}', 'UserController@userFormView')->name('admin_user_update_view');
        });

        /** USER ACTION */
        $router->group(['prefix' => 'user'], function ($router) {
            /** @var \Illuminate\Support\Facades\Route $router */
            $router->post('/update_status', 'UserController@updateStatus')->name('admin_user_status');
            $router->post('/create', 'UserController@userCreate')->name('admin_user_create');
            $router->post('/update', 'UserController@userUpdate')->name('admin_user_update');
            $router->post('/delete', 'UserController@userDelete')->name('admin_user_delete');
        });
    });

    /** ADMIN API */
    //    $router->get('test1', ['scope' => ['user-create', 'user-list'], 'use' => 'TestController@abc']);
});
/** =============================================================== */

/** ========================== ADMIN API ========================== */
$router->group(['namespace' => 'Admin', 'prefix' => 'api', 'middleware' => ['role.admin']], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    /** ADMIN API */
    $router->post('/uploadImage', 'ApiController@uploadImage')->name('admin_api_image_upload');
    $router->post('/getSlug', 'ApiController@getSlug')->name('admin_api_get_slug');
});
/** =============================================================== */

$router::get('/test', 'TestController@index');
/** ========================== CLIENT VIEW ======================== */
/** @var \Illuminate\Support\Facades\Route $router */
$router::group(['namespace' => 'Client', 'prefix' => '/'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    /** PAGE */
    $router::get('', 'IndexController@index')->name('client_home');
    $router::get('/tim-kiem', 'IndexController@search')->name('client_search');
    $router::get('/404-not-found', 'IndexController@error')->name('client_404');
    $router::get('/instagram', 'InstagramController@index')->name('client_instagram_callback');

    $router::get('/tat-ca-san-pham', 'CategoryController@archiveProductView')->name('client_archive_product_view');
    $router::get('/san-pham-moi', 'CategoryController@newProductView')->name('client_new_product_view');
    $router::get('/san-pham-khuyen-mai', 'CategoryController@saleProductView')->name('client_sale_product_view');
    $router::get('/san-pham-ban-chay', 'CategoryController@bestSellerProductView')->name('client_best_seller_product_view');

    /** CART VIEW */
    $router->group(['prefix' => 'gio-hang'], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        $router::get('/', 'CartController@indexView')->name('client_cart_index_view');
        $router::get('/thanh-toan', 'CartController@checkoutView')->name('client_cart_checkout_view');
        $router::get('/thanh-cong/{identifier}', 'CartController@successView')->name('client_cart_success_view')->where('identifier', '([A-Za-z0-9\-\/\_]+)');
    });

    /** CART ACTION */
    $router->group(['prefix' => 'cart'], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        $router::post('/add-product', 'CartController@cartAddProduct')->name('client_cart_add_product');

        $router::post('/add-coupon-ajax', 'CartController@cartAddCouponAjax')->name('client_cart_add_coupon_ajax');
        $router::post('/quickview-product-ajax', 'CartController@cartQuickviewProductAjax')->name('client_cart_quickview_product_ajax');
        $router::post('/add-product-ajax', 'CartController@cartAddProductAjax')->name('client_cart_add_product_ajax');
        $router::post('/delete-product-ajax', 'CartController@cartDeleteProductAjax')->name('client_cart_delete_product_ajax');
        $router::post('/update-product-ajax', 'CartController@cartUpdateProductAjax')->name('client_cart_update_product_ajax');
        $router::post('/checkout', 'CartController@cartCheckout')->name('client_cart_checkout');
    });

    /** ACCOUNT VIEW */
    $router::post('/forget_password', 'AccController@forgotPassword')->name('client_forget_password_account');
    $router->group(['prefix' => 'account', 'middleware' => ['auth:account']], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        $router::get('/', 'AccController@accountView')->name('client_account_view');
        $router::get('/update-password', 'AccController@updatePasswordView')->name('client_update_password_view');
        $router::get('/orders', 'AccController@orderView')->name('client_account_order_view');
        $router::get('/order-detail/{id}', 'AccController@orderDetail')->name('client_account_order_detail_view');
        $router::get('/coupons', 'AccController@couponView')->name('client_account_coupon_view');
        /**
         * Account action
         */
        $router::post('/update_info', 'AccController@updateInfo')->name('client_update_info_account');
        $router::post('/update_password', 'AccController@updatePassword')->name('client_update_password_account');
        $router::get('/logout', 'AccController@logout')->name('client_logout_account');
    });

    /** PERMALINK */
    $router::get('/danh-muc/{slug}', 'CategoryController@productCatView')->name('client_product_cat_view')->where('slug', '([A-Za-z0-9\-\/]+)')/*->where('id', '[0-9]+')*/
    ;
    $router::get('/chuyen-muc/{slug}', 'CategoryController@postCatView')->name('client_post_cat_view')->where('slug', '([A-Za-z0-9\-\/]+)')/*->where('id', '[0-9]+')*/
    ;
    $router::get('/san-pham/{slug}', 'ProductController@productDetailView')->name('client_product_detail_view')->where('slug', '([A-Za-z0-9\-\/]+)')/*->where('id', '[0-9]+')*/
    ;
    $router::get('/bai-viet/{slug}', 'PostController@postDetailView')->name('client_post_detail_view')->where('slug', '([A-Za-z0-9\-\/]+)')/*->where('id', '[0-9]+')*/
    ;
    $router::get('/{slug}', 'PageController@pageDetailView')->name('client_page_detail_view')->where('slug', '([A-Za-z0-9\-\/]+)')/*->where('id', '[0-9]+')*/
    ;
});
/** =============================================================== */
