<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('description', 500)->nullable($value = true);
            $table->longText('content')->nullable($value = true);
            $table->string('type', 20);
            $table->unsignedInteger('parent_id');
            $table->unsignedInteger('author_id');
            $table->longText('image')->nullable($value = true);
            $table->tinyInteger('status');
            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });

        Schema::create('product_attr', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('parent_attr_id');
            $table->unsignedInteger('child_attr_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute');
        Schema::dropIfExists('product_attr');
    }
}
