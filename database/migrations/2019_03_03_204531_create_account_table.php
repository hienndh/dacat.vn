<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('status');
            $table->string('full_name');
            $table->string('phone');
            $table->string('address');
            $table->string('image')->nullable($value = true);
            $table->rememberToken();
            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account');
    }
}
