<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->longText('description')->nullable($value = true);
            $table->longText('content')->nullable($value = true);

            $table->unsignedInteger('author_id');
            $table->longText('image')->nullable($value = true);
            $table->tinyInteger('status');

            $table->string('seo_title')->nullable($value = true);
            $table->string('seo_description', 500)->nullable($value = true);
            $table->longText('seo_keyword')->nullable($value = true);

            $table->unsignedInteger('views')->nullable($value = true);

            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });

        Schema::create('post_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('post_id');
            $table->string('meta_key');
            $table->longText('meta_value');
        });

        Schema::create('post_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('post_id');
            $table->unsignedInteger('cat_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
        Schema::dropIfExists('post_meta');
        Schema::dropIfExists('post_cat');
    }
}
