<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            //Cart
            $table->unsignedInteger('quantity');
            $table->double('total', 15, 2);
            $table->double('total_real', 15, 2);
            $table->string('note')->nullable($value = true);
            $table->string('coupon_code')->nullable($value = true);
            $table->unsignedInteger('status');
            //Customer Info
            $table->unsignedInteger('account_id')->nullable($value = true);
            $table->string('full_name');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->unsignedInteger('province_id')->nullable($value = true);
            $table->unsignedInteger('district_id')->nullable($value = true);
            $table->unsignedInteger('ward_id')->nullable($value = true);
            $table->string('post_code')->nullable($value = true);;

            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });

        Schema::create('order_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('product_id');
            $table->float('price');
            $table->unsignedInteger('quantity');
        });

        Schema::create('order_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('status');
            $table->string('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_product');
        Schema::dropIfExists('order_history');
    }
}
