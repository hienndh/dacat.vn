<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->longText('description')->nullable($value = true);
            $table->longText('content')->nullable($value = true);

            $table->string('sku')->nullable($value = true);
            $table->unsignedBigInteger('regular_price');
            $table->unsignedBigInteger('sale_price');
            $table->unsignedBigInteger('sale_number')->nullable($value = true);
            $table->string('sale_type')->nullable($value = true);
            $table->unsignedInteger('quantity');

            $table->unsignedInteger('author_id');
            $table->longText('image')->nullable($value = true);
            $table->longText('album')->nullable($value = true);
            $table->tinyInteger('status');

            $table->string('seo_title')->nullable($value = true);
            $table->string('seo_description', 500)->nullable($value = true);
            $table->longText('seo_keyword')->nullable($value = true);

            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });

        Schema::create('product_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->string('meta_key');
            $table->longText('meta_value');
        });

        Schema::create('product_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('cat_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_meta');
        Schema::dropIfExists('product_cat');
    }
}
