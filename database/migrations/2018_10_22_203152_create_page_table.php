<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->longText('description')->nullable($value = true);
            $table->longText('content')->nullable($value = true);
            $table->longText('content_html')->nullable($value = true);
            $table->string('type');

            $table->unsignedInteger('author_id');
            $table->longText('image')->nullable($value = true);
            $table->tinyInteger('status');

            $table->string('seo_title')->nullable($value = true);
            $table->string('seo_description', 500)->nullable($value = true);
            $table->longText('seo_keyword')->nullable($value = true);

            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });

        Schema::create('page_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('page_id');
            $table->string('meta_key');
            $table->longText('meta_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page');
        Schema::dropIfExists('page_meta');
    }
}
