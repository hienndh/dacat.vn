<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('image');
            $table->integer('value');
            $table->tinyInteger('value_type');
            $table->tinyInteger('status');
            $table->integer('startDate');
            $table->integer('endDate');
            $table->integer('total');
            $table->integer('total_used');
            $table->integer('user_created');
            $table->tinyInteger('type');
            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}
