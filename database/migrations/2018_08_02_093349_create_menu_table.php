<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location', 50)->nullable($value = true);
            $table->tinyInteger('status');
        });

        Schema::create('menu_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('menu_id');
            $table->string('title');
            $table->string('link')->nullable($value = true);
            $table->string('type', 20);
            $table->unsignedInteger('object_id')->nullable($value = true);
            $table->unsignedInteger('parent_id');
            $table->unsignedInteger('sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
        Schema::dropIfExists('menu_item');
    }
}
