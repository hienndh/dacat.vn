<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('description', 500)->nullable($value = true);
            $table->longText('content')->nullable($value = true);
            $table->string('type', 20);
            $table->unsignedInteger('parent_id');
            $table->unsignedInteger('author_id');
            $table->longText('image')->nullable($value = true);
            $table->longText('banner')->nullable($value = true);
            $table->longText('banner_link')->nullable($value = true);
            $table->tinyInteger('status');
            $table->string('seo_title')->nullable($value = true);
            $table->string('seo_description', 500)->nullable($value = true);
            $table->longText('seo_keyword')->nullable($value = true);
            $table->unsignedInteger('created_at')->nullable($value = true);
            $table->unsignedInteger('updated_at')->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
