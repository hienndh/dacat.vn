<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            //Role
            [
                'name' => 'role-create',
                'display_name' => 'Create Role',
                'description' => 'Create New Role'
            ],
            [
                'name' => 'role-list',
                'display_name' => 'Display Role Listing',
                'description' => 'List All Roles'
            ],
            [
                'name' => 'role-update',
                'display_name' => 'Update Role',
                'description' => 'Update Role Information'
            ],
            [
                'name' => 'role-delete',
                'display_name' => 'Delete Role',
                'description' => 'Delete Role'
            ],
            //User
            [
                'name' => 'user-create',
                'display_name' => 'Create User',
                'description' => 'Create New User'
            ],
            [
                'name' => 'user-list',
                'display_name' => 'Display User Listing',
                'description' => 'List All Users'
            ],
            [
                'name' => 'user-update',
                'display_name' => 'Update User',
                'description' => 'Update User Information'
            ],
            [
                'name' => 'user-delete',
                'display_name' => 'Delete User',
                'description' => 'Delete User'
            ],
            //BLog
            [
                'name' => 'blog-create',
                'display_name' => 'Create Blog',
                'description' => 'Create New Blog'
            ],
            [
                'name' => 'blog-list',
                'display_name' => 'Display Blog Listing',
                'description' => 'List All Blogs'
            ],
            [
                'name' => 'blog-update',
                'display_name' => 'Update Blog',
                'description' => 'Update Blog Information'
            ],
            [
                'name' => 'blog-delete',
                'display_name' => 'Delete Blog',
                'description' => 'Delete Blog'
            ],
            //Category
            [
                'name' => 'category-create',
                'display_name' => 'Create Category',
                'description' => 'Create New Category'
            ],
            [
                'name' => 'category-list',
                'display_name' => 'Display Category Listing',
                'description' => 'List All Category'
            ],
            [
                'name' => 'category-update',
                'display_name' => 'Update Category',
                'description' => 'Update Category Information'
            ],
            [
                'name' => 'category-delete',
                'display_name' => 'Delete Category',
                'description' => 'Delete Category'
            ],
            //Product
            [
                'name' => 'product-create',
                'display_name' => 'Create Product',
                'description' => 'Create New Product'
            ],
            [
                'name' => 'product-list',
                'display_name' => 'Display Product Listing',
                'description' => 'List All Products'
            ],
            [
                'name' => 'product-update',
                'display_name' => 'Update Product',
                'description' => 'Update Product Information'
            ],
            [
                'name' => 'product-delete',
                'display_name' => 'Delete Product',
                'description' => 'Delete Product'
            ],
            //Order
            [
                'name' => 'order-create',
                'display_name' => 'Create Order',
                'description' => 'Create New Order'
            ],
            [
                'name' => 'order-list',
                'display_name' => 'Display Order Listing',
                'description' => 'List All Orders'
            ],
            [
                'name' => 'order-update',
                'display_name' => 'Update Order',
                'description' => 'Update Order Information'
            ],
            [
                'name' => 'order-delete',
                'display_name' => 'Delete Order',
                'description' => 'Delete Order'
            ],
            //Menu
            [
                'name' => 'menu-create',
                'display_name' => 'Create Menu',
                'description' => 'Create New Menu'
            ],
            [
                'name' => 'menu-list',
                'display_name' => 'Display Menu Listing',
                'description' => 'List All Menus'
            ],
            [
                'name' => 'menu-update',
                'display_name' => 'Update Menu',
                'description' => 'Update Menu Information'
            ],
            [
                'name' => 'menu-delete',
                'display_name' => 'Delete Menu',
                'description' => 'Delete Menu'
            ],
            //Comment
            [
                'name' => 'comment-create',
                'display_name' => 'Create Comment',
                'description' => 'Create New Comment'
            ],
            [
                'name' => 'comment-list',
                'display_name' => 'Display Comment Listing',
                'description' => 'List All Comments'
            ],
            [
                'name' => 'comment-update',
                'display_name' => 'Update Comment',
                'description' => 'Update Comment Information'
            ],
            [
                'name' => 'comment-delete',
                'display_name' => 'Delete Comment',
                'description' => 'Delete Comment'
            ],
        ];

        foreach ($permission as $key => $value) {
            Permission::create($value);
        }
    }
}
