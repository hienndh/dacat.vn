<?php

use App\Models\Menu;
use App\Models\PermissionRole;
use App\Models\RoleUser;
use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;
use App\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        //Entrust
        DB::table('roles')->delete();
        DB::table('role_user')->delete();
        DB::table('permissions')->delete();
        DB::table('permission_role')->delete();
        //Menu
        DB::table('menu')->delete();
        //Address
        DB::table('locations')->delete();

        //Model::unguard();
        // Register the user seeder
        // $this->call(UsersTableSeeder::class);

        // Register the permission seeder
        $this->call(PermissionSeeder::class);

        // Register the Setting seeder
        $this->call(SettingSeeder::class);

        // Register the Locations seeder
        $this->call(LocationsSeeder::class);

        //1) Create Admin Role
        $role = ['name' => 'admin', 'display_name' => 'Admin', 'description' => 'Full Permission'];
        $role = Role::create($role);

        $role_guest = ['name' => 'guest', 'display_name' => 'Guest', 'description' => 'Just Read'];
        $role_guest = Role::create($role_guest);
        //2) Set Role Permissions
        // Get all permission, swift through and attach them to the role
        $permission = Permission::get();
        foreach ($permission as $key => $value) {
            PermissionRole::create(['permission_id' => $value->id, 'role_id' => $role->id]);
        }
        //3) Create Admin User
        $user = [
            'user_name'  => 'admin',
            'email'      => 'cuongnv.developer@gmail.com',
            'password'   => app('hash')->make('123456'),
            'status'     => 1,
            'first_name' => 'HH',
            'last_name'  => 'Nguyễn',
            'phone'      => '0353437303',
            'address'    => 'Hà Nội',
            'created_at' => time(),
            'updated_at' => time(),
        ];
        $user = User::create($user);

        //4) Set User Role
        RoleUser::create(['user_id' => $user->id, 'role_id' => $role->id]);
        //Model::reguard();

//        5) Create menu
        $menu_list = [
            [
                'name'     => 'Top Bar Menu',
                'location' => 'top_bar_menu',
                'status'   => 1
            ],
            [
                'name'     => 'Main Menu',
                'location' => 'main_menu',
                'status'   => 1
            ],
            [
                'name'     => 'Mobile Menu',
                'location' => 'mobile_menu',
                'status'   => 1
            ],
            [
                'name'     => 'Footer Menu',
                'location' => 'footer_menu',
                'status'   => 1
            ],
        ];
        foreach ($menu_list as $key => $value) {
            Menu::create($value);
        }
    }
}
