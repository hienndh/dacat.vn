<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting_arr = [
            //General
            'ta_logo'               => '/img/cuongdev-logo.png',
            'ta_logo_dark'          => '',
            'ta_favicon'            => '',
            'ta_seo_title'          => 'DỊCH VỤ THIẾT KẾ WEBSITE CUONGDEV',
            'ta_seo_description'    => 'Thiết kế website chuyên nghiệp, uy tín dành cho doanh nghiệp và bán hàng. Thiết kế website chuẩn SEO, tương thích với các thiết bị di dộng.',
            'ta_seo_keyword'        => '',
            'ta_seo_image'          => '',
            //Contact
            'ta_company_name'       => 'CuongDev',
            'ta_company_address'    => 'cuongnv.developer@gmail.com',
            'ta_company_phone'      => '0353437303',
            'ta_company_email'      => 'Hà Nội',
            //Custom code
            'ta_custom_script'      => '',
            'ta_custom_css'         => '',
            //Social
            'ta_social_email'       => '#',
            'ta_social_facebook'    => '#',
            'ta_social_zalo'        => '#',
            'ta_social_google_plus' => '#',
            'ta_social_instagram'   => '#',
            'ta_social_linkedin'    => '#',
            'ta_social_pinterest'   => '#',
            'ta_social_twitter'     => '#',
            'ta_social_youtube'     => '#',
        ];

        foreach ($setting_arr as $key => $value) {
            $setting_data = [
                'meta_key'   => $key,
                'meta_value' => $value,
            ];
            Setting::create($setting_data);
        }
    }
}
