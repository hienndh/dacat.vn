<?php

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vietnam_locations     = file_get_contents(__DIR__ . "/../../resources/json/vietnam_locations.json");
        $vietnam_locations_arr = json_decode($vietnam_locations, true);

        foreach ($vietnam_locations_arr as $key => $location) {
            $location_data = [
                'name'      => $location['name'],
                'type'      => $location['type'],
                'parent_id' => $location['parent_id'],
                'status'    => intval($location['status'])
            ];
            Location::create($location_data);
        }
    }
}
