/**

 This fiddle is using the latest version
 of Slick (from master) and jQuery.

 If your issue occurs in older version or a
 specific tag; please use the "External Resources"
 section in the sidebar to correct it.

 **/

function createSlick() {
    $('.slider.banner-slider').not('.slick-initialized').slick({
        autoplay: true,
        arrows: false,
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
    });

    $('.slider.partner-slider').not('.slick-initialized').slick({
        autoplay: true,
        arrows: true,
        dots: false,
        infinite: true,
        // centerMode: true,
        variableWidth: true,
        adaptiveHeight: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: 'unslick'
            // instead of a settings object
        ]
    });

    $('.slider.post-slider').not('.slick-initialized').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: 'unslick'
            // instead of a settings object
        ]
    });

    $('.product-images').not('.slick-initialized').slick({
        dots: false,
        arrows: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    vertical: true,
                    verticalSwiping: true,
                    arrows: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    vertical: true,
                    verticalSwiping: true,
                    arrows: false,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: 'unslick'
            // instead of a settings object
        ]
    });

    if ($(window).width() < 992) {
        $('.slider.product-slider').not('.slick-initialized').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: true,
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 1920,
                    settings: 'unslick'
                },
                {
                    breakpoint: 850,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: 'unslick'
                // instead of a settings object
            ]
        });
    }
}

createSlick();

//Now it will not throw error, even if called multiple times.
// $(window).on('resize', createSlick);

/*=================== COOKIE FUNCTION ========================*/
function setCookie(cname, cvalue, exhours) {
    var d = new Date();
    d.setTime(d.getTime() + (exhours * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(popup_id) {
    let popup = getCookie(popup_id);
    if (popup === "1") {
        $("#" + popup_id).modal('hide');
    } else {
        $("#" + popup_id).modal('show');
    }
}
/*============================================================*/

$(document).ready(function () {
    function hide_shop_menu() {
        $('.desktopnav-shop').removeClass('active');
        $('.desktopnav-shop-menu').removeClass('visible');
    }

    function hide_account_login() {
        $('.btn-account-login').removeClass('active');
        $('.nav-account').removeClass('visible');
    }

    if ($("img.lazyload").length > 0) {
        $("img.lazyload").lazyload();
    }

    // SHOP MENU
    $('.desktopnav-shop').on('click', function () {
        $(this).toggleClass('active');
        $('.desktopnav-shop-menu').toggleClass('visible');
        if (!$('.nav-account').hasClass('visible')) {
            $('.site-desktopnav-overlay').toggleClass('visible');
        }
        hide_account_login();
    });

    //ACCOUNT LOGIN
    $('.btn-account-login').on('click', function () {
        $(this).toggleClass('active');
        $('.nav-account').toggleClass('visible');
        if (!$('.desktopnav-shop-menu').hasClass('visible')) {
            $('.site-desktopnav-overlay').toggleClass('visible');
        }
        hide_shop_menu();
    });

    $('.site-desktopnav-overlay').on('click', function () {
        hide_shop_menu();
        hide_account_login();
        $(this).removeClass('visible');
    });

    $('.mobilenav-shop-open').on('click', function () {
        $(this).toggleClass('hide');
        $('.mobilenav-shop-close').toggleClass('active');

        $('.mobilenav-panel').toggleClass('visible');
        $('.mobilenav-overlay').toggleClass('visible');
    });

    $('.mobilenav-shop-close').on('click', function () {
        $(this).toggleClass('active');
        $('.mobilenav-shop-open').toggleClass('hide');

        $('.mobilenav-panel').toggleClass('visible');
        $('.mobilenav-overlay').toggleClass('visible');
    });

    // MAIN MENU
    $('.submenu-toggle-link').on('click', function () {
        $('.submenu-toggle-link').toggleClass('active');
        $('.submenu').toggleClass('visible');
    });

    //CART
    $('.site-actions-cart').on('click', function () {
        $('.mini-cart-overlay').toggleClass('visible');
        $('.mini-cart').toggleClass('visible');
    });

    $('.mini-cart-overlay').on('click', function () {
        $(this).toggleClass('visible');
        $('.mini-cart').toggleClass('visible');
    });

    $('.cart-close').on('click', function () {
        $('.mini-cart-overlay').toggleClass('visible');
        $('.mini-cart').toggleClass('visible');
    });

    $('.quantity-decrement').on('click', function () {
        let input__product_quantity = $(this).closest('.quantity').find('.input__product_quantity');
        let old_quantity = input__product_quantity.val();
        if (parseInt(old_quantity) === 1) {
            return toastError("Số lượng sản phẩm ít nhất là 1");
        }
        input__product_quantity.val(parseInt(old_quantity) - 1);

        let product_id = input__product_quantity.data('product-id');
        let product_row_id = input__product_quantity.data('product-row-id');
        let product_quantity = parseInt(input__product_quantity.val());

        updateProductQuantity(product_id, product_row_id, product_quantity);
    });

    $('.quantity-increment').on('click', function () {
        let input__product_quantity = $(this).closest('.quantity').find('.input__product_quantity');
        let old_quantity = input__product_quantity.val();
        input__product_quantity.val(parseInt(old_quantity) + 1);

        let product_id = input__product_quantity.data('product-id');
        let product_row_id = input__product_quantity.data('product-row-id');
        let product_quantity = parseInt(input__product_quantity.val());

        updateProductQuantity(product_id, product_row_id, product_quantity);
    });

    // SLIDER
    $('.slideshow').flickity({
        // options
        cellAlign: 'left',
        freeScroll: true,
        contain: true,
        pageDots: false
    });

    $('.product-highlights-normal-wrapper').flickity({
        // options
        cellAlign: 'left',
        freeScroll: true,
        contain: true,
        pageDots: false
    });

    // PRODUCT ADD TO CART
    $('form#client_cart_add_product_form').on('submit', function (e) {
        let empty_size = false;
        let empty_color = false;
        if ($('input[name="product_size"]').length > 0 && $('input[name="product_size"]:checked').length === 0) {
            toastWarning('Hãy chọn size trước khi thêm vào giỏ hàng!');
            empty_size = true;
        }

        if ($('input[name="product_color"]').length > 0 && $('input[name="product_color"]:checked').length === 0) {
            toastWarning('Hãy chọn màu trước khi thêm vào giỏ hàng!');
            empty_color = true;
        }
        if (empty_size || empty_color) {
            e.preventDefault();
            return false;
        }
    });
    //RESET FORM INPUT
    $('.product-options .option-value .option-value-input').prop('checked', false);

    $('.product-group-thumb').on('click', function () {
        $('.product-group-thumb').removeClass('is-current');
        $('.product-group-thumb').removeClass('is-selected');

        $(this).addClass('is-current');
        $(this).addClass('is-selected');

        let product_color_id = $(this).data('product_color_id');
        $('input[name="product_color"]').prop('checked', false);
        $('#product_color_' + product_color_id).prop('checked', true);
    });

    $('.product-options .option-value:not(.option-soldout)').on('click', function () {
        $(this).closest('.option-values').find('.option-value').removeClass('option-selected');
        $(this).closest('.option-values').find('.option-value .option-value-input').prop('checked', false);

        $(this).addClass('option-selected');
        $(this).find('.option-value-input').prop('checked', true);
    });

    // PRODUCT DETAIL - PRODUCT COLOR
    $('.product-group-thumbs').flickity({
        // options
        cellAlign: 'left',
        freeScroll: true,
        contain: true,
        pageDots: false
    });

    // PRODUCT FILTER
    $('.btn__filter_form').on('click', function () {
        $('form#Product_Filter_Form').toggleClass('active');
    });

    $('.collection-filters-title').on('click', function () {
        let data_trigger = $(this).data('collection-filters-item-trigger');
        let check_current = $('ul.collection-filters-item[data-collection-filters-item="' + data_trigger + '"]').hasClass('active');
        // Remove active
        $('ul.collection-filters-item').removeClass('active');
        $('.collection-filters-title-text').removeClass('active');
        $('.collection-filters-item-arrow').removeClass('active');

        if (!check_current) {
            $(this).find('.collection-filters-title-text').toggleClass('active');
            $(this).find('.collection-filters-item-arrow').toggleClass('active');
            $('ul.collection-filters-item[data-collection-filters-item="' + data_trigger + '"]').toggleClass('active');
        }
    });

    // =============================================================================
    // =============================================================================
    // =============================================================================
    // =================== CODE CŨ
    $("#modalCampaign .hidden-modal").on('click', function () {
        setCookie("modalCampaign", "1", 3);
    });
    /* ===================================== */
    function processCartView(res) {
        console.log("ress", res);
        $('.cart-subtotal').html(res.data.cart_subtotal + 'VND');
        // $('.cart-tax').html(res.data.cart_tax + 'VND');
        $('.cart-total').html(res.data.cart_total + 'VND');
        $('.cart-count').html(res.data.cart_count);
        res.data.cart_count === 0 ? $('.btn__checkout').attr('disabled', true) : $('.btn__checkout').attr('disabled', false);
    }

    function addToCart(product_id, product_quantity = 1) {
        if (!product_id) {
            return toastError("Mã sản phẩm đang để trống");
        }

        $.ajax({
            method: "POST",
            url: "/cart/add-product-ajax",
            data: {
                product_id: product_id,
                product_quantity: product_quantity,
                "_token": $("input[name='_token']").val(),
            }
        }).done(function (res) {
            if (res.code === 1) {
                let cart_item = res.data.cart_item;
                processCartView(res);
                if ($('.cart-item-' + cart_item.rowId).length > 0) {
                    $('.cart-item-' + cart_item.rowId).find('input.input__product_quantity').val(cart_item.qty);
                } else {
                    $('.sidebar-cart-list').append(res.data.cart_item_html);

                    $('.cart-item-' + cart_item.rowId + ' .btn__add_to_cart').on('click', function () {
                        let product_id = $(this).data('product-id');
                        let product_quantity = $(this).closest('.pi-action').find('input.input__quantity').length > 0 ? $(this).closest('.pi-action').find('input.input__quantity').val() : 1;
                        addToCart(product_id, product_quantity);
                    });

                    $('.cart-item-' + cart_item.rowId + ' .btn__remove_cart_item').on('click', function () {
                        let product_row_id = $(this).data('product-row-id');
                        removeCartItem(product_row_id);
                    });

                    $('.cart-item-' + cart_item.rowId + ' .input__product_quantity').on('change', function () {
                        let product_id = $(this).data('product-id');
                        let product_row_id = $(this).data('product-row-id');
                        let product_quantity = $(this).val();

                        updateProductQuantity(product_id, product_row_id, product_quantity);
                    });
                }

                toastSuccess(res.msg);
            } else {
                toastError(res.msg);
            }
        });
    }

    function removeCartItem(product_row_id) {
        if (!product_row_id) {
            return toastError("Mã sản phẩm đang để trống");
        }

        alertWarning(function () {
            $.ajax({
                method: "POST",
                url: "/cart/delete-product-ajax",
                data: {
                    product_row_id: product_row_id,
                    "_token": $("input[name='_token']").val(),
                }
            }).done(function (res) {
                if (res.code === 1) {
                    $('.cart-item-' + product_row_id).remove();
                    processCartView(res);
                    toastSuccess(res.msg);
                } else {
                    toastError(res.msg);
                }
            });
        }, "Bạn muốn xoá sản phẩm này khỏi giỏ hàng?");
    }

    function updateProductQuantity(product_id, product_row_id, product_quantity) {
        if (!product_id || !product_row_id) {
            return toastError("Mã sản phẩm đang để trống");
        }

        if (!parseInt(product_quantity)) {
            return toastError("Số lượng sản phẩm ít nhất là 1");
        }

        $.ajax({
            method: "POST",
            url: "/cart/update-product-ajax",
            data: {
                product_id: product_id,
                product_row_id: product_row_id,
                product_quantity: product_quantity,
                "_token": $("input[name='_token']").val(),
            }
        }).done(function (res) {
            if (res.code === 1) {
                processCartView(res);
                toastSuccess(res.msg);
            } else {
                toastError(res.msg);
            }
        });
    }

    $('.btn__quick_view').on('click', function () {
        let product_id = $(this).data('product-id');

        if (!product_id) {
            return toastError("Mã sản phẩm đang để trống");
        }

        $.ajax({
            method: "POST",
            url: "/cart/quickview-product-ajax",
            data: {
                product_id: product_id,
                "_token": $("input[name='_token']").val(),
            }
        }).done(function (res) {
            if (res.code === 1) {
                $('#popup_quickview #popup_quickview_html').html(res.data.quickview_product_html);
                $('#popup_quickview').addClass('active');

                $('#popup_quickview .btn__add_to_cart').on('click', function () {
                    let product_id = $(this).data('product-id');
                    let product_quantity = $(this).closest('.pi-action').find('input.input__quantity').length > 0 ? $(this).closest('.pi-action').find('input.input__quantity').val() : 1;
                    addToCart(product_id, product_quantity);
                });
            } else {
                toastError(res.msg);
            }
        });
    });

    $("#popup_quickview .popup_close").on('click', function () {
        $("#popup_quickview").removeClass('active');
        $('#popup_quickview #popup_quickview_html').html('');
    });

    /* CART */
    $('.btn__add_to_cart').on('click', function () {
        let product_id = $(this).data('product-id');
        addToCart(product_id);
    });

    $('.btn__remove_cart_item').on('click', function () {
        let product_row_id = $(this).data('product-row-id');
        removeCartItem(product_row_id);
    });

    $('.input__product_quantity').on('change', function () {
        let product_id = $(this).data('product-id');
        let product_row_id = $(this).data('product-row-id');
        let product_quantity = $(this).val();

        updateProductQuantity(product_id, product_row_id, product_quantity);
    });
    /* ===================================== */

    /* ===================================== */
});

$('.product-images .product-image').on('click', function () {
    var key = $(this).find('img').attr('data-product-image-index');
    var parent = $(this).closest('.product-images');
    var aryImg = JSON.parse(parent.attr('data-images'));
    $('.product-selected-image').find('img').attr('src', aryImg[key]);
    parent.find('.product-image').each(function (e) {
        if ($(this).hasClass('product-image-selected')) $(this).removeClass('product-image-selected');
    });
    $(this).addClass('product-image-selected');
});
