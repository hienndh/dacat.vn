<?php

return [
    'offline'        => env('ASSETS_OFFLINE', false),
    'enable_version' => env('ASSETS_ENABLE_VERSION', true),
    'version'        => env('ASSETS_VERSION', time()),
    'scripts'        => [
        'modernizr',
        'jquery-331',
        'bootstrap-js',
        'bootstrap-bundle-js',
        'slick-js',
        'lazyload',
        'jquery-toast',
        'custom-toastr-js',
        'sweetalert-js',
        'flickity-js',
        'cuongdev-js',
    ],
    'styles'         => [
        'bootstrap',
        'bootstrap-grid',
        'bootstrap-reboot',
        'cuongdev-style',
        'slick-css',
        'slick-theme-css',
        'toast-css',
        'sweetalert-css',
        'mnml-style',
        'trinh-style',
        'custom-style'
    ],
    'resources'      => [
        'scripts' => [
            'app'                 => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/js/app.js',
                ],
            ],
            'modernizr'           => [
                'use_cdn'  => true,
                'location' => 'header',
                'src'      => [
                    'local' => '/vendor/core/packages/modernizr/modernizr.min.js',
                    'cdn'   => '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js',
                ],
            ],
            'jquery-331'          => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/js/jquery-3.3.1.min.js',
                ],
            ],
            'bootstrap-js'        => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/js/bootstrap.min.js',
                ],
            ],
            'bootstrap-bundle-js' => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/js/bootstrap.bundle.min.js',
                ],
            ],
            'elevateZoom'         => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/js/jquery.elevateZoom-3.0.8.min.js',
                ],
            ],
            'slick-js'            => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/slick/slick.min.js',
                ],
            ],
            'lazyload'            => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/js/lazyload.min.js',
                ],
            ],
            'jquery-toast'        => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/admin/assets/plugins/toast-master/js/jquery.toast.js',
                ],
            ],
            'custom-toastr-js'    => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/admin/main/js/toastr.js',
                ],
            ],
            'sweetalert-js'       => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/admin/assets/plugins/sweetalert/sweetalert.min.js',
                ],
            ],
            'cuongdev-js'         => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/js/cuongdev-js.js',
                ],
            ],
            'flickity-js'         => [
                'use_cdn'  => false,
                'location' => 'footer',
                'src'      => [
                    'local' => '/client/js/flickity.pkgd.min.js',
                ],
            ]
        ],
        'styles'  => [
            'bootstrap'        => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/css/bootstrap.min.css',
                ]
            ],
            'bootstrap-grid'   => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/css/bootstrap-grid.min.css',
                ]
            ],
            'bootstrap-reboot' => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/css/bootstrap-reboot.min.css',
                ]
            ],
            'cuongdev-style'   => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/css/cuongdev-style.css',
                ]
            ],
            'slick-css'        => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/slick/slick.css',
                ]
            ],
            'slick-theme-css'  => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/slick/slick-theme.css',
                ]
            ],
            'toast-css'        => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/admin/assets/plugins/toast-master/css/jquery.toast.css',
                ]
            ],
            'sweetalert-css'   => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/admin/assets/plugins/sweetalert/sweetalert.css',
                ]
            ],

            'mnml-style' => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/assets/theme.scss8097.css',
                ]
            ],
            'custom-style' => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/css/custom.css',
                ]
            ],
            'trinh-style' => [
                'use_cdn'  => false,
                'location' => 'header',
                'src'      => [
                    'local' => '/client/assets/trinh-dev-css-2.css',
                ]
            ],
        ],
    ],
];
