<?php
/**
 * Created by PhpStorm.
 * User: zkenc
 * Date: 20/11/2018
 * Time: 4:13 CH
 */

use App\Models\BaseModel;
use App\Models\MenuItem;

class Helper
{
    /**
     * @param MenuItem $menu_item
     * @return string
     */
    static function getMenuLink($menu_item)
    {
        $type = $menu_item['type'];

        $link = '';
        if ($type == 'custom') {
            $link = $menu_item['link'];
        }elseif ($type == 'all_product'){
            $link = "/tat-ca-san-pham";
        }
        else {
            $item_object = $menu_item->getObject($type);
            if(!$item_object) return $link;
            $link        = $item_object->get_link($type, $item_object['id'], $item_object['slug']);
        }

        return $link;
    }

    static function getFormattedNumber($number = '', $decimal = 0, $dec_point = ',', $thousands_sep = '.')
    {
        if ($number == '') {
            return 0;
        }
        if (is_numeric($number)) {
            return $number = number_format($number, $decimal, $dec_point, $thousands_sep);
        }
        //Remove anything that isn't a number or decimal point
        $number = trim(preg_replace('/([^0-9\.])/i', '', $number));
        return number_format($number, $decimal, $dec_point, $thousands_sep);
    }

    static function getFormattedPrice($number = '', $decimal = 0, $dec_point = ',', $thousands_sep = '.')
    {
        if (!$number) {
            return 0 . '' . BaseModel::PROUCT_CURRENCY;
        }
        switch (BaseModel::PROUCT_CURRENCY) {
            case '$':
                $formatted_price = BaseModel::PROUCT_CURRENCY . '' . number_format($number, $decimal, $dec_point, $thousands_sep);
                break;
            case 'VND':
                $formatted_price = number_format($number, $decimal, $dec_point, $thousands_sep) . '' . BaseModel::PROUCT_CURRENCY;
                break;
            default:
                $formatted_price = number_format($number, $decimal, $dec_point, $thousands_sep) . ' ' . BaseModel::PROUCT_CURRENCY;
        }
        return $formatted_price;
    }

    static function getWidgetKey($widget_key)
    {
        return explode('___', $widget_key, 2)[0];
    }

    static function checkImage($image_path, $default_img = '/img/placeholder.png') {
        return asset($image_path);
//        return file_exists(public_path($image_path)) ? asset($image_path) : asset($default_img);

    }

    static function randString($length)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size  = strlen($chars);
        $str   = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }
}
