<?php

namespace App\Console\Commands;

use App\Models\BaseModel;
use App\Models\Campaign;
use App\Models\Coupon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;

class CheckExpiredCampaign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:check-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Expired Campaign everyday at 00:00';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list_expired_campaign = Campaign::where('status', BaseModel::STATUS_ACTIVE)->where('endDate', '<', time())->get();
        if (count($list_expired_campaign) > 0) {
            foreach ($list_expired_campaign as $expired_campaign) {
                try {
                    $expired_campaign->update(['status' => BaseModel::STATUS_INACTIVE]);
                    Coupon::where('status', BaseModel::STATUS_ACTIVE)->where('campaign_id', $expired_campaign->id)->update(['status' => BaseModel::STATUS_INACTIVE]);
                } catch (Exception $e) {
                    Log::error('Có lỗi xảy ra khi cho dừng hoạt động chiến dịch hết hạn. Mã chiến dịch: ' . $expired_campaign->id . 'Error: ' . $e->getMessage());
                }
                Log::info(date('d/m/Y', time()) . ' - Dừng hoạt động chiến dịch hết hạn thành công. Mã chiến dịch: ' . $expired_campaign->id);
            }
        } else {
            Log::info('Không có chiến dịch này đang hoạt động.');
        }
        return true;
    }
}
