<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\RoleUser;
use App\User;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create {user_name} {email} {password} {first_name} {last_name} {phone} {address}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Exception
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            //1) Create Admin User
            $user = [
                'user_name'  => $this->argument('user_name'),
                'email'      => $this->argument('email'),
                'password'   => app('hash')->make($this->argument('password')),
                'status'     => 1,
                'first_name' => $this->argument('first_name'),
                'last_name'  => $this->argument('last_name'),
                'phone'      => $this->argument('phone'),
                'address'    => $this->argument('address'),
                'created_at' => time(),
                'updated_at' => time(),
            ];
            /** @var User $user */
            $user = User::create($user);

            //2) Set User Role
            $role = Role::where('name', 'admin')->first();
            RoleUser::create(['user_id' => $user->id, 'role_id' => $role->id]);
            DB::commit();
            return json_encode($user->toArray());
        } catch (Exception $e) {
            DB::rollBack();
            return 'Error: Insert DB. ' . $e->getMessage();
        }
    }
}
