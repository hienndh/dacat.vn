<?php
/**
 * Created by PhpStorm.
 * User: zkenc
 * Date: 02/08/2018
 * Time: 11:38 CH
 */

namespace App\Models;

class MenuItem extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'menu_id', 'title', 'link', 'type', 'object_id', 'parent_id', 'sort'];
    protected $table = 'menu_item';

    public function getMenu()
    {
        return $this->hasOne('App\Models\Menu', 'id', 'menu_id');
    }

    public function childMenuItem()
    {
        return $this->hasMany('App\Models\MenuItem', 'parent_id', 'id')->orderBy('sort', 'asc');
    }

    public function getObject($type)
    {
        switch ($type) {
            case 'post':
                return $this->hasOne('App\Models\Post', 'id', 'object_id')->first();
                break;
            case 'page':
                return $this->hasOne('App\Models\Page', 'id', 'object_id')->first();
                break;
            case 'product':
                return $this->hasOne('App\Models\Product', 'id', 'object_id')->first();
                break;
            case 'post_cat':
            case 'product_cat':
                return $this->hasOne('App\Models\Category', 'id', 'object_id')->first();
                break;
            default:
                return null;
        }
    }
}
