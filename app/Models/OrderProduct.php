<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class OrderProduct extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'order_id', 'product_id', 'price', 'quantity'];
    protected $table = 'order_product';

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function order() {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }
}
