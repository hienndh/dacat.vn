<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 03/08/2018
 * Time: 9:19 SA
 */

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;

class Post extends BaseModel
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['id', 'title', 'slug', 'description', 'content', 'author_id', 'image', 'status', 'seo_title', 'seo_description', 'seo_keyword', 'created_at', 'updated_at'];
    protected $table = 'posts';

    public function category()
    {
        return $this->belongsToMany('App\Models\Category', 'post_cat', 'post_id', 'cat_id');
    }

    public function post_meta()
    {
        return $this->hasMany('App\Models\PostMeta', 'post_id', 'id');
    }

    public function author() {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

    public function relatedPostsByCat($limit = 5)
    {
        return Post::whereHas('category', function ($query) {
            $cat_ids = $this->category()->pluck('category.id')->all();
            $query->whereIn('category.id', $cat_ids);
        })->where('id', '<>', $this->id)->where('status', '=', '1')->inRandomOrder()->paginate($limit);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
