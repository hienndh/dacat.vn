<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 03/08/2018
 * Time: 9:06 SA
 */

namespace App\Models;

class Comment extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'user_id', 'content', 'rate', 'type', 'object_id', 'status', 'parent_id', 'created_at', 'updated_at'];
    protected $table = 'comments';
}
