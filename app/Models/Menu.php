<?php
/**
 * Created by PhpStorm.
 * User: zkenc
 * Date: 02/08/2018
 * Time: 11:36 CH
 */

namespace App\Models;

class Menu extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'name', 'location', 'status'];
    protected $table = 'menu';

    public function getMenuItem()
    {
        return $this->hasMany('App\Models\MenuItem', 'menu_id', 'id');
    }
}
