<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class ProductAttr extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'product_id', 'parent_attr_id', 'child_attr_id', 'sold_quantity', 'remain_quantity', 'total_quantity'];
    protected $table = 'product_attr';

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
