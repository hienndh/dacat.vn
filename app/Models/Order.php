<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 04/08/2018
 * Time: 3:01 CH
 */

namespace App\Models;

class Order extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'quantity', 'total', 'total_real', 'note', 'coupon_code', 'status', 'account_id', 'full_name', 'email', 'phone', 'address', 'province_id', 'district_id', 'ward_id', 'post_code', 'created_at', 'updated_at'];
    protected $table = 'orders';

    public function order_history()
    {
        return $this->hasMany('App\Models\OrderHistory', 'order_id', 'id');
    }

    public function shopping_cart()
    {
        return $this->hasOne('App\Models\ShoppingCart', 'identifier', 'id');
    }

    public function order_product()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    public function order_coupon()
    {
        return $this->hasOne(Coupon::class, 'code', 'coupon_code');
    }

    public function identifier()
    {
        $str_plus   = str_pad('', 5 - strlen($this->id), '0');
        $identifier = 'DH' . $str_plus . $this->id;
        return $identifier;
    }

    public function status_text()
    {
        switch ($this->status) {
            case BaseModel::ORDER_PENDING:
                $status_text = 'Chờ xử lý';
                break;
            case BaseModel::ORDER_PAID:
                $status_text = 'Đã thanh toán';
                break;
            case BaseModel::ORDER_DELIVERED:
                $status_text = 'Đang vận chuyển';
                break;
            case BaseModel::ORDER_COMPLETED:
                $status_text = 'Hoàn thành';
                break;
            case BaseModel::ORDER_CANCELED:
                $status_text = 'Đã huỷ';
                break;
            case BaseModel::ORDER_REFUNDED:
                $status_text = 'Chuyển hoàn';
                break;
            default:
                $status_text = 'Không xác định';
        }
        return $status_text;
    }

    public function status_color()
    {
        switch ($this->status) {
            case BaseModel::ORDER_PENDING:
                $status_text = 'warning';
                break;
            case BaseModel::ORDER_PAID:
                $status_text = 'info';
                break;
            case BaseModel::ORDER_DELIVERED:
                $status_text = 'primary';
                break;
            case BaseModel::ORDER_COMPLETED:
                $status_text = 'success';
                break;
            case BaseModel::ORDER_CANCELED:
                $status_text = 'danger';
                break;
            case BaseModel::ORDER_REFUNDED:
                $status_text = 'dark';
                break;
            default:
                $status_text = 'muted';
        }
        return $status_text;
    }

    function findCateByProduct($product_id)
    {
        $product = Product::where('id', $product_id)->first();
        if($product) {
            $list_product_cat = $product->category->pluck(['title'])->all();
            return $list_product_cat;
        } else {
            return [];
        }
    }
}
