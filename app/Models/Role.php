<?php

namespace App\Models;

class Role extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['name', 'display_name', 'description'];
    protected $table = 'roles';

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission');
    }
}
