<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:17 CH
 */

namespace App\Models;

class ProductMeta extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'product_id', 'meta_key', 'meta_value'];
    protected $table = 'product_meta';
}
