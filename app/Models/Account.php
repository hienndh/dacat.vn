<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Account extends Authenticatable
{
    protected $guard = "accounts";

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'account';
}
