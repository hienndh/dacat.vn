<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class ProductCat extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'product_id', 'cat_id'];
    protected $table = 'product_cat';
}
