<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 03/08/2018
 * Time: 9:06 SA
 */

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;

class Category extends BaseModel
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['id', 'title', 'slug', 'description', 'content', 'type', 'parent_id', 'author_id', 'image', 'banner', 'banner_link', 'status', 'seo_title', 'seo_description', 'seo_keyword', 'created_at', 'updated_at'];
    protected $table = 'category';

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post', 'post_cat', 'cat_id', 'post_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_cat', 'cat_id', 'product_id');
    }

    public function childCategory()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}


