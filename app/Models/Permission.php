<?php

namespace App\Models;

class Permission extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['name', 'display_name', 'description'];
    protected $table = 'permissions';
}
