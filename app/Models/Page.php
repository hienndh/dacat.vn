<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 03/08/2018
 * Time: 9:19 SA
 */

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;

class Page extends BaseModel
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['id', 'title', 'slug', 'description', 'content', 'content_html', 'type', 'author_id', 'image', 'status', 'seo_title', 'seo_description', 'seo_keyword', 'created_at', 'updated_at'];
    protected $table = 'page';

    public function page_meta()
    {
        return $this->hasMany('App\Models\PageMeta', 'page_id', 'id');
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
