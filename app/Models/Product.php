<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 03/08/2018
 * Time: 9:19 SA
 */

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Gloudemans\Shoppingcart\Contracts\Buyable;

class Product extends BaseModel implements Buyable
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['id', 'title', 'slug', 'description', 'content', 'sku', 'regular_price', 'sale_price', 'sale_number', 'sale_type', 'author_id', 'image', 'album', 'status', 'seo_title', 'seo_description', 'seo_keyword', 'created_at', 'updated_at'];
    protected $table = 'products';

    public function category()
    {
        return $this->belongsToMany('App\Models\Category', 'product_cat', 'product_id', 'cat_id');
    }

    public function attribute()
    {
        return $this->belongsToMany('App\Models\Attribute', 'product_attr', 'product_id', 'child_attr_id');
    }

    public function product_attr()
    {
        return $this->hasMany('App\Models\ProductAttr', 'product_id', 'id');
    }

    public function product_meta()
    {
        return $this->hasMany('App\Models\ProductMeta', 'product_id', 'id');
    }

    public function order()
    {
        return $this->belongsToMany('App\Models\Order', 'order_product', 'product_id', 'order_id');
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

    public function relatedProductsByCat($limit = 4)
    {
        return Product::whereHas('category', function ($query) {
            $cat_ids = $this->category()->pluck('category.id')->all();
            $query->whereIn('category.id', $cat_ids);
        })->where('id', '<>', $this->id)->inRandomOrder()->paginate($limit);
    }

    public function repository($attr_id)
    {
        return ProductAttr::where('product_id', $this->id)->where('child_attr_id', $attr_id)->first();
    }

    public function sold_quantity()
    {
        return ProductAttr::where('product_id', $this->id)->sum('sold_quantity');
    }

    public function remain_quantity()
    {
        return ProductAttr::where('product_id', $this->id)->sum('remain_quantity');
    }

    public function total_quantity()
    {
        return ProductAttr::where('product_id', $this->id)->sum('total_quantity');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->title;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->sale_price;
    }
}
