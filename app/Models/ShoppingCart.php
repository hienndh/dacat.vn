<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class ShoppingCart extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['identifier', 'instance', 'content'];
    protected $table = 'shopping_cart';
}
