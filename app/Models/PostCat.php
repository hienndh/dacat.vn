<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class PostCat extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'post_id', 'cat_id'];
    protected $table = 'post_cat';
}
