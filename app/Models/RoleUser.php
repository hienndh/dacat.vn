<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class RoleUser extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['user_id', 'role_id'];
    protected $table = 'role_user';
}
