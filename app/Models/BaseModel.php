<?php
/**
 * Created by PhpStorm.
 * User: zkenc
 * Date: 05/08/2018
 * Time: 1:21 CH
 */

namespace App\Models;


use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class BaseModel extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    //USER STATUS
    const USER_LOCKED = 0;
    const USER_ACTIVE = 1;

    //ORDER STATUS
    const ORDER_PENDING = 0;
    const ORDER_PAID = 1;
    const ORDER_DELIVERED = 2;
    const ORDER_COMPLETED = 3;
    const ORDER_CANCELED = 4;
    const ORDER_REFUNDED = 5;

    const ORDER_STATUS = [
        self::ORDER_PENDING   => "Chờ xử lý",
        self::ORDER_PAID      => "Đã thanh toán",
        self::ORDER_DELIVERED => "Đang vận chuyển",
        self::ORDER_COMPLETED => "Hoàn thành",
        self::ORDER_CANCELED  => "Đã huỷ",
        self::ORDER_REFUNDED  => "Chuyển hoàn",
    ];

    //ALERT TYPE
    const ALERT_SUCCESS = 'success';
    const ALERT_FAIL = 'fail';
    const ALERT_WARNING = 'warning';
    const ALERT_DANGER = 'danger';
    const ALERT_INFO = 'info';
    const ALERT_ARRAY = [self::ALERT_SUCCESS, self::ALERT_FAIL, self::ALERT_WARNING, self::ALERT_DANGER, self::ALERT_INFO];


    //CATEGORY TYPE
    const POST_CAT = 'post_cat';
    const PRODUCT_CAT = 'product_cat';

    //PAGE
    const PAGE_TYPE = ['default' => 'Mặc định', 'landing_page' => 'Landing Page'];
    const ATTRIBUTE_TYPE = ['text' => 'Văn bản', 'color' => 'Màu sắc'];

    //MENU
    const MENU_LOCATION = [
        'top_bar_menu'  => 'Menu trên',
        'main_menu'     => 'Menu chính',
        'mobile_menu'   => 'Menu trên di động',
        'footer_menu'   => 'Menu chân trang',
        'vertical_menu' => 'Menu dọc'
    ];
    const MENU_ITEM_TYPE = [
        'custom'      => 'Tuỳ chọn',
        'post'        => 'Bài viết',
        'product'     => 'Sản phẩm',
        'all_product'     => 'Danh sách sản phẩm',
        'page'        => 'Trang tĩnh',
        'post_cat'    => 'Chuyên mục bài viết',
        'product_cat' => 'Danh mục sản phẩm'
    ];

    //WIDGET SIDEBAR
    const WIDGET_SIDEBAR = [
        'sidebar_default' => 'KHU VỰC MẶC ĐỊNH',
        'sidebar_home'    => 'KHU VỰC TRANG CHỦ',
        'sidebar_post'    => 'KHU VỰC BÀI VIẾT',
        'sidebar_product' => 'KHU VỰC SẢN PHẨM',
        'sidebar_shop'    => 'KHU VỰC CỬA HÀNG',
        'sidebar_footer'  => 'KHU VỰC CHÂN TRANG',
    ];

    const WIDGET_LIST = [
        //        'widget__search_box'  => 'Tìm kiếm',
        'widget__text'             => 'Văn bản',
        'widget__email_subscriber' => 'Đăng ký nhận tin',
        //        'widget__image'       => 'Hình ảnh',
        'widget__menu___1'         => 'Thanh điều hướng 1',
        'widget__menu___2'         => 'Thanh điều hướng 2',
        'widget__menu___3'         => 'Thanh điều hướng 3',
        //        'widget__custom_html' => 'HTML tuỳ chỉnh',
        //        'widget__tags'        => 'Thẻ SEO từ khoá',
        //        'widget__comments'    => 'Bình luận',
        'widget__post___1'         => 'Danh sách bài viết 1',
        'widget__post___2'         => 'Danh sách bài viết 2',
        //        'widget__post_cat'    => 'Chuyên mục bài viết',
        //        'widget__product'     => 'Danh sách sản phẩm',
        //        'widget__product_cat' => 'Danh mục sản phẩm',
        'widget__policy'           => 'Các điểm nổi bật',
    ];

    const PROUCT_CURRENCY = 'VND';

    public function getFormattedPrice($price_field = '', $decimal = 0, $dec_point = ',', $thousands_sep = '.')
    {
        $number = $this->{$price_field};
        if (!$number) {
            return 0 . '' . BaseModel::PROUCT_CURRENCY;
        }
        switch (BaseModel::PROUCT_CURRENCY) {
            case '$':
                $formatted_price = BaseModel::PROUCT_CURRENCY . '' . number_format($number, $decimal, $dec_point, $thousands_sep);
                break;
            case 'VND':
                $formatted_price = number_format($number, $decimal, $dec_point, $thousands_sep) . '' . BaseModel::PROUCT_CURRENCY;
                break;
            default:
                $formatted_price = number_format($number, $decimal, $dec_point, $thousands_sep) . ' ' . BaseModel::PROUCT_CURRENCY;
        }
        return $formatted_price;
    }

    public function getFormattedDate($date_field, $date_format = 'd/m/Y H:i:s')
    {
        return Carbon::createFromTimestamp($this->{$date_field})->format($date_format);
    }

    public function get_link($model, $id, $slug)
    {
        switch ($model) {
            case 'page':
                $link = '/' . $slug;
                break;
            case 'post':
                $link = '/bai-viet/' . $slug;
                break;
            case 'product':
                $link = '/san-pham/' . $slug;
                break;
            case 'post_cat':
                $link = '/chuyen-muc/' . $slug;
                break;
            case 'product_cat':
                $link = '/danh-muc/' . $slug;
                break;
            default:
                $link = null;
        }
        return $link;
    }

    public static function getResponse($url)
    {
        $client = new \GuzzleHttp\Client();
        try {
            $request  = $client->get($url);
            $response = $request->getBody()->getContents();
            return $response;
        } catch (ClientException $e) {
            return false;
        }
    }
}
