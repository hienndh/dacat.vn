<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 03/08/2018
 * Time: 9:06 SA
 */

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;

class Attribute extends BaseModel
{
    use Sluggable;
    const SIZE = 1;
    const COLOR = 9;
    public $timestamps = false;
    protected $fillable = ['id', 'title', 'slug', 'description', 'content', 'type', 'parent_id', 'author_id', 'image', 'menu_order', 'status', 'created_at', 'updated_at'];
    protected $table = 'attribute';

    public function childAttribute()
    {
        return $this->hasMany('App\Models\Attribute', 'parent_id', 'id');
    }

    public function getRemainQuantity($product_id) {
        $res = ProductAttr::select('remain_quantity')->where('product_id', $product_id)->where('child_attr_id', $this->id)->first();
        return $res->remain_quantity;
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}


