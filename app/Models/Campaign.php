<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 04/08/2018
 * Time: 2:23 CH
 */

namespace App\Models;

class Campaign extends BaseModel
{
    const TRI_AN = 1;
    const CAMPAIGN = 2;
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'campaign';

    public function type_text()
    {
        switch ($this->type) {
            case self::TRI_AN:
                $type_text = 'Tri ân';
                break;
            case self::CAMPAIGN:
                $type_text = 'Chiến dịch';
                break;
            default:
                $type_text = 'Không xác định';
        }
        return $type_text;
    }

    public function type_color()
    {
        switch ($this->type) {
            case self::TRI_AN:
                $type_text = 'success';
                break;
            case self::CAMPAIGN:
                $type_text = 'info';
                break;
            default:
                $type_text = 'dark';
        }
        return $type_text;
    }

}
