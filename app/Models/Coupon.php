<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    const TRI_AN = 1;
    const CAMPAIGN = 2;
    const PREFIX = 'COUPON-';
    const CURRENCY = '1';
    const PERCENT = '2';

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'coupon';

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id', 'id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'user_id', 'id');
    }

    public function type_text()
    {
        switch ($this->type) {
            case self::TRI_AN:
                $type_text = 'Tri ân';
                break;
            case self::CAMPAIGN:
                $type_text = 'Chiến dịch';
                break;
            default:
                $type_text = 'Không xác định';
        }
        return $type_text;
    }

    public function type_color()
    {
        switch ($this->type) {
            case self::TRI_AN:
                $type_text = 'success';
                break;
            case self::CAMPAIGN:
                $type_text = 'info';
                break;
            default:
                $type_text = 'dark';
        }
        return $type_text;
    }
}
