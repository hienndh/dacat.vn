<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:17 CH
 */

namespace App\Models;

class UserMeta extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'user_id', 'meta_key', 'meta_value'];
    protected $table = 'user_meta';
}
