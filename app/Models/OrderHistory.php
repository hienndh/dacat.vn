<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class OrderHistory extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'order_id', 'status', 'note'];
    protected $table = 'order_history';
}
