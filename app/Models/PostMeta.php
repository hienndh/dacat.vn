<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:17 CH
 */

namespace App\Models;

class PostMeta extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'post_id', 'meta_key', 'meta_value'];
    protected $table = 'post_meta';
}
