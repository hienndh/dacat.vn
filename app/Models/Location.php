<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 04/08/2018
 * Time: 2:23 CH
 */

namespace App\Models;

class Location extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['id', 'name', 'type', 'parent_id', 'status'];
    protected $table = 'locations';
}
