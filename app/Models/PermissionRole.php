<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 02/08/2018
 * Time: 3:14 CH
 */

namespace App\Models;

class PermissionRole extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['permission_id', 'role_id'];
    protected $table = 'permission_role';
}
