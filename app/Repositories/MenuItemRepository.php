<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\MenuItem;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class MenuItemRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\MenuItem';
    }

    public function getList($params = [])
    {
        $data = MenuItem::where(function ($query) use ($params) {
            if (!empty($params['parent_id']) || isset($params['parent_id'])) {
                $query->where('parent_id', '=', intval($params['parent_id']));
            }
        })->where(function ($query) use ($params) {
            if (!empty($params['menu_id']) || isset($params['menu_id'])) {
                $query->where('menu_id', '=', intval($params['menu_id']));
            }
        })->orderBy('sort', 'asc')->get();
        return $data;
    }

    public function getByID($menu_item_id)
    {
        return MenuItem::where('id', '=', $menu_item_id)->first();
    }

    public function deleteAllMenuItemByMenuId($menu_id)
    {
        return MenuItem::where('menu_id', $menu_id)->delete();
    }

    /**
     * @param $params
     * @return MenuItem
     * @throws Exception
     */
    public function createMenuItem($params)
    {
        try {
            DB::beginTransaction();
            $menu = $this->create($params);
            DB::commit();
            return $menu;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return MenuItem
     * @throws Exception
     */
    public function updateMenuItem($id, $params = [])
    {
        try {
            $updated_menu_item = $this->update($params, $id);
            return $updated_menu_item;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteMenuItem($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
