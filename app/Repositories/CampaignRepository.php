<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\BaseModel;
use App\Models\Campaign;
use App\Models\Coupon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Exception;

class CampaignRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Campaign';
    }

    public function getList($params = [])
    {
        $data = Campaign::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->whereLike(['name'], $params['keyword']);
            }

            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }

            if (!empty($params['startDate']) || isset($params['startDate'])) {
                $query->where('startDate', '>=', intval($params['startDate']));
            }

            if (!empty($params['endDate']) || isset($params['endDate'])) {
                $query->where('endDate', '<=', intval($params['endDate']));
            }
        })->orderByDesc('id')->paginate(15);
        return $data;
    }


    public function getByID($campaignId)
    {
        return Campaign::where('id', '=', $campaignId)->first();
    }

    /**
     * @param $params
     * @return Campaign
     * @throws Exception
     */
    public function createCampaign($params)
    {
        try {
            DB::beginTransaction();

            if ($params['endDate'] < time()) {
                $params['status'] = 0;
            }

            $campaign     = $this->create($params);
            $total_coupon = $params['total'];

            if ($total_coupon > 0) {
                $couponClass  = new CouponRepository($this->app);
                $paramsCoupon = [
                    'campaign_id' => $campaign->id,
                    'total'       => abs($total_coupon)
                ];
                $couponClass->createMultipleCoupon($paramsCoupon);
            }

            DB::commit();
            return $campaign;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Campaign
     * @throws Exception
     */
    public function updateCampaign($id, $params = [])
    {
        try {
            $campaign = Campaign::find($id);

            $total_coupon = $params['total'] - $campaign->total;
            if ($campaign->total_used >= $params['total']) {
                $total_coupon = 0;
            }

            if ($total_coupon < 0) {
                DB::table('coupon')->where('campaign_id', $id)->where('status', BaseModel::STATUS_ACTIVE)->limit(abs($total_coupon))->delete();
            } elseif ($total_coupon > 0) {
                $couponClass  = new CouponRepository($this->app);
                $paramsCoupon = [
                    'campaign_id' => $id,
                    'total'       => abs($total_coupon)
                ];
                $couponClass->createMultipleCoupon($paramsCoupon);
            }

            if ($params['endDate'] < time()) {
                $params['status'] = 0;
                Coupon::where('campaign_id', $id)->update(['status' => BaseModel::STATUS_INACTIVE]);
            } else {
                Coupon::where('campaign_id', $id)->update(['status' => BaseModel::STATUS_ACTIVE]);
            }

            $updated_campaign = $this->update($params, $id);
            return $updated_campaign;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteCampaign($id)
    {
        try {
            DB::beginTransaction();
            DB::table('coupon')->where('campaign_id', $id)->delete();
            $res = $this->delete($id);
            DB::commit();
            return $res;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }
}
