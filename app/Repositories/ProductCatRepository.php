<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\ProductCat;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class ProductCatRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\ProductCat';
    }

    public function getAll($params)
    {
        return ProductCat::where(function ($query) use ($params) {

        })->get();
    }

    public function getProductCatByProductID($product_id) {
        return ProductCat::where('product_id', '=', $product_id)->get();
    }

    /**
     * @param $params
     * @return ProductCat
     * @throws Exception
     */
    public function createProductCat($params)
    {
        try {
            DB::beginTransaction();
            $product_cat = ProductCat::insert($params);
            DB::commit();
            return $product_cat;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return ProductCat
     * @throws Exception
     */
    public function updateProductCat($id, $params = [])
    {
        try {
            $updated_product_cat = $this->update($params, $id);
            return $updated_product_cat;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteProductCat($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
