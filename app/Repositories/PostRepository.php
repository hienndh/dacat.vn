<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Post;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class PostRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Post';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return Post::with(['category'])
            ->where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->whereLike(['title', 'seo_keyword'], $params['keyword']);
                }

                if (!empty($params['cat_id'])) {
                    $query->whereHas('category', function ($query) use ($params) {
                        $query->where('category.id', $params['cat_id']);
                    });
                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }
            })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getAll()
    {
        return Post::where('status', '=', 1)->get();
    }

    public function getByID($post_id)
    {
        return Post::where('id', '=', $post_id)->first();
    }

    public function getBySlug($post_slug)
    {
        return Post::where('slug', '=', $post_slug)->first();
    }

    /**
     * @param $params
     * @return Post
     * @throws Exception
     */
    public function createPost($params)
    {
        try {
            DB::beginTransaction();
            $post = $this->create($params);
            DB::commit();
            return $post;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Post
     * @throws Exception
     */
    public function updatePost($id, $params = [])
    {
        try {
            $updated_post = $this->update($params, $id);
            return $updated_post;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deletePost($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
