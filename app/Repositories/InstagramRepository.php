<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\BaseModel;
use GuzzleHttp\Client;

class InstagramRepository
{
    public $client_id;
    public $client_secret;
    public $redriect_url;
    protected $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->client_id     = env('INSTAGRAM_CLIENT_ID');
        $this->client_secret = env('INSTAGRAM_CLIENT_SECRET');
        $this->redriect_url  = env('INSTAGRAM_REDRIECT_URL');

        $this->settingRepository = $settingRepository;
    }

    public function getAccessToken()
    {
        $setting_token = $this->settingRepository->findByMetaKey('instagram_access_token');
        if ($setting_token && $setting_token['meta_value'] != '') {
            return $setting_token['meta_value'];
        } else {
            $url = "https://api.instagram.com/oauth/authorize/?client_id=$this->client_id&redirect_uri=$this->redriect_url&response_type=code";
            $res = BaseModel::getResponse($url);
            /*$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_exec($ch);
            curl_close($ch);
            sleep(3);*/
            $setting_token = $this->settingRepository->findByMetaKey('instagram_access_token');
            if ($setting_token && $setting_token['meta_value'] != '') {
                return $setting_token['meta_value'];
            } else {
                return false;
            }
        }
    }

    public function getListImage()
    {
        $token = $this->getAccessToken();

        if (!$token) {
            return [];
        }

        $limit = 8;
        $url   = "https://api.instagram.com/v1/users/self/media/recent/?access_token=$token&count=$limit";

        $response = BaseModel::getResponse($url);
        $res = json_decode($response, true);

        if ($res['meta']['code'] != 200 || !isset($res['data']) || !count($res['data'])) {
            if ($res['meta']['error_type'] == 'OAuthAccessTokenException' || $res['meta']['error_type'] == 'OAuthException') {
                $this->getAccessToken();
            }
        }
        if (!isset($res['data'])) $res['data'] = [];

        return $res['data'];
    }
}
