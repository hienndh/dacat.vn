<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductAttr;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class ProductAttrRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\ProductAttr';
    }

    public function getAll($params)
    {
        return ProductAttr::where(function ($query) use ($params) {

        })->get();
    }

    public function getProductAttrByProductID($product_id)
    {
        return ProductAttr::where('product_id', '=', $product_id)->get();
    }

    public function getRepository($product_id, $child_attr_id)
    {
        return ProductAttr::where('product_id', '=', $product_id)->where('child_attr_id', '=', $child_attr_id)->first();
    }

    public function updateRepoFromOrder($type, $cart_list)
    {
        try {
            DB::beginTransaction();
            foreach ($cart_list as $cart_item) {
                $product_id       = $cart_item->id;
                $product_quantity = $cart_item->qty;
                $child_attr_id    = $cart_item->options->has('size_id') && $cart_item->options->size_id ? $cart_item->options->size_id : null;
                if ($product_id && $child_attr_id) {
                    if ($type == 'create') {
                        //Check kho
                        /** @var ProductAttr $repo */
                        $repo = $this->getRepository($product_id, $child_attr_id);
                        if (!$repo) {
                            throw new Exception('Không tìm thấy sản phẩm');
                        }
                        if ($repo->remain_quantity < $product_quantity) {
                            throw new Exception('Sản phẩm [' . $cart_item->name . '] size [' . $cart_item->options->size . '] không còn đủ số lượng yêu cầu');
                        }

                        ProductAttr::where('product_id', '=', $product_id)->where('child_attr_id', '=', $child_attr_id)->increment('sold_quantity', $product_quantity);
                        ProductAttr::where('product_id', '=', $product_id)->where('child_attr_id', '=', $child_attr_id)->decrement('remain_quantity', $product_quantity);
                    } elseif ($type == 'cancel') {
                        ProductAttr::where('product_id', '=', $product_id)->where('child_attr_id', '=', $child_attr_id)->decrement('sold_quantity', $product_quantity);
                        ProductAttr::where('product_id', '=', $product_id)->where('child_attr_id', '=', $child_attr_id)->increment('remain_quantity', $product_quantity);
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage());
        }

    }

    /**
     * @param $params
     * @return ProductAttr
     * @throws Exception
     */
    public function createProductAttr($params)
    {
        try {
            DB::beginTransaction();
            $product_attr = ProductAttr::insert($params);
            DB::commit();
            return $product_attr;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return ProductAttr
     * @throws Exception
     */
    public function updateProductAttr($id, $params = [])
    {
        try {
            $updated_product_attr = $this->update($params, $id);
            return $updated_product_attr;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteProductAttr($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
