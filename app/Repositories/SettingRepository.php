<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Setting;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class SettingRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Setting';
    }

    public function getAll() {
            return Setting::all();
    }

    /**
     * @param $meta_key
     * @return mixed
     */
    public function findByMetaKey($meta_key) {
        return Setting::where('meta_key', '=', $meta_key)->first();
    }

    /**
     * @param $params
     * @return Setting
     * @throws Exception
     */
    public function createSetting($params)
    {
        try {
            DB::beginTransaction();
            $setting_option = $this->create($params);
            DB::commit();
            return $setting_option;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Setting
     * @throws Exception
     */
    public function updateSetting($id, $params = [])
    {
        try {
            $updated_setting = $this->update($params, $id);
            return $updated_setting;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteSetting($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
