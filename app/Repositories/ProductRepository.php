<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\BaseModel;
use App\Models\Product;
use Prettus\Repository\Eloquent\BaseRepository;
use \Illuminate\Support\Facades\DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class ProductRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Product';
    }

    public function filterProduct($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;

        $listBestSellerProductId = null;
        if (isset($params['is_best_seller']) && intval($params['is_best_seller']) == BaseModel::STATUS_ACTIVE) {
            $params['ids'] = DB::table('order_product')
                ->selectRaw('product_id')
                ->groupBy('product_id')
                ->orderByRaw('count(id) DESC')
                ->get()->pluck('product_id')->all();
        }

        $listProduct = Product::with(['category', 'attribute', 'product_attr'])
            ->where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->whereLike(['title', 'seo_keyword'], $params['keyword']);
                }

                if (!empty($params['attr_ids'])) {
                    $query->whereHas('attribute', function ($query) use ($params) {
                        $query->whereIn('attribute.id', $params['attr_ids']);
                        $query->where('remain_quantity', '>', 0);
                    });
                }

                if (!empty($params['price'])) {
                    list($_price_from, $_price_to) = explode('-', $params['price']);
                    if ($_price_to == 'n') {
                        $query->where('sale_price', '>', intval($_price_from));
                    } elseif ($_price_from == '0') {
                        $query->where('sale_price', '<', intval($_price_to));
                    } else {
                        $query->whereBetween('sale_price', [intval($_price_from), intval($_price_to)]);
                    }
                }

                if (!empty($params['cat_id'])) {
                    $query->whereHas('category', function ($query) use ($params) {
                        $query->where('category.id', $params['cat_id']);
                    });
                }

                if (isset($params['is_best_seller']) && intval($params['is_best_seller']) == BaseModel::STATUS_ACTIVE) {
                    $query->whereIn('id', $params['ids']);
                }

                if (isset($params['is_sale']) && intval($params['is_sale']) == BaseModel::STATUS_INACTIVE) {
                    $query->whereColumn('regular_price', '=', 'sale_price');
                }

                if (isset($params['is_sale']) && intval($params['is_sale']) == BaseModel::STATUS_ACTIVE) {
                    $query->whereColumn('regular_price', '>', 'sale_price');
                }

                if (isset($params['in_stock']) && intval($params['in_stock']) == BaseModel::STATUS_INACTIVE) {
                    $query->whereHas('product_attr', function ($query) use ($params) {
                        $query->havingRaw('SUM(product_attr.remain_quantity) = 0');
                    });
                }


                if (isset($params['is_hot']) && intval($params['is_hot']) == 1) {

                    $query->whereHas('product_attr', function ($query) use ($params) {
                        $query->havingRaw('SUM(product_attr.remain_quantity) > 0');
                    });
                }
//                if (isset($params['is_new']) && intval($params['is_new']) == 1) {
//                    $product_model = new Product();
//                    $query->whereRaw('FROM_UNIXTIME(created_at, "%Y%m%d") = (SELECT FROM_UNIXTIME(MAX(created_at), "%Y%m%d") FROM ' . $product_model->getTable() . ')');
//                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }
            });

        if (isset($params['is_sale']) && intval($params['is_sale']) == BaseModel::STATUS_ACTIVE) {
            return $listProduct->orderByRaw('(regular_price - sale_price) ' . ($params['order_direction'] ?? 'desc'))->paginate($limit);
        } elseif (isset($params['is_best_seller']) && intval($params['is_best_seller']) == BaseModel::STATUS_ACTIVE) {
            $ids_ordered = implode(',', $params['ids']);
            return $listProduct->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->paginate($limit);
        } elseif (isset($params['is_new']) && intval($params['is_new']) == 1) {
            return $listProduct->orderBy('created_at', 'desc')->paginate($limit);
        } else {
            return $listProduct->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
        }
    }

    public function getRamdom($limit = 20)
    {
        return Product::inRandomOrder()->paginate($limit);
    }

    public function getAll()
    {
        return Product::where('status', '=', 1)->get();
    }

    public function getByID($product_id)
    {
        return Product::where('id', '=', $product_id)->first();
    }

    public function getBySlug($product_slug)
    {
        return Product::where('slug', '=', $product_slug)->first();
    }

    /**
     * @param $params
     * @return Product
     * @throws Exception
     */
    public function createProduct($params)
    {
        try {
            DB::beginTransaction();
            $product = $this->create($params);
            DB::commit();
            return $product;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Product
     * @throws Exception
     */
    public function updateProduct($id, $params = [])
    {
        try {
            $updated_product = $this->update($params, $id);
            return $updated_product;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function deleteProduct($id)
    {
        try {
            $updated_product = $this->update(['status' => 0], $id);
            return $updated_product;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }
    }
}
