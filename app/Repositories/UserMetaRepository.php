<?php

namespace App\Repositories;

use App\Models\UserMeta;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class UserMetaRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\UserMeta';
    }

    public function findByUserId($user_id)
    {
        return UserMeta::where('user_id', '=', $user_id)->get();
    }

    public function findByMetaKey($user_id, $meta_key) {
        return UserMeta::where('user_id', '=', $user_id)->where('meta_key', '=', $meta_key)->first();
    }

    /**
     * @param $params
     * @return UserMeta
     * @throws Exception
     */
    public function createUserMeta($params)
    {
        try {
            DB::beginTransaction();
            $user_meta = $this->create($params);
            DB::commit();
            return $user_meta;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return UserMeta
     * @throws Exception
     */
    public function updateUserMeta($id, $params = [])
    {
        try {
            $updated_user = $this->update($params, $id);
            return $updated_user;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteUserMeta($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
