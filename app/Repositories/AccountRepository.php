<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Account;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Exception;

class AccountRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Account';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return Account::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->whereLike(['user_name', 'phone', 'email'], $params['keyword']);
            }

            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }
        })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getByID($accountId)
    {
        return Account::find($accountId);
    }

    public function findAccount($user_name)
    {
        $data = DB::table('account')
            ->where('user_name', $user_name)
            ->orWhere('phone', $user_name)
            ->orWhere('email', $user_name)->first();
        return $data;
    }

    /**
     * @param $params
     * @return Account
     * @throws Exception
     */
    public function createAccount($params)
    {
        try {
            DB::beginTransaction();
            $account = $this->create($params);
            DB::commit();
            return $account;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Account
     * @throws Exception
     */
    public function updateAccount($id, $params = [])
    {
        try {
            $updated_account = $this->update($params, $id);
            return $updated_account;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteAccount($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
