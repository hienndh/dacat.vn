<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Menu;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class MenuRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Menu';
    }

    public function getList($params = [])
    {
        $data = Menu::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->where('name', 'LIKE', '%' . $params['keyword'] . '%');
            }
        })->where(function ($query) use ($params) {
            if (!empty($params['location'])) {
                $query->where('location', '=', $params['location']);
            }
        })->where(function ($query) use ($params) {
            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }
        })->orderBy('name', 'asc')->get();
        return $data;
    }

    public function getByID($menu_id)
    {
        return Menu::where('id', '=', $menu_id)->first();
    }

    public function getByLocation($location)
    {
        return Menu::where('location', '=', $location)->first();
    }

    /**
     * @param $params
     * @return Menu
     * @throws Exception
     */
    public function createMenu($params)
    {
        try {
            DB::beginTransaction();
            $menu = $this->create($params);
            DB::commit();
            return $menu;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Menu
     * @throws Exception
     */
    public function updateMenu($id, $params = [])
    {
        try {
            $updated_menu = $this->update($params, $id);
            return $updated_menu;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteMenu($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
