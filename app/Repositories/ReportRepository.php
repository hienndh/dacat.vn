<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Order;
use Prettus\Repository\Eloquent\BaseRepository;

class ReportRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Order';
    }

    public function reportStatus($params = [])
    {
        $data = Order::where(function ($query) use ($params) {
            if (!empty($params['from']) || isset($params['from'])) {
                $query->where('created_at', '>=', intval($params['from']));
            } else {
                $query->where('created_at', '>=', time() - 7 * 86400);
            }

            if (!empty($params['to']) || isset($params['to'])) {
                $query->where('created_at', '<=', intval($params['to']));
            } else {
                $query->where('created_at', '<=', time());
            }
        })->groupBy('status')
            ->selectRaw('status,count(*) as total_order, sum(total_real) as sum_total_real, sum(total) as sum_total')
            ->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->get();
        return $data;
    }

    public function reportProduct($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;

        $data = Order::where(function ($query) use ($params) {
            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('orders.status', '=', intval($params['status']));
            }
            if (!empty($params['from']) || isset($params['from'])) {
                $query->where('orders.created_at', '>=', intval($params['from']));
            } else {
                $query->where('orders.created_at', '>=', time() - 7 * 86400);
            }

            if (!empty($params['to']) || isset($params['to'])) {
                $query->where('orders.created_at', '<=', intval($params['to']));
            } else {
                $query->where('orders.created_at', '<=', time());
            }
        })->leftJoin('order_product', 'order_product.order_id', '=', 'orders.id')
            ->leftJoin('products', 'products.id', '=', 'order_product.product_id')
            ->groupBy('order_product.product_id', 'orders.status', 'order_product.price')
            ->selectRaw('products.title as product_title, order_product.price as order_product_price, sum(order_product.quantity) as total_product, sum(order_product.quantity * order_product.price) as total_money, order_product.product_id as product_id, orders.status, count(distinct orders.id) as total_order')
            ->orderBy($params['order_by'] ?? 'product_title', $params['order_direction'] ?? 'desc');
        if ($limit == 'all') {
            $data = $data->get();
        } else {
            $data = $data->paginate($limit);
        }
        return $data;
    }
}
