<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Category;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class CategoryRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Category';
    }

    public function getList($params = [])
    {
        $data = Category::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->whereLike(['title', 'slug'], $params['keyword']);
            }

            if (!empty($params['type'])) {
                $query->where('type', '=', $params['type']);
            }

            if (!empty($params['parent_id']) || isset($params['parent_id'])) {
                $query->where('parent_id', '=', intval($params['parent_id']));
            }

            if (!empty($params['ids']) || isset($params['ids'])) {
                $query->whereIn('id', $params['ids']);
            }

            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }
        })->orderBy(!empty($params['order_by']) ? $params['order_by'] : 'created_at', !empty($params['order_direction']) ?
            $params['order_direction'] : 'desc')->get();
        return $data;
    }

    public function getAllPostCat()
    {
        return Category::where('type', '=', 'post_cat')->where('status', '=', 1)->where('parent_id', '=', 0)->orderBy('title', 'asc')->get();
    }

    public function getAllProductCat()
    {
        return Category::where('type', '=', 'product_cat')->where('status', '=', 1)->where('parent_id', '=', 0)->orderBy('title', 'asc')->get();
    }

    public function getByID($cat_id, $cat_type)
    {
        return Category::where('id', '=', $cat_id)->where('type', '=', $cat_type)->first();
    }

    public function getBySlug($cat_slug, $cat_type)
    {
        return Category::where('slug', '=', $cat_slug)->where('type', '=', $cat_type)->first();
    }

    /**
     * @param $params
     * @return Category
     * @throws Exception
     */
    public function createCategory($params)
    {
        try {
            DB::beginTransaction();
            $category = $this->create($params);
            DB::commit();
            return $category;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Category
     * @throws Exception
     */
    public function updateCategory($id, $params = [])
    {
        try {
            $updated_category = $this->update($params, $id);
            return $updated_category;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteCategory($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
