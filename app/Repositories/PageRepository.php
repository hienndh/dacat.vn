<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Page;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class PageRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Page';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return Page::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->whereLike(['title', 'seo_keyword'], $params['keyword']);
            }
        })->where(function ($query) use ($params) {
            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }
        })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getAll()
    {
        return Page::where('status', '=', 1)->get();
    }

    public function getByID($page_id)
    {
        return Page::where('id', '=', $page_id)->first();
    }

    // Show a page by slug
    public function getBySlug($page_slug)
    {
        return Page::where('slug', '=', $page_slug)->first();
    }

    /**
     * @param $params
     * @return Page
     * @throws Exception
     */
    public function createPage($params)
    {
        try {
            DB::beginTransaction();
            $page = $this->create($params);
            DB::commit();
            return $page;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Page
     * @throws Exception
     */
    public function updatePage($id, $params = [])
    {
        try {
            $updated_page = $this->update($params, $id);
            return $updated_page;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deletePage($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
