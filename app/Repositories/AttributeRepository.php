<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\Attribute;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class AttributeRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Attribute';
    }

    public function getList($params = [])
    {
        $data = Attribute::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->whereLike(['title', 'slug'], $params['keyword']);
            }

            if (!empty($params['type'])) {
                $query->where('type', '=', $params['type']);
            }

            if (!empty($params['parent_id']) || isset($params['parent_id'])) {
                $query->where('parent_id', '=', intval($params['parent_id']));
            }

            if(!empty($params['is_child']) || isset($params['is_child'])) {
                $query->where('parent_id', '!=', 0);
            }

            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }
        })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->get();
        return $data;
    }

    public function getByID($attr_id)
    {
        return Attribute::where('id', '=', $attr_id)->first();
    }

    /**
     * @param $params
     * @return Attribute
     * @throws Exception
     */
    public function createAttribute($params)
    {
        try {
            DB::beginTransaction();
            $attribute = $this->create($params);
            DB::commit();
            return $attribute;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Attribute
     * @throws Exception
     */
    public function updateAttribute($id, $params = [])
    {
        try {
            $updated_attribute = $this->update($params, $id);
            return $updated_attribute;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteAttribute($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
