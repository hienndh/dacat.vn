<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\BaseModel;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderProduct;
use Prettus\Repository\Eloquent\BaseRepository;
use \Illuminate\Support\Facades\DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use Gloudemans\Shoppingcart\Facades\Cart as Cart;

class OrderRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Order';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        $data  = Order::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->whereLike(['full_name', 'email', 'phone', 'address', 'note'], $params['keyword']);
            }
        })->where(function ($query) use ($params) {
            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }
        })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
        return $data;
    }

    public function getByID($order_id)
    {
        return Order::where('id', '=', $order_id)->first();
    }

    public function checkOrder($identifier)
    {
        $order_id = intval(ltrim(substr($identifier, 2), '0'));
        return Order::where('id', '=', $order_id)->first();
    }

    /**
     * @param $params
     * @return Order
     * @throws Exception
     */
    public function createOrder($params)
    {
        try {
            DB::beginTransaction();

            /*
             * use coupon
             */
            if (isset($params['coupon_code']) && $params['coupon_code'] != '') {
                $couponClass = new CouponRepository($this->app);
                $coupon      = $couponClass->useCoupon($params['coupon_code']);
                if (!$coupon) {
                    throw new Exception('Có lỗi xảy ra khi sử dụng coupon');
                }
                if ($coupon->value_type == Coupon::CURRENCY) {
                    $params['total'] = ($params['total'] - $coupon->value) > 0 ? ($params['total'] - $coupon->value) : 0;
                } else {
                    $params['total'] = ($params['total'] - $params['total'] * $coupon->value / 100) ?: 0;
                }
            }

            $order        = $this->create($params);
            $listItem     = Cart::search(function ($cartItem) {
                return $cartItem;
            });
            $listItem     = json_decode(json_encode($listItem), true);
            $orderHistory = [
                'order_id' => $order['id'],
                'status'   => BaseModel::ORDER_PENDING,
                'note'     => "Tạo đơn hàng mới"
            ];
            DB::table('order_history')->insert($orderHistory);
            $listProductOrder = [];
            foreach ($listItem as $value) {
                $listProductOrder[] = [
                    'product_id' => $value["id"],
                    'quantity'   => $value["qty"],
                    'order_id'   => $order['id'],
                    'price'      => $value['price'],
                ];
            }
            DB::table('order_product')->insert($listProductOrder);
            DB::commit();
            return $order;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Order
     * @throws Exception
     */
    public function updateOrder($id, $params = [])
    {
        try {
            $updated_order = $this->update($params, $id);
            return $updated_order;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteOrder($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
