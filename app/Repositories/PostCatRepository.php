<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\PostCat;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class PostCatRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\PostCat';
    }

    public function getAll($params)
    {
        return PostCat::where(function ($query) use ($params) {

        })->get();
    }

    public function getPostCatByPostID($post_id) {
        return PostCat::where('post_id', '=', $post_id)->get();
    }

    /**
     * @param $params
     * @return PostCat
     * @throws Exception
     */
    public function createPostCat($params)
    {
        try {
            DB::beginTransaction();
            $post_cat = PostCat::insert($params);
            DB::commit();
            return $post_cat;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return PostCat
     * @throws Exception
     */
    public function updatePostCat($id, $params = [])
    {
        try {
            $updated_post_cat = $this->update($params, $id);
            return $updated_post_cat;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deletePostCat($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
