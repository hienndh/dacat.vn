<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 13/09/2018
 * Time: 1:35 CH
 */

namespace App\Repositories;

use App\Models\BaseModel;
use App\Models\Campaign;
use App\Models\Coupon;
use Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Exception;

class CouponRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Coupon';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return Coupon::where(function ($query) use ($params) {
            if (!empty($params['keyword'])) {
                $query->whereLike(['code'], $params['keyword']);
            }

            if (!empty($params['status']) || isset($params['status'])) {
                $query->where('status', '=', intval($params['status']));
            }

            if (!empty($params['campaign_id']) || isset($params['campaign_id'])) {
                $query->where('campaign_id', '=', intval($params['campaign_id']));
            }
        })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getListCampaign()
    {
        return DB::table('campaign')->where('status', BaseModel::STATUS_ACTIVE)->get();
    }


    public function getByID($campaignId)
    {
        return Coupon::where('id', '=', $campaignId)->first();
    }

    /**
     * @param $params
     * @return mixed
     * @throws Exception
     */
    public function createCoupon($campaignId)
    {
        try {
            DB::beginTransaction();
            $campaign = Campaign::find($campaignId);
            $code     = Helper::randString(10);
            $params   = [
                'code'        => strtoupper(Coupon::PREFIX . $code),
                'created_at'  => time(),
                'type'        => $campaign->type,
                'value'       => $campaign->value,
                'value_type'  => $campaign->value_type,
                'campaign_id' => $campaign->id,
                'status'      => $campaign->status
            ];
            $coupon   = $this->create($params);

            $campaign->update(['total' => $campaign->total + 1]);
            DB::commit();
            return $coupon;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    public function createMultipleCoupon($params)
    {
        try {
            DB::beginTransaction();
            $campaign = Campaign::find($params['campaign_id']);

            $listCoupon = [];
            while (count($listCoupon) < $params['total']) {
                $code = Helper::randString(10);
                if (!DB::table('coupon')->where('code', $code)->count()) {
                    $listCoupon[] = [
                        'code'        => strtoupper(Coupon::PREFIX . $code),
                        'created_at'  => time(),
                        'type'        => $campaign->type,
                        'value'       => $campaign->value,
                        'value_type'  => $campaign->value_type,
                        'campaign_id' => $campaign->id,
                        'status'      => $campaign->status
                    ];
                }
            }
            Coupon::insert($listCoupon);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }

    /*public function deleteCampaign($id)
    {
        try {
            DB::beginTransaction();
            DB::table('coupons')->where('campaign_id', $id)->delete();
            $res = $this->delete($id);
            DB::commit();
            return $res;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB. ' . $e->getMessage());
        }
    }*/


    function useCoupon($code)
    {
        try {
            $coupon = $this->checkCoupon($code);
            return $coupon;
        } catch (Exception $e) {
            throw new Exception('Có lỗi xảy ra, quý khách vui lòng trở lại sau');
        }
    }

    function checkCoupon($code)
    {
        $coupon = Coupon::where('code', $code)->first();
        if (!$coupon) {
            throw new Exception('Mã coupon không tồn tại!');
        }
        if ($coupon->account_id != null && $coupon->account_id != Auth::guard('account')->user()->id) {
            throw new Exception('Mã coupon thuộc sở hữu của người khác');
        }
        if ($coupon->campaign->startDate > time()) {
            throw new Exception('Chưa đến ngày có thể sử dụng coupon!');
        }
        if ($coupon->campaign->endDate < time()) {
            try {
                DB::table('coupon')->where('code', $code)->update([
                    'status'     => BaseModel::STATUS_INACTIVE,
                    'used_date'  => time(),
                    'account_id' => Auth::guard('account')->user()->id
                ]);

                $campaign = Campaign::find($coupon->campaign_id);

                DB::table('campaign')->where('id', $coupon->campaign_id)->update([
                    'status'     => BaseModel::STATUS_INACTIVE,
                    'total_used' => $campaign->total_used + 1
                ]);
            } catch (Exception $e) {
                throw new Exception('Có lỗi xảy ra, quý khách vui lòng trở lại sau');
            }
            throw new Exception('Coupon của bạn đã hết hạn từ ngày ' . date('d/m/Y', $coupon->campaign->endDate) . '!');
        }
        if ($coupon->status == BaseModel::STATUS_INACTIVE) {
            throw new Exception('Mã coupon đã dừng hoạt động');
        }

        return $coupon;
    }


    function getCoupon()
    {
        $campaign_ids = Campaign::where(function ($query) {
            $query->where('status', '=', BaseModel::STATUS_ACTIVE);
            $query->where('type', '=', Campaign::CAMPAIGN);
            $query->where('startDate', '<=', time());
            $query->where('endDate', '>=', time());
            $query->orderByDesc('id');
        })->get(['id'])->pluck('id')->all();

        $account_campaign_ids = Coupon::where('account_id', '=', Auth::guard('account')->user()->id)
            ->distinct('campaign_id')
            ->where('type', '=', Coupon::CAMPAIGN)
            ->where('status', '=', BaseModel::STATUS_ACTIVE)
            ->orderByDesc('id')
            ->get(['campaign_id'])->pluck('campaign_id')->all();

        $campaign_ids = array_diff($campaign_ids, $account_campaign_ids);
        if (!count($campaign_ids)) {
            $coupon = Coupon::where('account_id', '=', Auth::guard('account')->user()->id)->where('status', '=', BaseModel::STATUS_ACTIVE)->orderByDesc('id')->first();
            return $coupon;
        }

        $coupon = Coupon::where('status', '=', BaseModel::STATUS_ACTIVE)
            ->where('type', '=', Coupon::CAMPAIGN)
            ->where('account_id', 0)->whereIn('campaign_id', $campaign_ids)
            ->orderByDesc('campaign_id')
            ->first();

        if (!$coupon) {
            try {
                $new_coupon = $this->createCoupon($campaign_ids[0]);
            } catch (Exception $e) {
                return null;
            }
            return $new_coupon;
        }

        $coupon->update([
            'account_id' => Auth::guard('account')->user()->id
        ]);
        return $coupon;
    }

    function getCouponUser()
    {
        $coupons = Coupon::where('account_id', '=', Auth::guard('account')->user()->id)->orderByDesc('id')->paginate(10);
        return $coupons;
    }
}
