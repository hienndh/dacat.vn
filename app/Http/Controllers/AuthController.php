<?php

namespace App\Http\Controllers;

use App\Models\BaseModel;
use App\Repositories\SettingRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController
{
    protected $auth;
    protected $userRepository;
    public $loginAfterSignUp = true;

    public function __construct(Auth $auth, UserRepository $userRepository, SettingRepository $settingRepository)
    {
        parent::__construct($settingRepository);
        $this->auth           = $auth;
        $this->userRepository = $userRepository;
    }

    public function loginView()
    {
        $layout = "LANDING";
        if (Auth::check()) {
            return redirect()->route('admin_dashboard');
        } else {
            return view('admin.auth.login', compact('layout'));
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'user_name' => 'required',
            'password'   => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $credentials = $request->only('user_name', 'password', 'is_reset_pass');

        /** @var User $user */
        $user = $this->userRepository->findByEmail($credentials['user_name']) ? $this->userRepository->findByEmail($credentials['user_name']) : $this->userRepository->findByUsername($credentials['user_name']);
        if (!$user) {
            return redirect()->back()->withErrors('Tài khoản này không tồn tài!')->withInput();
        }

        if (!app('hash')->check($credentials['password'], $user->password)) {
            return redirect()->back()->withErrors('Sai mật khẩu!')->withInput();
        }

        if ($user->status == BaseModel::USER_LOCKED) {
            return redirect()->back()->withErrors('Tài khoản của bạn đã bị khoá!')->withInput();
        }

        $params = [
            'user_name' => $user->user_name,
            'email'     => $user->email,
            'password'  => $credentials['password']
        ];

        if (Auth::attempt($params)) {
            $msg_success = isset($credentials['is_reset_pass']) && $credentials['is_reset_pass'] ? 'Đổi mật khẩu thành công!' : 'Đăng nhập thành công!';

            $role = Auth::user()->role->toArray();
            if ($role && in_array("admin", array_column($role, 'name'))) {
                return redirect()->route('admin_dashboard')->with(BaseModel::ALERT_SUCCESS, $msg_success);
            }

            return redirect()->route('client_home')->with(BaseModel::ALERT_SUCCESS, $msg_success);
        } else {
            return redirect()->back()->withErrors('Sai mật khẩu!')->withInput();
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'user_name'  => 'required|min:3|max:50',
            'email'      => 'required|email',
            'password'   => 'required|confirmed|min:6',
            'first_name' => 'required|min:3|max:255',
            'last_name'  => 'required|min:3|max:255',
            'phone'      => 'required|regex:/[0-9]{10,11}/',
            'address'    => 'required',
        ]);

        $credentials = $request->only('user_name', 'email', 'password', 'first_name', 'last_name', 'phone', 'address');

        $params['user_name']  = $credentials['user_name'];
        $params['email']      = $credentials['email'];
        $params['password']   = Hash::make($credentials['password']);
        $params['first_name'] = $credentials['first_name'];
        $params['last_name']  = $credentials['last_name'];
        $params['phone']      = $credentials['phone'];
        $params['address']    = $credentials['address'];

        $user = $this->userRepository->findByUsername($params['user_name']);
        if ($user) {
            return $this->resFail(null, 'Tài khoản này đã tồn tại. Vui lòng nhập tài khoản khác.', 406);
        }
        /** @var User $user */
        $user = $this->userRepository->findByEmail($params['email']);
        if ($user) {
            return $this->resFail(null, 'Email này đã được sử dụng. Vui lòng nhập email khác.', 406);
        }

        //Create new user
        try {
            $user = $this->userRepository->createUser($params);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage(), 406, 0);
        }

        if (!$user) {
            return $this->resFail(null, "Đăng ký không thành công!", 406, 0);
        }

        //Login after sign up
        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }

        return $this->resSuccess($user, 'Đăng ký thành công!');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('client_home')->with(BaseModel::ALERT_SUCCESS, 'Đăng xuất thành công!');
    }

    public function resetPasswordView()
    {
        $layout = "LANDING";
        return view('admin.auth.reset_password', compact('layout'));
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'current_password' => 'required|min:6',
            'new_password'     => 'required|confirmed|min:6|different:current_password',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $credentials = $request->only('current_password', 'new_password');

        /** @var User $current_user */
        $current_user           = Auth::user();
        $params['user_name']   = $current_user->user_name ? $current_user->user_name : $current_user->email;
        $params['password']     = $credentials['current_password'];
        $params['new_password'] = $credentials['new_password'];
        /** @var User $user */
        $user = $this->userRepository->findByEmail($params['user_name']) ? $this->userRepository->findByEmail($params['user_name']) : $this->userRepository->findByUsername($params['user_name']);
        if (!$user) {
            return redirect()->back()->withErrors('Tài khoản này không tồn tài!')->withInput();
        }

        if (!app('hash')->check($params['password'], $user->password)) {
            return redirect()->back()->withErrors('Mật khẩu hiện tại mật khẩu không chính xác!')->withInput();
        }

        if ($user->status == BaseModel::USER_LOCKED) {
            return redirect()->back()->withErrors('Tài khoản của bạn đã bị khoá!')->withInput();
        }

        //Update new password
        try {
            $updated_user = $this->userRepository->updateUser($user->id, ['password' => app('hash')->make($params['new_password'])]);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Đổi mật khẩu không thành công! Vui lòng thử lại')->withInput();
        }

        if (!$updated_user) {
            return redirect()->back()->withErrors('Đổi mật khẩu không thành công! Vui lòng thử lại')->withInput();
        }

        //Login after reset password
        if ($this->loginAfterSignUp) {
            $login_request = new Request(['user_name' => $params['user_name'], 'password' => $params['new_password'], 'is_reset_pass' => true]);
            return $this->login($login_request);
        }

        return redirect()->route('client_home')->with(BaseModel::ALERT_SUCCESS, 'Đổi mật khẩu thành công!');
    }
}
