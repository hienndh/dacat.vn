<?php

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class InstagramController extends Controller
{
    protected $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    function index(Request $request)
    {
        Log::info('Instagram callback - Request data:' . json_encode($request->all()));

        if (!$request->has('code')) {
            Log::info('Instagram callback - "code" field not found. ' . json_encode($request->all()));
            return false;
        }

        $url   = "https://api.instagram.com/oauth/access_token";
        $param = [
            'code'          => $request->get('code'),
            'client_id'     => env('INSTAGRAM_CLIENT_ID'),
            'client_secret' => env('INSTAGRAM_CLIENT_SECRET'),
            'redriect_url'  => env('INSTAGRAM_REDRIECT_URL'),
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => env('INSTAGRAM_REDRIECT_URL')
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $res = curl_exec($ch);
        curl_close($ch);

        Log::info('Instagram callback - Response: ' . $res);
        $res = json_decode($res, true);

        if(!isset($res['access_token'])) {
            return false;
        }

        $setting_data = [
            'meta_key'   => 'instagram_access_token',
            'meta_value' => $res['access_token']
        ];

        $setting_token = $this->settingRepository->findByMetaKey('instagram_access_token');
        try {
            if ($setting_token) {
                $this->settingRepository->updateSetting($setting_token->id, $setting_data);
            } else {
                $this->settingRepository->createSetting($setting_data);
            }
            Log::info('Save Instagram Access Token to database.');
        } catch (Exception $e) {
            Log::error('================= Save Instagram Access Token failed.');
            return false;
        }

        Log::info('================= Save Instagram Access Token successfully.');
        return $res['access_token'];
    }
}
