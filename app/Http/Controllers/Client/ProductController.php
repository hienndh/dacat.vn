<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 10/09/2018
 * Time: 11:32 SA
 */

namespace App\Http\Controllers\Client;

use App\Models\Product;
use App\Repositories\AttributeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CouponRepository;
use App\Repositories\InstagramRepository;
use App\Repositories\MenuRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PageRepository;
use App\Repositories\PostRepository;
use App\Repositories\ProductAttrRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;

class ProductController extends ClientBaseController
{
    function __construct(SettingRepository $settingRepository, MenuRepository $menuRepository, PostRepository $postRepository, PageRepository $pageRepository, ProductRepository $productRepository, ProductAttrRepository $productAttrRepository, CategoryRepository $categoryRepository, OrderRepository $orderRepository, AttributeRepository $attributeRepository, CouponRepository $couponRepository, InstagramRepository $instagramRepository)
    {
        parent::__construct($settingRepository, $menuRepository, $postRepository, $pageRepository, $productRepository, $productAttrRepository, $categoryRepository, $orderRepository, $attributeRepository, $couponRepository, $instagramRepository);

        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    function productDetailView($slug)
    {
        $product_object = null;
        $seo_setting    = null;
        if (!empty($slug)) {
            /** @var Product $product_object */
            $product_object = $this->productRepository->getBySlug($slug);

            if (!$product_object || !$product_object['status']) {
                return redirect()->route('client_404')->withErrors('Không tìm thấy sản phẩm này. Vui lòng thử lại!');
            }

            $list_related_product = $product_object->relatedProductsByCat(4);

            $cat_title_list             = $product_object->category()->pluck('category.title')->all();
            $product_object['list_cat'] = implode(', ', $cat_title_list);

            //Attr
            $product_object['list_attr_color'] = $product_object->attribute()->where('parent_id', \App\Models\Attribute::COLOR)->get();
            $product_object['list_attr_size']  = $product_object->attribute()->where('parent_id', \App\Models\Attribute::SIZE)->get();

            $seo_setting = [
                'seo_title'       => $product_object['seo_title'] ?? $product_object['title'] ?? '',
                'seo_description' => $product_object['seo_description'] ?? $product_object['description'] ?? '',
                'seo_keyword'     => $product_object['seo_keyword'] ?? $product_object['keyword'] ?? '',
                'seo_image'       => $product_object['image'] ?? '',
            ];
        }

        $breadcrumb = [
            'main_title' => $product_object['title'],
        ];

        return view('client.product-detail', compact('breadcrumb', 'product_object', 'list_related_product', 'seo_setting'));
    }
}
