<?php

namespace App\Http\Controllers\Client;

use App\Models\Account;
use App\Models\BaseModel;
use App\Models\Order;
use App\Repositories\AccountRepository;
use App\Repositories\AttributeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CouponRepository;
use App\Repositories\InstagramRepository;
use App\Repositories\MenuRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PageRepository;
use App\Repositories\PostRepository;
use App\Repositories\ProductAttrRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SettingRepository;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AccController extends ClientBaseController
{
    protected $auth;
    protected $accountRepository;
    protected $couponRepository;
    public $loginAfterSignUp = true;

    public function __construct(Auth $auth, AccountRepository $accountRepository, SettingRepository $settingRepository, MenuRepository $menuRepository, PostRepository $postRepository, PageRepository $pageRepository, ProductRepository $productRepository, ProductAttrRepository $productAttrRepository, CategoryRepository $categoryRepository, OrderRepository $orderRepository, AttributeRepository $attributeRepository, CouponRepository $couponRepository, InstagramRepository $instagramRepository)
    {
        parent::__construct($settingRepository, $menuRepository, $postRepository, $pageRepository, $productRepository, $productAttrRepository, $categoryRepository, $orderRepository, $attributeRepository, $couponRepository, $instagramRepository);
        $this->auth              = $auth;
        $this->accountRepository = $accountRepository;
        $this->couponRepository  = $couponRepository;
    }

    public function loginView()
    {
        if (Auth::guard('account')->check()) {
            return redirect()->route('client_home');
        } else {
            return view('client.auth.login');
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'user_name' => 'required',
            'password'  => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $credentials = $request->only('user_name', 'password');

        /** @var Account $user */
        $user = $this->accountRepository->findAccount($credentials['user_name']);
        if (!$user) {
            return redirect()->back()->withErrors('Tài khoản này không tồn tài!')->withInput($credentials);
        }

        if (!app('hash')->check($credentials['password'], $user->password)) {
            return redirect()->back()->withErrors('Sai mật khẩu!')->withInput($credentials);
        }

        if ($user->status == BaseModel::USER_LOCKED) {
            return redirect()->back()->withErrors('Tài khoản của bạn đã bị khoá!')->withInput($credentials);
        }

        $params = [
            'user_name' => $user->user_name,
            'email'     => $user->email,
            'password'  => $credentials['password']
        ];

        if (Auth::guard('account')->attempt($params)) {
            $coupon = $this->couponRepository->getCoupon();
            return redirect()->route('client_home')->with(BaseModel::ALERT_SUCCESS, 'Đăng nhập thành công')->with('coupon', $coupon);
        } else {
            return redirect()->back()->withErrors('Sai mật khẩu!')->withInput($credentials);
        }
    }

    public function registerView()
    {
        if (Auth::guard('account')->check()) {
            return redirect()->route('client_home');
        } else {
            return view('client.auth.register');
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'user_name' => 'required|min:3|max:50',
            'email'     => 'required|email',
            'password'  => 'required|confirmed|min:6',
            'full_name' => 'required|min:3|max:255',
            //            'phone'     => 'required|regex:/[0-9]{10}/',
            'phone'     => 'required',
            'address'   => 'required',
        ]);

        $credentials               = $request->only('user_name', 'email', 'password', 'full_name', 'phone', 'address');
        $credentials['status']     = BaseModel::USER_ACTIVE;
        $credentials['password']   = Hash::make($credentials['password']);
        $credentials['created_at'] = $credentials['updated_at'] = time();

        if (DB::table('account')->where('user_name', $credentials['user_name'])->count()) {
            return redirect()->back()->withErrors('Tài khoản này đã tồn tại. Vui lòng nhập tài khoản khác.')->withInput($credentials);
            //            return $this->resFail(null, 'Tài khoản này đã tồn tại. Vui lòng nhập tài khoản khác.', 406);
        }

        if (DB::table('account')->where('email', $credentials['email'])->count()) {
            return redirect()->back()->withErrors('Email này đã được sử dụng. Vui lòng sử dụng email khác')->withInput($credentials);
            //            return $this->resFail(null, 'Email này đã được sử dụng. Vui lòng sử dụng email khác', 406);
        }

        if (DB::table('account')->where('phone', $credentials['phone'])->count()) {
            return redirect()->back()->withErrors('Số điện thoại này đã được sử dụng. Vui lòng sử dụng số điện thoại khác')->withInput($credentials);
            //            return $this->resFail(null, 'Số điện thoại này đã được sử dụng. Vui lòng sử dụng số điện thoại khác', 406);
        }


        try {
            $account = $this->accountRepository->createAccount($credentials);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput($credentials);
            //            return $this->resFail(null, $e->getMessage(), 406, 0);
        }


        if (!$account) {
            return redirect()->back()->withErrors('Có lỗi xảy ra, vui lòng thử lại sau!')->withInput($credentials);
        }

        //Login after sign up
        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }

        return redirect()->route(' client_home')->with(BaseModel::ALERT_SUCCESS, 'Đăng ký thành công!');
        //        return $this->resSuccess($account, 'Đăng ký thành công!');
    }

    public function logout(Request $request)
    {
        Auth::guard('account')->logout();
        return redirect()->route('client_home')->with(BaseModel::ALERT_SUCCESS, 'Đăng xuất thành công!');
    }

    /*ACCOUNT PAGE*/
    public function getInfo($id)
    {
        return $this->accountRepository->getByID($id);
    }

    public function accountView()
    {
        $account = Auth::guard('account')->user();
        return view('client.account.index', compact('account'));
    }

    public function updateInfo(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'full_name' => 'required',
            'email'     => 'required|email',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $account = [
            'full_name' => $request->get('full_name'),
            'email'     => $request->get('email'),
            'gender'    => intval($request->get('gender') == 'on' ? 1 : 0),
            'dob'       => strtotime(join('-', $request->get('birth')))
        ];

        $account_update = DB::table('account')->where('id', Auth::guard('account')->user()->id)->update($account);

        if ($account_update) {
            return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công');
        } else {
            return redirect()->back()->withErrors('Cập nhật không thành công')->withInput();
        }
    }

    public function updatePasswordView()
    {
        $account = Auth::guard('account')->user();
        return view('client.account.update-password', compact('account'));
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'current'          => 'required',
            'new_password'     => 'required|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $account = Auth::guard('account')->user();

        if (!app('hash')->check($request->get('current'), $account->password)) {
            return redirect()->back()->withErrors('Sai mật khẩu!')->withInput();
        }

        $account_update = DB::table('account')->where('id', $account->id)->update(['password' => Hash::make($request->get('new_password'))]);

        if ($account_update) {
            return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công');
        } else {
            return redirect()->back()->withErrors('Cập nhật không thành công')->withInput();
        }
    }

    public function orderView()
    {
        $account = Auth::guard('account')->user();

        $listOrder = Order::where('account_id', $account->id)->paginate(6);

        return view('client.account.orders', compact('account', 'listOrder'));
    }

    public function orderDetail($id)
    {
        $account = Auth::guard('account')->user();

        $order = Order::find($id);

        if(!$order) {
            return redirect()->back()->withErrors('Không tìm thấy đơn hàng')->withInput();
        }

        $cart_content = unserialize(($order->shopping_cart)['content']);

        return view('client.account.order-detail', compact('order', 'account', 'cart_content'));
    }

    public function couponView()
    {
        $account = Auth::guard('account')->user();

        $listCoupon = $this->couponRepository->getCouponUser();

        return view('client.account.coupons', compact('account', 'listCoupon'));
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $email = $request->get('email');

        $account = Account::where('email', $email)->first();

        if (!$account) {
            return redirect()->back()->withErrors('Email không tồn tại, vui lòng đăng ký tài khoản mới!')->withInput();
        }

        $new_pass = Helper::randString(6);

        try {
            $account = $this->accountRepository->updateAccount($account->id, ['password' => Hash::make($new_pass)]);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Có lỗi xảy ra khi cập nhật tài khoản. Xin vui lòng thử lại!')->withInput();
        }

        $web_setting = $this->web_setting;

        try {
            Mail::send('client.email-template.forget-password', compact('account', 'new_pass', 'web_setting'), function ($message) use ($account, $web_setting) {
                $message->from($web_setting['ta_company_email'], $web_setting['ta_company_name']);
                $message->to($account->email, $account->full_name)->subject("Email quên mật khẩu từ " . $web_setting['ta_company_name']);
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors('Có lỗi xảy ra khi gửi mail!');
        }

        return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Chúng tôi đã gửi mật khẩu mới tới email ' . $account->email);
    }
}
