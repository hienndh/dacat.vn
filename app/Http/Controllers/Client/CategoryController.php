<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 10/09/2018
 * Time: 11:32 SA
 */

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends ClientBaseController
{

    /*POST CATEGORY*/
    function postCatView($slug, Request $request)
    {
        $params = [
            'page'   => 1,
            'limit'  => 12,
            'status' => 1
        ];
        $query  = $request->only('page');
        $params = array_merge($params, $query);

        $post_cat_object = null;
        $list_post       = null;
        $seo_setting     = null;
        if (!empty($slug)) {
            /** @var Category $post_cat_object */
            $post_cat_object = $this->categoryRepository->getBySlug($slug, BaseModel::POST_CAT);

            if (!$post_cat_object || !$post_cat_object['status']) {
                return redirect()->route('client_404')->withErrors('Không tìm thấy chuyên mục này. Vui lòng thử lại!');
            }

            $params['cat_id'] = $post_cat_object['id'];

            $list_post = $this->postRepository->getList($params);

            unset($params['cat_id']);
            unset($params['limit']);
            unset($params['status']);

            $seo_setting = [
                'seo_title'       => $post_cat_object['seo_title'] ?? $post_cat_object['title'] ?? '',
                'seo_description' => $post_cat_object['seo_description'] ?? $post_cat_object['description'] ?? '',
                'seo_keyword'     => $post_cat_object['seo_keyword'] ?? $post_cat_object['keyword'] ?? '',
                'seo_image'       => $post_cat_object['image'] ?? '',
            ];
        }

        $breadcrumb = [
            'main_title' => $post_cat_object['title'],
        ];

        return view('client.post-cat', compact('breadcrumb', 'post_cat_object', 'list_post', 'params', 'seo_setting'));
    }

    /*PRODUCT CATEGORY*/
    function productCatView($slug, Request $request)
    {
        $params = [
            'page'   => 1,
            'limit'  => 12,
            'status' => 1
        ];
        $query  = $request->only('page', 'limit', 'attr_ids', 'price', 'is_sale', 'in_stock', 'keyword', 'sort_by');

        $params = array_merge($params, $query);
        //        dd($request->all());

        $product_cat_object = null;

        $list_product   = null;
        $list_attribute = null;
        $seo_setting    = null;
        if (!empty($slug)) {
            $product_cat_object = $this->categoryRepository->getBySlug($slug, BaseModel::PRODUCT_CAT);

            if (!$product_cat_object || !$product_cat_object['status']) {
                return redirect()->route('client_404')->withErrors('Không tìm thấy danh mục này. Vui lòng thử lại!');
            }

            $attr_ids_string = null;
            if (isset($params['attr_ids'])) {
                $attr_ids_string    = $params['attr_ids'];
                $params['attr_ids'] = explode(',', $attr_ids_string);
            } else {
                $params['attr_ids'] = [];
            }

            $params['cat_id'] = $product_cat_object['id'];

            if (isset($params['sort_by'])) {
                $sort_by                   = explode('__', $params['sort_by'], 2);
                $params['order_by']        = $sort_by[0];
                $params['order_direction'] = $sort_by[1];
            }

            $list_product = $this->productRepository->filterProduct($params);
            $hh_slug = $slug;

            unset($params['cat_id']);
            unset($params['limit']);
            unset($params['status']);
            if (isset($params['sort_by'])) {
                unset($params['order_by']);
                unset($params['order_direction']);
            }

            $sort_by_params = [
                'page'     => 1,
                //                'limit'    => $params['limit'],
                'attr_ids' => $attr_ids_string,
                //                'keyword'  => $params['keyword'] ?? null,
            ];

            $seo_setting = [
                'seo_title'       => $product_cat_object['seo_title'] ?? $product_cat_object['title'] ?? '',
                'seo_description' => $product_cat_object['seo_description'] ?? $product_cat_object['description'] ?? '',
                'seo_keyword'     => $product_cat_object['seo_keyword'] ?? $product_cat_object['keyword'] ?? '',
                'seo_image'       => $product_cat_object['image'] ?? '',
            ];

            $list_attribute = $this->attributeRepository->getList(['parent_id' => 0, 'status' => 1, 'order_by' => 'menu_order', 'order_direction' => 'asc']);
        }

        $breadcrumb = [
            'main_title' => $product_cat_object['title'],
        ];

        return view('client.archive-product', compact('breadcrumb', 'product_cat_object', 'list_product',
            'list_attribute', 'params', 'sort_by_params', 'seo_setting','hh_slug'));
    }

    function archiveProductView(Request $request)
    {
        $params = [
            'page'   => 1,
            'limit'  => 12,
            'status' => 1
        ];
        $query  = $request->only('page', 'limit', 'cat_id', 'attr_ids', 'price', 'is_sale', 'in_stock', 'keyword', 'sort_by');

        $params = array_merge($params, $query);

        $list_product     = null;
        $list_product_cat = null;
        $list_attribute   = null;
        $seo_setting      = null;

        $attr_ids_string = null;
        if (isset($params['attr_ids'])) {
            $attr_ids_string    = $params['attr_ids'];
            $params['attr_ids'] = explode(',', $attr_ids_string);
        } else {
            $params['attr_ids'] = [];
        }

        if (isset($params['sort_by'])) {
            $sort_by                   = explode('__', $params['sort_by'], 2);
            $params['order_by']        = $sort_by[0];
            $params['order_direction'] = $sort_by[1];
        }

        $list_product = $this->productRepository->filterProduct($params);

        unset($params['limit']);
        unset($params['status']);
        if (isset($params['sort_by'])) {
            unset($params['order_by']);
            unset($params['order_direction']);
        }

        $sort_by_params = [
            'page'     => 1,
            //                'limit'    => $params['limit'],
            'attr_ids' => $attr_ids_string,
            //                'keyword'  => $params['keyword'] ?? null,
        ];

        $list_product_cat = $this->categoryRepository->getAllProductCat();

        $list_attribute = $this->attributeRepository->getList(['parent_id' => 0, 'status' => 1, 'order_by' => 'menu_order', 'order_direction' => 'asc']);

        $home_archive_product = $this->settingRepository->findByMetaKey('home_archive_product');
        $main_title = json_decode($home_archive_product->meta_value);

        $seo_setting = [
            'seo_title'       => $main_title->title ?? 'PRODUCTS',
            'seo_description' => '',
            'seo_keyword'     => '',
            'seo_image'       => '',
        ];

        $breadcrumb = [
            'main_title' => $main_title->title ?? 'PRODUCTS',
        ];

        return view('client.archive-product', compact('breadcrumb', 'list_product', 'list_product_cat', 'list_attribute', 'params', 'sort_by_params', 'seo_setting'));
    }

    function newProductView(Request $request)
    {
        $params = [
            'page'   => 1,
            'limit'  => 12,
            'is_new' => 1,
            'status' => 1
        ];
        $query  = $request->only('page', 'limit', 'cat_id', 'attr_ids', 'price', 'is_sale', 'in_stock', 'keyword', 'sort_by');

        $params = array_merge($params, $query);

        $list_product     = null;
        $list_product_cat = null;
        $list_attribute   = null;
        $seo_setting      = null;

        $attr_ids_string = null;
        if (isset($params['attr_ids'])) {
            $attr_ids_string    = $params['attr_ids'];
            $params['attr_ids'] = explode(',', $attr_ids_string);
        } else {
            $params['attr_ids'] = [];
        }

        if (isset($params['sort_by'])) {
            $sort_by                   = explode('__', $params['sort_by'], 2);
            $params['order_by']        = $sort_by[0];
            $params['order_direction'] = $sort_by[1];
        }

        $list_product = $this->productRepository->filterProduct($params);

        unset($params['is_new']);
        unset($params['limit']);
        unset($params['status']);
        if (isset($params['sort_by'])) {
            unset($params['order_by']);
            unset($params['order_direction']);
        }

        $sort_by_params = [
            'page'     => 1,
            //                'limit'    => $params['limit'],
            'attr_ids' => $attr_ids_string,
            //                'keyword'  => $params['keyword'] ?? null,
        ];

        $list_product_cat = $this->categoryRepository->getAllProductCat();

        $list_attribute = $this->attributeRepository->getList(['parent_id' => 0, 'status' => 1, 'order_by' => 'menu_order', 'order_direction' => 'asc']);

        $home_new_product = $this->settingRepository->findByMetaKey('home_new_product');
        $main_title = json_decode($home_new_product->meta_value);

        $seo_setting = [
            'seo_title'       => $main_title->title ?? 'WHATS NEW',
            'seo_description' => '',
            'seo_keyword'     => '',
            'seo_image'       => '',
        ];

        $breadcrumb = [
            'main_title' => $main_title->title ?? 'WHATS NEW',
        ];

        return view('client.archive-product', compact('breadcrumb', 'list_product', 'list_product_cat', 'list_attribute', 'params', 'sort_by_params', 'seo_setting'));
    }

    function saleProductView(Request $request)
    {
        $params = [
            'page'    => 1,
            'limit'   => 12,
            'is_sale' => 1,
            'status'  => 1
        ];
        $query  = $request->only('page', 'limit', 'cat_id', 'attr_ids', 'price', 'is_sale', 'in_stock', 'keyword', 'sort_by');

        $params = array_merge($params, $query);

        $list_product     = null;
        $list_product_cat = null;
        $list_attribute   = null;
        $seo_setting      = null;

        $attr_ids_string = null;
        if (isset($params['attr_ids'])) {
            $attr_ids_string    = $params['attr_ids'];
            $params['attr_ids'] = explode(',', $attr_ids_string);
        } else {
            $params['attr_ids'] = [];
        }

        if (isset($params['sort_by'])) {
            $sort_by                   = explode('__', $params['sort_by'], 2);
            $params['order_by']        = $sort_by[0];
            $params['order_direction'] = $sort_by[1];
        }

        $list_product = $this->productRepository->filterProduct($params);

        unset($params['is_sale']);
        unset($params['limit']);
        unset($params['status']);
        if (isset($params['sort_by'])) {
            unset($params['order_by']);
            unset($params['order_direction']);
        }

        $sort_by_params = [
            'page'     => 1,
            //                'limit'    => $params['limit'],
            'attr_ids' => $attr_ids_string,
            //                'keyword'  => $params['keyword'] ?? null,
        ];

        $list_product_cat = $this->categoryRepository->getAllProductCat();

        $list_attribute = $this->attributeRepository->getList(['parent_id' => 0, 'status' => 1, 'order_by' => 'menu_order', 'order_direction' => 'asc']);

        $home_best_sale_product = $this->settingRepository->findByMetaKey('home_best_sale_product');
        $main_title = json_decode($home_best_sale_product->meta_value);

        $seo_setting = [
            'seo_title'       => $main_title->title ?? 'SALE PRODUCTS',
            'seo_description' => '',
            'seo_keyword'     => '',
            'seo_image'       => '',
        ];

        $breadcrumb = [
            'main_title' => $main_title->title ?? 'SALE PRODUCTS',
        ];

        return view('client.archive-product', compact('breadcrumb', 'list_product', 'list_product_cat', 'list_attribute', 'params', 'sort_by_params', 'seo_setting'));
    }

    function bestSellerProductView(Request $request)
    {
        $params = [
            'page'           => 1,
            'limit'          => 12,
            'is_best_seller' => 1,
            'status'         => 1
        ];
        $query  = $request->only('page', 'limit', 'cat_id', 'attr_ids', 'price', 'is_sale', 'in_stock', 'keyword', 'sort_by');

        $params = array_merge($params, $query);

        $list_product     = null;
        $list_product_cat = null;
        $list_attribute   = null;
        $seo_setting      = null;

        $attr_ids_string = null;
        if (isset($params['attr_ids'])) {
            $attr_ids_string    = $params['attr_ids'];
            $params['attr_ids'] = explode(',', $attr_ids_string);
        } else {
            $params['attr_ids'] = [];
        }

        if (isset($params['sort_by'])) {
            $sort_by                   = explode('__', $params['sort_by'], 2);
            $params['order_by']        = $sort_by[0];
            $params['order_direction'] = $sort_by[1];
        }

        $list_product = $this->productRepository->filterProduct($params);

        unset($params['is_best_seller']);
        unset($params['limit']);
        unset($params['status']);
        if (isset($params['sort_by'])) {
            unset($params['order_by']);
            unset($params['order_direction']);
        }

        $sort_by_params = [
            'page'     => 1,
            //                'limit'    => $params['limit'],
            'attr_ids' => $attr_ids_string,
            //                'keyword'  => $params['keyword'] ?? null,
        ];

        $list_product_cat = $this->categoryRepository->getAllProductCat();

        $list_attribute = $this->attributeRepository->getList(['parent_id' => 0, 'status' => 1, 'order_by' => 'menu_order', 'order_direction' => 'asc']);

        $home_best_seller_product = $this->settingRepository->findByMetaKey('home_best_seller_product');
        $main_title = json_decode($home_best_seller_product->meta_value);

        $seo_setting = [
            'seo_title'       => $main_title->title ?? 'PRODUCTS',
            'seo_description' => '',
            'seo_keyword'     => '',
            'seo_image'       => '',
        ];

        $breadcrumb = [
            'main_title' => $main_title->title ?? 'PRODUCTS',
        ];

        return view('client.archive-product', compact('breadcrumb', 'list_product', 'list_product_cat', 'list_attribute', 'params', 'sort_by_params', 'seo_setting'));
    }
}
