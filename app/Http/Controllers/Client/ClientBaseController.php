<?php
/**
 * Created by PhpStorm.
 * User: zkenc
 * Date: 20/11/2018
 * Time: 3:21 CH
 */

namespace App\Http\Controllers\Client;


use App\Http\Controllers\BaseController;
use App\Models\BaseModel;
use App\Models\Campaign;
use App\Models\Category;
use App\Models\Menu;
use App\Repositories\AttributeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CouponRepository;
use App\Repositories\InstagramRepository;
use App\Repositories\MenuRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PageRepository;
use App\Repositories\PostRepository;
use App\Repositories\ProductAttrRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SettingRepository;
use Helper;
use Illuminate\Support\Facades\View;

class ClientBaseController extends BaseController
{
    protected $settingRepository;
    protected $menuRepository;
    protected $postRepository;
    protected $pageRepository;
    protected $productRepository;
    protected $productAttrRepository;
    protected $categoryRepository;
    protected $orderRepository;
    protected $attributeRepository;
    protected $couponRepository;
    protected $instagramRepository;
    protected $slug;

    public function __construct(SettingRepository $settingRepository, MenuRepository $menuRepository,
        PostRepository $postRepository, PageRepository $pageRepository, ProductRepository $productRepository,
        ProductAttrRepository $productAttrRepository, CategoryRepository $categoryRepository,
        OrderRepository $orderRepository, AttributeRepository $attributeRepository,
        CouponRepository $couponRepository, InstagramRepository $instagramRepository)
    {
        parent::__construct($settingRepository);
        $this->settingRepository     = $settingRepository;
        $this->menuRepository        = $menuRepository;
        $this->postRepository        = $postRepository;
        $this->pageRepository        = $pageRepository;
        $this->productRepository     = $productRepository;
        $this->productAttrRepository = $productAttrRepository;
        $this->categoryRepository    = $categoryRepository;
        $this->orderRepository       = $orderRepository;
        $this->attributeRepository   = $attributeRepository;
        $this->couponRepository      = $couponRepository;
        $this->instagramRepository   = $instagramRepository;

        //         <editor-fold title="Main Menu">
        /** @var Menu $main_menu_object */
        $main_menu_object = $this->menuRepository->getByLocation('main_menu');
        $main_menu        = null;
        if ($main_menu_object) {
            $main_menu = $main_menu_object->getMenuItem()->orderBy('sort', 'asc')->get();
        }
                $test = null;
        if ($main_menu){
            $main_menu = $this->getMainMenuParentHtml($main_menu,0);
        }

        //         </editor-fold>
        //         <editor-fold title="Menu trái">
        /** @var Menu $menu_left_object */
        $menu_left_object = $this->menuRepository->getByLocation('vertical_menu');
        $menu_left        = null;
        if ($menu_left_object) {
            $menu_left = $menu_left_object->getMenuItem()->orderBy('sort', 'asc')->get();
        }
        if ($menu_left){
            $menu_left = $this->getMenuLeftParentHtml($menu_left,0);
        }
        //         </editor-fold>

        //         <editor-fold title="Footer Menu">
        /** @var Menu $shop_menu_object */
        $footer_menu_object = $this->menuRepository->getByLocation('footer_menu');

        $footer_menu = null;
        if ($footer_menu_object) {
            $footer_menu = $footer_menu_object->getMenuItem()->where('parent_id', '=', 0)->orderBy('sort', 'asc')->get();
        }
        //         </editor-fold>

        $list_post_footer = $this->postRepository->getList($params = [
            'page'    => 1,
            'limit'   => 5,
            'status'  => 1
        ]);

        //         <editor-fold title="CAMPAIGN ACTIVE">
        $active_campaign = Campaign::where('status', '=', BaseModel::STATUS_ACTIVE)
            ->where('type', '=', Campaign::CAMPAIGN)
            ->where('startDate', '<=', time())
            ->where('endDate', '>=', time())
            ->first();
        //         </editor-fold>

        $sidebar_footer      = $this->settingRepository->findByMetaKey('sidebar_footer');
        $sidebar_footer_data = $this->processSidebar($sidebar_footer);
        View::share('main_menu', $main_menu);
        View::share('menu_left', $menu_left);
        View::share('footer_menu', $footer_menu);
        View::share('sidebar_footer', $sidebar_footer_data);
        View::share('active_campaign', $active_campaign);
        View::share('list_post_footer', $list_post_footer);

    }

    /**
     * @param  $data
     * @param int $parent_id
     * @return array
     */

    protected function getMenuLeftParentHtml($data, $parent_id =0){
        $html="";
        foreach ($data as $key=>$val){
            if ($val->parent_id == $parent_id){
                unset($data[$key]);
                $_html = $this->getMenuLeftParentHtml($data,$val->id);
                if ($_html){
                    $html = $html.'
                                <li class="dropdown">
                                <a href="'.Helper::getMenuLink($val).'">'.$val->title.'</a>
                                    <ul>
                                        '.$_html.'
                                    </ul>
                                </li>
                                    ';
                }else{
                    $html = $html.'<li>
                                    <a href="'.Helper::getMenuLink($val).'">'.$val->title.'</a>
                                    </li>';
                }
            }
        }
        return $html;
    }
    protected function getMainMenuParentHtml($data, $parent_id =0){
        $html="";
        foreach ($data as $key=>$val){
            if ($val->parent_id == $parent_id){
                unset($data[$key]);
                $_html = $this->getMainMenuParentHtml($data,$val->id);
                if ($_html){
                    $html = $html.'<li class="ya-menu-custom level1"><a href="'.Helper::getMenuLink($val).'" class="item-link"><span
                                            class="have-title"><span class="menu-title">'.$val->title.'</span></span></a>
                                        <ul class="nav vertical-megamenu flytheme-menures">
                                        '.$_html.'
                                        </ul>
                                    </li>
                                    ';
                }else{
                    $html = $html.'<li class="ya-menu-custom level1"><a
                                            href="'.Helper::getMenuLink($val).'" class="item-link"><span
                                            class="have-title"><span class="menu-title">'.$val->title.'</span></span></a>
                                        </li>';
                }
            }
        }
        return $html;
    }
    function processSidebar($sidebar_setting)
    {
        $sidebar_setting_data = [];
        if ($sidebar_setting && $sidebar_setting['meta_value']) {
            $sidebar_setting_data = json_decode($sidebar_setting['meta_value'], true);
        }
        foreach ($sidebar_setting_data as $widget_key => $widget_data) {
            switch (Helper::getWidgetKey($widget_key)) {
                case 'widget__menu':
                    /** @var Menu $main_menu_object */
                    $widget_menu_object = $this->menuRepository->getByID($widget_data['menu_id']);
                    $widget_menu        = null;
                    if ($widget_menu_object) {
                        $widget_menu = $widget_menu_object->getMenuItem()->where('parent_id', '=', 0)->get();
                    }
                    $sidebar_setting_data[$widget_key]['widget_content'] = $widget_menu;
                    break;
                case 'widget__post':
                    $params = [
                        'page'   => 1,
                        'limit'  => $widget_data['post_limit'] ?? 5,
                        'status' => 1,
                    ];
                    switch ($widget_data['list_type']) {
                        case 'most_view':
                            $params['order_by']        = 'views';
                            $params['order_direction'] = 'desc';
                            break;
                        case 'lastest':
                            $params['order_by']        = 'created_at';
                            $params['order_direction'] = 'desc';
                            break;
                    }
                    $widget_list_post                                    = $this->postRepository->getList($params);
                    $sidebar_setting_data[$widget_key]['widget_content'] = $widget_list_post;
                    break;
            }
        }
        return $sidebar_setting_data;
    }
}
