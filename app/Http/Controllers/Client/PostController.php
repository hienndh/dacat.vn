<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 10/09/2018
 * Time: 11:32 SA
 */

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Models\Post;

class PostController extends ClientBaseController
{
    function postDetailView($slug)
    {
        $post_object = null;
        $seo_setting = null;
        if (!empty($slug)) {
            /** @var Post $post_object */
            $post_object = $this->postRepository->getBySlug($slug);

            if (!$post_object) {
                return redirect()->route('client_404')->withErrors('Không tìm thấy bài viết này. Vui lòng thử lại!');
            }

            $list_related_post = $post_object->relatedPostsByCat(10);

            $cat_title_list          = $post_object->category()->pluck('category.title')->all();
            $post_object['list_cat'] = implode(', ', $cat_title_list);

            $seo_setting = [
                'seo_title'       => $post_object['seo_title'] ?? $post_object['title'] ?? '',
                'seo_description' => $post_object['seo_description'] ?? $post_object['description'] ?? '',
                'seo_keyword'     => $post_object['seo_keyword'] ?? $post_object['keyword'] ?? '',
                'seo_image'       => $post_object['image'] ?? '',
            ];
        }

        $sidebar_post      = $this->settingRepository->findByMetaKey('sidebar_post');
        $sidebar_post_data = $this->processSidebar($sidebar_post);

        $breadcrumb = [
            'main_title' => $post_object['title'],
        ];

        return view('client.post-detail', compact('breadcrumb', 'post_object', 'list_related_post', 'seo_setting', 'sidebar_post_data'));
    }
}
