<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 10/09/2018
 * Time: 11:32 SA
 */

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Models\Coupon;
use App\Models\Location;
use App\Models\Order;
use App\Models\Setting;
use Gloudemans\Shoppingcart\Facades\Cart as Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CartController extends ClientBaseController
{
    function indexView()
    {
        return view('client.cart.index', compact(''));
    }

    function checkoutView()
    {
        $account = null;
        if (Auth::guard('account')->check()) {
            $account = Auth::guard('account')->user();
        }

        if (Cart::count() == 0) {
            return redirect()->route('client_home')->withErrors('Bạn không thể thanh toán giỏ hàng rỗng!');
        }

        $listCity = Location::where('type', 'city')->get();

        return view('client.cart.checkout', compact('account', 'listCity'));
    }

    function successView($identifier = null)
    {
        if (!$identifier) {
            return redirect()->route('client_home')->withErrors('Mã đơn hàng không tồn tại!');
        }
        $order_object = $this->orderRepository->checkOrder($identifier);

        return view('client.cart.success', compact('identifier', 'order_object'));
    }

    /** CART ACTION
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    function cartCheckout(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'order'           => 'required',
            'order.full_name' => 'required',
            'order.email'     => 'required|email',
            'order.phone'     => 'required|regex:/(0)[0-9]/',
            'order.address'   => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $account = null;
        if (Auth::guard('account')->check()) {
            $account = Auth::guard('account')->user();
        }

        //         <editor-fold title="Xử lý cập nhật kho">
        try {
            $this->productAttrRepository->updateRepoFromOrder('create', Cart::content());
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
        //         </editor-fold>

        //         <editor-fold title="Xử lý thông tin đơn hàng">
        $order_data               = $request->get('order');
        $order_data['quantity']   = Cart::count();
        $order_data['total']      = doubleval(Cart::total(0, '', ''));
        $order_data['total_real'] = doubleval(Cart::subtotal(0, '', ''));
        $order_data['status']     = BaseModel::ORDER_PENDING;
        $order_data['created_at'] = time();
        $order_data['updated_at'] = time();
        $order_data['account_id'] = $account ? $account->id : null;

        try {
            $order = $this->orderRepository->createOrder($order_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput($order_data);
        }
        if (!$order) {
            return redirect()->back()->withErrors('Tạo không thành công. Vui lòng thử lại!');
        }
        //         </editor-fold>

        $order      = Order::find($order['id']);
        $identifier = $order->identifier();

        //         <editor-fold title="Gửi email">
        $web_setting = $this->web_setting;
        try {
            Mail::send('client.email-template.order', compact('order_data', 'order', 'identifier', 'account'), function ($message) use ($account, $web_setting) {
                $message->from($web_setting['ta_company_email'], $web_setting['ta_company_name']);
                $message->to($account->email, $account->full_name)->subject("Đơn hàng từ " . $web_setting['ta_company_name']);
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        //         </editor-fold>

        Cart::store($order['id']);
        Cart::destroy();

        return redirect()->route('client_cart_success_view', ['identifier' => $identifier])->with(BaseModel::ALERT_SUCCESS, 'Đặt hàng thành công!');
    }

    function cartAddCouponAjax(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'coupon_code' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $cart_total = doubleval(Cart::total(0, '', ''));

        $res_data = [
            'cart_total' => \Helper::getFormattedNumber($cart_total),
        ];

        //CHECK COUPON CODE
        $coupon_code = trim($request->input('coupon_code'));
        try {
            $coupon = $this->couponRepository->checkCoupon($coupon_code);
        } catch (\Exception $e) {
            return $this->resFail($res_data, $e->getMessage(), 200);
        }
        if (!$coupon) {
            return $this->resFail($res_data, 'Có lỗi xảy ra khi sử dụng coupon');
        }

        if ($coupon->value_type == Coupon::CURRENCY) {
            $cart_total = ($cart_total - $coupon->value) > 0 ? ($cart_total - $coupon->value) : 0;
        } else {
            $cart_total = ($cart_total - $cart_total * $coupon->value / 100) ?: 0;
        }

        $res_data = [
            'cart_total' => \Helper::getFormattedNumber($cart_total),
            'coupon'     => $coupon,
        ];

        return $this->resSuccess($res_data, 'Thêm mã Coupon thành công!');
    }

    function cartAddProduct(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'product_id' => 'required',
            //            'product_size' => 'required',
            //            'product_color' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $product_id       = $request->get('product_id');
        $product_size_id  = $request->has('product_size') ? $request->get('product_size') : null;
        $product_color_id = $request->has('product_color') ? $request->get('product_color') : null;
        $product_size     = $this->attributeRepository->getByID($product_size_id);
        $product_color    = $this->attributeRepository->getByID($product_color_id);

        $product_quantity = $request->get('product_quantity') ?? 1;

        if (!$product_id) {
            return redirect()->back()->withErrors(BaseModel::ALERT_FAIL, 'Mã sản phẩm không tồn tại! Vui lòng thử lại!');
        }

        $product_object = $this->productRepository->getByID($product_id);

        if (!$product_object) {
            return redirect()->back()->withErrors(BaseModel::ALERT_FAIL, 'Không tìm thấy sản phẩm này. Vui lòng thử lại!');
        }

        $cart_item_option = [
            'size_id'  => $product_size ? $product_size->id : null,
            'size'     => $product_size ? $product_size->title : null,
            'color_id' => $product_color ? $product_color->id : null,
            'color'    => $product_color ? $product_color->title : null,
        ];

        Cart::add($product_object, $product_quantity, $cart_item_option);

        return redirect()->route('client_product_detail_view', ['slug' => $product_object->slug])->with(BaseModel::ALERT_SUCCESS, 'Thêm giỏ hàng thành công!');
    }

    /** CART AJAX */
    function cartQuickviewProductAjax(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'product_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $product_id = $request->get('product_id');

        if (!$product_id) {
            return $this->resFail(null, 'Mã sản phẩm không tồn tại! Vui lòng thử lại!');
        }

        $product_object = $this->productRepository->getByID($product_id);

        if (!$product_object) {
            return $this->resFail(null, 'Không tìm thấy sản phẩm này. Vui lòng thử lại!');
        }

        $quickview_product_html = view('client.template.product-quickview')->with('product_object', $product_object)->render();

        $return_data = [
            'quickview_product_html' => $quickview_product_html,
        ];

        return $this->resSuccess($return_data, '');
    }

    function cartAddProductAjax(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'product_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $product_id = $request->get('product_id');

        $product_quantity = $request->get('product_quantity') ?? 1;

        if (!$product_id) {
            return $this->resFail(null, 'Mã sản phẩm không tồn tại! Vui lòng thử lại!');
        }

        $product_object = $this->productRepository->getByID($product_id);

        if (!$product_object) {
            return $this->resFail(null, 'Không tìm thấy sản phẩm này. Vui lòng thử lại!');
        }

        $cart_item = Cart::add($product_object, $product_quantity);

        $compiled_cart_item = view('client.template.sidebar-cart-item')->with('cart_item', $cart_item)->render();

        $cart_data = [
            'cart_item'      => $cart_item,
            'added_product'  => $product_object,
            'cart_item_html' => $compiled_cart_item,
            'cart_subtotal'  => Cart::subtotal(),
            'cart_tax'       => Cart::tax(),
            'cart_total'     => Cart::total(),
            'cart_count'     => Cart::count()
        ];

        return $this->resSuccess($cart_data, 'Thêm giỏ hàng thành công!');
    }

    function cartDeleteProductAjax(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'product_row_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $product_row_id = $request->get('product_row_id');

        if (!$product_row_id) {
            return $this->resFail(null, 'Mã sản phẩm không tồn tại! Vui lòng thử lại!');
        }

        Cart::remove($product_row_id);

        $cart_data = [
            'cart_subtotal' => Cart::subtotal(),
            'cart_tax'      => Cart::tax(),
            'cart_total'    => Cart::total(),
            'cart_count'    => Cart::count()
        ];

        return $this->resSuccess($cart_data, 'Xoá sảm phẩm khỏi giỏ hàng thành công!');
    }

    function cartUpdateProductAjax(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'product_id'       => 'required',
            'product_row_id'   => 'required',
            'product_quantity' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $product_id       = $request->get('product_id');
        $product_row_id   = $request->get('product_row_id');
        $product_quantity = $request->get('product_quantity');

        $product_object = $this->productRepository->getByID($product_id);

        if (!$product_object) {
            return $this->resFail(null, 'Không tìm thấy sản phẩm này. Vui lòng thử lại!');
        }

        Cart::update($product_row_id, $product_quantity);

        $cart_data = [
            'cart_subtotal' => Cart::subtotal(),
            'cart_tax'      => Cart::tax(),
            'cart_total'    => Cart::total(),
            'cart_count'    => Cart::count()
        ];

        return $this->resSuccess($cart_data, 'Cập nhật giỏ hàng thành công!');
    }
}
