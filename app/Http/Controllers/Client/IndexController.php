<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 10/09/2018
 * Time: 11:32 SA
 */

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Models\Menu;
use App\Repositories\InstagramRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class IndexController extends ClientBaseController

{
    function index()
    {

        //         <editor-fold title="Home Slider">
        $home_slider      = $this->settingRepository->findByMetaKey('home_slider');
        $home_slider_data = [];
        if ($home_slider && $home_slider['meta_value']) {
            $home_slider_data = json_decode($home_slider['meta_value'], true);
        }
        //         </editor-fold>

        //         <editor-fold title="Partner Slider">
        $partner_slider      = $this->settingRepository->findByMetaKey('partner_slider');
        $partner_slider_data = [];
        if ($partner_slider && $partner_slider['meta_value']) {
            $partner_slider_data = json_decode($partner_slider['meta_value'], true);
        }
        //         </editor-fold>

        //         <editor-fold title="New Product List">
        $list_new_product = $this->productRepository->filterProduct([
            'page'   => 1,
            'limit'  => 10,
            'is_new' => 1,
            'status' => 1
        ]);
        //         </editor-fold>

        //         <editor-fold title="Archive Product List">
        $list_archive_product = $this->productRepository->filterProduct($params = [
            'page'           => 1,
            'limit'          => 10,
            'status'         => 1
        ]);
        //         </editor-fold>

        //         <editor-fold title="Big Sale Product List">
        $list_best_sale_product = $this->productRepository->filterProduct($params = [
            'page'    => 1,
            'limit'   => 10,
            'is_sale' => 1,
            'status'  => 1
        ]);
        //         </editor-fold>

        //         <editor-fold title="San pham ban chạy">
        $list_best_hot_product = $this->productRepository->filterProduct($params = [
            'page'    => 1,
            'limit'   => 5,
            'is_hot' => 1,
            'status'  => 1
        ]);
        //         </editor-fold>

        //         <editor-fold title="List Image Instagram">
        if (config('app.env') == 'production') {
            $list_image_instagram = $this->instagramRepository->getListImage();
        }
        //$list_image_instagram = null;
        //         </editor-fold>
        $coupon = Session::get('coupon', null);

        if ($coupon) Session::forget('coupon');

        return view('client.homepage', compact('list_new_product', 'list_archive_product', 'list_best_sale_product','list_best_hot_product', 'home_slider_data', 'partner_slider_data', 'coupon', 'list_image_instagram'));
    }

    function search(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'keyword' => 'required',
        ]);

        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator->errors());
        }

        $params = [
            'page'   => 1,
            'limit'  => 12,
            'status' => 1,
        ];
        $query  = $request->only('page', 'limit', 'keyword');

        $params = array_merge($params, $query);

        $list_product = $this->productRepository->filterProduct($params);

        $list_post = $this->postRepository->getList($params);

        $breadcrumb = [
            'main_title' => 'Kết quả tìm kiếm dành cho "' . $params['keyword'] . '"',
        ];

        return view('client.search', compact('breadcrumb', 'list_product', 'list_post', 'params'));
    }

    function error()
    {
        return view('client.404', compact(''));
    }
}
