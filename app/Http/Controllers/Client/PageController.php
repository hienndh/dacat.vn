<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 10/09/2018
 * Time: 11:32 SA
 */

namespace App\Http\Controllers\Client;

class PageController extends ClientBaseController
{
    function pageDetailView($slug)
    {
        $page_object = null;
        $seo_setting = null;
        if (!empty($slug)) {
            $page_object = $this->pageRepository->getBySlug($slug);

            if (!$page_object) {
                return redirect()->route('client_404')->withErrors('Không tìm thấy trang này. Vui lòng thử lại!');
            }

            $seo_setting = [
                'seo_title'       => $page_object['seo_title'] ?? $page_object['title'] ?? '',
                'seo_description' => $page_object['seo_description'] ?? $page_object['description'] ?? '',
                'seo_keyword'     => $page_object['seo_keyword'] ?? $page_object['keyword'] ?? '',
                'seo_image'       => $page_object['image'] ?? '',
            ];
        }

        $breadcrumb = [
            'main_title' => $page_object['title'],
        ];

        switch ($page_object['type']) {
            case 'landing_page':
                return view('client.page-template.page-landing', compact('breadcrumb', 'page_object', 'seo_setting'));
                break;
            default:
                return view('client.page-template.page-default', compact('breadcrumb', 'page_object', 'seo_setting'));
        }
    }
}
