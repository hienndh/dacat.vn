<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:15 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Models\Product;
use App\Repositories\AttributeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductAttrRepository;
use App\Repositories\ProductCatRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RepositoryController extends AdminBaseController
{
    protected $productRepository;
    protected $categoryRepository;
    protected $productCatRepository;
    protected $attributeRepository;
    protected $productAttrRepository;

    public function __construct(SettingRepository $settingRepository, ProductRepository $productRepository, CategoryRepository $categoryRepository, ProductCatRepository $productCatRepository, AttributeRepository $attributeRepository, ProductAttrRepository $productAttrRepository)
    {
        parent::__construct($settingRepository);
        $this->productRepository     = $productRepository;
        $this->categoryRepository    = $categoryRepository;
        $this->productCatRepository  = $productCatRepository;
        $this->attributeRepository   = $attributeRepository;
        $this->productAttrRepository = $productAttrRepository;
    }

    public function repositoryListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Kho',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page' => 1,
        ];
        $query  = $request->only('page', 'limit', 'keyword', 'status', 'cat_id');
        $params = array_merge($params, $query);

        $list_product = $this->productRepository->filterProduct($params);

        $list_cat = $this->categoryRepository->getList(['type' => BaseModel::PRODUCT_CAT, 'parent_id' => 0, 'order_by' => 'title', 'order_direction' => 'asc']);

        /** @var Product $product_item */
        foreach ($list_product as $product_item) {
            $cat_title_list           = $product_item->category()->pluck('category.title')->all();
            $product_item['list_cat'] = implode(', ', $cat_title_list);
        }

        return view('admin.repository.list', compact('breadcrumb', 'list_product', 'list_cat', 'params'));
    }

    public function repositoryFormView($product_id)
    {
        if (empty($product_id)) {
            return redirect()->route('admin_repository_list_view')->withErrors('Thiếu ID sản phẩm');
        }

        $breadcrumb = [
            'main_title'  => 'Quản lý kho sản phẩm',
            'return_back' => 'admin_repository_list_view'
        ];

        $list_attr = $this->attributeRepository->getList(['parent_id' => 0]);

        /** @var Product $product_object */
        $product_object = $this->productRepository->getByID($product_id);

        if (!$product_object) {
            return redirect()->route('admin_repository_list_view')->withErrors('Không tìm thấy sản phẩm này. Vui lòng thử lại!');
        }

        return view('admin.repository.form', compact('breadcrumb', 'product_object', 'list_attr'));
    }

    public function repositoryUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'repository.product_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $repository_data = $request->get('repository');

        try {
            /** @var Product $product_object */
            $product_object = $this->productRepository->getByID($repository_data['product_id']);

            if (!$product_object) {
                return redirect()->route('admin_repository_list_view')->withErrors('Không tìm thấy sản phẩm này. Vui lòng thử lại!');
            }

            foreach ($repository_data['repo'] as $child_attr_id => $repo_data) {
                $repo = $this->productAttrRepository->getRepository($product_object->id, $child_attr_id);
                if($repo) {
                    $this->productAttrRepository->updateProductAttr($repo->id, $repo_data);
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        return redirect()->route('admin_repository_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }
}
