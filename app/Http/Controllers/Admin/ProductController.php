<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:15 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\Attribute;
use App\Models\BaseModel;
use App\Models\Product;
use App\Repositories\AttributeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductAttrRepository;
use App\Repositories\ProductCatRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SettingRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends AdminBaseController
{
    protected $productRepository;
    protected $categoryRepository;
    protected $productCatRepository;
    protected $attributeRepository;
    protected $productAttrRepository;

    public function __construct(SettingRepository $settingRepository, ProductRepository $productRepository, CategoryRepository $categoryRepository, ProductCatRepository $productCatRepository, AttributeRepository $attributeRepository, ProductAttrRepository $productAttrRepository)
    {
        parent::__construct($settingRepository);
        $this->productRepository     = $productRepository;
        $this->categoryRepository    = $categoryRepository;
        $this->productCatRepository  = $productCatRepository;
        $this->attributeRepository   = $attributeRepository;
        $this->productAttrRepository = $productAttrRepository;
    }

    public function productListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Danh sách sản phẩm',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page' => 1,
        ];
        $query  = $request->only('page', 'limit', 'keyword', 'status', 'cat_id');
        $params = array_merge($params, $query);

        $list_product = $this->productRepository->filterProduct($params);

        $list_cat = $this->categoryRepository->getList(['type' => BaseModel::PRODUCT_CAT, 'parent_id' => 0, 'order_by' => 'title', 'order_direction' => 'asc']);

        /** @var Product $product_item */
        foreach ($list_product as $product_item) {
            $cat_title_list           = $product_item->category()->pluck('category.title')->all();
            $product_item['list_cat'] = implode(', ', $cat_title_list);
        }

        return view('admin.product.list', compact('breadcrumb', 'list_product', 'list_cat', 'params'));
    }

    public function productFormView($product_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($product_id) ? 'Thêm mới sản phẩm' : 'Cập nhật sản phẩm',
            'return_back' => 'admin_product_list_view'
        ];

        $params = [
            'type'      => BaseModel::PRODUCT_CAT,
            'parent_id' => 0
        ];

        $list_cat = $this->categoryRepository->getList($params);

        /** @var Product $product_object */
        $product_object = null;
        if (!empty($product_id)) {
            $product_object = $this->productRepository->getByID($product_id);

            if (!$product_object) {
                return redirect()->route('admin_product_list_view')->withErrors('Không tìm thấy sản phẩm này. Vui lòng thử lại!');
            }

            $cat_ids                   = $product_object->category()->pluck('category.id')->all();
            $product_object['cat_ids'] = $cat_ids;
        }

        return view('admin.product.form', compact('breadcrumb', 'product_object', 'list_cat'));
    }

    public function productCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'product.title' => 'required',
            'product.slug'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $product_data                  = $request->get('product');
        $product_data['slug']          = preg_replace('/\s+/', '-', trim($product_data['slug']));
        $product_data['regular_price'] = isset($product_data['regular_price']) ? $product_data['regular_price'] : 0;
        $product_data['sale_price']    = isset($product_data['sale_price']) ? $product_data['sale_price'] : 0;
        $product_data['sale_number']   = isset($product_data['sale_number']) ? $product_data['sale_number'] : 0;
        $product_data['sale_type']     = isset($product_data['sale_type']) ? $product_data['sale_type'] : 'percent';
        $product_data['status']        = isset($product_data['status']) && $product_data['status'] == 'on' ? 1 : 0;
        $product_data['author_id']     = Auth::id();
        $product_data['image']         = str_replace(url('/'), '', $product_data['image']);
        $product_data['created_at']    = time();
        $product_data['updated_at']    = time();

        $product_data['seo_title']       = isset($product_data['seo_title']) ? $product_data['seo_title'] : $product_data['title'];
        $product_data['seo_description'] = isset($product_data['seo_description']) ? $product_data['seo_description'] : substr(strip_tags($product_data['description']), 0, 170);

        if (!empty($product_data['album'])) {
            foreach ($product_data['album'] as &$album_item) {
                $album_item = str_replace(url('/'), '', $album_item);
            }

            $product_data['album'] = json_encode($product_data['album']);
        }

        try {
            $product = $this->productRepository->createProduct($product_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$product) {
            return redirect()->back()->withErrors('Tạo sản phẩm không thành công. Vui lòng thử lại!');
        }

        if (!empty($product_data['category'])) {
            $product->category()->attach($product_data['category']);
        }

        //Tạo ProductAttr
        $list_attr = $this->attributeRepository->getList(['parent_id' => 0]);
        if (!empty($list_attr)) {
            /** @var Attribute $parent_attr */
            foreach ($list_attr as $parent_attr) {
                foreach ($parent_attr->childAttribute as $child_attr) {
                    $product->attribute()->attach([$child_attr->id => ['parent_attr_id' => $parent_attr->id]]);
                }
            }
        }

        return redirect()->route('admin_product_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới sản phẩm thành công!');
    }

    public function productUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'product.id'    => 'required',
            'product.title' => 'required',
            'product.slug'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $product_data                  = $request->get('product');
        $product_data['slug']          = preg_replace('/\s+/', '-', trim($product_data['slug']));
        $product_data['regular_price'] = isset($product_data['regular_price']) ? $product_data['regular_price'] : 0;
        $product_data['sale_price']    = isset($product_data['sale_price']) ? $product_data['sale_price'] : 0;
        $product_data['sale_number']   = isset($product_data['sale_number']) ? $product_data['sale_number'] : 0;
        $product_data['sale_type']     = isset($product_data['sale_type']) ? $product_data['sale_type'] : 'percent';
        $product_data['status']        = isset($product_data['status']) && $product_data['status'] == 'on' ? 1 : 0;
        $product_data['author_id']     = Auth::id();
        $product_data['image']         = str_replace(url('/'), '', $product_data['image']);
        $product_data['updated_at']    = time();

        $product_data['seo_title']       = isset($product_data['seo_title']) ? $product_data['seo_title'] : $product_data['title'];
        $product_data['seo_description'] = isset($product_data['seo_description']) ? $product_data['seo_description'] : substr(strip_tags($product_data['description']), 0, 170);

        if (!empty($product_data['album'])) {
            foreach ($product_data['album'] as &$album_item) {
                $album_item = str_replace(url('/'), '', $album_item);
            }
            $product_data['album'] = json_encode($product_data['album']);
        }

        try {
            $product = $this->productRepository->updateProduct($product_data['id'], $product_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$product) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        /** TODO: Xử lý Product-Cat */
        if (empty($product_data['category'])) {
            $product_data['category'] = [];
        }
        $product->category()->sync($product_data['category']);

        /** TODO: Xử lý Product-Attr */
        /** @var Attribute $list_attr */
        $list_attr = $this->attributeRepository->getList(['parent_id' => 0]);
        if (!empty($list_attr)) {
            // Cần tách product data attribute ra để 2 mảng parent và child
            $list_child_attr_id = $this->attributeRepository->getList(['is_child' => 1])->pluck('id')->all();
            /** @var Collection $list_old_product_attr */
            $list_old_product_attr  = $this->productAttrRepository->getProductAttrByProductID($product_data['id']);
            $list_old_child_attr_id = $list_old_product_attr->pluck('child_attr_id')->all();

            $child_attr_id_to_delete = array_values(array_diff($list_old_child_attr_id, $list_child_attr_id));
            $child_attr_id_to_create = array_values(array_diff($list_child_attr_id, $list_old_child_attr_id));

            //Xoá ProductAttr cũ
            $product->attribute()->detach($child_attr_id_to_delete);
            // Tạo ProductAttr mới
            /** @var Attribute $parent_attr */
            foreach ($list_attr as $parent_attr) {
                foreach ($parent_attr->childAttribute as $child_attr) {
                    if (in_array($child_attr['id'], $child_attr_id_to_create)) {
                        $product->attribute()->attach([$child_attr['id'] => ['parent_attr_id' => $parent_attr['id']]]);
                    }
                }
            }
        }

        return redirect()->route('admin_product_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    public function productDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $product_data = $request->only('id');

        /** @var Product $product_obj */
        $product_obj = $this->productRepository->getByID($product_data['id']);

        if (!$product_obj) {
            return $this->resFail(null, 'Không tìm thấy sản phẩm cần xoá');
        }

        // <editor-fold title="Kiểm tra xem sản phẩm này có đang đc đặt hàng hay không?">
        $order_actived = $product_obj->order()->whereIn('status', [BaseModel::ORDER_PENDING, BaseModel::ORDER_PAID, BaseModel::ORDER_DELIVERED, BaseModel::ORDER_COMPLETED])->count();
        if ($order_actived > 0) {
            return $this->resFail(null, 'Không thể xoá sản phẩm này do có đơn hàng đang hoạt động');
        }
        // </editor-fold>

        //Xoá kết nối Product-Cat
        //        $product_cat_ids = $product_obj->category()->pluck('category.id')->all();
        //        $product_obj->category()->detach($product_cat_ids);
        //Xoá kết nối Product-Attr
        //        $product_attr_ids = $product_obj->attribute()->pluck('attribute.id')->all();
        //        $product_obj->attribute()->detach($product_attr_ids);

        try {
            $product_deleted = $this->productRepository->deleteProduct($product_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$product_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($product_deleted, 'Xoá thành công!');
    }
}
