<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:15 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Repositories\CategoryRepository;
use App\Repositories\MenuItemRepository;
use App\Repositories\MenuRepository;
use App\Repositories\PageRepository;
use App\Repositories\PostRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

class MenuController extends AdminBaseController
{
    protected $menuRepository;
    protected $menuItemRepository;
    protected $postRepository;
    protected $pageRepository;
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(SettingRepository $settingRepository, MenuRepository $menuRepository, MenuItemRepository $menuItemRepository, PostRepository $postRepository, PageRepository $pageRepository, ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        parent::__construct($settingRepository);
        $this->menuRepository     = $menuRepository;
        $this->menuItemRepository = $menuItemRepository;
        $this->postRepository     = $postRepository;
        $this->pageRepository     = $pageRepository;
        $this->productRepository  = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /** MENU VIEW */
    public function menuListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Menu điều hướng',
            'return_back' => 'admin_dashboard'
        ];

        $params = [];
        $query  = $request->only('keyword', 'location', 'status');
        $params = array_unique(array_merge($params, $query));

        $list_menu = $this->menuRepository->getList($params);

        $menu_location = BaseModel::MENU_LOCATION;

        return view('admin.menu.list', compact('breadcrumb', 'list_menu', 'menu_location', 'params'));
    }

    public function menuFormView($menu_id, $menu_item_id = null)
    {

        $menu_object    = null;
        $list_menu_item = [];
        if (!empty($menu_id)) {
            /** @var Menu $menu_object */
            $menu_object = $this->menuRepository->getByID($menu_id);

            if (!$menu_object) {
                return redirect()->route('admin_menu_list_view')->withErrors('Không tìm thấy menu này. Vui lòng thử lại!');
            }

            $list_menu_item = $this->menuItemRepository->getList(['parent_id' => 0, 'menu_id' => $menu_object['id']]);
        }

        $menu_item_object = null;
        if (!empty($menu_item_id)) {
            /** @var MenuItem $menu_item_object */
            $menu_item_object = $this->menuItemRepository->getByID($menu_item_id);

            if (!$menu_item_object) {
                return redirect()->route('admin_menu_update_view', ['menu_id' => $menu_id])->withErrors('Không tìm thấy menu con này. Vui lòng thử lại!');
            }

            if ($menu_item_object['menu_id'] != $menu_id) {
                return redirect()->route('admin_menu_update_view', ['menu_id' => $menu_id])
                    ->withErrors('Menu con này không thuộc Menu "' . $menu_object['name'] . '". Vui lòng thử lại!');
            }
        }

        $breadcrumb = [
            'main_title'  => 'Cài đặt menu: ' . $menu_object['name'],
            'return_back' => 'admin_menu_list_view'
        ];

        $list_post           = $this->postRepository->getAll();
        $list_page           = $this->pageRepository->getAll();
        $list_product        = $this->productRepository->getAll();
        $list_post_cat       = $this->categoryRepository->getAllPostCat();
        $list_product_cat    = $this->categoryRepository->getAllProductCat();
        $list_menu_item_type = BaseModel::MENU_ITEM_TYPE;
        $list_menu_location  = BaseModel::MENU_LOCATION;


        return view('admin.menu.form', compact('breadcrumb', 'menu_object', 'menu_item_object', 'list_menu_item', 'list_post', 'list_page', 'list_product', 'list_post_cat', 'list_product_cat', 'list_menu_item_type', 'list_menu_location'));
    }

    /** === MENU ACTION === */
    /** ACTION CREATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function menuCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'menu.name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $menu_data             = $request->get('menu');
        $menu_data['status']   = 1;
        $menu_data['location'] = isset($menu_data['location']) && $menu_data['location'] != '0' ? $menu_data['location'] : '';
        try {
            $menu = $this->menuRepository->createMenu($menu_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$menu) {
            return redirect()->back()->withErrors('Tạo mới không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_menu_update_view', ['menu_id' => $menu['id']])->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    /** ACTION UPDATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function menuUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'menu.id'   => 'required',
            'menu.name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $menu_data             = $request->get('menu');
        $menu_data['status']   = isset($menu_data['status']) && $menu_data['status'] == 'on' ? 1 : 0;
        $menu_data['location'] = isset($menu_data['location']) && $menu_data['location'] != '0' ? $menu_data['location'] : '';
        try {
            $menu = $this->menuRepository->updateMenu($menu_data['id'], $menu_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$menu) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_menu_update_view', ['menu_id' => $menu['id']])->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    /** ACTION DELETE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function menuDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $menu_data = $request->only('id');

        /** TODO: XOÁ TOÀN BỘ CÁC MENU ITEM CỦA MENU NÀY */
        /** @var Menu $old_menu */
        $old_menu = $this->menuRepository->getByID($menu_data);

        if (!$old_menu) {
            return $this->resFail(null, 'Không tìm thấy đối tượng này trong cơ sở dữ liệu!');
        }
        $this->menuItemRepository->deleteAllMenuItemByMenuId($old_menu['id']);

        /** TODO: XOÁ */
        try {
            $menu = $this->menuRepository->deleteMenu($menu_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$menu) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($menu, 'Xoá thành công!');
    }

    /** === MENU ITEM ACTION === */
    /** ACTION CREATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function menuItemCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'menu_item.menu_id'   => 'required',
            'menu_item.title'     => 'required',
            'menu_item.type'      => 'required',
            'menu_item.parent_id' => 'required',
            'menu_item.sort'      => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $menu_item_type_key = array_keys(BaseModel::MENU_ITEM_TYPE);

        $menu_item_data         = $request->get('menu_item');
        $menu_item_data['link'] = isset($menu_item_data['link']) ? $menu_item_data['link'] : '#';
        $menu_item_data['type'] = isset($menu_item_data['type']) && in_array($menu_item_data['type'], $menu_item_type_key) ? $menu_item_data['type'] : $menu_item_type_key[0];

        try {
            $menu_item = $this->menuItemRepository->createMenuItem($menu_item_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$menu_item) {
            return redirect()->back()->withErrors('Tạo mới không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_menu_update_view', ['menu_id' => $menu_item['menu_id']])->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    /** ACTION UPDATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function menuItemUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'menu_item.id'        => 'required',
            'menu_item.menu_id'   => 'required',
            'menu_item.title'     => 'required',
            'menu_item.type'      => 'required',
            'menu_item.parent_id' => 'required',
            'menu_item.sort'      => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $menu_item_type_key = array_keys(BaseModel::MENU_ITEM_TYPE);

        $menu_item_data              = $request->get('menu_item');
        $menu_item_data['link']      = isset($menu_item_data['link']) ? $menu_item_data['link'] : '#';
        $menu_item_data['type']      = isset($menu_item_data['type']) && in_array($menu_item_data['type'], $menu_item_type_key) ? $menu_item_data['type'] : $menu_item_type_key[0];
        $menu_item_data['parent_id'] = $menu_item_data['parent_id'] != $menu_item_data['id'] ? $menu_item_data['parent_id'] : 0;
        try {
            $menu_item = $this->menuItemRepository->updateMenuItem($menu_item_data['id'], $menu_item_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$menu_item) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_menu_update_view', ['menu_id' => $menu_item['menu_id']])->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    /** ACTION DELETE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function menuItemDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $menu_item_data = $request->only('id');

        /** TODO: THAY ĐỔI PARENT_ID CỦA NHỮNG MENU ITEM CON */
        /** @var MenuItem $old_menu_item */
        $old_menu_item = $this->menuItemRepository->getByID($menu_item_data);

        if (!$old_menu_item) {
            return $this->resFail(null, 'Không tìm thấy đối tượng này trong cơ sở dữ liệu!');
        }
        /** @var Collection $list_child_menu_item */
        $list_child_menu_item = $old_menu_item->childMenuItem;
        if (count($list_child_menu_item)) {
            /** @var MenuItem $child_menu_item */
            foreach ($list_child_menu_item as $child_menu_item) {
                $child_menu_item->update(['parent_id' => $old_menu_item['parent_id']]);
            }
        }

        /* XOÁ */
        try {
            $menu_item = $this->menuItemRepository->deleteMenuItem($menu_item_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$menu_item) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($menu_item, 'Xoá thành công!');
    }
}
