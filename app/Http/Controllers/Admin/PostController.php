<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:15 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Models\Post;
use App\Repositories\CategoryRepository;
use App\Repositories\PostCatRepository;
use App\Repositories\PostRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PostController extends AdminBaseController
{
    protected $postRepository;
    protected $categoryRepository;
    protected $postCatRepository;

    public function __construct(SettingRepository $settingRepository, PostRepository $postRepository, CategoryRepository $categoryRepository, PostCatRepository $postCatRepository)
    {
        parent::__construct($settingRepository);
        $this->postRepository     = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->postCatRepository  = $postCatRepository;
    }

    public function postListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Danh sách bài viết',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page' => 1,
        ];
        $query  = $request->only('page', 'limit', 'keyword', 'status');
        $params = array_merge($params, $query);

        $list_post = $this->postRepository->getList($params);

        /** @var Post $post_item */
        foreach ($list_post as $post_item) {
            $cat_title_list        = $post_item->category()->pluck('category.title')->all();
            $post_item['list_cat'] = implode(', ', $cat_title_list);
        }

        return view('admin.post.list', compact('breadcrumb', 'list_post', 'params'));
    }

    public function postFormView($post_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($post_id) ? 'Thêm mới bài viết' : 'Cập nhật bài viết',
            'return_back' => 'admin_post_list_view'
        ];

        $params = [
            'type'      => BaseModel::POST_CAT,
            'parent_id' => 0
        ];

        $list_cat = $this->categoryRepository->getList($params);

        $post_object = null;
        if (!empty($post_id)) {
            /** @var Post $post_object */
            $post_object = $this->postRepository->getByID($post_id);

            if (!$post_object) {
                return redirect()->route('admin_post_list_view')->withErrors('Không tìm thấy bài viết này. Vui lòng thử lại!');
            }

            $cat_ids                = $post_object->category()->pluck('category.id')->all();
            $post_object['cat_ids'] = $cat_ids;
        }

        return view('admin.post.form', compact('breadcrumb', 'post_object', 'list_cat'));
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'post.title' => 'required',
            'post.slug'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $post_data               = $request->get('post');
        $post_data['slug']       = preg_replace('/\s+/', '-', trim($post_data['slug']));
        $post_data['status']     = isset($post_data['status']) && $post_data['status'] == 'on' ? 1 : 0;
        $post_data['author_id']  = Auth::id();
        $post_data['image']      = str_replace(url('/'), '', $post_data['image']);
        $post_data['created_at'] = time();
        $post_data['updated_at'] = time();

        $post_data['seo_title']       = isset($post_data['seo_title']) ? $post_data['seo_title'] : $post_data['title'];
        $post_data['seo_description'] = isset($post_data['seo_description']) ? $post_data['seo_description'] : $post_data['description'];

        try {
            $post = $this->postRepository->createPost($post_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$post) {
            return redirect()->back()->withErrors('Tạo bài viết không thành công. Vui lòng thử lại!');
        }

        if (!empty($post_data['category'])) {
            $post->category()->attach($post_data['category']);
        }

        return redirect()->route('admin_post_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới bài viết thành công!');
    }

    public function postUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'post.id'    => 'required',
            'post.title' => 'required',
            'post.slug'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $post_data               = $request->get('post');
        $post_data['slug']       = preg_replace('/\s+/', '-', trim($post_data['slug']));
        $post_data['status']     = isset($post_data['status']) && $post_data['status'] == 'on' ? 1 : 0;
        $post_data['author_id']  = Auth::id();
        $post_data['image']      = str_replace(url('/'), '', $post_data['image']);
        $post_data['updated_at'] = time();

        $post_data['seo_title']       = isset($post_data['seo_title']) ? $post_data['seo_title'] : $post_data['title'];
        $post_data['seo_description'] = isset($post_data['seo_description']) ? $post_data['seo_description'] : $post_data['description'];
        try {
            $post = $this->postRepository->updatePost($post_data['id'], $post_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$post) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        /** Xử lý Post-Cat */
        if (empty($post_data['category'])) {
            $post_data['category'] = [];
        }
        $post->category()->sync($post_data['category']);

        return redirect()->route('admin_post_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    public function postDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $post_data = $request->only('id');

        /** @var Post $post_obj */
        $post_obj     = $this->postRepository->getByID($post_data['id']);
        $post_cat_ids = $post_obj->category()->pluck('category.id')->all();
        $post_obj->category()->detach($post_cat_ids);

        try {
            $post_deleted = $this->postRepository->deletePost($post_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$post_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($post_deleted, 'Xoá thành công!');
    }
}
