<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:20 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Repositories\CategoryRepository;
use App\Repositories\MenuItemRepository;
use App\Repositories\MenuRepository;
use App\Repositories\PageRepository;
use App\Repositories\PostRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SettingRepository;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingController extends AdminBaseController
{
    protected $settingRepository;
    protected $menuRepository;
    protected $menuItemRepository;
    protected $postRepository;
    protected $pageRepository;
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(SettingRepository $settingRepository, MenuRepository $menuRepository, MenuItemRepository $menuItemRepository, PostRepository $postRepository, PageRepository $pageRepository, ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        parent::__construct($settingRepository);
        $this->settingRepository  = $settingRepository;
        $this->menuRepository     = $menuRepository;
        $this->menuItemRepository = $menuItemRepository;
        $this->postRepository     = $postRepository;
        $this->pageRepository     = $pageRepository;
        $this->productRepository  = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function widgetView()
    {
        $breadcrumb = [
            'main_title'  => 'Widget',
            'return_back' => 'admin_dashboard'
        ];

        $list_sidebar      = BaseModel::WIDGET_SIDEBAR;
        $list_sidebar_data = [];
        foreach ($list_sidebar as $sidebar_key => $sidebar_name) {
            $sidebar_data = $this->settingRepository->findByMetaKey($sidebar_key);
            if ($sidebar_data) {
                $list_sidebar_data[$sidebar_key] = json_decode($sidebar_data['meta_value'], true);
            }
        }

        $list_widget = BaseModel::WIDGET_LIST;

        $widget_param = [];
        foreach ($list_widget as $widget_key => $widget_name) {
            switch (Helper::getWidgetKey($widget_key)) {
                case 'widget__menu':
                    $list_menu                 = $this->menuRepository->getList();
                    $widget_param[$widget_key] = $list_menu;
                    break;
                default:
                    $widget_param[$widget_key] = null;
            }
        }

        return view('admin.setting.widget', compact('breadcrumb', 'list_sidebar', 'list_sidebar_data', 'list_widget', 'widget_param'));
    }

    public function websiteInfoView()
    {
        $breadcrumb = [
            'main_title'  => 'Thông tin website',
            'return_back' => 'admin_dashboard'
        ];

        return view('admin.setting.website_info', compact('breadcrumb'));
    }

    public function homePageView() {
        $breadcrumb = [
            'main_title'  => 'Cài đặt trang chủ',
            'return_back' => 'admin_dashboard'
        ];

        return view('admin.setting.homepage', compact('breadcrumb'));
    }

    public function partnerSliderView()
    {
        $breadcrumb = [
            'main_title'  => 'Slider logo khách hàng',
            'return_back' => 'admin_dashboard'
        ];

        $slider_meta_key = 'partner_slider';

        $home_slider = $this->settingRepository->findByMetaKey($slider_meta_key);
        $slider_data = [];
        if ($home_slider && $home_slider['meta_value']) {
            $slider_data = json_decode($home_slider['meta_value'], true);
        }

        return view('admin.setting.image_slider', compact('breadcrumb', 'slider_data', 'slider_meta_key'));
    }

    public function homeSliderView()
    {
        $breadcrumb = [
            'main_title'  => 'Slider trang chủ',
            'return_back' => 'admin_dashboard'
        ];

        $slider_meta_key = 'home_slider';

        $home_slider = $this->settingRepository->findByMetaKey($slider_meta_key);
        $slider_data = [];
        if ($home_slider && $home_slider['meta_value']) {
            $slider_data = json_decode($home_slider['meta_value'], true);
        }

        return view('admin.setting.image_slider', compact('breadcrumb', 'slider_data', 'slider_meta_key'));
    }

    public function adsConfigView()
    {
        $breadcrumb = [
            'main_title'  => 'Cài đặt quảng cáo',
            'return_back' => 'admin_dashboard'
        ];

        return view('admin.setting.ads_config', compact('breadcrumb'));
    }

    public function customizerView()
    {
        $breadcrumb = [
            'main_title'  => 'Tuỳ chỉnh',
            'return_back' => 'admin_dashboard'
        ];

        return view('admin.setting.customizer', compact('breadcrumb'));
    }

    /** === SETTING ACTION === */
    /** ACTION SAVE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveSetting(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'setting' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $list_setting_data = $request->get('setting');

        $setting = null;
        try {
            foreach ($list_setting_data as $meta_key => $meta_value) {
                $setting_data['meta_key']   = $meta_key;
                $setting_data['meta_value'] = is_array($meta_value) ? json_encode($meta_value) : ($meta_value ? $meta_value : '');
                $setting_data['meta_value'] = str_replace(url('/'), '', $setting_data['meta_value']);
                $setting_data['meta_value'] = str_replace(trim(json_encode(url('/')), '"'), '', $setting_data['meta_value']);

                $old_setting = $this->settingRepository->findByMetaKey($meta_key);

                if (!$old_setting) {
                    $setting = $this->settingRepository->createSetting($setting_data);
                } else {
                    $setting = $this->settingRepository->updateSetting($old_setting['id'], $setting_data);
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$setting) {
            return redirect()->back()->withErrors('Lưu không thành công. Vui lòng thử lại!');
        }

        return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Lưu thành công!');
    }
}
