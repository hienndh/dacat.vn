<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Models\Role;
use App\Repositories\UserRepository;
use App\Repositories\SettingRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends AdminBaseController
{
    protected $userRepository;

    public function __construct(SettingRepository $settingRepository, UserRepository $userRepository)
    {
        parent::__construct($settingRepository);
        $this->userRepository = $userRepository;
    }

    public function userListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Danh sách tài khoản',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page' => 1,
        ];

        $query  = $request->only('page', 'limit', 'keyword', 'status');
        $params = array_merge($params, $query);

        $list_user = $this->userRepository->getList($params);

        /** @var User $user_item */
        foreach ($list_user as $user_item) {
            $role_title_list        = $user_item->role()->pluck('roles.display_name')->all();
            $user_item['list_role'] = implode(', ', $role_title_list);
        }

        return view('admin.user.list', compact('breadcrumb', 'list_user', 'params'));
    }

    public function userFormView($user_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($user_id) ? 'Thêm mới tài khoản' : 'Cập nhật tài khoản',
            'return_back' => 'admin_user_list_view'
        ];

        $list_role = Role::all();

        $user_object = null;
        if (!empty($user_id)) {
            /** @var User $user_object */
            $user_object = $this->userRepository->getByID($user_id);

            if (!$user_object) {
                return redirect()->route('admin_user_list_view')->withErrors('Không tìm thấy tài khoản này. Vui lòng thử lại!');
            }

            $role_ids                = $user_object->role()->pluck('roles.id')->all();
            $user_object['role_ids'] = $role_ids;
        }

        return view('admin.user.form', compact('breadcrumb', 'user_object', 'list_role'));
    }

    /** ACTION UPDATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus(Request $request)
    {
        $id   = $request->get('id');
        $user = $this->userRepository->getByID($id);

        if ($user->status == BaseModel::STATUS_ACTIVE) $user->status = BaseModel::STATUS_INACTIVE;
        else $user->status = BaseModel::STATUS_ACTIVE;

        $user->update();
        return $this->resSuccess($user, 'cập nhật thành công!');
    }

    public function userCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'user.email'      => 'required|email',
            'user.user_name'  => 'required|min:3|max:50',
            'user.password'   => 'required|min:6',
            'user.first_name' => 'required|min:3|max:255',
            'user.last_name'  => 'required|min:3|max:255',
            'user.phone'      => 'required|regex:/[0-9]{10,11}/',
            'user.address'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $user_data               = $request->get('user');
        $user_data['status']     = isset($user_data['status']) && $user_data['status'] == 'on' ? 1 : 0;
        $user_data['created_at'] = time();
        $user_data['updated_at'] = time();

        try {
            $user = $this->userRepository->createUser($user_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$user) {
            return redirect()->back()->withErrors('Tạo tài khoản không thành công. Vui lòng thử lại!');
        }

        if (!empty($user_data['role'])) {
            $user->role()->attach($user_data['role']);
        }

        return redirect()->route('admin_user_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới tài khoản thành công!');
    }

    public function userUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'user.email'      => 'required|email',
            'user.first_name' => 'required|min:3|max:255',
            'user.last_name'  => 'required|min:3|max:255',
            'user.phone'      => 'required|regex:/[0-9]{10,11}/',
            'user.address'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $user_data               = $request->get('user');
        $user_data['status']     = isset($user_data['status']) && $user_data['status'] == 'on' ? 1 : 0;
        $user_data['updated_at'] = time();
        if (isset($user_data['password']) && $user_data['password'] != '') {
            $user_data['password'] = Hash::make($user_data['password']);
        } else {
            unset($user_data['password']);
        }

        try {
            $user = $this->userRepository->updateUser($user_data['id'], $user_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$user) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        /** Xử lý Role-User */
        if (empty($user_data['role'])) {
            $user_data['role'] = [];
        }
        $user->role()->sync($user_data['role']);

        return redirect()->route('admin_user_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    public function userDelete(Request $request)
    {
        return $this->resFail(null, 'Chức năng này hiện đang khoá');
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $user_data = $request->only('id');

        try {
            $user_deleted = $this->userRepository->deleteUser($user_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$user_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($user_deleted, 'Xoá thành công!');
    }

}
