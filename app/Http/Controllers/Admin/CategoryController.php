<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:15 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\SettingRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends AdminBaseController
{
    protected $categoryRepository;

    public function __construct(SettingRepository $settingRepository, CategoryRepository $categoryRepository)
    {
        parent::__construct($settingRepository);
        $this->categoryRepository = $categoryRepository;
    }

    /** CHUYÊN MỤC BÀI VIẾT */
    public function postCategoryListView(Request $request)
    {
        $cat_type = BaseModel::POST_CAT;

        $breadcrumb = [
            'main_title'  => 'Chuyên mục bài viết',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'type'      => $cat_type,
            'parent_id' => 0,
        ];
        $query  = $request->only('parent_id', 'keyword', 'status');
        $params = array_unique(array_merge($params, $query));
        if (isset($params['keyword']) && $params['keyword'] != '') {
            unset($params['parent_id']);
        }
        $list_cat = $this->categoryRepository->getList($params);

        return view('admin.category.list', compact('breadcrumb', 'cat_type', 'list_cat', 'params'));
    }

    public function postCategoryFormView($cat_id = null)
    {
        $cat_type = BaseModel::POST_CAT;

        $breadcrumb = [
            'main_title'  => empty($cat_id) ? 'Thêm mới chuyên mục' : 'Cập nhật chuyên mục',
            'return_back' => 'admin_post_category_list_view'
        ];

        $params = [
            'type'      => $cat_type,
            'parent_id' => 0
        ];

        $list_cat = $this->categoryRepository->getList($params);

        $cat_object = null;
        if (!empty($cat_id)) {
            $cat_object = $this->categoryRepository->getByID($cat_id, $cat_type);

            if (!$cat_object) {
                return redirect()->route('admin_' . $cat_type . 'egory_list_view')->withErrors('Không tìm thấy chuyên mục này. Vui lòng thử lại!');
            }
        }

        return view('admin.category.form', compact('breadcrumb', 'cat_type', 'cat_object', 'list_cat'));
    }

    /** DANH MỤC SẢN PHẨM */
    public function productCategoryListView(Request $request)
    {
        $cat_type = BaseModel::PRODUCT_CAT;

        $breadcrumb = [
            'main_title'  => 'Danh mục sản phẩm',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'type'      => $cat_type,
            'parent_id' => 0,
        ];
        $query  = $request->only('parent_id', 'keyword', 'status');
        $params = array_unique(array_merge($params, $query));
        if (isset($params['keyword']) && $params['keyword'] != '') {
            unset($params['parent_id']);
        }
        $list_cat = $this->categoryRepository->getList($params);

        return view('admin.category.list', compact('breadcrumb', 'cat_type', 'list_cat', 'params'));
    }

    public function productCategoryFormView($cat_id = null)
    {
        $cat_type = BaseModel::PRODUCT_CAT;

        $breadcrumb = [
            'main_title'  => empty($cat_id) ? 'Thêm mới danh mục' : 'Cập nhật danh mục',
            'return_back' => 'admin_product_category_list_view'
        ];

        $params = [
            'type'      => $cat_type,
            'parent_id' => 0
        ];

        $list_cat = $this->categoryRepository->getList($params);

        $cat_object = null;
        if (!empty($cat_id)) {
            $cat_object = $this->categoryRepository->getByID($cat_id, $cat_type);

            if (!$cat_object) {
                return redirect()->route('admin_' . $cat_type . 'egory_list_view')->withErrors('Không tìm thấy danh mục này. Vui lòng thử lại!');
            }
        }

        return view('admin.category.form', compact('breadcrumb', 'cat_type', 'cat_object', 'list_cat'));
    }

    /** === ACTION === */
    /** ACTION CREATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function categoryCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'category.title' => 'required',
            'category.slug'  => 'required',
            'category.type'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $category_data               = $request->get('category');
        $category_data['slug']       = preg_replace('/\s+/', '-', trim($category_data['slug']));
        $category_data['status']     = isset($category_data['status']) && $category_data['status'] == 'on' ? 1 : 0;
        $category_data['author_id']  = Auth::id();
        $category_data['image']      = str_replace(url('/'), '', $category_data['image']);
        $category_data['banner']     = str_replace(url('/'), '', $category_data['banner']);
        $category_data['created_at'] = time();
        $category_data['updated_at'] = time();

        $category_data['seo_title']       = isset($category_data['seo_title']) ? $category_data['seo_title'] : $category_data['title'];
        $category_data['seo_description'] = isset($category_data['seo_description']) ? $category_data['seo_description'] : $category_data['description'];

        try {
            $category = $this->categoryRepository->createCategory($category_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$category) {
            return redirect()->back()->withErrors('Tạo mới không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_' . $category_data['type'] . 'egory_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    /** ACTION UPDATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function categoryUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'category.id'    => 'required',
            'category.title' => 'required',
            'category.slug'  => 'required',
            'category.type'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $category_data               = $request->get('category');
        $category_data['slug']       = preg_replace('/\s+/', '-', trim($category_data['slug']));
        $category_data['status']     = isset($category_data['status']) && $category_data['status'] == 'on' ? 1 : 0;
        $category_data['parent_id']  = $category_data['parent_id'] != $category_data['id'] ? $category_data['parent_id'] : 0;
        $category_data['author_id']  = Auth::id();
        $category_data['image']      = str_replace(url('/'), '', $category_data['image']);
        $category_data['banner']     = str_replace(url('/'), '', $category_data['banner']);
        $category_data['updated_at'] = time();

        $category_data['seo_title']       = isset($category_data['seo_title']) ? $category_data['seo_title'] : $category_data['title'];
        $category_data['seo_description'] = isset($category_data['seo_description']) ? $category_data['seo_description'] : $category_data['description'];

        try {
            $category = $this->categoryRepository->updateCategory($category_data['id'], $category_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$category) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_' . $category_data['type'] . 'egory_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    /** ACTION DELETE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function categoryDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id'   => 'required',
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $category_data = $request->only('id', 'type');

        /** @var Category $category_obj */
        $category_obj = $this->categoryRepository->getByID($category_data['id'], $category_data['type']);

        if (!$category_obj) {
            return $this->resFail(null, 'Không tìm thấy ' . ($category_data['type'] == BaseModel::POST_CAT ? 'chuyên mục' : 'danh mục') . ' cần xoá');
        }

        //Xoá kết nối Product-Cat, Post-Cat
        if ($category_data['type'] == BaseModel::POST_CAT) {
            $post_ids = $category_obj->posts()->pluck('posts.id')->all();
            $category_obj->posts()->detach($post_ids);
        } else {
            $product_ids = $category_obj->products()->pluck('products.id')->all();
            $category_obj->products()->detach($product_ids);
        }

        //Đổi parent của các child category
        /** @var Collection $list_child_category */
        $list_child_category = $category_obj->childCategory;
        if (count($list_child_category)) {
            /** @var Category $child_category */
            foreach ($list_child_category as $child_category) {
                $child_category->update(['parent_id' => $category_obj['parent_id']]);
            }
        }


        try {
            $category = $this->categoryRepository->deleteCategory($category_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$category) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($category, 'Xoá thành công!');
    }
}
