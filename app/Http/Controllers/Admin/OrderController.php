<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:19 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Models\Location;
use App\Repositories\OrderRepository;
use App\Repositories\ProductAttrRepository;
use App\Repositories\SettingRepository;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends AdminBaseController
{
    protected $orderRepository;
    protected $productAttrRepository;

    public function __construct(SettingRepository $settingRepository, OrderRepository $orderRepository, ProductAttrRepository $productAttrRepository)
    {
        parent::__construct($settingRepository);
        $this->orderRepository       = $orderRepository;
        $this->productAttrRepository = $productAttrRepository;
    }

    public function orderListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Danh sách đơn hàng',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page' => 1,
        ];
        $query  = $request->only('page', 'limit', 'keyword', 'status');
        $params = array_merge($params, $query);

        if (isset($params['status']) && $params['status'] == 'all') {
            unset($params['status']);
        }

        $list_order = $this->orderRepository->getList($params);

        return view('admin.order.list', compact('breadcrumb', 'list_order', 'params'));
    }

    public function orderFormView($order_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($order_id) ? 'Thêm mới đơn hàng' : 'Cập nhật đơn hàng',
            'return_back' => 'admin_order_list_view'
        ];

        $order_object = null;
        if (!empty($order_id)) {
            $order_object = $this->orderRepository->getByID($order_id);

            if (!$order_object) {
                return redirect()->route('admin_order_list_view')->withErrors('Không tìm thấy đơn hàng này. Vui lòng thử lại!');
            }

            $cart_content = unserialize(($order_object->shopping_cart)['content']);
        }

        $listCity = Location::where('type', 'city')->get();

        return view('admin.order.form', compact('breadcrumb', 'order_object', 'cart_content', 'listCity'));
    }

    public function orderCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'order.quantity'   => 'required',
            'order.total'      => 'required',
            'order.total_real' => 'required',
            'order.full_name'  => 'required',
            'order.email'      => 'required',
            'order.phone'      => 'required',
            'order.address'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $order_data               = $request->get('order');
        $order_data['quantity']   = Cart::count();
        $order_data['total']      = doubleval(Cart::total(0, '', ''));
        $order_data['total_real'] = doubleval(Cart::subtotal(0, '', ''));
        $order_data['status']     = $order_data['status'] ?? BaseModel::ORDER_PENDING;
        $order_data['created_at'] = time();
        $order_data['updated_at'] = time();

        try {
            $order = $this->orderRepository->createOrder($order_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$order) {
            return redirect()->back()->withErrors('Tạo đơn hàng không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_order_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới đơn hàng thành công!');
    }

    public function orderUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'order.id'        => 'required',
            'order.full_name' => 'required',
            'order.email'     => 'required',
            'order.phone'     => 'required',
            'order.address'   => 'required',
            'order.status'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $order_data               = $request->get('order');
        $order_data['updated_at'] = time();

        $old_order = $this->orderRepository->getByID($order_data['id']);
        if (!$old_order) {
            return redirect()->back()->withErrors('Không tìm thấy đơn hàng. Vui lòng thử lại!');
        }
        $status_to_update_repo = [BaseModel::ORDER_CANCELED, BaseModel::ORDER_REFUNDED];

        $cart_content = unserialize(($old_order->shopping_cart)['content']);
        //         <editor-fold title="Xử lý cập nhật kho">
        try {
            if (!in_array($old_order->status, $status_to_update_repo) && in_array($order_data['status'], $status_to_update_repo)) {
                $this->productAttrRepository->updateRepoFromOrder('cancel', $cart_content);
            } elseif (in_array($old_order->status, $status_to_update_repo) && !in_array($order_data['status'], $status_to_update_repo)) {
                $this->productAttrRepository->updateRepoFromOrder('create', $cart_content);
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
        //         </editor-fold>

        try {
            $order = $this->orderRepository->updateOrder($order_data['id'], $order_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$order) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_order_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    public function orderDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $order_data = $request->only('id');

        try {
            $order_deleted = $this->orderRepository->deleteOrder($order_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$order_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($order_deleted, 'Xoá thành công!');
    }
}
