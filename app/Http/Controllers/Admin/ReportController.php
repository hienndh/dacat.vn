<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ReportRepository;
use App\Repositories\SettingRepository;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;


class ReportController extends AdminBaseController
{
    protected $reportRepository;

    public function __construct(SettingRepository $settingRepository, ReportRepository $reportRepository)
    {
        parent::__construct($settingRepository);
        $this->reportRepository = $reportRepository;
    }

    public function reportStatusView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Báo cáo theo trạng thái',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page' => 1,
        ];
        $query  = $request->only('page', 'status', 'from', 'to');
        $params = array_merge($params, $query);

        if (isset($params['status']) && $params['status'] == 'all') {
            unset($params['status']);
        }

        $reports = $this->reportRepository->reportStatus($params);

        return view('admin.report.status', compact('breadcrumb', 'reports', 'params'));
    }

    public function reportProductView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Báo cáo theo sản phẩm',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page'   => 1,
            'status' => 'all'
        ];
        $query  = $request->only('page', 'status', 'from', 'to');
        $params = array_merge($params, $query);

        if (isset($params['status']) && $params['status'] == 'all') {
            unset($params['status']);
        }

        $reports = $this->reportRepository->reportProduct($params);

        return view('admin.report.product', compact('breadcrumb', 'reports', 'params'));
    }

    public function ExportPDF(Request $request)
    {
        $params = [
            'page'   => 1,
            'status' => 'all'
        ];
        $query  = $request->only('page', 'status', 'from', 'to');
        $params = array_merge($params, $query);

        if (isset($params['status']) && $params['status'] == 'all') {
            unset($params['status']);
        }

        if (empty($params['from']) || !isset($params['from'])) {
            $params['from'] = time() - 7 * 86400;
        }

        if (empty($params['to']) || !isset($params['to'])) {
            $params['to'] = time();
        }

        $reports = $this->reportRepository->reportProduct($params, 'all');

        $pdf = PDF::loadView('admin.report.export-template', compact('reports', 'params'))->setPaper('a4', 'landscape');
        return $pdf->download('report_' . date('Ymd', $params['from']) . '_' . date('Ymd', $params['to']) . '.pdf');
    }
}
