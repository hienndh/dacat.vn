<?php

namespace App\Http\Controllers\Admin;

use App\Models\Account;
use App\Models\BaseModel;
use App\Repositories\AccountRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AccountController extends AdminBaseController
{
    protected $accountRepository;

    public function __construct(SettingRepository $settingRepository, AccountRepository $accountRepository)
    {
        parent::__construct($settingRepository);
        $this->accountRepository = $accountRepository;
    }

    public function accountListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Danh sách tài khoản',
            'return_back' => 'admin_dashboard'
        ];

        $params = [
            'page' => 1,
        ];

        $query  = $request->only('page', 'limit', 'keyword', 'status');
        $params = array_merge($params, $query);

        $list_account = $this->accountRepository->getList($params);

        return view('admin.account.list', compact('breadcrumb', 'list_account', 'params'));
    }

    public function accountFormView($account_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($account_id) ? 'Thêm mới khách hàng' : 'Cập nhật khách hàng',
            'return_back' => 'admin_account_list_view'
        ];

        $account_object = null;
        if (!empty($account_id)) {
            /** @var Account $account_object */
            $account_object = $this->accountRepository->getByID($account_id);

            if (!$account_object) {
                return redirect()->route('admin_account_list_view')->withErrors('Không tìm thấy khách hàng này. Vui lòng thử lại!');
            }
        }

        return view('admin.account.form', compact('breadcrumb', 'account_object'));
    }

    /** ACTION UPDATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus(Request $request)
    {
        $id      = $request->get('id');
        $account = $this->accountRepository->getByID($id);

        if ($account->status == BaseModel::STATUS_ACTIVE) $account->status = BaseModel::STATUS_INACTIVE;
        else $account->status = BaseModel::STATUS_ACTIVE;

        $account->update();
        return $this->resSuccess($account, 'cập nhật thành công!');
    }

    public function accountCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'account.email'  => 'required',
            'account.user_name'  => 'required',
            'account.password'  => 'required',
            'account.full_name' => 'required',
            'account.phone'  => 'required',
            'account.address'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $account_data               = $request->get('account');
        $account_data['password']   = Hash::make($account_data['password']);
        $account_data['status']     = isset($account_data['status']) && $account_data['status'] == 'on' ? 1 : 0;
        $account_data['image']      = str_replace(url('/'), '', $account_data['image']);
        $account_data['created_at'] = time();
        $account_data['updated_at'] = time();

        try {
            $account = $this->accountRepository->createAccount($account_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$account) {
            return redirect()->back()->withErrors('Tạo khách hàng không thành công. Vui lòng thử lại!');
        }

        if (!empty($account_data['category'])) {
            $account->category()->attach($account_data['category']);
        }

        return redirect()->route('admin_account_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới khách hàng thành công!');
    }

    public function accountUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'account.email'  => 'required',
            'account.full_name' => 'required',
            'account.phone'  => 'required',
            'account.address'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $account_data               = $request->get('account');
        $account_data['status']     = isset($account_data['status']) && $account_data['status'] == 'on' ? 1 : 0;
        $account_data['image']      = str_replace(url('/'), '', $account_data['image']);
        $account_data['updated_at'] = time();

        try {
            $account = $this->accountRepository->updateAccount($account_data['id'], $account_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$account) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_account_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    public function accountDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $account_data = $request->only('id');

        try {
            $account_deleted = $this->accountRepository->deleteAccount($account_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$account_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($account_deleted, 'Xoá thành công!');
    }

}
