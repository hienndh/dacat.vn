<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:15 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Repositories\PageRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PageController extends AdminBaseController
{
    protected $pageRepository;

    public function __construct(SettingRepository $settingRepository, PageRepository $pageRepository)
    {
        parent::__construct($settingRepository);
        $this->pageRepository = $pageRepository;
    }

    public function pageListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Danh sách trang',
            'return_back' => 'admin_dashboard'
        ];

        $page_type = BaseModel::PAGE_TYPE;

        $params = [
            'page' => 1,
        ];
        $query  = $request->only('page', 'limit', 'keyword', 'status');
        $params = array_merge($params, $query);

        $list_page = $this->pageRepository->getList($params);
//        dd($query, $params, $list_page->toArray());

        return view('admin.page.list', compact('breadcrumb', 'list_page', 'params', 'page_type'));
    }

    public function pageFormView($page_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($page_id) ? 'Thêm mới trang' : 'Cập nhật trang',
            'return_back' => 'admin_page_list_view'
        ];

        $page_type = BaseModel::PAGE_TYPE;

        $page_object = null;
        if (!empty($page_id)) {
            $page_object = $this->pageRepository->getByID($page_id);

            if (!$page_object) {
                return redirect()->route('admin_page_list_view')->withErrors('Không tìm thấy trang này. Vui lòng thử lại!');
            }
        }

        return view('admin.page.form', compact('breadcrumb', 'page_object', 'page_type'));
    }

    public function pageCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'page.title' => 'required',
            'page.slug'  => 'required',
            'page.type'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $page_type_key = array_keys(BaseModel::PAGE_TYPE);

        $page_data               = $request->get('page');
        $page_data['slug']       = preg_replace('/\s+/', '-', trim($page_data['slug']));
        $page_data['status']     = isset($page_data['status']) && $page_data['status'] == 'on' ? 1 : 0;
        $page_data['type']       = isset($page_data['type']) && in_array($page_data['type'], $page_type_key) ? $page_data['type'] : $page_type_key[0];
        $page_data['author_id']  = Auth::id();
        $page_data['image']      = str_replace(url('/'), '', $page_data['image']);
        $page_data['created_at'] = time();
        $page_data['updated_at'] = time();

        $page_data['seo_title']       = isset($page_data['seo_title']) ? $page_data['seo_title'] : $page_data['title'];
        $page_data['seo_description'] = isset($page_data['seo_description']) ? $page_data['seo_description'] : $page_data['description'];

        try {
            $page = $this->pageRepository->createPage($page_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$page) {
            return redirect()->back()->withErrors('Tạo không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_page_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    public function pageUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'page.id'    => 'required',
            'page.title' => 'required',
            'page.slug'  => 'required',
            'page.type'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $page_type_key = array_keys(BaseModel::PAGE_TYPE);

        $page_data               = $request->get('page');
        $page_data['slug']       = preg_replace('/\s+/', '-', trim($page_data['slug']));
        $page_data['status']     = isset($page_data['status']) && $page_data['status'] == 'on' ? 1 : 0;
        $page_data['type']       = isset($page_data['type']) && in_array($page_data['type'], $page_type_key) ? $page_data['type'] : $page_type_key[0];
        $page_data['author_id']  = Auth::id();
        $page_data['image']      = str_replace(url('/'), '', $page_data['image']);
        $page_data['updated_at'] = time();

        $page_data['seo_title']       = isset($page_data['seo_title']) ? $page_data['seo_title'] : $page_data['title'];
        $page_data['seo_description'] = isset($page_data['seo_description']) ? $page_data['seo_description'] : $page_data['description'];

        try {
            $page = $this->pageRepository->updatePage($page_data['id'], $page_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$page) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_page_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    public function pageDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $page_data = $request->only('id');
        try {
            $page_deleted = $this->pageRepository->deletePage($page_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$page_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($page_deleted, 'Xoá thành công!');
    }
}
