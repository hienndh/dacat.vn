<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Repositories\CouponRepository;
use Illuminate\Http\Request;
use App\Repositories\SettingRepository;

class CouponController extends AdminBaseController
{
    protected $couponRepository;

    public function __construct(SettingRepository $settingRepository, CouponRepository $couponRepository)
    {
        parent::__construct($settingRepository);
        $this->couponRepository     = $couponRepository;
    }

    public function listCoupon(Request $request){
        $breadcrumb = [
            'main_title'  => 'Mã coupon giảm giá',
            'return_back' => 'admin_dashboard'
        ];
        $params = [];
        $query = $request->only('keyword','status','campaign_id');

        $params = array_merge($params, $query);

        $listCoupon = $this->couponRepository->getList($params);

        $listCampaign = $this->couponRepository->getListCampaign();

        return view('admin.coupon.list',compact('breadcrumb','listCoupon','params','listCampaign'));
    }

    public function couponCreate(Request $request){
        $req = $request->get('coupon');
        if($this->couponRepository->createMultipleCoupon($req)){
            return redirect()->route('admin_coupon_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
        }
        return redirect()->route('admin_coupon_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới không thành công!');
    }


    /** ACTION DELETE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function campaignDelete (Request $request)
    {

    }

}
