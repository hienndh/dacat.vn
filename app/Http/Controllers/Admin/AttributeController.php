<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:15 SA
 */

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Repositories\AttributeRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AttributeController extends AdminBaseController
{
    protected $attributeRepository;

    public function __construct(SettingRepository $settingRepository, AttributeRepository $attributeRepository)
    {
        parent::__construct($settingRepository);
        $this->attributeRepository = $attributeRepository;
    }

    /** DANH MỤC SẢN PHẨM
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function attributeListView(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Thuộc tính sản phẩm',
            'return_back' => 'admin_dashboard'
        ];

        $attr_type = BaseModel::ATTRIBUTE_TYPE;

        $params = [
            'parent_id' => 0,
            'order_by' => 'menu_order',
            'order_direction' => 'asc'
        ];

        $query  = $request->only('type', 'parent_id', 'keyword', 'status');
        $params = array_unique(array_merge($params, $query));

        $parent_attr_object = null;
        if (!empty($params['parent_id'])) {
            $parent_attr_object = $this->attributeRepository->getByID($params['parent_id']);

            if (!$parent_attr_object) {
                return redirect()->route('admin_attribute_list_view')->withErrors('Không tìm thấy thuộc tính cha này. Vui lòng thử lại!');
            }

            $breadcrumb = [
                'main_title'  => 'Thuộc tính con của: ' . $parent_attr_object['title'],
                'return_back' => 'admin_attribute_list_view'
            ];
        }

        $list_attr = $this->attributeRepository->getList($params);

        return view('admin.attribute.list', compact('breadcrumb', 'parent_attr_object', 'list_attr', 'params', 'attr_type'));
    }

    public function attributeFormView($attr_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($attr_id) ? 'Thêm mới thuộc tính' : 'Cập nhật thuộc tính',
            'return_back' => 'admin_attribute_list_view'
        ];

        $attr_type = BaseModel::ATTRIBUTE_TYPE;

        $params = [
            'parent_id' => 0
        ];

        $list_attr = $this->attributeRepository->getList($params);

        $attr_object = null;
        $query       = request()->only('parent_id');
        if (!empty($attr_id)) {
            $attr_object = $this->attributeRepository->getByID($attr_id);

            if (!$attr_object) {
                return redirect()->route('admin_attribute_list_view')->withErrors('Không tìm thấy thuộc tính này. Vui lòng thử lại!');
            }

            $parent_id = $attr_object['parent_id'];
        } else {
            $parent_id = isset($query['parent_id']) ? $query['parent_id'] : 0;
        }

        if ($parent_id) {
            $parent_attr_object = $this->attributeRepository->getByID($parent_id);

            if (!$parent_attr_object) {
                return redirect()->route('admin_attribute_list_view')->withErrors('Không tìm thấy thuộc tính cha này. Vui lòng thử lại!');
            }

            $breadcrumb = [
                'main_title'  => (empty($attr_id) ? 'Thêm mới thuộc tính' : 'Cập nhật thuộc tính') . ' con của: ' . $parent_attr_object['title'],
                'return_back' => 'admin_attribute_list_view',
                'query'       => $query
            ];
        }

        return view('admin.attribute.form', compact('breadcrumb', 'attr_object', 'parent_id', 'list_attr', 'attr_type'));
    }

    /** === ACTION === */
    /** ACTION CREATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attributeCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'attribute.title' => 'required',
            'attribute.slug'  => 'required',
            'attribute.type'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $attr_type_key = array_keys(BaseModel::ATTRIBUTE_TYPE);

        $attribute_data               = $request->get('attribute');
        $attribute_data['slug']       = preg_replace('/\s+/', '-', trim($attribute_data['slug']));
        $attribute_data['type']       = isset($attribute_data['type']) && in_array($attribute_data['type'], $attr_type_key) ? $attribute_data['type'] : $attr_type_key[0];
        $attribute_data['status']     = isset($attribute_data['status']) && $attribute_data['status'] == 'on' ? 1 : 0;
        $attribute_data['author_id']  = Auth::id();
        $attribute_data['image']      = str_replace(url('/'), '', $attribute_data['image']);
        $attribute_data['menu_order'] = $attribute_data['menu_order'] ?? 0;
        $attribute_data['created_at'] = time();
        $attribute_data['updated_at'] = time();
        try {
            $attribute = $this->attributeRepository->createAttribute($attribute_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$attribute) {
            return redirect()->back()->withErrors('Tạo mới không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_attribute_list_view', ['parent_id' => $attribute_data['parent_id']])->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    /** ACTION UPDATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attributeUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'attribute.id'    => 'required',
            'attribute.title' => 'required',
            'attribute.slug'  => 'required',
            'attribute.type'  => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $attr_type_key = array_keys(BaseModel::ATTRIBUTE_TYPE);

        $attribute_data               = $request->get('attribute');
        $attribute_data['slug']       = preg_replace('/\s+/', '-', trim($attribute_data['slug']));
        $attribute_data['type']       = isset($attribute_data['type']) && in_array($attribute_data['type'], $attr_type_key) ? $attribute_data['type'] : $attr_type_key[0];
        $attribute_data['status']     = isset($attribute_data['status']) && $attribute_data['status'] == 'on' ? 1 : 0;
        $attribute_data['parent_id']  = $attribute_data['parent_id'] != $attribute_data['id'] ? $attribute_data['parent_id'] : 0;
        $attribute_data['author_id']  = Auth::id();
        $attribute_data['image']      = str_replace(url('/'), '', $attribute_data['image']);
        $attribute_data['menu_order'] = $attribute_data['menu_order'] ?? 0;
        $attribute_data['updated_at'] = time();
        try {
            $attribute = $this->attributeRepository->updateAttribute($attribute_data['id'], $attribute_data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$attribute) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_attribute_list_view', ['parent_id' => $attribute_data['parent_id']])->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    /** ACTION DELETE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attributeDelete(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $attribute_data = $request->only('id');
        try {
            $attribute = $this->attributeRepository->deleteAttribute($attribute_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$attribute) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($attribute, 'Xoá thành công!');
    }
}
