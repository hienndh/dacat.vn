<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:12 SA
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class DashboardController extends AdminBaseController
{
    public function index()
    {
        $breadcrumb = [
            'main_title' => 'Bảng tin',
        ];
        return view('admin.dashboard.index', compact('breadcrumb'));
    }

    public function media()
    {
        $breadcrumb = [
            'main_title'  => 'Thư viện',
            'return_back' => 'admin_dashboard'
        ];
        return view('admin.media.index', compact('breadcrumb'));
    }
}
