<?php

namespace App\Http\Controllers\Admin;


use App\Models\BaseModel;
use App\Models\Campaign;
use App\Repositories\CampaignRepository;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CampaignController extends AdminBaseController
{
    protected $campaignRepository;

    public function __construct(SettingRepository $settingRepository, CampaignRepository $campaignRepository)
    {
        parent::__construct($settingRepository);
        $this->campaignRepository = $campaignRepository;
    }

    public function listCampaign(Request $request)
    {
        $breadcrumb = [
            'main_title'  => 'Chiến dịch giảm giá',
            'return_back' => 'admin_dashboard'
        ];
        $params     = [
            'startDate' => null,
            'endDate'   => null
        ];
        $query      = $request->only('keyword', 'status', 'startDate', 'endDate');

        $params       = array_merge($params, $query);
        $listCampaign = $this->campaignRepository->getList($params);

        return view('admin.campaign.list', compact('breadcrumb', 'listCampaign', 'params'));
    }

    public function campaignFromView($campaign_id = null)
    {
        $breadcrumb = [
            'main_title'  => empty($campaign_id) ? 'Thêm mới chiến dịch' : 'Cập nhật chiến dịch',
            'return_back' => 'admin_campaign_list_view'
        ];

        $campaign = null;
        if (!empty($campaign_id)) {
            /** @var Campaign $campaign */
            $campaign = $this->campaignRepository->getByID($campaign_id);

            if (!$campaign) {
                return redirect()->route('admin_campaign_list_view')->withErrors('Không tìm thấy chiến dịch này. Vui lòng thử lại!');
            }
        }
        return view('admin.campaign.form', compact('breadcrumb', 'campaign'));
    }

    public function campaignCreate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'campaign.name'  => 'required',
            'campaign.value' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $req                 = $request->get('campaign');
        $req['value']        = intval($req['value']);
        $req['value_type']   = intval($req['value_type']);
        $req['image']        = str_replace(url('/'), '', $req['image']);
        $req['startDate']    = intval($req['startDate']);
        $req['endDate']      = intval($req['endDate']);
        $req['user_created'] = Auth::user()->id;
        $req['status']       = isset($req['status']) && $req['status'] == 'on' ? 1 : 0;
        $req['created_at']   = time();

        try {
            $campaign = $this->campaignRepository->createCampaign($req);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$campaign) {
            return redirect()->back()->withErrors('Tạo chiến dịch không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_campaign_list_view')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới chiến dịch thành công!');
    }

    /** ACTION UPDATE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function campaignUpdate(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'campaign.id'    => 'required',
            'campaign.name'  => 'required',
            'campaign.value' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $campaign = $request->get('campaign');

        $campaign['value']      = intval($campaign['value']);
        $campaign['value_type'] = intval($campaign['value_type']);
        $campaign['image']      = str_replace(url('/'), '', $campaign['image']);
        $campaign['startDate']  = intval($campaign['startDate']);
        $campaign['endDate']    = intval($campaign['endDate']);
        $campaign['status']     = isset($campaign['status']) && $campaign['status'] == 'on' ? 1 : 0;
        $campaign['updated_at'] = time();

        try {
            $category = $this->campaignRepository->updateCampaign($campaign['id'], $campaign);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if (!$category) {
            return redirect()->back()->withErrors('Cập nhật không thành công. Vui lòng thử lại!');
        }

        return redirect()->route('admin_campaign_list_view')->with(BaseModel::ALERT_SUCCESS, 'Cập nhật thành công!');
    }

    /** ACTION DELETE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function campaignDelete(Request $request)
    {
        try {
            $campaign = $this->campaignRepository->deleteCampaign($request->get('id'));
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$campaign) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($campaign, 'Xoá thành công!');
    }
}
