<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 29/08/2018
 * Time: 11:12 SA
 */

namespace App\Http\Controllers\Admin;

use App\Models\Attribute;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use App\Models\Product;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Validator;

class ApiController extends AdminBaseController
{
    public function uploadImage(Request $request)
    {
        $img_path = $request->file('file')->store('', 'upload_folder');
        return Response()->json(['location' => url('/uploads/' . $img_path)]);
    }

    public function getSlug(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'title' => 'required',
            'type'  => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $data = $request->only('title', 'type');
        $slug = '';
        switch ($data['type']) {
            case 'page':
                $slug = SlugService::createSlug(Page::class, 'slug', $data['title']);
                break;
            case 'post':
                $slug = SlugService::createSlug(Post::class, 'slug', $data['title']);
                break;
            case 'category':
                $slug = SlugService::createSlug(Category::class, 'slug', $data['title']);
                break;
            case 'attribute':
                $slug = SlugService::createSlug(Attribute::class, 'slug', $data['title']);
                break;
            case 'product':
                $slug = SlugService::createSlug(Product::class, 'slug', $data['title']);
                break;
        }

        if ($slug) {
            return $this->resSuccess($slug);
        } else {
            return $this->resFail(null, 'Chuỗi đường dẫn tĩnh đang trống!');
        }
    }
}
