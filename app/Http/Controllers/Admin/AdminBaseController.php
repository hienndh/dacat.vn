<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Repositories\SettingRepository;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class AdminBaseController extends BaseController
{
    protected $_settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        parent::__construct($settingRepository);
        $this->_settingRepository = $settingRepository;

        $admin_sidebar = self::admin_sidebar();
        View::share('admin_sidebar', $admin_sidebar);

        $fm_key = env('FM_KEY', 'CuongDevFileManagerKey');
        View::share('FM_KEY', $fm_key);

        /* CURRENT ROUTE */
        $route = Route::current();
        View::share('admin_route_uri', trim($route->uri, 'ta-admin'));
    }

    public function admin_sidebar()
    {
        $admin_sidebar = [
            ['label' => 'QUẢN LÝ NỘI DUNG', 'href' => '', 'icon' => '', 'type' => 'caption', 'child' => []],
            ['label' => 'Bảng tin', 'href' => route('admin_dashboard'), 'icon' => 'mdi mdi-layers', 'type' => 'link', 'child' => []],
            ['label' => 'Sản phẩm', 'prefix' => 'products', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-dropbox', 'type' => 'parent', 'child' => [
                ['label' => 'Danh sách sản phẩm', 'href' => route('admin_product_list_view'), 'icon' => '', 'type' => 'link', 'child' => []],
                ['label' => 'Thêm sản phẩm mới', 'href' => route('admin_product_new_view'), 'icon' => '', 'type' => 'link', 'child' => []],
            ]],
            ['label' => 'Danh mục sản phẩm', 'href' => route('admin_product_category_list_view'), 'icon' => 'mdi mdi-folder-star', 'type' => 'link', 'child' => []],
            ['label' => 'Bài viết', 'prefix' => 'posts', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-newspaper', 'type' => 'parent', 'child' => [
                ['label' => 'Danh sách bài viết', 'href' => route('admin_post_list_view'), 'icon' => '', 'type' => 'link', 'child' => []],
                ['label' => 'Thêm bài viết mới', 'href' => route('admin_post_new_view'), 'icon' => '', 'type' => 'link', 'child' => []],
            ]],
            ['label' => 'Danh mục bài viết', 'href' => route('admin_post_category_list_view'), 'icon' => 'mdi mdi-folder', 'type' => 'link', 'child' => []],
            ['label' => 'Trang', 'href' => route('admin_page_list_view'), 'icon' => 'mdi mdi-file', 'type' => 'link', 'child' => []],
            ['label' => 'Thư viện', 'href' => route('admin_media'), 'icon' => 'mdi mdi-image-album', 'type' => 'link', 'child' => []],

            ['label' => 'Thuộc tính sản phẩm', 'href' => route('admin_attribute_list_view'), 'icon' => 'mdi mdi-folder-star', 'type' => 'link', 'child' => []],
            ['label' => 'Kho', 'href' => route('admin_repository_list_view'), 'icon' => 'mdi mdi-folder-star', 'type' => 'link', 'child' => []],
            ['label' => 'Đơn hàng', 'prefix' => 'orders', 'href' => route('admin_order_list_view'), 'icon' => 'mdi mdi-clipboard-text', 'type' => 'link', 'child' => []],

            ['label' => 'Chiến dịch', 'prefix' => 'campaign', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-newspaper', 'type' => 'parent', 'child' => [
                ['label' => 'Danh sách chiến dịch', 'href' => route('admin_campaign_list_view'), 'icon' => '', 'type' => 'link', 'child' => []],
                ['label' => 'Thêm bài chiến dịch', 'href' => route('admin_campaign_new_view'), 'icon' => '', 'type' => 'link', 'child' => []],
            ]],
            ['label' => 'Danh sách coupon', 'href' => route('admin_coupon_list_view'), 'icon' => 'mdi mdi-ticket-percent', 'type' => 'link', 'child' => []],
            ['label' => 'Khách hàng', 'href' => route('admin_account_list_view'), 'icon' => 'mdi mdi-account-box', 'type' => 'link', 'child' => []],

            ['label' => 'Báo cáo', 'prefix' => 'report', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-newspaper', 'type' => 'parent', 'child' => [
                ['label' => 'Thống kê sản phẩm đơn hàng', 'href' => route('admin_order_report_product_view'), 'icon' => '', 'type' => 'link', 'child' => []],
                //                ['label' => 'Thống kê trạng thái đơn hàng', 'href' => route('admin_order_report_status_view'), 'icon' => '', 'type' => 'link', 'child' => []],
            ]],

            //            ['label' => 'Bình luận', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-comment-multiple-outline', 'type' => 'link', 'child' => []],
            ['label' => 'Thành viên', 'prefix' => 'users', 'href' => route('admin_user_list_view'), 'icon' => 'mdi mdi-account-multiple', 'type' => 'link', 'child' => []],
            //            ['label' => 'Liên hệ', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-contact-mail', 'type' => 'link', 'child' => []],


            ['label' => 'Tuỳ chỉnh', 'href' => route('admin_setting_customizer_view'), 'icon' => 'mdi mdi-tune', 'type' => 'link', 'child' => []],
            ['label' => 'QUẢN LÝ HIỂN THỊ', 'href' => '', 'icon' => '', 'type' => 'caption', 'child' => []],
            ['label' => 'Giao diện', 'prefix' => 'ui', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-web', 'type' => 'parent', 'child' => [
                ['label' => 'Thông tin website', 'href' => route('admin_setting_website_info_view'), 'icon' => '', 'type' => 'link', 'child' => []],
                ['label' => 'Menu điều hướng', 'href' => route('admin_menu_list_view'), 'icon' => '', 'type' => 'link', 'child' => []],
                ['label' => 'Cài đặt trang chủ', 'href' => route('admin_setting_homepage_view'), 'icon' => '', 'type' => 'link', 'child' => []],
                //                ['label' => 'Slider logo khách hàng', 'href' => route('admin_setting_partner_slider_view'), 'icon' => '', 'type' => 'link', 'child' => []],
            ]],
            //            ['label' => 'Cài đặt', 'prefix' => 'cai-dat', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-settings', 'type' => 'parent', 'child' => [
            //                ['label' => 'Tổng quan', 'href' => 'javascript: 0;', 'icon' => '', 'type' => 'link', 'child' => []],
            //                ['label' => 'Đa ngôn ngữ', 'href' => 'javascript: 0;', 'icon' => '', 'type' => 'link', 'child' => []],
            //            ]],
            //            ['label' => 'QUẢN LÝ NÂNG CAO', 'href' => '', 'icon' => '', 'type' => 'caption', 'child' => []],
            //            ['label' => 'Phân quyền', 'prefix' => 'phan-quyen', 'href' => 'javascript: 0;', 'icon' => 'mdi mdi-folder-account', 'type' => 'link', 'child' => []]

        ];

        return $admin_sidebar;
    }
}
