<?php
/**
 * Created by PhpStorm.
 * User: Nguyen Vinh Cuong
 * Date: 22/08/2018
 * Time: 3:40 CH
 */

namespace App\Http\Controllers;


use App\Http\Controllers\Client\ClientBaseController;

class TestController extends ClientBaseController
{
    public function index() {
        $list_image_instagram = $this->instagramRepository->getListImage();
        dd($list_image_instagram);
    }
}
