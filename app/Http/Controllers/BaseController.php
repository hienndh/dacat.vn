<?php

namespace App\Http\Controllers;

use App\Repositories\SettingRepository;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    protected $_settingRepository;
    public $web_setting;

    public function __construct(SettingRepository $settingRepository)
    {

        $this->_settingRepository = $settingRepository;

        $setting_list = $this->_settingRepository->getAll();

        $keyed = $setting_list->mapWithKeys(function ($item) {
            return [$item->meta_key => (json_decode($item->meta_value, true) ?? $item->meta_value)];
        });

        $web_setting = $keyed->all();
        $this->web_setting = $web_setting;
        View::share('web_setting', $web_setting);
        View::share('admin_url', url('/ta-admin/'));
    }

    public function resFail($data = [], $msg = 'fail', $status = 405, $code = 0)
    {
        return Response()->json([
            'data' => $data,
            'msg'  => $msg,
            'code' => $code
        ], $status);
    }

    public function resSuccess($data, $msg = 'success', $status = 200, $code = 1)
    {
        return Response()->json([
            'data' => $data,
            'msg'  => $msg,
            'code' => $code
        ], $status);
    }

    /**
     * @param \Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function generalExceptionHandler($e = null)
    {
        return Response()->json([
            'Error'    => 'Invalid Request',
            'DevError' => $e ? $e->getMessage() : "",
            'Code'     => isset($e) && $e ? $e->getCode() : 0
        ], 400);
    }

    /**
     * @param \Exception $ex
     * @return \Illuminate\Http\JsonResponse
     */
    public function queryExceptionHandler($ex)
    {
        return Response()->json([
            'Error'    => "Data error",
            'DevError' => isset($ex) && $ex ? $ex->getMessage() : "",
            'Code'     => 4096
        ], 400);
    }
}
