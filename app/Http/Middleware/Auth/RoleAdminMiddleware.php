<?php

namespace App\Http\Middleware\Auth;

use Closure;
use Auth;

class RoleAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $role = Auth::user()->role->toArray();
            if (!$role || !in_array("admin", array_column($role, 'name'))) {
                return redirect()->route('client_home')->withErrors('Access denied!')->withInput();
            }
        } else {
            return redirect()->route('admin_auth_login_view')->withErrors('Hãy đăng nhập để tiếp tục!');
        }

        return $next($request);
    }
}
