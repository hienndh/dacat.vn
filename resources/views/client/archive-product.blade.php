@extends('client.master')

@section('client_main')
    <div id="shopify-section-static-collection" class="shopify-section section-collection">
        <div class="collection-content">
            <div class="collection-menu-overlay"></div>
            <header class="collection-header">
                <div class="collection-header-inner">
                    <h1 class="collection-title">{{$breadcrumb['main_title']}}</h1>
                </div>
            </header>

            <div class="collection-container">
                <div class="collection-filters-wrapper" style="z-index: 10000">
                    <div class="collection-filters-wrapper-inner">

                        <aside style="margin-top: -33px">
                            <div class="mega-left-title">
                                <strong>SẢN PHẨM</strong>
                            </div>
                            <div class="wrapper_vertical_menu" >
                                <ul>
                                    <?php echo $menu_left ?>
                                </ul>
                            </div>
                        </aside>

                    </div>
                    <div class="collection-filters-wrapper-inner">
                        <h4 class="btn__filter_form"><i class="fa fa-filter"></i> Bộ lọc</h4>

                        <form id="Product_Filter_Form" action="{{url()->current()}}" method="GET">
                            {{--LOẠI SẢN PHẨM--}}
                            @if(Route::currentRouteName() != 'client_product_cat_view')
                                <div class="product_cat_filter">
                                    <input type="hidden" id="cat_id" name="cat_id" value="{{isset($params['cat_id']) && $params['cat_id'] != '' ? $params['cat_id'] : null }}">
                                    <button class="collection-filters-title" type="button" data-collection-filters-item-trigger="product_cat_filter">
                                        <span class="collection-filters-title-text">Prodcut Category</span>
                                        <span class="collection-filters-item-arrow"></span>
                                    </button>
                                    <ul class="collection-filters-item filter-group {{isset($params['cat_id']) && $params['cat_id'] != '' ? 'active  filter-group-active' : null }}"
                                        data-collection-filters-item="product_cat_filter" data-collection-filters-tag-active="">
                                        @foreach($list_product_cat as $product_cat_item)
                                            <li class="filter-item {{isset($params['cat_id']) && $params['cat_id'] == $product_cat_item['id'] ? 'filter-item--active text-strikethrough' : 'filter-item--inactive'}}">
                                                <a href="javascript:void(0);"
                                                   title="{{$product_cat_item['title']}}" onclick="addParams('cat_id', {{$product_cat_item['id']}})">
                                                    <span class="filter-text">{{$product_cat_item['title']}}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            {{--THUỘC TÍNH SẢN PHẨM--}}
                            @if(count($list_attribute))
                                <div class="attribute_filter">
                                    <input type="hidden" id="attr_ids" name="attr_ids" value="{{isset($params['attr_ids']) && !empty($params['attr_ids']) ? implode(',', $params['attr_ids']) : null }}">
                                    @foreach($list_attribute as $parent_attribute)
                                        <button class="collection-filters-title" type="button" data-collection-filters-item-trigger="{{$parent_attribute['slug']}}_filter">
                                            <span class="collection-filters-title-text">Kích thước</span>
                                            <span class="collection-filters-item-arrow"></span>
                                        </button>
                                        <ul class="collection-filters-item filter-group {{isset($params['attr_ids']) && !empty($params['attr_ids']) ? 'active filter-group-active' : null }}"
                                            data-collection-filters-item="{{$parent_attribute['slug']}}_filter" data-collection-filters-tag-active="">
                                            @foreach($parent_attribute->childAttribute->sortBy('menu_order') as $child_attribute)
                                                <li class="filter-item {{isset($params['attr_ids']) && in_array($child_attribute['id'], $params['attr_ids']) ? 'filter-item--active text-strikethrough' : 'filter-item--inactive'}}">
                                                    <a href="javascript:void(0);"
                                                       title="{{$child_attribute['title']}}" onclick="addParams('attr_ids', {{$child_attribute['id']}})">
                                                        <span class="filter-text">{{$child_attribute['title']}}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endforeach
                                </div>
                            @endif

                            {{--STOCK--}}
                            <div class="stock_filter">
                                <input type="hidden" id="in_stock" name="in_stock" value="{{isset($params['in_stock']) && $params['in_stock'] != '' ? $params['in_stock'] : null }}">
                                <button class="collection-filters-title" type="button" data-collection-filters-item-trigger="stock_filter">
                                    <span class="collection-filters-title-text">Tình trạng hàng</span>
                                    <span class="collection-filters-item-arrow"></span>
                                </button>
                                <ul class="collection-filters-item filter-group {{isset($params['in_stock']) && $params['in_stock'] != '' ? 'active  filter-group-active' : null }}"
                                    data-collection-filters-item="stock_filter" data-collection-filters-tag-active="">
                                    <li class="filter-item {{isset($params['in_stock']) && $params['in_stock'] == \App\Models\BaseModel::STATUS_INACTIVE ? 'filter-item--active text-strikethrough' : 'filter-item--inactive'}}">
                                        <a href="javascript:void(0);"
                                           title="Out of stock" onclick="addParams('in_stock', 0)">
                                            <span class="filter-text">Hết hàng</span>
                                        </a>
                                    </li>

                                    <li class="filter-item {{isset($params['in_stock']) && $params['in_stock'] == \App\Models\BaseModel::STATUS_ACTIVE ? 'filter-item--active text-strikethrough' : 'filter-item--inactive'}}">
                                        <a href="javascript:void(0);"
                                           title="In stock" onclick="addParams('in_stock', 1)">
                                            <span class="filter-text">Còn hàng</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            {{--SALE OFF--}}
                            <div class="sale_off_filter">
                                <input type="hidden" id="is_sale" name="is_sale" value="{{isset($params['is_sale']) && $params['is_sale'] != '' ? $params['is_sale'] : null }}">
                                <button class="collection-filters-title" type="button" data-collection-filters-item-trigger="sale_off_filter">
                                    <span class="collection-filters-title-text">Giảm giá</span>
                                    <span class="collection-filters-item-arrow"></span>
                                </button>
                                <ul class="collection-filters-item filter-group {{isset($params['is_sale']) && $params['is_sale'] != '' ? 'active  filter-group-active' : null }}"
                                    data-collection-filters-item="sale_off_filter" data-collection-filters-tag-active="">
                                    <li class="filter-item {{isset($params['is_sale']) && $params['is_sale'] == \App\Models\BaseModel::STATUS_INACTIVE ? 'filter-item--active text-strikethrough' : 'filter-item--inactive'}}">
                                        <a href="javascript:void(0);"
                                           title="Sale Off" onclick="addParams('is_sale', 0)">
                                            <span class="filter-text">Không giảm giá</span>
                                        </a>
                                    </li>

                                    <li class="filter-item {{isset($params['is_sale']) && $params['is_sale'] == \App\Models\BaseModel::STATUS_ACTIVE ? 'filter-item--active text-strikethrough' : 'filter-item--inactive'}}">
                                        <a href="javascript:void(0);"
                                           title="Sale Off" onclick="addParams('is_sale', 1)">
                                            <span class="filter-text">Giảm giá</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            {{--PRICE--}}
                            @if(isset($web_setting['filter_price'] ))
                                <div class="price_filter">
                                    <input type="hidden" id="price" name="price" value="{{isset($params['price']) && $params['price'] != '' ? $params['price'] : null }}">
                                    <button class="collection-filters-title" type="button" data-collection-filters-item-trigger="price_filter">
                                        <span class="collection-filters-title-text">Giá</span>
                                        <span class="collection-filters-item-arrow"></span>
                                    </button>
                                    <ul class="collection-filters-item filter-group {{isset($params['price']) && $params['price'] != '' ? 'active  filter-group-active' : null }}"
                                        data-collection-filters-item="price_filter" data-collection-filters-tag-active="">
                                        @foreach($web_setting['filter_price'] as $index => $price_filter_item)
                                            <li class="filter-item {{isset($params['price']) && $params['price'] == $price_filter_item['value'] ? 'filter-item--active text-strikethrough' : 'filter-item--inactive'}}">
                                                <a href="javascript:void(0);" title="{{$price_filter_item['title']}}" onclick="addParams('price', '{{$price_filter_item['value']}}')">
                                                    <span class="filter-text">{{$price_filter_item['title']}}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="my-2">
                                <a href="{{url()->current()}}" class="btn btn-primary">Làm mới bộ lọc</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="collection-content--inner">
                    @if(count($list_product))
                        <div class="collection-grid collection-grid-uninit">
                            <div class="collection-page-group">
                                @foreach($list_product as $product_obj)
                                    <div class="collection-grid-item">
                                        @include('client.template.product-item', ['product_object' => $product_obj])
                                    </div>
                                @endforeach
                            </div>

                            <div class="pagination">
                                {{ $list_product->appends($params)->links('client.layout.pagination', ['list_object' => $list_product]) }}
                            </div>
                        </div>
                    @else
                        <p class="text-center" style="font-size: 20px;">Updating…</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('client_script')
    <script type="text/javascript">
        function addParams(id, value) {
            var filter_item = $('#Product_Filter_Form input[name="' + id + '"]');
            switch (id) {
                case 'cat_id':
                    if (filter_item.val() === value.toString()) {
                        value = null;
                    }
                    filter_item.val(value);
                    break;
                case 'attr_ids':
                    let attr_ids = (filter_item.val()).length > 0 ? (filter_item.val()).split(',') : [];
                    if (attr_ids.indexOf(value.toString()) > -1) {
                        attr_ids.splice(attr_ids.indexOf(value.toString()), 1);
                    } else {
                        attr_ids.push(value);
                    }
                    filter_item.val(attr_ids.join());
                    break;
                default:
                    if (filter_item.val() === value.toString()) {
                        value = null;
                    }
                    filter_item.val(value);
            }
            $('#Product_Filter_Form').submit();
        }

        $(document).ready(function () {

        });
    </script>
@endsection
