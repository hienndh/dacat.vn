@extends('client.master')

@section('client_main')
    <div class="container">
        <div id="Post_Cat_Content" class="col-12 col-md-12">
            {{--POST_LIST--}}
            <div class="row mt-4 cd-post-list">
                @if(count($list_post))
                    @foreach($list_post as $post_item)
                        <div class="col-12 col-md-4 col-lg-4">
                            @include('client.template.post-item', ['post_object' => $post_item, 'view_type' => 'grid_view'])
                        </div>
                    @endforeach

                    <div class="col-12">
                        {{ $list_post->appends($params)->links('client.layout.pagination', ['list_object' => $list_post]) }}
                    </div>
                @endif
            </div>
        </div>
{{--        <div id="Post_Sidebar" class="col-12 col-md-3">

        </div>--}}
    </div>
@endsection

