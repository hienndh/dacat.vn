@extends('client.master')

@section('client_main')
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9">
                <div class="cd-post-item">
                    <div class="pi-text box-text">
                        <div class="pi-meta">
                            <span class="pi-meta-author"><i class="fa fa-user" aria-hidden="true"></i> {{$post_object->author->first_name}}</span>
                            <span class="pi-meta-date"><i class="fa fa-calendar" aria-hidden="true"></i> {{$post_object->getFormattedDate('created_at', 'd/m/Y')}}</span>
                            <span class="pi-meta-category"><i class="fa fa-folder" aria-hidden="true"></i> {{$post_object['list_cat']}}</span>
                        </div>
                        <div class="pi-content">
                            {!! $post_object['content'] !!}
                        </div>
                        <div class="pi-share">
                            <div class="fb-like" data-href="{{url()->current()}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                        </div>
                    </div>
                </div>

                <div id="related_post">
                    <h4>RELATED POST</h4>
                    @if(count($list_related_post))
                        @foreach($list_related_post as $post_item)
                            @include('client.template.post-item', ['post_object' => $post_item, 'view_type' => 'title_view'])
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="row">
                    @if(count($sidebar_post_data))
                        @foreach($sidebar_post_data as $widget_key => $widget_data)
                            <div class="col-12">
                                @include('client.widget.' . Helper::getWidgetKey($widget_key), ['widget_key' => $widget_key,'widget_data' => $widget_data])
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('client_script')

@endsection


