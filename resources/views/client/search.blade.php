@extends('client.master')

@section('client_main')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('client.template.title', ['title' => 'PRODUCTS', 'align' => 'center'])
            </div>
            <div id="Product_Cat_Content" class="col-12 col-md-12">
                <div class="row mt-4 cd-product-list">
                    @if(count($list_product))
                        @foreach($list_product as $product_item)
                            <div class="col-12 col-md-4 col-xl-3">
                                @include('client.template.product-item', ['product_object' => $product_item, 'view_type' => 'grid_view'])
                            </div>
                        @endforeach
                        <div class="col-12">
                            {{ $list_product->appends($params)->links('client.layout.pagination', ['list_object' => $list_product]) }}
                        </div>
                    @else
                        <div class="col-12">
                            <p class="text-center">Not found!</p>
                        </div>
                    @endif
                </div>
            </div>

           {{--<div class="col-12">
                @include('client.template.title', ['title' => 'BÀI VIẾT', 'align' => 'center'])
            </div>
            <div id="Post_Cat_Content" class="col-12 col-md-12">
                <div class="row mt-4 cd-post-list">
                    @if(count($list_post))
                        @foreach($list_post as $post_item)
                            <div class="col-12 col-md-4 col-xl-4">
                                @include('client.template.post-item', ['post_object' => $post_item, 'view_type' => 'grid_view'])
                            </div>
                        @endforeach
                        <div class="col-12">
                            {{ $list_post->appends($params)->links('client.layout.pagination', ['list_object' => $list_post]) }}
                        </div>
                    @else
                        <div class="col-12">
                            <p class="text-center">Không tìm thấy bài viết nào!</p>
                        </div>
                    @endif
                </div>
            </div>--}}
            {{--<div id="Product_Sidebar" class="col-12 col-md-3">

            </div>--}}
        </div>

    </div>
@endsection

