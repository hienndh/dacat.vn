<form class="nav-search-form" action="{{route('client_search')}}">
    <input class="nav-search-input" aria-label="Searh product name..." type="text" placeholder="Searh product name..." name="keyword" value="">
    <button class="nav-search-button" type="submit" aria-label="Search">
        <span class="nav-search-icon">
            <svg class="" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path fill="currentColor" fill-rule="evenodd" d="M.732 6.225A5.498 5.498 0 0 1 6.225.733a5.498 5.498 0 0 1 5.493 5.492 5.498 5.498 0 0 1-5.493 5.493A5.498 5.498 0 0 1 .732 6.225m10.14 4.13a6.19 6.19 0 0 0 1.578-4.13A6.233 6.233 0 0 0 6.225 0 6.233 6.233 0 0 0 0 6.225a6.233 6.233 0 0 0 6.225 6.226 6.196 6.196 0 0 0 4.128-1.579l3.022 3.021a.37.37 0 0 0 .518 0 .366.366 0 0 0 0-.517l-3.022-3.021z"/></svg>
        </span>
    </button>
</form>
