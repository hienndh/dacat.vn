<article class="cart-item cart-item-{{$cart_item->rowId}}">
    <div class="cart-item-column cart-item-column-1">
        <figure class="cart-item-image">
            <a href="{{$cart_item->model->get_link('product', $cart_item->model->id, $cart_item->model->slug)}}">
                <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{Helper::checkImage($cart_item->model->image)}}" alt="" title="{{$cart_item->name}}">
            </a>
        </figure>
    </div>
    <div class="cart-item-column cart-item-column-2">
        <div class="cart-item-row cart-item-row-1">
            <span class="cart-item-title"><a href="{{$cart_item->model->get_link('product', $cart_item->model->id, $cart_item->model->slug)}}">{{$cart_item->name}}{{$cart_item->options->has('color') && $cart_item->options->color ? ' - ' . $cart_item->options->color : ''}}{{$cart_item->options->has('size') && $cart_item->options->size ? ' - ' . $cart_item->options->size : ''}}</a></span>
        </div>
        <div class="cart-item-row cart-item-row-2">
            <span class="cart-item-options">{{$cart_item->options->has('color') && $cart_item->options->color ? $cart_item->options->color . '<br>' : ''}}{{$cart_item->options->has('size') && $cart_item->options->size ? $cart_item->options->size : ''}}</span>
        </div>
        <div class="cart-item-row cart-item-row-3">
            <span class="cart-item-price money"><span class="money">{{Helper::getFormattedPrice($cart_item->price)}}</span></span>
        </div>
        <div class="cart-item-button cart-item-button-dismiss cart-item-row-4">
            <span class="cart-dismiss-text btn__remove_cart_item" data-product-row-id="{{$cart_item->rowId}}">Delete</span>
        </div>
        <div class="cart-item-row cart-item-row-5">
            <div class="quantity" data-quantity="">
                <button class="quantity-decrement" type="button" aria-label="Decrease">−</button>
                <input class="quantity-input input__product_quantity" data-product-id="{{$cart_item->id}}" data-product-row-id="{{$cart_item->rowId}}" type="number" min="1" step="1" value="{{$cart_item->qty}}">
                <button class="quantity-increment" type="button" aria-label="Increase">+</button>
            </div>
        </div>
    </div>
</article>
