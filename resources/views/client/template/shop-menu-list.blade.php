<li class="navmenu-item navmenu-id-all-products">
    <a class="navmenu-link" href="{{route('client_archive_product_view')}}">
        <span class="navmenu-link-title">{{isset($web_setting['home_archive_product']['title']) ? $web_setting['home_archive_product']['title'] : 'PRODUCTS'}}</span>
    </a>
</li>
@if($list_menu_item)
    @include('client.template.recursive-menu-item', ['list_menu_item' => $list_menu_item, 'level' => 0, 'recursive_type' => 'grid_view'])
@endif


