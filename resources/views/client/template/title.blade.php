<div class="container section-title-container">
    <h2 class="section-title section-title-{{$align ?? 'left'}}">
        <span class="section-title-main">{{$title ?? ''}}</span>
    </h2>
</div>
