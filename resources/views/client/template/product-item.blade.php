@php
    $view_type = isset($view_type) ? $view_type : 'grid_view';
@endphp

@switch($view_type)
    @case('slide_view')
    <article class="slideshow-slide">
        <figure class="slideshow-slide-image">
            <a href="{{$product_object->get_link('product', $product_object->id, $product_object->slug)}}">
                <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{Helper::checkImage($product_object->image)}}" alt="{{$product_object->slug}}" title="{{$product_object->title}}">
            </a>
        </figure>
        <div class="slideshow-slide-info">
            <div class="slideshow-slide-text">
                <span class="money mr-2 {{$product_object->regular_price == $product_object->sale_price ? 'd-none' : 'text-strikethrough' }}">{{$product_object->getFormattedPrice('regular_price')}}</span>
                @if($product_object->regular_price != $product_object->sale_price)
                    <br>
                @endif
                <span class="money">{{$product_object->getFormattedPrice('sale_price')}}</span>
            </div>
            <h2 class="slideshow-slide-title"><a href="{{$product_object->get_link('product', $product_object->id, $product_object->slug)}}">{{$product_object->title}}</a></h2>
            {{--<a class="slideshow-slide-link" href="javascript: void(0)"><span class="btn__add_to_cart" data-product-id="{{$product_object->id}}">Mua ngay</span></a>--}}
            <a class="slideshow-slide-link" href="{{$product_object->get_link('product', $product_object->id, $product_object->slug)}}">Mua ngay</a>
        </div>
    </article>
    @break
    @default
    <article class="product-item" data-product-item="{{$product_object->id}}">
        <figure class="product-item-image">
            <a href="{{$product_object->get_link('product', $product_object->id, $product_object->slug)}}">
                <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{Helper::checkImage($product_object->image)}}" alt="{{$product_object->slug}}" title="{{$product_object->title}}">
                <span class="product-item-badge">{{$product_object->remain_quantity() > 0 ? 'còn hàng' : 'hết hàng'}}</span>
            </a>
            <div class="collection-product-size-overlay">
                @if($product_object->remain_quantity() > 0)
                    <span class="product-size-title">Kích cỡ</span>
                    <ul class="product-size-list">
                        @foreach($product_object->attribute()->where('parent_id', \App\Models\Attribute::SIZE)->get() as $attr)
                            @continue($attr->getRemainQuantity($product_object->id) == 0)
                            <li class="product-size-label">{{$attr->title}}</li>
                        @endforeach
                    </ul>
                @endif

                {{--<span class="product-size-button btn__add_to_cart" data-product-id="{{$product_object->id}}">Mua ngay</span>--}}
                <a class="product-size-button" href="{{$product_object->get_link('product', $product_object->id, $product_object->slug)}}">{{$product_object->remain_quantity() > 0 ? 'mua ngay' : 'hết hàng'}}</a>
            </div>
        </figure>

        <div class="product-item-titles">
            <h1 class="product-item-title">
                <a href="{{$product_object->get_link('product', $product_object->id, $product_object->slug)}}">{{$product_object->title}}</a>
            </h1>
            @php
                $list_attr_color = $product_object->attribute()->where('parent_id', \App\Models\Attribute::COLOR)->get()->pluck('title')->all();
            @endphp
            <div class="product-color">{{implode('/', $list_attr_color)}}</div>
            <div class="product-price product-item-price">
                <span class="money">
                    <span class="money mr-2 {{$product_object->regular_price == $product_object->sale_price ? 'd-none' : 'text-strikethrough' }}">{{$product_object->getFormattedPrice('regular_price')}}</span>
                    @if($product_object->regular_price != $product_object->sale_price)
                        <br>
                    @endif
                    <span class="money">{{$product_object->getFormattedPrice('sale_price')}}</span>
                </span>
            </div>
        </div>
    </article>
@endswitch
