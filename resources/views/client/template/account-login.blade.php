<div class="{{ Route::currentRouteName() == 'client_auth_login_view' || Route::currentRouteName() == 'login' ? 'page-content' : '' }} template-account-login">
    <h2 class="template-account-login-title">SIGN IN</h2>

    <article class="template-account-login-forms">
        <div class="account-login">
            <form method="post" action="{{route('client_auth_login')}}" accept-charset="UTF-8">
                <div class="input-wrapper">
                    <label for="" class="visually-hidden">User name</label>
                    <input class="input-field" type="text" name="user_name" value="">
                </div>

                <div class="input-wrapper">
                    <label for="" class="visually-hidden">Password</label>
                    <input class="input-field" type="password" name="password">
                </div>

                <div class="input-wrapper">
                    <input class="button" type="submit" value="SIGN IN">
                </div>
            </form>
        </div>
        <div><span class="account-register"><a data-toggle="modal" data-target="#modalForgotPassword" href="javascript:;">Forgot password</a></span></div>
        <span class="account-register">Don't have an account? <a href="{{route('client_auth_register_view')}}">Sign up</a></span>
    </article>
</div>
