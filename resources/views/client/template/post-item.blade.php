@switch($view_type)
    @case('title_view')
    <div class="cd-post-item title_view">
        <div class="pi-text box-text text-left">
            <div class="pi-title">
                <a href="{{$post_object->get_link('post', $post_object['id'], $post_object['slug'])}}">{{$post_object['title']}}</a>
            </div>
        </div>
    </div>
    @break
    @case('list_view')
    <div class="cd-post-item list_view">
        <div class="row">
            <div class="col-3 col-md-4 col-lg-3">
                <div class="pi-image box-image">
                    <div class="image-cover">
                        <a href="{{$post_object->get_link('post', $post_object['id'], $post_object['slug'])}}">
                            <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{$post_object['image']}}" alt="{{$post_object['slug']}}" title="{{$post_object['title']}}">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-9 col-md-8 col-lg-9">
                <div class="pi-text box-text text-left">
                    <div class="pi-title">
                        <a href="{{$post_object->get_link('post', $post_object['id'], $post_object['slug'])}}">{{$post_object['title']}}</a>
                    </div>
                    <div class="pi-meta">
                        <span class="pi-meta-author"><i class="fa fa-user" aria-hidden="true"></i> {{$post_object->author->first_name}}</span>
                        <span class="pi-meta-date"><i class="fa fa-calendar" aria-hidden="true"></i> {{$post_object->getFormattedDate('created_at', 'd/m/Y')}}</span>
                    </div>
                    <div class="pi-desc">{{$post_object['description']}}...</div>
                </div>
            </div>
        </div>
    </div>
    @break
    @default
    <div class="cd-post-item grid_view">
        <div class="pi-image box-image">
            <div class="image-cover">
                <a href="{{$post_object->get_link('post', $post_object['id'], $post_object['slug'])}}">
                    <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{$post_object['image']}}" alt="{{$post_object['slug']}}" title="{{$post_object['title']}}">
                </a>
            </div>
        </div>
        <div class="pi-text box-text text-left">
            <div class="pi-title">
                <a href="{{$post_object->get_link('post', $post_object['id'], $post_object['slug'])}}">{{$post_object['title']}}</a>
            </div>
            <div class="pi-meta">
                <span class="pi-meta-author"><i class="fa fa-user" aria-hidden="true"></i> {{$post_object->author->first_name}}</span>
                <span class="pi-meta-date"><i class="fa fa-calendar" aria-hidden="true"></i> {{$post_object->getFormattedDate('created_at', 'd/m/Y')}}</span>
            </div>
            <div class="pi-desc">{{$post_object['description']}}...</div>
        </div>
    </div>
@endswitch
