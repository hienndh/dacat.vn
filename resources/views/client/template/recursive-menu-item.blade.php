@switch($recursive_type)
    @case('grid_view')
    @if(count($list_menu_item))
        @foreach($list_menu_item as $menu_item)
            <li class="navmenu-item">
                <a class="navmenu-link" href="{{Helper::getMenuLink($menu_item)}}">
                    <span class="navmenu-link-title">{{$menu_item['title']}}</span>
                    <div class="navmenu-image">
                        @php
                            $item_object = $menu_item->getObject($menu_item['type']);
                        @endphp
                        <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{$item_object['image']}}" alt="{{$item_object['slug']}}" title="{{$menu_item['title']}}">
                    </div>
                </a>
            </li>
        @endforeach
    @endif
    @break

    @case('ul_view')
    @if(count($list_menu_item))
        @foreach($list_menu_item as $menu_item)
            <li class="submenu-item">
                <a class="submenu-link" href="{{Helper::getMenuLink($menu_item)}}">{{$menu_item['title']}}</a>
            </li>
        @endforeach
    @endif
    @break

    @case('footer_view')
    @if(count($list_menu_item))
        @foreach($list_menu_item as $menu_item)
            <li class="footnav-menu-link">
                <a href="{{Helper::getMenuLink($menu_item)}}">{{$menu_item['title']}}</a>
            </li>
        @endforeach
    @endif
    @break

    @case('vertical_view')
    @if(count($list_menu_item))
        @foreach($list_menu_item as $menu_item)
            <li class="nav-item {{$level == 0 && count($menu_item->childMenuItem) ? 'nav-has-child' : ''}} {{$level > 0 && count($menu_item->childMenuItem) ? 'nav-child-has-child' : ''}}">
                <a class="nav-link" href="{{Helper::getMenuLink($menu_item)}}">
                    <span>{{$menu_item['title']}}</span>
                </a>
                @if(count($menu_item->childMenuItem))
                    <ul class="nav nav-child">
                        @include('client.template.recursive-menu-item', ['list_menu_item' => $menu_item->childMenuItem, 'level' => ($level+1), 'recursive_type' => 'vertical_view'])
                    </ul>
                @endif
            </li>

        @endforeach
    @endif
    @break
@endswitch

