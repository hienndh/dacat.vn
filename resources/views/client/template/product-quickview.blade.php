<div id="Product_Primary" class="row cd-product-single">
    <div class="col-12 col-md-5">
        <div class="pi-image box-image">
            <div class="image-cover">
                <a href="{{$product_object->get_link('product', $product_object['id'], $product_object['slug'])}}">
                    <img src="{{$product_object['image']}}" alt="{{$product_object['title']}}" title="{{$product_object['title']}}">
                </a>
            </div>
            <div class="pi-label">
                @if($product_object['created_at'] > (time() - 3 * 24 * 3600))
                    <div class="label-element new-label">
                        <span>New</span>
                    </div>
                @endif
                @if($product_object['regular_price'] != $product_object['sale_price'])
                    <div class="label-element sale-label">
                        <span>-{{$product_object['sale_number'] }}%</span>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-12 col-md-7">
        <div class="pi-title">
            <a href="{{$product_object->get_link('product', $product_object['id'], $product_object['slug'])}}">{{$product_object['title']}}</a>
        </div>
        <div class="pi-price">
            <s class="pi-regular-price {{$product_object['regular_price'] == $product_object['sale_price'] ? 'd-none' : '' }}">{{$product_object->getFormattedPrice('regular_price')}}</s>
            <span class="pi-sale-price">{{$product_object->getFormattedPrice('sale_price')}}</span>
        </div>
        <div class="pi-desc">
            {!! $product_object['description'] !!}
        </div>
        <div class="pi-action">
            <div class="form-group form-inline pi-quantity">
                <label class="mr-2" for="">Quantity: </label>
                <input class="form-control form-control-sm input__quantity" type="number" min="1" step="1" value="1" style="max-width: 100px;">
            </div>
            <button type="button" class="btn btn-primary btn__add_to_cart" data-product-id="{{$product_object['id']}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> ADD TO CART</button>
        </div>
    </div>
</div>
