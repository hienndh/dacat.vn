@extends('client.master')
@section('client_css')
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/trinh-dev-css-2.css')}}"/>
@endsection
@section('client_main')
    <div class="index-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <aside>
                        <div class="mega-left-title">
                            <strong>SẢN PHẨM</strong>
                        </div>
                        <div class="wrapper_vertical_menu">
                            <ul>
                                <?php echo $menu_left ?>
                            </ul>
                        </div>
                    </aside>
                    <div id="bestsale-200654031564584862"
                         class="sw-best-seller-product vc_element carousel slide bestsale-block" data-interval="0">
                        <div class="block-title title1"><h2><span>BÁN CHẠY</span></h2>
                            <div class="customNavigation nav-left-product">
                                <a title="Previous" class="btn-bs prev-bs fa fa-angle-left"
                                   href="#bestsale-200654031564584862" role="button" data-slide="prev"></a>
                                <a title="Next" class="btn-bs next-bs fa fa-angle-right"
                                   href="#bestsale-200654031564584862" role="button" data-slide="next"></a>
                            </div>
                        </div>
                        <div class="carousel-inner">
                            <div class="item active">
                                @foreach($list_best_hot_product as $hot_product)
                                <div class="bs-item cf">
                                    <div class="bs-item-inner">

                                        <div class="item-img"><a
                                                    href="{{$hot_product->get_link('product', $hot_product->id, $hot_product->slug)}}"
                                                    title="{{$hot_product->title}}"><img
                                                                                                   src="{{Helper::checkImage($hot_product->image)}}"
                                                                                                   class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image"
                                                                                                   alt="{{$hot_product->slug}}"
                                                                                                   srcset="{{Helper::checkImage($hot_product->image)}} 180w, {{Helper::checkImage($hot_product->image)}} 150w, {{Helper::checkImage($hot_product->image)}} 200w"
                                                                                                   sizes="(max-width: 180px) 100vw, 180px"></a>
                                        </div>
                                        <div class="item-content">
                                            <div class="star"></div>
                                            <h4>
                                                <a href="{{$hot_product->get_link('product', $hot_product->id, $hot_product->slug)}}"
                                                   title="{{$hot_product->title}}">{{$hot_product->title}}</a></h4>
                                            <p><span class="woocommerce-Price-amount amount">{{$hot_product->getFormattedPrice('sale_price')}}&nbsp;<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></span></p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    @if(isset($web_setting['home_new_product']) && isset($web_setting['home_new_product']['status']) && $web_setting['home_new_product']['status'] == 'on')
                        <div class="shopify-section section-featured-products">
                            <div class="featured-products-header">
                                <h4 class="section-title">{{isset($web_setting['home_new_product']['title']) ? $web_setting['home_new_product']['title'] : 'WHATS NEW'}}</h4>
                                <a class="view-all-link" href="{{route('client_new_product_view')}}">View all</a>
                            </div>
                            <section class="product-highlights">
                                <div class="product-highlights-normal-wrapper">
                                    @foreach($list_new_product as $new_product)
                                        @include('client.template.product-item', ['product_object' => $new_product])
                                    @endforeach
                                </div>
                            </section>
                        </div>
                    @endif

                    @if(isset($web_setting['home_archive_product']) && isset($web_setting['home_archive_product']['status']) && $web_setting['home_archive_product']['status'] == 'on')
                        <div class="shopify-section section-featured-products">
                            <div class="featured-products-header">
                                <h4 class="section-title">{{isset($web_setting['home_archive_product']['title']) ? $web_setting['home_archive_product']['title'] : 'PRODUCTS'}}</h4>
                                <a class="view-all-link" href="{{route('client_archive_product_view')}}">View all</a>
                            </div>
                            <section class="product-highlights">
                                <div class="product-highlights-normal-wrapper">
                                    @foreach($list_archive_product as $archive_product)
                                        @include('client.template.product-item', ['product_object' => $archive_product])
                                    @endforeach
                                </div>
                            </section>
                        </div>
                    @endif

                    @if(isset($web_setting['home_best_sale_product']) && isset($web_setting['home_best_sale_product']['status']) && $web_setting['home_best_sale_product']['status'] == 'on')
                        <div class="shopify-section section-featured-products">
                            <div class="featured-products-header">
                                <h4 class="section-title">{{isset($web_setting['home_best_sale_product']['title']) ? $web_setting['home_best_sale_product']['title'] : 'SẢN PHẨM KHUYẾN MÃI'}}</h4>
                                <a class="view-all-link" href="{{route('client_sale_product_view')}}">View all</a>
                            </div>
                            <section class="product-highlights">
                                <div class="product-highlights-normal-wrapper">
                                    @foreach($list_best_sale_product as $best_sale_product)
                                        @include('client.template.product-item', ['product_object' => $best_sale_product])
                                    @endforeach
                                </div>
                            </section>
                        </div>
                    @endif

                    @if(isset($web_setting['home_instagram_album']) && isset($web_setting['home_instagram_album']['status']) && $web_setting['home_instagram_album']['status'] == 'on')
                        @if(!empty($list_image_instagram))
                            <div class="shopify-section section-slideshow">
                                <div class="slideshow-header">
                                    <h4 class="section-title">{{isset($web_setting['home_instagram_album']['title']) ? $web_setting['home_instagram_album']['title'] : ''}}</h4>
                                    <a class="view-all-link" href="{{$web_setting['ta_social_instagram'] ?? '#'}}"
                                       target="_blank">View all</a>
                                </div>

                                <div class="slideshow slideshow-slides-8">
                                    @foreach($list_image_instagram as $instgram_image)
                                        <article class="slideshow-slide insta-item">
                                            <a href="{{$instgram_image['link']}}" target="_blank">
                                                <figure class="slideshow-slide-image">
                                                    <img class="lazyload" src="{{asset('img/placeholder.png')}}"
                                                         data-src="{{$instgram_image['images']['standard_resolution']['url']}}"
                                                         alt="instagram" title="instagram">
                                                </figure>
                                                <div class="overlay-effect">
                                                    <div class="over">
                                                        <svg height="56.7px" id="Layer_1"
                                                             style="enable-background:new 0 0 56.7 56.7;" version="1.1"
                                                             viewBox="0 0 56.7 56.7" width="56.7px"
                                                             xmlns="http://www.w3.org/2000/svg" xml:space="preserve"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             class="item-open-icon"><g>
                                                                <path d="M28.2,16.7c-7,0-12.8,5.7-12.8,12.8s5.7,12.8,12.8,12.8S41,36.5,41,29.5S35.2,16.7,28.2,16.7z M28.2,37.7   c-4.5,0-8.2-3.7-8.2-8.2s3.7-8.2,8.2-8.2s8.2,3.7,8.2,8.2S32.7,37.7,28.2,37.7z"></path>
                                                                <circle cx="41.5" cy="16.4" r="2.9"></circle>
                                                                <path d="M49,8.9c-2.6-2.7-6.3-4.1-10.5-4.1H17.9c-8.7,0-14.5,5.8-14.5,14.5v20.5c0,4.3,1.4,8,4.2,10.7c2.7,2.6,6.3,3.9,10.4,3.9   h20.4c4.3,0,7.9-1.4,10.5-3.9c2.7-2.6,4.1-6.3,4.1-10.6V19.3C53,15.1,51.6,11.5,49,8.9z M48.4,39.9c0,3.1-0.9,5.6-2.7,7.3   c-1.8,1.7-4.3,2.6-7.3,2.6H18c-3,0-5.5-0.9-7.3-2.6C8.9,45.4,8,42.9,8,39.8V19.3c0-3,0.9-5.5,2.7-7.3c1.7-1.7,4.3-2.6,7.3-2.6h20.6   c3,0,5.5,0.9,7.3,2.7c1.7,1.8,2.7,4.3,2.7,7.2V39.9z"></path>
                                                            </g></svg>
                                                    </div>
                                                </div>
                                            </a>
                                        </article>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>

        </div>


    </div>

    @if($coupon)
        <div class="modal fade show" id="modalCoupon">
            <div class="modal-dialog modal-dialog-centere advanced-modal">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <img src="{{asset('img/img_cancel.png')}}" alt="Thoát" class="hidden-modal"
                                     data-dismiss="modal">
                                <div class="col-lg-8 col-md-6">
                                    <div class="cover-img">
                                        <img src="{{Helper::checkImage($coupon->campaign->image, ($web_setting['home_default_coupon_banner'] ?? 'img/placeholder.png'))}}"
                                             alt="{{$coupon->campaign->name}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 content">
                                    <p>
                                        Cảm ơn bạn đã tin tưởng và sử dụng dịch vụ của chúng tôi. Hiện tại chúng tôi
                                        đang có
                                        chương trình ưu đãi cho các khách hàng đã là thành viên của mình. Dưới đây là mã
                                        giảm giá của bạn.
                                    </p>
                                    <div class="code">
                                        <label for="code"><strong>Coupon code:</strong></label>
                                        <span id="code">{{$coupon->code}}</span>
                                    </div>
                                    <div>
                                        <a href="{{route('client_archive_product_view')}}" class="btn btn-primary">
                                            SHOP NOW
                                        </a>
                                    </div>
                                    <div class="detail-coupons">
                                        <a href="{{route('client_account_coupon_view')}}">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(!Auth::guard('account')->check() && $active_campaign)
        <div class="modal fade show" id="modalCampaign">
            <div class="modal-dialog modal-dialog-center advanced-modal">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <img src="{{asset('img/img_cancel.png')}}" alt="Thoát" class="hidden-modal"
                                     data-dismiss="modal">
                                <div class="col-lg-8 col-md-6">
                                    <div class="cover-img">
                                        <img src="{{Helper::checkImage($active_campaign->image, ($web_setting['home_default_coupon_banner'] ?? 'img/placeholder.png'))}}"
                                             alt="{{$active_campaign->name}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 content">
                                    <p>
                                        DINGO.CLOTHING xin gửi tới quý khách chương trình khuyến mãi
                                        "{{$active_campaign->name}}" với ưu đãi lên
                                        tới {{Helper::getFormattedNumber($active_campaign->value)}}{{$active_campaign->value_type == 1 ? 'đ' : '%'}}
                                        / 1 đơn hàng. Hãy nhanh tay <a href="{{route('client_auth_register_view')}}">đăng
                                            ký tài khoản</a> hoặc <a href="{{route('client_auth_login_view')}}">đăng
                                            nhập</a> để nhận được mã khuyến mãi ngay hôm nay.
                                    </p>
                                    <p class="text-left">
                                        Thời gian: Từ {{date('d/m/Y H:i',$active_campaign->startDate)}}
                                        đến {{date('d/m/Y H:i',$active_campaign->endDate)}}
                                    </p>
                                    <div class="text-center">
                                        <a href="{{route('client_auth_register_view')}}" class="btn btn-primary"
                                           style="font-size: 12px;">
                                            ĐĂNG KÝ NGAY
                                        </a>
                                        <a href="{{route('client_auth_login_view')}}" class="btn btn-primary"
                                           style="font-size: 12px;">
                                            ĐĂNG NHẬP
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('client_script')
    <script type="text/javascript">
        $(document).ready(function () {
            @if($coupon)
            setTimeout(x => {
                $('#modalCoupon').modal('show');
            }, 1000);
            @endif

            @if(!Auth::guard('account')->check() && $active_campaign)
            setTimeout(x => {
                checkCookie('modalCampaign');
            }, 2000);
            @endif
        });
    </script>
@endsection
<style>

</style>


