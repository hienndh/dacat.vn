@extends('client.master')

@section('client_main')
    <div class="container">
        <div class="row">
            <div class="col-12 py-5">
                @if($order_object)
                    <p><strong>ORDER: #{{$identifier}}</strong></p>
                    <p><strong>TRẠNG THÁI: {{$order_object->status_text()}}</strong></p>
                    <p>Thank you for your order. As soon as possible, we will contact you to confirm your order.</p>
                    <p>Thank you very much!</p>
                @else
                    <p>Order code incorrect!</p>
                @endif
                <p><a href="{{route('client_home')}}" class="btn btn-primary">RETURN TO HOMEPAGE</a></p>
            </div>
        </div>
    </div>
@endsection
