@extends('client.master')

@section('client_css')
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/theme.scss8097.css')}}"/>
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/trinh-dev-css-2.css')}}"/>
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/page-checkout.css')}}"/>
@endsection

@section('client_main')
    <div class="content">
        <div class="wrap">
            <div class="main" role="main">
                <div class="main__header">
                    <a class="logo logo--left" href="/">
                        {{--<img alt="mnml" class="logo__image logo__image--small" src="../../img/mnml-black-logo.png">--}}
                    </a>
                    <h1 class="visually-hidden">
                        Check customer information
                    </h1>

                </div>
                <div class="main__content">
                    <div class="step">
                        <form class="edit_checkout animate-floating-labels" action="{{route('client_cart_checkout')}}" method="post">
                            <div class="step__sections">
                                @if($account)
                                    <div class="section section--contact-information d-none">
                                        <div class="section__header">
                                            <div class="layout-flex layout-flex--tight-vertical layout-flex--loose-horizontal layout-flex--wrap">
                                                <h2 class="section__title layout-flex__item layout-flex__item--stretch" id="main-header" tabindex="-1">
                                                    Contact
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="section__content">
                                            <div class="logged-in-customer-information">
                                                <div class="logged-in-customer-information__avatar-wrapper">
                                                    <div class="logged-in-customer-information__avatar gravatar"
                                                         style="background-image: url(../../img/icon-user.png);filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../../img/icon-user.png', sizingMethod='scale')" role="img" aria-label="Avatar">

                                                    </div>
                                                </div>
                                                <p class="logged-in-customer-information__paragraph">
                                                    <span class="page-main__emphasis">{{ isset($account->full_name) ? $account->full_name : ''}}</span>
                                                    <span>({{isset($account->email) ? $account->email : ''}})</span>
                                                    <br>
                                                    <a href="{{route('client_auth_logout')}}">Sign out</a>
                                                </p>
                                            </div>

                                            <div class="logged-in-customer-newsletter d-none">
                                                <div class="checkbox-wrapper">
                                                    <div class="checkbox__input">
                                                        <input class="input-checkbox" type="checkbox" value="1" checked="checked" name="checkout[buyer_accepts_marketing]" id="checkout_buyer_accepts_marketing">
                                                    </div>
                                                    <label class="checkbox__label" for="checkout_buyer_accepts_marketing">
                                                        Send me the news and sale information!
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="section section--shipping-address">
                                    <div class="section__header">
                                        <h2 class="section__title">
                                            Shipping Info
                                        </h2>
                                    </div>
                                    <div class="section__content">
                                        <div class="fieldset">
                                            <div class="field field--required field--half field--show-floating-label">
                                                <div class="field__input-wrapper">
                                                    <label class="field__label field__label--visible" for="checkout_shipping_first_name">Fullname</label>
                                                    <input placeholder="Fullname" class="field__input" size="30" type="text" value="{{isset($account->full_name) ? $account->full_name : ''}}" name="order[full_name]" id="checkout_shipping_first_name" required>
                                                </div>
                                            </div>
                                            <div class="field field--required field--half field--show-floating-label">
                                                <div class="field__input-wrapper">
                                                    <label class="field__label field__label--visible" for="checkout_shipping_phone">Phone</label>
                                                    <input placeholder="Phone number" class="field__input field__input--numeric" type="text" minlength="10" maxlength="15" value="{{isset($account->phone) ? $account->phone : ''}}" name="order[phone]" id="checkout_shipping_phone" required>
                                                </div>
                                            </div>
                                            <div class="field field--required">
                                                <div class="field__input-wrapper">
                                                    <label class="field__label field__label--visible" for="checkout_shipping_email">Email</label>
                                                    <input placeholder="Email address" class="field__input" type="email" name="order[email]" id="checkout_shipping_email" value="{{isset($account->email) ? $account->email : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="field field--required">
                                                <div class="field__input-wrapper">
                                                    <label class="field__label field__label--visible" for="checkout_shipping_address">Address</label>
                                                    <input placeholder="Address" class="field__input" type="text" value="{{ old('address') }}" name="order[address]" id="checkout_shipping_address" required>
                                                </div>
                                            </div>
                                            <div class="field field--required d-none">
                                                <div class="field__input-wrapper">
                                                    <label class="field__label field__label--visible" for="coupon-code">Coupon</label>
                                                    <input placeholder="Coupon code" class="field__input" size="30" type="text" name="order[coupon_code]" value="{{ old('coupon_code') }}" id="coupon-code">
                                                </div>
                                            </div>
                                            <div class="field field--required field--half field--show-floating-label" data-address-field="country" data-google-places="true">
                                                <div class="field__input-wrapper field__input-wrapper--select">
                                                    <label class="field__label field__label--visible" for="checkout_shipping_province">City</label>
                                                    <select size="1" class="field__input field__input--select" name="order[province_id]" id="checkout_shipping_province" required>
                                                        @foreach($listCity as $city)
                                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="field field--required field--half">
                                                <div class="field__input-wrapper">
                                                    <label class="field__label field__label--visible" for="checkout_shipping_zip">Postcode</label>
                                                    <input placeholder="Postcode" class="field__input field__input--zip" size="30" type="text" name="order[post_code]" value="{{ old('post_code') }}" id="checkout_shipping_zip">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="step__footer">
                                <button name="button" type="submit" class="step__footer__continue-btn btn">
                                <span class="btn__content">
                                    Continue ordering
                                </span>
                                </button>
                                <a class="step__footer__previous-link" href="{{route('client_cart_index_view')}}">
                                    <svg focusable="false" aria-hidden="true" class="icon-svg icon-svg--color-accent icon-svg--size-10 previous-link__icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10">
                                        <path d="M8 1L7 0 3 4 2 5l1 1 4 4 1-1-4-4"></path>
                                    </svg>
                                    <span class="step__footer__previous-link-content">Return to the cart</span></a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="main__footer">
                    <div role="contentinfo" aria-label="Footer">
                        {{--<ul class="policy-list">
                            <li class="policy-list__item">
                                <a aria-haspopup="dialog" data-modal="policy-22226817" data-close-text="Close" href="#">Chính sách hoàn tiền</a>
                            </li>
                            <li class="policy-list__item">
                                <a aria-haspopup="dialog" data-modal="policy-22226881" data-close-text="Close" href="#">Chính sách cá nhân</a>
                            </li>
                            <li class="policy-list__item">
                                <a aria-haspopup="dialog" data-modal="policy-22226945" data-close-text="Close" href="#">Điều khoản dịch vụ</a>
                            </li>
                        </ul>--}}
                    </div>
                </div>
            </div>
            <div class="sidebar" role="complementary">
                <div class="sidebar__content">
                    <div id="order-summary" class="order-summary order-summary--is-expanded">
                        <div class="order-summary__sections">
                            <div class="order-summary__section order-summary__section--product-list">
                                <div class="order-summary__section__content">
                                    <table class="product-table">
                                        <tbody data-order-summary-section="line-items">
                                        @if(!empty(Cart::content()))
                                            @foreach(Cart::content() as $cart_item)
                                                <tr class="product">
                                                    <td class="product__image">
                                                        <div class="product-thumbnail">
                                                            <div class="product-thumbnail__wrapper">
                                                                <img alt="Product name" class="product-thumbnail__image" src="{{$cart_item->model->image ?? asset('img/placeholder.png')}}" data-src="{{$cart_item->model->image ?? asset('img/placeholder.png')}}">
                                                            </div>
                                                            <span class="product-thumbnail__quantity">{{$cart_item->qty}}</span>
                                                        </div>
                                                    </td>
                                                    <td class="product__description">
                                                        <span class="product__description__name order-summary__emphasis">{{$cart_item->name}}{{$cart_item->options->has('color') && $cart_item->options->color ? ' - ' . $cart_item->options->color : ''}}{{$cart_item->options->has('size') && $cart_item->options->size ? ' - ' . $cart_item->options->size : ''}}</span>
                                                        <span class="product__description__variant order-summary__small-text">
                                                            {{$cart_item->options->has('color') && $cart_item->options->color ? ' - ' . $cart_item->options->color . '<br>' : ''}}
                                                            {{$cart_item->options->has('size') && $cart_item->options->size ? ' - ' . $cart_item->options->size : ''}}
                                                        </span>
                                                    </td>
                                                    <td class="product__quantity" style="min-width: 60px;">
                                                        <span class="order-summary__emphasis">{{$cart_item->qty}} x</span>
                                                    </td>
                                                    <td class="product__price">
                                                        <span class="order-summary__emphasis">{{Helper::getFormattedPrice($cart_item->price)}}</span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="order-summary__section order-summary__section--discount">
                                <h3 class="visually-hidden">Gift code or discount code</h3>
                                <form class="edit_checkout animate-floating-labels" action="#" method="post">
                                    <div class="fieldset">
                                        <div class="field">
                                            <div class="field__input-btn-wrapper">
                                                <div class="field__input-wrapper">
                                                    <label class="field__label field__label--visible" for="checkout_reduction_code">Gift code or discount code</label>
                                                    <input value="{{ old('coupon_code') }}" placeholder="Gift code or discount code" class="field__input" size="30" type="text" name="checkout[reduction_code]" id="checkout_reduction_code" onkeyup="check_coupon()" required>
                                                </div>
                                                {{--<button onclick="check_coupon()" name="button" type="button" class="field__input-btn btn btn--disabled">
                                                  <span class="btn__content visually-hidden-on-mobile" aria-hidden="true">
                                                    Áp dụng
                                                  </span>
                                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                </button>--}}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="order-summary__section order-summary__section--total-lines">
                                <table class="total-line-table">
                                    <tbody class="total-line-table__tbody">
                                    <tr class="total-line total-line--subtotal">
                                        <th class="total-line__name" scope="row">Subtotal</th>
                                        <td class="total-line__price">
                                        <span class="order-summary__emphasis">
                                            {{ Helper::getFormattedPrice(Cart::subtotal(0, '', ''))}}
                                        </span>
                                        </td>
                                    </tr>
                                    {{--<tr class="total-line total-line--shipping">
                                        <th class="total-line__name" scope="row">Phí vận chuyển</th>
                                        <td class="total-line__price">
                                        <span class="order-summary__emphasis">
                                            <span aria-hidden="true">—</span><span class="visually-hidden">Not yet available</span>
                                        </span>
                                        </td>
                                    </tr>--}}
                                    </tbody>
                                    <tfoot class="total-line-table__footer">
                                    <tr class="total-line">
                                        <th class="total-line__name payment-due-label" scope="row">
                                            <span class="payment-due-label__total">Total</span>
                                        </th>
                                        <td class="total-line__price payment-due">
                                            <span class="payment-due__price" id="cart-total"  data-cart_total="{{ Helper::getFormattedPrice(Cart::total(0, '', ''))}}">
                                                {{ Helper::getFormattedPrice(Cart::total(0, '', ''))}}
                                            </span>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('client_script')
    <script src="{{asset('client/js/trinhdev-checkout.js')}}"></script>

    <script>
        function check_coupon() {
            var coupon = $('#checkout_reduction_code').val();
            if (coupon.length < 6) {
                $('#coupon-code').val('');
                $('#cart-total').text($('#cart-total').data('cart_total'));
                return toastError('Invalid counpon code');
            } else {
                 $.ajax({
                    type: "POST",
                    url: "{{route('client_cart_add_coupon_ajax')}}",
                    data: {
                        coupon_code: coupon,
                        _token: "{{csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (result) {
                        $('#cart-total').text(result.data.cart_total + '{{\App\Models\BaseModel::PROUCT_CURRENCY}}');
                        if (result.code === 1) {
                            $('#coupon-code').val(result.data.coupon.code);
                            return toastSuccess(result.msg);
                        } else {
                            $('#coupon-code').val('');
                            return toastError(result.msg);
                        }
                    },
                    error: function (xhr, status, error) {
                        $('#coupon-code').val('');
                        $('#cart-total').text($('#cart-total').data('cart_total'));
                        return toastError(xhr.responseJSON.msg);
                    }
                });
            }
        }
    </script>
@endsection
