@extends('client.master')

@section('client_main')
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3">
                @include('client.template.title', ['title' => 'CART', 'align' => 'center'])
            </div>
            <div id="Section_Content" class="col-12 col-md-9">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td></td>
                            <th>Image</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty(Cart::content()))
                            @foreach(Cart::content() as $cart_item)
                                <tr class="cart-item-{{$cart_item->rowId}}">
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm btn__remove_cart_item" data-product-row-id="{{$cart_item->rowId}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                    <td width="120px">
                                        <img class="lazyload cart-item-image" src="{{$cart_item->model->image ?? asset('img/placeholder.png')}}" data-src="{{$cart_item->model->image ?? asset('img/placeholder.png')}}" alt="">
                                    </td>
                                    <td>
                                        <p class="cart-item-name">{{$cart_item->name}}{{$cart_item->options->has('color') && $cart_item->options->color ? ' - ' . $cart_item->options->color : ''}}{{$cart_item->options->has('size') && $cart_item->options->size ? ' - ' . $cart_item->options->size : ''}}</p>
                                    </td>
                                    <td>
                                        <input class="form-control form-control-sm cart-item-quantity input__product_quantity" data-product-id="{{$cart_item->id}}" data-product-row-id="{{$cart_item->rowId}}" type="number" min="1" step="1" value="{{$cart_item->qty}}">
                                    </td>
                                    <td width="130px"><span>{{Helper::getFormattedPrice($cart_item->price)}}</span></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="row">
                    <div class="col-7"><span class="font-weight-bold text-uppercase">SUBTOTAL:</span></div>
                    <div class="col-5 text-right"><span class="font-weight-bold cart-subtotal">{{Cart::subtotal()}}VND</span></div>
                    {{--<div class="col-7"><span class="font-weight-bold text-uppercase">THUẾ:</span></div>--}}
                    {{--<div class="col-5 text-right"><span class="font-weight-bold cart-tax">{{Cart::tax()}}VND</span></div>--}}
                    <div class="col-7"><span class="font-weight-bold text-uppercase">TOTAL:</span></div>
                    <div class="col-5 text-right"><span class="font-weight-bold cart-total">{{Cart::total()}}VND</span></div>
                    <div class="col-12">
                        <a href="{{route('client_cart_checkout_view')}}" class="btn btn-primary w-100 font-weight-bold my-2 btn__payment"
                           style="display: {{Cart::count() ? 'block' : 'none'}};">CHECKOUT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
