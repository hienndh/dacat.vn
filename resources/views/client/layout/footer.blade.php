<footer class="footer theme-clearfix" style="background: #19197d;">
    <div class="container theme-clearfix">
        <div class="row pt-4" style="color: #fff">
            <div class="col-lg-3 col-md-3  col-sm-6 widget text-9 widget_text"
                 data-scroll-reveal="enter right move 20px wait 0.3s">
                <div class="widget-inner">
                    <div class="textwidget">
                        <div class="col-lg-3 col-md-3  col-sm-6 widget text-9 widget_text">
                            <h2 class="footer-logo"><a href="">
                                    <img alt="SW Maxshop" style=" height: 55px;"
                                         src="{{$web_setting['ta_logo'] ?? asset('img/cuongdev-logo.png')}}"></a></h2>
                            <p></p></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3  col-sm-6 footer-column">
                <div class="footer-border footer_hh_menu"><h2>Sản phẩm</h2>
                    <ul class="menu">
                        @foreach($footer_menu as $menu_item)
                            <li class="menu-chinh-sach-bao-hanh ya-menu-custom level1"><a
                                        href="{{Helper::getMenuLink($menu_item)}}" class="item-link"><span
                                            class="have-title"><span
                                                class="menu-title">{{$menu_item['title']}}</span></span></a></li>
                        @endforeach

                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3  col-sm-6 footer-column">
                <div class="footer-border footer_hh_menu"><h2>TIN MỚI</h2>
                    <ul>
                        @if(count($list_post_footer))
                            @foreach($list_post_footer as $post_item)
                                <li>
                                    <a href="{{$post_item->get_link('post', $post_item['id'], $post_item['slug'])}}">{{$post_item['title']}}</a>
                                </li>
                            @endforeach
                        @endif

                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3  col-sm-6 footer-column">
                <div class="footer-border"><h2>LIÊN HỆ</h2>
                    <div class="textwidget" style="color: #dad0d0">{{$web_setting['ta_company_name'] }}<br>
                        Địa chỉ: {{$web_setting['ta_company_address'] }}<br>
                        Điện thoại: {{$web_setting['ta_company_phone'] }}<br>
                        Email: {{$web_setting['ta_company_email'] }}<br>
                        Hotline: {{$web_setting['ta_company_phone'] }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright theme-clearfix">
        <div class="container clearfix">
            <div class="copyright-text pull-left">

            </div>

        </div>
    </div>
</footer>



