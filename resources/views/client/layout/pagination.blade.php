<nav aria-label="Page navigation">
    <div class="row">
        <div class="col-12 col-md-6">
            <ul class="pagination m-0">
                @if(!$list_object->onFirstPage())
                    <li class="page-item">
                        <a class="page-link" href="{{ $list_object->previousPageUrl() }}" aria-label="Trước">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                @endif
                @for($i=1;$i<=$list_object->lastPage();$i++)
                    <li class="page-item {{ $list_object->currentPage() == $i ? 'active' : ''}}"><a class="page-link" href="{{ $list_object->url($i) }}">{{$i}}</a></li>
                @endfor
                @if($list_object->hasMorePages())
                    <li class="page-item">
                        <a class="page-link" href="{{ $list_object->nextPageUrl() }}" aria-label="Sau">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
        <div class="col-12 col-md-6 text-right">
            <div class="d-inline-block">Show from {{ $list_object->firstItem() }} to {{ $list_object->lastItem() }} of {{$list_object->total()}}</div>
        </div>
    </div>
</nav>
