<!-- ============================================================== -->
<!-- Bread crumb -->
<!-- ============================================================== -->
<div id="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="breadcrumb-title">{{$breadcrumb['main_title']}}</h3>

                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a class="b" href="{{route('client_home')}}">Home</a>
                    </li>
                    @if(isset($breadcrumb['router_list']) && count($breadcrumb['router_list']))
                        @foreach($breadcrumb['router_list'] as $index => $route_item)
                            <li class="breadcrumb-item">
                                <a href="{{route($route_item['name'])}}">{{$route_item['title']}}</a>
                            </li>
                        @endforeach
                    @else
                        <li class="breadcrumb-item active">{{$breadcrumb['main_title']}}</li>
                    @endif
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb -->
<!-- ============================================================== -->
