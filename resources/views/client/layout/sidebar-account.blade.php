<div class="collection-filters-wrapper">
    <div class="collection-filters-wrapper-inner">
        <h4>{{$account->full_name}}</h4>
        <button class="collection-filters-title" type="button">
            <span class="fa fa-user-o" aria-hidden="true"></span>
            <a href="{{route('client_account_view')}}"><span class="collection-filters-title-text">Account Information</span></a>
        </button>
        <button class="collection-filters-title" type="button">
            <span class="fa fa-list" aria-hidden="true"></span>
            <a href="{{route('client_account_order_view')}}"><span class="collection-filters-title-text">Orders Manager</span></a>
        </button>
        <button class="collection-filters-title" type="button">
            <span class="fa fa-exchange icon" aria-hidden="true"></span>
            <a href="{{route('client_update_password_view')}}"><span class="collection-filters-title-text">Change Password</span></a>
        </button>
        <button class="collection-filters-title" type="button">
            <span class="fa fa-list" aria-hidden="true"></span>
            <a href="{{route('client_account_coupon_view')}}"><span class="collection-filters-title-text">Coupon List</span></a>
        </button>
        <button class="collection-filters-title" type="button">
            <span class="fa fa-exchange icon" aria-hidden="true"></span>
            <a href="{{route('client_logout_account')}}" class="btn_acc_logout"><span class="collection-filters-title-text">Sign out</span></a>
        </button>

    </div>
</div>

@section('client_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn_acc_logout').on('click', function (e) {
                let check = confirm('Do you want to sign out?');
                if(!check) {
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endsection
