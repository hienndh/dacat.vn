<div class="header-style5">
    <header id="header" role="banner" class="header clearfix">

        <div class="container">
            <div class="top-header">
                <div class="ya-logo pull-left">
                    <a href="http://dacat.vn/">
                        <img src="{{$web_setting['ta_logo'] ?? asset('img/cuongdev-logo.png')}}" alt="TB06">
                    </a>

                </div>
                <div class="logobaoan">
                    <img src="{{$web_setting['ta_logo_dark'] ?? asset('img/cuongdev-logo.png')}}"></div>
                <div id="sidebar-top-header" class="sidebar-top-header">
                    <div class="box-contact-email-phone">
                        <h2>HOTLINE:</h2>
                        <a href="#" title="Call:  {{$web_setting['ta_company_phone']}}"> {{$web_setting['ta_company_phone']}}</a>
                        <h2>EMAIL:</h2>
                        <a href="#"> {{$web_setting['ta_company_email']}} </a> <br>
                        <a href="#">{{$web_setting['ta_company_email_2']}} </a>
                    </div>

                </div>

            </div>
        </div>
    </header>
    <!-- Primary navbar -->
    <div id="main-menu" class="main-menu">
        <nav id="primary-menu" class="primary-menu" role="navigation">
            <div class="container">
                <div class="mid-header clearfix">

                    <a href="javascript:void(0)" class="phone-icon-menu"></a>
                    <div class="navbar-inner navbar-inverse">
                        <ul id="menu-main-menu-1" class="nav nav-pills nav-mega flytheme-menures">
                            <li class="ya-menu-custom level1"><a
                                        href="http://dacat.vn" class="item-link"><span
                                            class="have-title"><span class="menu-title">Trang chủ</span></span></a>
                            </li>
                            <?php echo $main_menu ?>
                        </ul>
                    </div>

                    <div id="sidebar-top-menu" class="sidebar-top-menu" >
                        <div class="widget ya_top-3 ya_top non-margin">
                            <div class="widget-inner">
                                <div class="top-form top-search pull-left">
                                    <div class="topsearch-entry">
                                        <form role="search" method="get" id="searchform_special"
                                              action="{{route('client_search')}}">
                                            <div>
                                                <input type="text" value="" name="s" id="s"
                                                       placeholder="Điền từ khóa tìm kiếm...">
                                                <button type="submit" title="Search"
                                                        class="fa fa-search button-search-pro form-button"></button>
                                                <input type="hidden" name="search_posttype" value="product">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="site-actions-cart">
                                    <a href="javascript:void(0)" aria-label="View cart">
                                        <svg class="" xmlns="http://www.w3.org/2000/svg" width="18" height="23" viewBox="0 0 18 23">
                                            <path fill="currentColor" fill-rule="evenodd" d="M17.4 5.942h-3.75V4.736C13.65 2.119 11.55 0 9 0 6.45 0 4.35 2.118 4.35 4.736v1.206H.6c-.33 0-.6.264-.6.588v14.705C0 22.206.81 23 1.8 23h14.4c.99 0 1.8-.794 1.8-1.765V6.53a.595.595 0 0 0-.6-.588zM5.85 4.736C5.85 2.94 7.26 1.47 9 1.47s3.15 1.47 3.15 3.265v1.206h-6.3V4.736z"/>
                                        </svg>
                                        <span class="site-actions-cart-label cart-count">{{Cart::count()}}</span>
                                    </a>
                                </div>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </nav>
    </div>
    <!-- /Primary navbar -->

    {{--CART--}}
    <div class="mini-cart-overlay"></div>
    <div class="mini-cart">
        <div class="cart-header">
            <h1 class="cart-title">Cart</h1>
            <div class="cart-logo">
                <img class="lazyload site-logo-image" src="{{asset('img/placeholder.png')}}" data-src="{{$web_setting['ta_logo'] ?? asset('img/cuongdev-logo.png')}}" alt="" title="{{$web_setting['ta_seo_title'] ?? ''}}">
            </div>
            <span class="cart-close">Close</span>
        </div>

        <div class="cart-form-outer">
            <div class="cart-shipping-message"></div>

            <form class="cart-form" action="{{route('client_cart_checkout_view')}}" method="get">
                @if(Cart::count())
                    <section class="cart-items">
                        @if(!empty(Cart::content()))
                            @foreach(Cart::content() as $cart_item)
                                @include('client.template.sidebar-cart-item', ['cart_item' => $cart_item])
                            @endforeach
                        @endif
                    </section>
                @else
                    <div class="cart-empty-message">
                        <p>It looks like your cart is empty. Please come to the store to choose which product!</p>
                        <a class="cart-empty-button" href="{{route('client_archive_product_view')}}">SHOP NOW</a>
                    </div>
                @endif
                <section class="cart-menu-container">
                    <div class="cart-menu">
                        <div class="cart-menu-wrapper">
                            <div class="cart-total-row">
                                <span class="cart-total-row-title">Subtotal</span>
                                <span class="cart-total-row-items">(<span class="cart-count">{{Cart::count()}}</span> product)</span>
                                <span class="cart-total-row-value money"><span class="money cart-subtotal">{{Cart::subtotal()}}VND</span></span>
                            </div>

                            <div class="cart-menu-buttons cart-menu-buttons-slideout-closed">
                                <button class="cart-menu-button cart-menu-button-checkout btn__checkout" type="submit" {{Cart::count() ? '' : 'disabled'}}>
                                    <span class="cart-menu-button-checkout-text">CHECKOUT</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
        </div>
    </div>

</div>

