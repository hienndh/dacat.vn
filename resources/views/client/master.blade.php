<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="{{$web_setting['ta_seo_description']}}">
    <meta name="author" content="{{$web_setting['ta_seo_description']}}">
    <meta name="keywords" content=">{{$web_setting['ta_seo_keyword']}}, {{$seo_setting['seo_keyword'] ?? ''}}">

    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$seo_setting['seo_title'] ?? $web_setting['ta_seo_title']}}">
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:description" content="{{$seo_setting['seo_description'] ?? $web_setting['ta_seo_description']}}">
    <meta property="og:image" content="{{url($seo_setting['seo_image'] ?? $web_setting['ta_seo_image'])}}">
    <meta property="og:site_name" content="{{$seo_setting['seo_title'] ?? $web_setting['ta_seo_title']}}">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{$web_setting['ta_seo_title']}}">
    <meta name="twitter:description" content="{{$web_setting['ta_seo_description']}}">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{$web_setting['ta_favicon'] ?: asset('admin/assets/images/favicon.png')}}">
    <link rel="canonical" href="{{url('/')}}">
    <meta name="robots" content="all">


    <title>{{isset($seo_setting) && $seo_setting['seo_title'] ? $seo_setting['seo_title'] . ' - ' . $web_setting['ta_seo_title'] : $web_setting['ta_seo_title']}}</title>

    <!-- Client CSS -->
    {!! Assets::renderHeader() !!}

    @yield('client_css')

    {{-- Custom style --}}
    <style type="text/css">
        {{isset($web_setting['ta_custom_css']) ? : ''}}
    </style>

    <script type="text/javascript">
        {{isset($web_setting['ta_custom_script']) ? : ''}}
    </script>

    {{--{!! $web_setting['customizer_google_analytics'] ?? '' !!}--}}
</head>
<body class="page-template-index">

@if(isset($layout))
    @switch($layout)
        @case('LANDING')
        @yield('client_main')
        @break

        @case('FULL_WIDTH')
        @include('client.layout.header')
        <main id="client_main">
            <div class="site-desktopnav-overlay"></div>
            @yield('client_main')
        </main>
        @include('client.layout.footer')
        @break

        @case('CONTAINER')
        @include('client.layout.header')
        <main id="client_main">
            <div class="site-desktopnav-overlay"></div>
            <div class="container">
                @yield('client_main')
            </div>
        </main>
        @include('client.layout.footer')
        @break

        @case('CONTAINER_FLUID')
        @include('client.layout.header')
        <main id="client_main">
            <div class="site-desktopnav-overlay"></div>
            <div class="container-fluid">
                @yield('client_main')
            </div>
        </main>
        @include('client.layout.footer')
        @break

        @default
        @include('client.layout.header')
        <main id="client_main">
            <div class="site-desktopnav-overlay"></div>
            @yield('client_main')
        </main>
        @include('client.layout.footer')
    @endswitch
@else
    @include('client.layout.header')
    <main id="client_main site-main">
        <div class="site-desktopnav-overlay"></div>
        @yield('client_main')
    </main>
    @include('client.layout.footer')
@endif

<div id="shop_menu_bg"></div>
{{--Client JS--}}
{!! Assets::renderFooter() !!}

@include('admin.layout.notification')

@yield('client_script')

</body>
</html>
