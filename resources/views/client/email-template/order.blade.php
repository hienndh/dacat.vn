<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"
          id="viewport">
    <meta content="index, follow" name="ROBOTS">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body {
            font-family: 'Open Sans';
            font-size: 14px;
        }

        .content {
            margin: auto;
            max-width: 815px;
        }

        .header, .footer {
            background: #333;
            height: 60px;
        }

        .title {
            margin-bottom: 25px;
            text-align: center;
            border-bottom: 2px solid #dadada;
        }

        .messages table tr td {
            padding: 0 10px;
        }

        .messages .policy {
            color: #777;
        }

        .messages table tr td:first-child {
            border-right: 1px dashed #bbb;
            width: 60%;
        }

        .text-right {
            text-align: right;
        }

        .c-a2 {
            color: #0ab6a2;
        }

        .font-bold {
            font-weight: bold;
        }

        .title-info {
            margin-bottom: 30px;
        }

        .title-info h2 {
            margin-bottom: 5px;
            margin-top: 40px;
        }

        .box-content-info table.table-product {
            /*padding: 0 10px;*/
            width: 100%;
        }

        .box-content-info table.table-product tr th {
            text-align: left;
        }

        .box-content-info table.table-product tr td.total-price {
            text-align: right;
        }

        .box-content-info table.table-product tr {
            height: 50px;
        }

        .box-content-info table.table-product tr td {
            border-top: 1px solid #d8d8d8;
        }

        .text-center {
            text-align: center;
        }

        table.total-pay {
            background: #f1f1f1;
        }

        table.total-pay tr {
            height: 35px !important;
        }

        .box-content-info table.table-product tr td:first-child, .box-content-info table.table-product tr td:last-child,
        .box-content-info table.table-product tr th:first-child, .box-content-info table.table-product tr th:last-child {
            padding: 0 20px;
        }

        table.total-pay {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        table.total-pay tr td {
            border-top: none !important;
        }

        table.table-info-cus tr {
            height: 40px;
        }

        .info-cus {
            margin-top: 15px;
            margin-bottom: 30px;
        }

        table.table-info-cus tr td:first-child {
            width: 40%;
        }
    </style>
</head>

<body>
<section class="content">
    <div class="header"></div>
    <div class="box-content">
        <div class="title">
            <h2>Order Info</h2>
        </div>
        <div class="messages">
            <table>
                <tr>
                    <td style="width: 60%">
                        <h3 class="font-bold">
                            Thank you for trusting and using our services!
                        </h3>
                        <p class="policy">
                            We contact and deliver the products to you in the shortest time. Thank you again!
                        </p>
                    </td>
                    <td class="text-right">
                        <h3>Store Info</h3>
                        <div>
                            <label for="name-admin" class="font-bold">Store name: </label>
                            <span id="name-admin" class="c-a2">{{$web_setting['ta_company_name']}}</span>
                        </div>
                        <div>
                            <label for="phone-admin" class="font-bold">phone: </label>
                            <span id="phone-admin" class="c-a2">{{$web_setting['ta_company_phone']}}</span>
                        </div>
                        <div>
                            <label for="email-ad" class="font-bold">Email:</label>
                            <span id="email-ad" class="c-a2">{{$web_setting['ta_company_email']}}</span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="info">
            <div class="title-info text-center">
                <h2>Opder ID: <span class="font-bold">#{{$identifier}}</span></h2>
                <div>
                    <label for="time-create">Thời gian khởi tạo:</label>
                    <span id="time-create">{{date('H:i:s d/m/Y', $order_data['created_at'])}}</span>
                </div>
            </div>
            <div class="box-content-info">
                <table class="table-product">
                    <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Price (VND)</th>
                        <th>Quantity</th>
                        <th>Total (VND)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty(Cart::content()))
                        @foreach(Cart::content() as $cart_item)
                            <tr class="product">
                                <td>
                                    {{$cart_item->name}}
                                </td>
                                <td>
                                    {{Helper::getFormattedPrice($cart_item->price)}}
                                </td>
                                <td style="text-align: center;">
                                    {{$cart_item->qty}}
                                </td>
                                <td style="text-align: right;">
                                    {{Helper::getFormattedPrice($cart_item->price * $cart_item->qty)}}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="4" class="text-right" style="border-top: none; padding: 0">
                            <table class="total-pay" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td>Tổng tiền:</td>
                                    <td>{{ Helper::getFormattedPrice($order->total_real)}}</td>
                                </tr>
                                @if(isset($order->order_coupon))
                                    <tr>
                                        <td>Ưu đãi:</td>
                                        <td>-{{$order->order_coupon->value}} {{$order->order_coupon->type == 1 ? 'VND' : '%'}}</td>
                                    </tr>
                                @endif
                                {{--<tr>--}}
                                    {{--<td>Giao nhận hàng:</td>--}}
                                    {{--<td>Miễn phí</td>--}}
                                {{--</tr>--}}
                                <tr>
                                    <td>Payment method</td>
                                    <td>COD</td>
                                </tr>
                                <tr>
                                    <td><span class="font-bold">Total payment:</span></td>
                                    <td><span class="font-bold">{{ Helper::getFormattedPrice($order->total)}}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="info-cus">
                    <table class="table-info-cus" style="width: 100%;">
                        <tbody>
                        <tr>
                            <td><span class="font-bold">Fullname:</span></td>
                            <td>{{$order_data['full_name']}}</td>
                        </tr>
                        <tr>
                            <td><span class="font-bold">Email:</span></td>
                            <td>{{$order_data['email']}}</td>
                        </tr>
                        <tr>
                            <td><span class="font-bold">Phone:</span></td>
                            <td>{{$order_data['phone']}}</td>
                        </tr>
                        <tr>
                            <td><span class="font-bold">Address:</span></td>
                            <td>{{$order_data['address']}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="footer"></div>
</section>
</body>
</html>
