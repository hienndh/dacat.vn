@extends('client.master')
@section('client_css')
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/trinh-dev-css-2.css')}}"/>
@endsection
@section('client_main')
    <div id="shopify-section-static-collection"
         class="shopify-section section-collection customer-page customer-order-page">
        <div class="collection-content">
            <div class="collection-menu-overlay"></div>
            <header class="collection-header">
                <div class="collection-header-inner">
                    <h1 class="collection-title">
                        Customer Information
                    </h1>
                </div>
            </header>
            <div class="collection-container">
                @include('client.layout.sidebar-account', ['account' => $account])
                <div class="collection-content--inner">
                    <div class="collection-grid-uninit">
                        <div class="collection-page-group user-profile__wrap">
                            <div class="wrap__profile__info">
                                <h2 class="title-profile">Orders Manager</h2>
                                <div class="profile-order">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th class="xdot">Order ID</th>
                                                <th>Created at</th>
                                                <th>Last access</th>
                                                <th class="xdot">Total</th>
                                                <th>Status</th>
                                            </tr>
                                            @foreach($listOrder as $order)
                                                <tr>
                                                    <td>
                                                        <div class="xdot code"><a href="{{route('client_account_order_detail_view',['id' => $order->id])}}">{{$order->identifier()}}</a></div>
                                                    </td>
                                                    <td>{{date('d/m/Y',$order->created_at)}}</td>
                                                    <td>{{date('d/m/Y',$order->updated_at)}}</td>
                                                    <td>
                                                        <div class="product__price xdot">{{Helper::getFormattedNumber($order->total)}} <span class="unit">VND</span></div>
                                                    </td>
                                                    <td>
                                                        <div class="status">
                                                            <span class="text-{{$order->status_color()}}">{{$order->status_text()}}</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="6" class="text-center">{{$listOrder->links()}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
