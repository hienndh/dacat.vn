@extends('client.master')
@section('client_css')
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/trinh-dev-css-2.css')}}"/>
@endsection
@section('client_main')
    <div id="shopify-section-static-collection" class="shopify-section section-collection customer-page customer-order-page">
        <div class="collection-content">
            <div class="collection-menu-overlay"></div>
            <header class="collection-header">
                <div class="collection-header-inner">
                    <h1 class="collection-title">
                        Customer Information
                    </h1>
                </div>
            </header>
            <div class="collection-container">
                @include('client.layout.sidebar-account', ['account' => $account])
                <div class="collection-content--inner">
                    <div class="collection-grid-uninit">
                        <div class="collection-page-group user-profile__wrap">
                            <div class="wrap__profile__info">
                                <h2 class="title-profile">Order: {{$order->identifier()}}</h2>
                                <div class="profile-order">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th class="xdot">Product name</th>
                                                <th class="xdot">Price</th>
                                                <th>Quantity</th>
                                                <th class="xdot">Total</th>
                                            </tr>
                                            @foreach($cart_content as $cart_item)
                                                <tr>
                                                    <td>
                                                        <div class="xdot">{{$cart_item->name}}</div>
                                                    </td>
                                                    <td>{{Helper::getFormattedPrice($cart_item->price)}}</td>
                                                    <td>{{$cart_item->qty}}</td>
                                                    <td>
                                                        <div class="product__price xdot">
                                                            {{Helper::getFormattedNumber($cart_item->price * $cart_item->qty)}}<span class="unit">VND</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @if($order && $order->coupon_code && $order->order_coupon)
                                                <tr>
                                                    <td><b>Coupon code</b></td>
                                                    <td colspan="2">{{$order->order_coupon->code}}</td>
                                                    <td><b>-{{$order->order_coupon->type == 1 ? Helper::getFormattedNumber($order->order_coupon->value) : $order->order_coupon->value}}{{$order->order_coupon->type == 1 ? \App\Models\BaseModel::PROUCT_CURRENCY : '%'}}</b></td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td><b>Total order</b></td>
                                                <td></td>
                                                <td>{{$order->quantity}}</td>
                                                <td><b>{{Helper::getFormattedPrice($order->total)}}</b></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
