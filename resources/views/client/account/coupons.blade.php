@extends('client.master')
@section('client_css')
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/trinh-dev-css-2.css')}}"/>
@endsection
@section('client_main')
    <div id="shopify-section-static-collection"
         class="shopify-section section-collection customer-page customer-order-page">
        <div class="collection-content">
            <div class="collection-menu-overlay"></div>
            <header class="collection-header">
                <div class="collection-header-inner">
                    <h1 class="collection-title">
                        Customer Information
                    </h1>
                </div>
            </header>
            <div class="collection-container">
                @include('client.layout.sidebar-account', ['account' => $account])
                <div class="collection-content--inner">
                    <div class="collection-grid-uninit">
                        <div class="collection-page-group user-profile__wrap">
                            <div class="wrap__profile__info">
                                <h2 class="title-profile">Coupon Manager</h2>
                                <div class="profile-order">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th>Coupon code</th>
                                                <th>Status</th>
                                                <th>Start date</th>
                                                <th>End date</th>
                                                <th>Campaign</th>
                                                <th>Value</th>
                                            </tr>
                                            @if(count($listCoupon))
                                                @foreach($listCoupon as $coupon)
                                                    <tr>
                                                        <td>{{$coupon->code}}</td>
                                                        <td align="center">
                                                            <button type="button" class="btn btn-{{$coupon->status == 1 ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$coupon->status == 1 ? 'Hoạt động' : 'Không hoạt động'}}">
                                                                <i class="fa fa-{{$coupon->status == 1 ? 'eye' : 'eye-slash'}}"></i>
                                                            </button>
                                                        </td>
                                                        <td>{{isset($coupon->campaign->startDate) ? date('d/m/Y H:i',$coupon->campaign->startDate) : ''}}</td>
                                                        <td>{{isset($coupon->campaign->endDate) ? date('d/m/Y H:i',$coupon->campaign->endDate) : ''}}</td>
                                                        <td>{{isset($coupon->campaign->name) ? $coupon->campaign->name : ''}}</td>
                                                        <td>{{isset($coupon->campaign) ? Helper::getFormattedNumber($coupon->campaign->value) : ''}}{{isset($coupon->campaign) ? $coupon->campaign->value_type == 1 ? 'đ' : '%' : ''}}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="text-center" colspan="7"><strong>NOT FOUND</strong></td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td colspan="6" class="text-center">{{$listCoupon->links()}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
