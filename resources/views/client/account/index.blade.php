@extends('client.master')
@section('client_css')
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/trinh-dev-css-2.css')}}" />
@endsection
@section('client_main')
    <div id="shopify-section-static-collection" class="shopify-section section-collection customer-page">
        <div class="collection-content">
            <div class="collection-menu-overlay"></div>
            <header class="collection-header">
                <div class="collection-header-inner">
                    <h1 class="collection-title">
                        Customer Information
                    </h1>
                </div>
            </header>
            <div class="collection-container">
                @include('client.layout.sidebar-account', ['account' => $account])
                <div class="collection-content--inner">
                    <div class="collection-grid-uninit">
                        <div class="collection-page-group user-profile__wrap">
                            <div class="wrap__profile__info">
                                <h2 class="title-profile">Information</h2>
                                <form id="info-account" action="{{route('client_update_info_account')}}" method="post">
                                    <div class="profile-content">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="input__label">
                                                    <div class="label">Fullname</div>
                                                </div>
                                                <div class="input__wrap">
                                                    <input type="text" class="form-control" name="full_name" placeholder="Fullname"
                                                           value="{{$account->full_name}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="input__label">
                                                    <div class="label">Email</div>
                                                </div>
                                                <div class="input__wrap">
                                                    <input type="text" class="form-control" name="email" placeholder="Email..."
                                                           value="{{$account->email}}"  required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="input__label">
                                                    <div class="label">Sex:</div>
                                                </div>
                                                <div class="input__wrap">
                                                    <div class="row" style="margin-top: 6px">
                                                        <div class="col-xs-4" style="margin-right: 20px">
                                                            <div class="check__action -radio">
                                                                <input type="radio" {{$account->gender == 1 ? 'checked' : ''}} class="checkbox" name="gender">
                                                                <span class="icon"></span>
                                                                Male
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="check__action -radio">
                                                                <input type="radio" {{$account->gender == 0 ? 'checked' : ''}} class="checkbox" name="gender">
                                                                <span class="icon"></span>
                                                                Female
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="input__label">
                                                    <div class="label">Birthday:</div>
                                                </div>
                                                <div class="input__wrap birthday-picker">
                                                    <select class="birth-day form-control pointer" name="birth[day]">
                                                        <option value="0">Day</option>
                                                        @for($d = 1;$d < 31; $d++)
                                                            <option {{date('d',$account->dob) == $d ? 'selected' : ''}}  value="{{$d}}">{{$d}}</option>
                                                        @endfor
                                                    </select>
                                                    <select class="birth-month form-control pointer" name="birth[month]">
                                                        <option value="0">Month</option>
                                                        @for($m = 1;$m < 12; $m++)
                                                            <option {{date('m',$account->dob) == $m ? 'selected' : ''}} value="{{$m}}">{{$m}}</option>
                                                        @endfor
                                                    </select>
                                                    <select class="birth-year form-control pointer" name="birth[year]">
                                                        <option value="0">Year</option>
                                                        @for($y = 1950;$y < 2019; $y++)
                                                            <option {{date('Y',$account->dob) == $y ? 'selected' : ''}} value="{{$y}}">{{$y}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-submit pxs-image-with-text-button">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
