@extends('client.master')
@section('client_css')
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('client/assets/trinh-dev-css-2.css')}}" />
@endsection
@section('client_main')
    <div id="shopify-section-static-collection" class="shopify-section section-collection customer-page">
        <div class="collection-content">
            <div class="collection-menu-overlay"></div>
            <header class="collection-header">
                <div class="collection-header-inner">
                    <h1 class="collection-title">
                        Customer Information
                    </h1>
                </div>
            </header>
            <div class="collection-container">
                @include('client.layout.sidebar-account', ['account' => $account])
                <div class="collection-content--inner">
                    <div class="collection-grid-uninit">
                        <div class="collection-page-group user-profile__wrap">
                            <div class="wrap__profile__info">
                                <h2 class="title-profile">Change Password</h2>
                                <form id="update-password" method="post" action="{{route('client_update_password_account')}}">
                                    <div class="profile-content">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="input__label">
                                                    <div class="label">Old password</div>
                                                </div>
                                                <div class="input__wrap">
                                                    <input type="password" class="form-control" name="current" placeholder="Old password"
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="input__label">
                                                    <div class="label">New password</div>
                                                </div>
                                                <div class="input__wrap">
                                                    <input type="password" class="form-control" name="new_password" placeholder="New password"
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="input__label">
                                                    <div class="label">Confirm new password</div>
                                                </div>
                                                <div class="input__wrap">
                                                    <input type="password" class="form-control" name="confirm_password" placeholder="Confirm new password"
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-submit pxs-image-with-text-button">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
