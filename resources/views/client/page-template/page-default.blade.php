@extends('client.master')

@section('client_main')
    <div class="container">
        <div class="row">
            <div class="col-12">
                {!! $page_object['content'] !!}
            </div>
        </div>
    </div>
@endsection
