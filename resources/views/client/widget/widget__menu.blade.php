<div class="widget {{$widget_key}}">
    <div class="widget-title">{{$widget_data['widget_title']}}</div>
    <div class="widget-content">
        @if($widget_data['widget_content'])
            <ul class="nav nav-vertical widget-nav">
                @include('client.template.recursive-menu-item', ['list_menu_item' => $widget_data['widget_content'], 'level' => 0, 'recursive_type' => 'vertical_view'])
            </ul>
        @endif
    </div>
</div>
