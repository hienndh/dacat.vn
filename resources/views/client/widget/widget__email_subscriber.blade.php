<div class="widget {{$widget_key}}">
    <div class="widget-title">{{$widget_data['widget_title']}}</div>
    <div class="widget-content">
        <div>
            {!! $widget_data['widget_content']['content'] !!}
        </div>
        <div class="marketing_script">
            <script>
                setTimeout(function () {
                    {!! $widget_data['widget_content']['script'] ?? null !!}
                }, 6000);
            </script>
        </div>
    </div>
    <div class="social_icon">
        @if(isset($web_setting['ta_social_facebook']))
            <a href="{{$web_setting['ta_social_facebook']}}" target="_blank"><img src="{{asset('img/icon-facebook.png')}}"></a>
        @endif
        @if(isset($web_setting['ta_social_youtube']))
            <a href="{{$web_setting['ta_social_youtube']}}" target="_blank"><img src="{{asset('img/icon-youtube.png')}}"></a>
        @endif
        @if(isset($web_setting['ta_social_zalo']))
            <a href="{{$web_setting['ta_social_zalo']}}" target="_blank"><img src="{{asset('img/icon-zalo.png')}}"></a>
        @endif
    </div>
</div>
