<div class="widget {{$widget_key}}">
    <div class="widget-title">{{$widget_data['widget_title']}}</div>
    <div class="widget-content">
        @if(count($widget_data['widget_content']))
            @foreach($widget_data['widget_content'] as $post_item)
                @include('client.template.post-item', ['post_object' => $post_item, 'view_type' => $widget_data['view_type']])
            @endforeach
        @endif
    </div>
</div>
