<div class="widget {{$widget_key}}">
    <div class="widget-title {{$widget_data['widget_title'] ? : 'd-none'}}">{{$widget_data['widget_title']}}</div>
    {{--@if($widget_data['widget_image'])
        <div class="widget-image">
            <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{$widget_data['widget_image']}}" alt="" title="{{$widget_data['widget_title']}}">
        </div>
    @endif--}}
    <div class="widget-content">
        @if($widget_data['widget_content'])
            <div class="row policy_list justify-content-center">
                @foreach($widget_data['widget_content'] as $policy_item)
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="policy_item">
                            @if(strpos($policy_item['icon'], 'uploads') !== false)
                                <div class="policy_icon">
                                    <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{$policy_item['icon']}}" alt="" title="{{$widget_data['widget_title']}}">
                                </div>
                            @else
                                <i class="policy_icon {{$policy_item['icon'] ?? 'fa fa-dot-circle-o'}}" aria-hidden="true"></i>
                            @endif
                            <div class="policy_text">
                                <a href="javascript: void 0;">{{$policy_item['title'] ?? ''}}</a>
                                <p>{{$policy_item['content'] ?? ''}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
