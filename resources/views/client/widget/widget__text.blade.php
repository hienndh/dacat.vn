<div class="widget {{$widget_key}}">
    <div class="widget-title">{{$widget_data['widget_title']}}</div>
    <div class="widget-content">{!! $widget_data['widget_content'] !!}</div>
</div>
