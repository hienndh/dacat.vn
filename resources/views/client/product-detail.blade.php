@extends('client.master')

@section('client_main')
    <div id="shopify-section-static-product" class="shopify-section section-product">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <aside>
                        <div class="mega-left-title">
                            <strong>SẢN PHẨM</strong>
                        </div>
                        <div class="wrapper_vertical_menu">
                            <ul>
                                <?php echo $menu_left ?>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="col-sm-9">
                    <div class="product-wrapper">
                        <section class="product-gallery product-gallery-layout-alternate">
                            <div class="product-images-scrollable">
                                <button class="product-images-scroll-up" type="button">
                                    <svg class="" xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                                        <path fill="currentColor" fill-rule="evenodd" d="M5.138 5.344l.004-.54L.005 9.457.567 10l5.138-4.652L6 5.08l-.29-.27L.57 0 0 .535"/>
                                    </svg>
                                </button>
                                @php
                                    $product_album = $product_object && $product_object['album'] ? json_decode($product_object['album'], true) : [];
                                @endphp
                                <div class="product-images" data-images="{{ $product_object['album'] }}">
                                    @foreach($product_album as $index => $image_item)
                                        @continue($image_item == '')
                                        <figure class="product-image product-image-selected">
                                            <img class="lazyload" data-product-image-index="{{$index}}" src="{{asset('img/placeholder.png')}}" data-src="{{Helper::checkImage($image_item)}}" alt="{{$product_object->slug}}" title="{{$product_object->title}}">
                                        </figure>
                                    @endforeach
                                </div>
                                <button class="product-images-scroll-down" type="button">
                                    <svg class="" xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                                        <path fill="currentColor" fill-rule="evenodd" d="M5.138 5.344l.004-.54L.005 9.457.567 10l5.138-4.652L6 5.08l-.29-.27L.57 0 0 .535"/>
                                    </svg>
                                </button>
                            </div>
                            <figure class="product-selected-image">
                                <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{Helper::checkImage($product_object->image)}}" alt="{{$product_object->slug}}" title="{{$product_object->title}}">
                            </figure>
                            <!--<span class="product-size-guide-button-mobile">Kích thước</span>-->
                        </section>
                        <section class="product-details product-details-relative">
                            <div class="product-info">
                                <h1 class="product-title">{{$product_object->title}}</h1>
                                <div class="product-price">
                        <span class="money">
                            <span class="money mr-2 {{$product_object->regular_price == $product_object->sale_price ? 'd-none' : 'text-strikethrough' }}">
                                {{$product_object->getFormattedPrice('regular_price')}}
                            </span>
                            @if($product_object->regular_price != $product_object->sale_price)
                                <br>
                            @endif
                            <span class="money">{{$product_object->getFormattedPrice('sale_price')}}</span>
                        </span>
                                </div>
                            </div>
                            @if(!$product_object['list_attr_color']->isEmpty())
                                <div class="product-group-thumbs">
                                    @foreach($product_object['list_attr_color'] as $attr)
                                        <a href="javascript:void(0)" class="product-group-thumb" data-product_color_id="{{$attr->id}}">
                                            <figure class="product-group-thumb-wrapper">
                                                <img class="lazyload" src="{{asset('img/placeholder.png')}}" data-src="{{Helper::checkImage($product_object->image)}}" alt="{{$product_object->slug}}" title="{{$product_object->title}}">
                                            </figure>
                                            <div class="product-thumb-color">
                                                <div class="product-color">{{$attr->title}}</div>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                            <form id="client_cart_add_product_form" method="post" action="{{route('client_cart_add_product')}}" accept-charset="UTF-8" class="product-menu" enctype="multipart/form-data">
                                <input type="hidden" name="product_id" value="{{$product_object->id}}">
                                <div class="product-menu-slideout">
                                    <div class="product-menu-slideout-addtocart">
                                        <p class="product-menu-addtocart-failure-message"></p>
                                        <div class="product-options product-options-count-2 product-options-type-radio">
                                            @if(!$product_object['list_attr_color']->isEmpty())
                                                <div class="option color">
                                                    <div class="option-values">
                                                        @foreach($product_object['list_attr_color'] as $attr)
                                                            <div class="option-value">
                                                                <input class="option-value-input" type="radio" name="product_color" id="product_color_{{$attr->id}}" value="{{$attr->id}}">
                                                                <label class="option-value-name">{{$attr->title}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                            @if(!$product_object['list_attr_size']->isEmpty())
                                                <div class="option size">
                                                    <div class="option-values">
                                                        @foreach($product_object['list_attr_size'] as $attr)
                                                            @php
                                                                $sold_out = $attr->getRemainQuantity($product_object['id']) == 0;
                                                            @endphp
                                                            @continue($sold_out)
                                                            <div class="option-value {{$sold_out ? 'option-soldout' : ''}}">
                                                                <input class="option-value-input" type="radio" name="product_size" id="product_size_{{$attr->id}}" value="{{$attr->id}}" {{$sold_out ? 'disabled' : ''}}>
                                                                <label class="option-value-name">{{$attr->title}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="product-menu-buttons">
                                    <div class="product-menu-buttons-content">
                                        <div class="product-menu-buttons-addtocart">
                                            @if($product_object->remain_quantity() > 0)
                                                <button class="product-menu-button btn__addtocart" type="submit">
                                                    <div class="product-menu-button-text-wrapper">
                                                        <span class="product-menu-addtocart">thêm vào giỏ</span>
                                                    </div>
                                                </button>
                                            @else
                                                <button id="BIS_trigger" class="product-back-in-stock-button" type="button">
                                                    <span class="unavailable-text">HẾT HÀNG</span>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="product-rte-wrapper">
                                <div class="product-rte rte">
                                    @if($product_object->description)
                                        <h2 class="product-rte-heading">INFORMATION</h2>
                                        {!! $product_object->description !!}
                                    @endif
                                </div>
                            </div>
                        </section>
                        <section class="product-content" style="margin: 30px 0 0;width: 100%;">
                            @if($product_object->description)
                                <div class="related-products-header" style="padding: 0;">
                                    <h2>Thông tin sản phẩm</h2>
                                </div>

                                <div class="product-content-view">
                                    {!! $product_object->content !!}
                                </div>
                            @endif
                        </section>
                    </div>

                    <section class="section-related-products">
                        <div class="realated-products-container">
                            @if(count($list_related_product))
                                <div class="related-products-header">
                                    <h2>RELATED PRODUCT</h2>
                                </div>

                                <div class="product-highlights-normal-wrapper">
                                    @foreach($list_related_product as $product_item)
                                        @include('client.template.product-item', ['product_object' => $product_item])
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>
@endsection

