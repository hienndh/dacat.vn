@extends('client.master')

@section('client_main')
    <div class="container">
        <form id="Product_Filter_Form" action="{{url()->current()}}" method="GET">
            <div class="row">
                <div id="Product_Sortby" class="col-12">
                    @foreach($sort_by_params as $key => $val)
                        <input type="hidden" name="{{$key}}" value="{{$val}}">
                    @endforeach

                    <div class="form-inline pull-right">
                        <div class="form-group">
                            <label class="mr-2" for="">Sort by:</label>
                            <select class="form-control" name="sort_by" onchange="this.form.submit()">
                                <option value="created_at__desc" {{isset($params['sort_by']) && $params['sort_by'] == 'created_at__desc' ? 'selected' : ''}}>Lastest</option>
                                <option value="sale_price__asc" {{isset($params['sort_by']) && $params['sort_by'] == 'sale_price__asc' ? 'selected' : ''}}>Price low to high</option>
                                <option value="sale_price__desc" {{isset($params['sort_by']) && $params['sort_by'] == 'sale_price__desc' ? 'selected' : ''}}>Price high to low</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="Product_Filter" class="col-12 py-3">
                    <div class="row">
                        <div id="price-filter" class="col-12 col-md-3">
                            <p class="mb-1"><strong>Price</strong></p>
                            <ul class="nav-filter">
                                @if(isset($web_setting['filter_price'] ))
                                    @foreach($web_setting['filter_price'] as $index => $price_filter_item)
                                        <li>
                                            <input type="radio" name="price" id="filter_price_{{$index}}" class="filter_price"
                                                   value="{{$price_filter_item['value']}}" {{isset($params['price']) && $price_filter_item['value'] == $params['price'] ? 'checked' : ''}}>
                                            <label for="filter_price_{{$index}}">{{$price_filter_item['title']}}</label>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        @if(count($list_attribute))
                            @foreach($list_attribute as $parent_attribute)
                                <div id="attr-{{$parent_attribute['slug']}}" class="col-12 col-md-3">
                                    <p class="mb-2"><strong>{{$parent_attribute['title']}}</strong></p>
                                    <ul class="nav-filter {{count($parent_attribute->childAttribute) > 4 ? 'nav-2-column' : ''}} nav-attr-{{$parent_attribute['type']}}">
                                        @foreach($parent_attribute->childAttribute as $child_attribute)
                                            <li>
                                                @if($parent_attribute['type'] == 'text')
                                                    <input type="checkbox" id="attr_{{$child_attribute['id']}}" class="filter_attr attr-text"
                                                           value="{{$child_attribute['id']}}" {{in_array($child_attribute['id'], $params['attr_ids']) ? 'checked' : ''}} />
                                                    <label for="attr_{{$child_attribute['id']}}">{{$child_attribute['title']}}</label>
                                                @else
                                                    <span class="attr-color" style="background-color: {{$child_attribute['content']}};"></span>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </form>

        <div class="row">
            <div id="Product_Cat_Content" class="col-12 col-md-12">
                <div class="row mt-4 cd-product-list">
                    @if(count($list_product))
                        @foreach($list_product as $product_item)
                            <div class="col-12 col-md-4 col-xl-3">
                                @include('client.template.product-item', ['product_object' => $product_item, 'view_type' => 'grid_view'])
                            </div>
                        @endforeach

                        <div class="col-12">
                            {{ $list_product->appends($params)->links('client.layout.pagination', ['list_object' => $list_product]) }}
                        </div>
                    @endif
                </div>
            </div>
            {{--<div id="Product_Sidebar" class="col-12 col-md-3">

            </div>--}}
        </div>

    </div>
@endsection

