@extends('client.master')

@section('client_main')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-5">
                <p class="font-weight-bold font-20 ">ERROR! PAGE NOT FOUND</p>
                <a class="btn btn-primary" href="{{url('/')}}">RETURBN TO HOMEPAGE</a>
            </div>
        </div>
    </div>
@endsection
