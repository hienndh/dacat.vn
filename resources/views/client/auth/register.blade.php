@extends('client.master')

@section('client_main')

<div class="page-content template-account-register">
    <h2 class="template-account-register-title">REGISTER</h2>

    <form method="post" action="{{route('client_auth_register')}}" id="create_customer" accept-charset="UTF-8">
        <div class="input-wrapper">
            <label class="input-label" for="user_name">Username</label>
            <input id="account_user_name" class="input-field " type="text" value="{{old('user_name') ?? null}}" name="user_name" autocomplete="off">
        </div>

        <div class="input-wrapper">
            <label class="input-label" for="email">Email</label>
            <input id="account_email" class="input-field " type="email" value="{{$_GET['email'] ?? old('email') ?? null}}" name="email" autocomplete="off">
        </div>

        <div class="input-wrapper">
            <label class="input-label" for="password">Password</label>
            <input id="account_password" class="input-field " type="password" name="password" autocomplete="off">
        </div>

        <div class="input-wrapper">
            <label class="input-label" for="password_confirmation">Password confirm</label>
            <input id="account_password" class="input-field " type="password" name="password_confirmation" autocomplete="off">
        </div>

        <div class="input-wrapper">
            <label class="input-label" for="full_name">Fullname</label>
            <input id="account_full_name" class="input-field " type="text" value="{{old('full_name') ?? null}}" name="full_name" autocomplete="off">
        </div>

        <div class="input-wrapper">
            <label class="input-label" for="phone">Phone number</label>
            <input id="account_phone" class="input-field " type="text" value="{{old('phone') ?? null}}" name="phone" minlength="10" autocomplete="off">
        </div>

        <div class="input-wrapper">
            <label class="input-label" for="address">Address</label>
            <input id="account_address" class="input-field " type="text" value="{{old('address') ?? null}}" name="address" autocomplete="off">
        </div>

        <div class="input-wrapper">
            <input class="button" type="submit" value="Đăng ký">
        </div>
    </form>

    <span class="account-signin">Have an account? <a href="{{route('client_auth_login_view')}}">Sign up</a></span>
</div>

@endsection
