@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Product_{{ $product_object ? 'Update' : 'Create' }}_Form" action="{{$product_object ? route('admin_product_update') : route('admin_product_create')}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($product_object)
                <input type="hidden" name="product[id]" value="{{$product_object['id']}}">
            @endif
            <div class="row">
                <div id="Product_Content" class="col-12 col-md-8 col-lg-9">
                    {{--NỘI DUNG--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN SẢN PHẨM</h4>
                            <div class="form-group">
                                <label for="product_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="product[title]" id="product_title" aria-describedby="helpProductTitle" placeholder="Nhập tiêu đề sản phẩm"
                                       autocomplete="off" value="{{$product_object ? $product_object['title'] : null}}" onkeyup="getSlug('product_title', 'product_slug', 'product')" required>
                                <small id="helpProductTitle" class="form-text text-muted">Tiêu đề sản phẩm là bắt buộc</small>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="product_slug_pre">{{url('/san-pham/')}}/</span>
                                    </div>
                                    <input type="text" class="form-control" name="product[slug]" id="product_slug" aria-describedby="product_slug_pre"
                                           placeholder="Chuỗi cho đường dẫn tĩnh" autocomplete="off" value="{{$product_object ? $product_object['slug'] : null}}" readonly required>
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button" data-toggle="tooltip" title="Tạo slug tự động từ tiêu đề" onclick="getSlug('product_title', 'product_slug', 'product')">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="product_content">Nội dung</label>
                                <textarea class="form-control mce_editor mce_editor_content" name="product[content]" id="product_content" rows="3">{{$product_object ? $product_object['content'] : null}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="product_description">Mô tả ngắn</label>
                                <textarea class="form-control mce_editor_description" name="product[description]" id="product_description" rows="3">{{$product_object ? $product_object['description'] : null}}</textarea>
                            </div>
                        </div>
                    </div>
                    {{--DỮ LIỆU SẢN PHẨM--}}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="card-title font-bold">DỮ LIỆU SẢN PHẨM</h4>
                                </div>
                                {{--SKU - GIÁ - SỐ LƯỢNG--}}
                                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group form-inline">
                                        <label for="product_sku">Mã SKU:</label>
                                        <input type="text" name="product[sku]" id="product_sku" class="form-control ml-3"
                                               value="{{$product_object ? $product_object['sku'] : null}}" autocomplete="off">
                                    </div>
                                    <div class="form-group form-inline">
                                        <div class="input-group">
                                            <label for="product_regular_price">Khuyến mãi:</label>
                                            <input type="number" name="product[sale_number]" id="product_sale_number" class="form-control ml-3"
                                                   min="0" max="999999999" step="0.1" onchange="calSalePrice('product_sale_type', 'product_sale_number', 'product_regular_price', 'product_sale_price')"
                                                   value="{{$product_object ? $product_object['sale_number'] : 0}}" autocomplete="off">
                                            <div class="input-group-append">
                                                <select class="form-control" name="product[sale_type]" id="product_sale_type" onchange="calSalePrice('product_sale_type', 'product_sale_number', 'product_regular_price', 'product_sale_price')">
                                                    <option value="percent" {{$product_object && $product_object['sale_type'] == 'percent' ? 'selected' : ''}}>%</option>
                                                    <option value="currency" {{$product_object && $product_object['sale_type'] == 'currency' ? 'selected' : ''}}>VND</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group form-inline">
                                        <div class="input-group">
                                            <label for="product_regular_price">Giá bán thường:</label>
                                            <input type="number" name="product[regular_price]" id="product_regular_price" class="form-control ml-3"
                                                   min="0" max="999999999" step="1" onchange="calSalePrice('product_sale_type', 'product_sale_number', 'product_regular_price', 'product_sale_price')"
                                                   value="{{$product_object ? $product_object['regular_price'] : 0}}" autocomplete="off">
                                            <div class="input-group-append">
                                                <button class="btn btn-default" type="button">
                                                    đ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline">
                                        <div class="input-group">
                                            <label for="product_sale_price">Giá khuyến mại:</label>
                                            <input type="number" name="product[sale_price]" id="product_sale_price" class="form-control ml-3"
                                                   min="0" max="999999999" step="1" readonly
                                                   value="{{$product_object ? $product_object['sale_price'] : 0}}" autocomplete="off">
                                            <div class="input-group-append">
                                                <button class="btn btn-default" type="button">
                                                    đ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    {{--SEO--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">CẤU HÌNH SEO</h4>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="product_seo_title">SEO Tiêu đề</label>
                                <input type="text" name="product[seo_title]" id="product_seo_title" class="form-control col-9 col-xs-9 col-md-10" maxlength="70"
                                       placeholder="" value="{{$product_object ? $product_object['seo_title'] : null}}" autocomplete="off">
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="product_seo_title">SEO Mô tả</label>
                                <textarea name="product[seo_description]" id="product_seo_description" class="form-control col-9 col-xs-9 col-md-10" rows="3"
                                          maxlength="170" placeholder="" autocomplete="off">{{$product_object ? $product_object['seo_description'] : null}}</textarea>
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="product_seo_title">SEO Từ khóa</label>
                                <div class="col-9 col-xs-9 col-md-10 p-0">
                                    <input type="text" name="product[seo_keyword]" id="product_seo_keyword" class="form-control" data-role="tagsinput"
                                           placeholder="" value="{{$product_object ? $product_object['seo_keyword'] : null}}" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Product_Sidebar" class="col-12 col-md-4 col-lg-3">
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <div class="form-group">
                                <label for="product_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="product[status]" id="product_status" {{$product_object && $product_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info">{{$product_object ? 'Cập nhật' : 'Đăng bài'}}</button>
                            @if($product_object)
                                <a href="{{route('client_product_detail_view', ['slug' => $product_object['slug']])}}" class="btn btn-success pull-right" target="_blank">Xem</a>
                            @endif
                        </div>
                    </div>
                    {{--ẢNH ĐẠI DIỆN--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.template.image_preview', ['input_name' => 'product[image]', 'input_id' => 'product_image', 'input_image' => ($product_object ? $product_object['image'] : null)])
                        </div>
                    </div>
                    {{--ALBUM--}}
                    <div class="card product_album">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ALBUM</h4>
                            <div class="row">
                                @php
                                    $product_album = $product_object && $product_object['album'] ? json_decode($product_object['album'], true) : [];
                                @endphp
                                @for($i=0;$i<9;$i++)
                                    <div class="col-6 col-md-4">
                                        @include('admin.template.image_preview', ['input_name' => 'product[album][]', 'input_id' => 'product_album_image_' . $i, 'input_image' => ($product_object && $product_object['album'] ? $product_album[$i] : null)])
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                    {{--DANH MỤC SẢN PHẨM--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">DANH MỤC SẢN PHẨM</h4>
                            <div class="cat_list_wrapper product_cat_list">
                                <ul class="list-style-none">
                                    @include('admin.template.recursive_category', ['list_cat' => $list_cat, 'level' => 0, 'recursive_type' => 'checkbox', 'type' => 'product', 'cat_ids' => ($product_object ? $product_object['cat_ids'] : [])])
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/javascript">
        function calSalePrice(sale_type_selector, sale_number_id, regular_price_id, sale_price_id) {
            let sale_type = $('#' + sale_type_selector).val();
            let sale_number = $('#' + sale_number_id).val();
            let regular_price = $('#' + regular_price_id).val();
            let sale_price = 0;
            if (sale_type === 'percent') {
                sale_price = regular_price - ((regular_price / 100) * sale_number);
            } else {
                sale_price = regular_price - sale_number;
            }

            $('#' + sale_price_id).val(sale_price);
        }

        $(document).ready(function () {
            if ($(".mce_editor").length > 0) {
                admin_tinymce('mce_editor', 400);
            }

            admin_tinymce('mce_editor_description', 300);
        });
    </script>
@endsection
