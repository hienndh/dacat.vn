@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-info" onclick="location.href = '{{ route("admin_product_new_view") }}'">Thêm mới</button>
                        <div class="pull-right">
                            <form class="form-inline" action="{{route('admin_product_list_view')}}" method="GET">
                                <div class="form-group mr-2">
                                    <label for="order_status">Danh mục: </label>
                                    <select class="form-control custom-select ml-2" name="cat_id">
                                        <option value="0" {{isset($params['cat_id']) && isset($params['cat_id']) == 0 ? 'selected' : ''}}>-- Để trống --</option>
                                        @include('admin.template.recursive_category', ['list_cat' => $list_cat, 'level' => 0, 'recursive_type' => 'select', 'selected_cat_id' => (isset($params['cat_id']) ? $params['cat_id'] : 0)])
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="postTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="postTable" class="table table-bordered" style="min-width: 1200px;">
                                    <thead>
                                    <tr>
                                        <th>SKU</th>
                                        <th>Ảnh</th>
                                        <th>Tiêu đề</th>
                                        <th>Giá</th>
                                        <th>Danh mục</th>
                                        <th>Tác giả</th>
                                        <th width="100px">Trạng thái</th>
                                        <th width="180px">Ngày tạo</th>
                                        <th width="180px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_product))
                                        @foreach($list_product as $product_item)
                                            <tr>
                                                <td>{{$product_item['sku']}}</td>
                                                <td><img height="30" class="image_preview" src="{{$product_item['image'] ? $product_item['image'] : asset('img/placeholder.png')}}" alt="{{$product_item['title']}}" title="{{$product_item['title']}}"></td>
                                                <td width="300px"><a href="{{route('admin_product_update_view', ['product_id' => $product_item['id']])}}" class="text-dark">{{$product_item['title']}}</a></td>
                                                <td class="text-right">
                                                    @if($product_item['regular_price'] == $product_item['sale_price'])
                                                        {{$product_item->getFormattedPrice('regular_price')}}
                                                    @else
                                                        <s>{{$product_item->getFormattedPrice('regular_price')}}</s><br>
                                                        {{$product_item->getFormattedPrice('sale_price')}}
                                                    @endif
                                                </td>
                                                <td width="300px">{{$product_item['list_cat']}}</td>
                                                <td>{{$product_item->author->first_name ?? ''}}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-{{$product_item['status'] ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$product_item['status'] ? 'Hiện' : 'Ẩn'}}">
                                                        <i class="fa fa-{{$product_item['status'] ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>{{$product_item->getFormattedDate('created_at')}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_product_update_view', ['product_id' => $product_item['id']])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    @if($product_item['status'])
                                                        <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-product_id="{{$product_item['id']}}" data-toggle="tooltip" title="Xoá">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    @endif
                                                    <a href="{{route('admin_repository_update_view', ['product_id' => $product_item['id']])}}" class="btn btn-primary btn-rounded btn-sm" data-toggle="tooltip" title="Xem kho" target="_blank">
                                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="{{route('client_product_detail_view', ['slug' => $product_item['slug']])}}" class="btn btn-info btn-rounded btn-sm" data-toggle="tooltip" title="Xem" target="_blank">
                                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="9"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                        @if(count($list_product))
                            {{ $list_product->appends($params)->links('admin.layout.pagination', ['list_object' => $list_product]) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let product_id = $(this).attr("data-product_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_product_delete')}}",
                        data: {
                            id: product_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection

