@extends('admin.master')

@section('admin_main')
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{$web_setting['ta_login_background'] ?? asset('admin/assets/images/background/login-register.jpg')}});">
            {{--<video id="bg_login_video" autoplay muted loop width="1280" height="720">--}}
                {{--<source type="video/mp4" src="{{asset('admin/assets/images/background/login-bg.mp4')}}">--}}
                {{--<source type="video/webm" src="{{asset('admin/assets/images/background/login-bg.mp4')}}">--}}
                {{--<source type="video/ogg" src="{{asset('admin/assets/images/background/login-bg.mp4')}}">--}}
            {{--</video>--}}

            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" action="{{route('admin_auth_login')}}" method="post">
                        {{csrf_field()}}
                        <h3 class="box-title m-b-20 text-center">Đăng nhập</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control px-3" type="text" name="user_name" required="" placeholder="Tên đăng nhập hoặc email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control px-3" type="password" name="password" required="" placeholder="Mật khẩu">
                            </div>
                        </div>
                        <div class="form-group row d-none">
                            <div class="col-md-12 font-14">
                                <div class="checkbox checkbox-primary pull-left p-t-0">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">Ghi nhớ mật khẩu</label>
                                </div>
                                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><!-- <i class="fa fa-lock m-r-5"></i> --> Quên mật khẩu?</a>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Đăng nhập</button>
                            </div>
                        </div>
                        {{--<div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                <div class="social">
                                    <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                    <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>
                                </div>
                            </div>
                        </div>--}}
                        <div class="form-group m-b-0 d-none">
                            <div class="col-sm-12 text-center">
                                <div>Bạn chưa có tài khoản? <a href="{{route('admin_auth_register')}}" class="text-info m-l-5"><b>Đăng ký ngay!</b></a></div>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal" id="recoverform" action="{{route('admin_auth_recover')}}">
                        {{csrf_field()}}
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <h3>Quên mật khẩu</h3>
                                <p class="text-muted">Nhập địa chỉ email của bạn và chỉ dẫn sẽ được gửi cho bạn!</p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Lấy lại mật khẩu</button>

                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <a href="javascript:void(0)" id="to-login" class="text-dark"><i class="fa fa-backward m-r-5"></i> Quay lại đăng nhập</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
@endsection

@section('admin_css')
    <style>
        .login-box {
            margin: 0 auto 0 25px !important;
        }
    </style>
@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
@endsection
