@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Order_{{ $order_object ? 'Update' : 'Create' }}_Form" action="{{$order_object ? route('admin_order_update') : route('admin_order_create')}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($order_object)
                <input type="hidden" name="order[id]" value="{{$order_object['id']}}">
            @endif
            <div class="row">
                <div id="Order_Content" class="col-12 col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN ĐƠN HÀNG: {{$order_object ? '#' . $order_object->identifier() : ''}} </h4>

                            <div class="table-responsive">
                                <div class="dataTables_wrapper no-footer">
                                    <table class="table table-bordered" style="min-width: 700px;">
                                        <thead>
                                        <tr>
                                            <th>Ảnh</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Số lượng</th>
                                            <th>Đơn giá</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($cart_content))
                                            @foreach($cart_content as $cart_item)
                                                <tr class="cart-item-{{$cart_item->rowId}}">
                                                    <td width="120px">
                                                        <img width="120" class="lazyload cart-item-image" src="{{$cart_item->model->image ?? asset('img/placeholder.png')}}" data-src="{{$cart_item->model->image ?? asset('img/placeholder.png')}}" alt="">
                                                    </td>
                                                    <td>
                                                        {{$cart_item->name}}{{$cart_item->options->has('color') && $cart_item->options->color ? ' - ' . $cart_item->options->color : ''}}{{$cart_item->options->has('size') && $cart_item->options->size ? ' - ' . $cart_item->options->size : ''}}
                                                    </td>
                                                    <td>{{$cart_item->qty}}</td>
                                                    <td class="text-right">{{Helper::getFormattedPrice($cart_item->price)}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Tổng phụ</strong></td>
                                            <td class="text-right">{{$order_object ? $order_object->getFormattedPrice('total_real') : 0}}</td>
                                        </tr>
                                        @if($order_object && $order_object->coupon_code && $order_object->order_coupon)
                                            <tr>
                                                <td colspan="3" class="text-right"><strong>Mã khuyến mãi ({{$order_object->coupon_code}})</strong></td>
                                                <td class="text-right">-{{$order_object->order_coupon->type == 1 ? Helper::getFormattedNumber($order_object->order_coupon->value) : $order_object->order_coupon->value}}{{$order_object->order_coupon->type == 1 ? \App\Models\BaseModel::PROUCT_CURRENCY : '%'}}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Tổng</strong></td>
                                            <td class="text-right font-weight-bold">{{$order_object ? $order_object->getFormattedPrice('total') : 0}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Order_Sidebar" class="col-12 col-md-4">
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN KHÁCH HÀNG</h4>
                            <div class="form-group">
                                <label for="order_full_name">Khách hàng:</label>
                                <input type="text" name="order[full_name]" id="order_full_name" class="form-control" value="{{$order_object ? $order_object['full_name'] : null}}" required>
                            </div>

                            <div class="form-group">
                                <label for="order_email">Email:</label>
                                <input type="email" name="order[email]" id="order_email" class="form-control" value="{{$order_object ? $order_object['email'] : null}}" required>
                            </div>

                            <div class="form-group">
                                <label for="order_phone">Số điện thoại:</label>
                                <input type="text" name="order[phone]" id="order_phone" class="form-control" value="{{$order_object ? $order_object['phone'] : null}}" required>
                            </div>

                            <div class="form-group">
                                <label for="order_address">Địa chỉ:</label>
                                <input type="text" name="order[address]" id="order_address" class="form-control" value="{{$order_object ? $order_object['address'] : null}}" required>
                            </div>

                            <div class="form-group">
                                <label for="">Tỉnh/Thành phố:</label>
                                <select class="form-control" name="order[province_id]" id="order_province_id">
                                    @foreach($listCity as $city)
                                        <option value="{{$city->id}}" {{$order_object && $city->id == $order_object['province_id'] ? 'selected' : ''}}>{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="order_note">Ghi chú:</label>
                                <textarea name="order[note]" id="order_note" class="form-control">{{$order_object ? $order_object['note'] : null}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="order_status">Trạng thái: </label>
                                <select name="order[status]" id="order_status" class="form-control">
                                    @foreach(\App\Models\BaseModel::ORDER_STATUS as $order_status_key => $order_status_name)
                                        <option value="{{$order_status_key}}" {{$order_object && $order_object['status'] == $order_status_key ? 'selected' : ''}}>{{$order_status_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-info">{{$order_object ? 'Cập nhật' : 'Tạo mới'}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
