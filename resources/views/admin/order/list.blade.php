@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        {{--<button type="button" class="btn btn-info" onclick="location.href = '{{ route("admin_order_new_view") }}'">Thêm mới</button>--}}
                        <div class="pull-right">
                            <form action="{{route('admin_order_list_view')}}" method="GET">
                                <div class="form-inline">
                                    <div class="form-group mr-2">
                                        <label for="order_status">Trạng thái: </label>
                                        <select name="status" id="order_status" class="form-control ml-2">
                                            <option value="all" {{isset($params['status']) && $params['status'] == 'all' ? 'selected' : ''}}>Tất cả</option>
                                            @foreach(\App\Models\BaseModel::ORDER_STATUS as $order_status_key => $order_status_name)
                                                <option value="{{$order_status_key}}" {{isset($params['status']) && $params['status'] == $order_status_key ? 'selected' : ''}}>{{$order_status_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="orderTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="orderTable" class="table table-bordered" style="min-width: 1200px;">
                                    <thead>
                                    <tr>
                                        <th>Mã đơn hàng</th>
                                        <th>Khách hàng</th>
                                        <th>Email</th>
                                        <th>Điện thoại</th>
                                        <th>Địa chỉ</th>
                                        <th>Số lượng</th>
                                        <th>Tổng phụ</th>
                                        <th>Thành tiền</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Ghi chú</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_order))
                                        @foreach($list_order as $order_item)
                                            <tr>
                                                <td><a href="{{route('admin_order_update_view', ['order_id' => $order_item['id']])}}" class="text-dark">#{{$order_item->identifier()}}</a></td>
                                                <td>{{$order_item['full_name']}}</td>
                                                <td>{{$order_item['email']}}</td>
                                                <td>{{$order_item['phone']}}</td>
                                                <td width="200px"> {{$order_item['address']}}</td>
                                                <td width="100px">{{$order_item['quantity']}}</td>
                                                <td>{{$order_item->getFormattedPrice('total_real')}}</td>
                                                <td>{{$order_item->getFormattedPrice('total')}}</td>
                                                <td>{{$order_item->getFormattedDate('created_at')}}</td>
                                                <td width="100px">
                                                    <button type="button" class="btn btn-{{$order_item->status_color()}} btn-sm">
                                                        {{$order_item->status_text()}}
                                                    </button>
                                                </td>
                                                <td width="200px">{{$order_item['note']}}</td>
                                                <td width="100px">
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_order_update_view', ['order_id' => $order_item['id']])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    {{--<button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-order_id="{{$order_item['id']}}" data-toggle="tooltip" title="Xoá">
                                                        <i class="fa fa-trash"></i>
                                                    </button>--}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="9"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                        @if(count($list_order))
                            {{ $list_order->appends($params)->links('admin.layout.pagination', ['list_object' => $list_order]) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let order_id = $(this).attr("data-order_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_order_delete')}}",
                        data: {
                            id: order_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection

