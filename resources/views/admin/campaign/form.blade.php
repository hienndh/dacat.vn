@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Campaign_{{ isset($campaign->id) ? 'Update' : 'Create' }}_Form" class="Campaign_Form"
              action="{{ isset($campaign->id) ? route('admin_campaign_update') : route('admin_campaign_create') }}"
              method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @if(isset($campaign->id))
                <input type="hidden" name="campaign[id]" value="{{$campaign->id}}">
            @endif
            <div class="row">
                <div class="card" style="width: 100%">
                    <div class="card-body">
                        <div class="row form-group">
                            <label class="col-3" for="campaign_name">Tên chiến dịch<span class="text-danger"> *</span></label>
                            <div class="col-9">
                                <input id="campaign_name" class="form-control" name="campaign[name]" required
                                       placeholder="Tên chiến dịch"
                                       value="{{isset($campaign->name) ? $campaign->name : ''}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-3" for="campaign_name">Giá trị khuyến mãi<span class="text-danger"> *</span></label>
                            <div class="col-9">
                                <div class="input-group">
                                    <input id="campaign_value" class="form-control" name="campaign[value]" type="number" onchange="checkValue()" onkeyup="checkValue()"
                                           placeholder="Giá trị coupon" value="{{isset($campaign->value) ? $campaign->value : ''}}" required>
                                    <div class="input-group-prepend">
                                        <select name="campaign[value_type]" id="campaign_value_type" class="form-control">
                                            <option value="1" {{isset($campaign->value_type) && $campaign->value_type == 1 ? 'selected' : ''}} >
                                                đ
                                            </option>
                                            <option value="2" {{isset($campaign->value_type) && $campaign->value_type == 2 ? 'selected' : ''}} >
                                                %
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-3" for="campaign_time">Thời gian chiến dịch</label>
                            <div class="col-3">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;text-align: center">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span></span> <i class="fa fa-caret-down"></i>
                                </div>
                                <input class="d-none" type="hidden" id="startDate" name="campaign[startDate]" value="{{isset($campaign->startDate) ? $campaign->startDate : ''}}">
                                <input class="d-none" type="hidden" id="endDate" name="campaign[endDate]" value="{{isset($campaign->endDate) ? $campaign->endDate : ''}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-3" for="campaign_time">Số lượng coupon(đã dùng/còn lại))</label>
                            <div class="col-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon3">{{isset($campaign->total_used) ? $campaign->total_used : 0}} / </span>
                                    </div>
                                    <input type="number" min="{{isset($campaign->total_used) ? $campaign->total_used : 0}}" name="campaign[total]" class="form-control" id="campaign_total" value="{{isset($campaign->total) ? $campaign->total : 0}}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-3">Ảnh đại diện</label>
                            <div class="col-9">
                                @include('admin.template.image_preview', ['input_name' => 'campaign[image]', 'input_id' => 'campaign_image', 'input_image' => (isset($campaign->image) ? $campaign->image : null)])
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-3" for="campaign_time">Loại</label>
                            <div class="col-9">
                                <select name="campaign[type]" id="campaign_type" class="form-control">
                                    <option value="1" {{isset($campaign->type) && $campaign->type == 1 ? 'selected' : ''}}>Tri ân</option>
                                    <option value="2" {{isset($campaign->type) && $campaign->type == 2 ? 'selected' : ''}}>Chiến dịch</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-3" for="campaign_time">Trạng thái chiến dịch</label>
                            <div class="col-9">
                                <div class="form-group">
                                    <label for="campaign_status">Trạng thái: </label>
                                    <div class="switch">
                                        <label>Ẩn<input type="checkbox" name="campaign[status]" id="campaign_status" {{(isset($campaign) && $campaign->status) == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group pull-right">
                            <button type="submit" id="action" class="btn btn-primary">{{ !isset($campaign->id) ? 'Tạo chiến dịch' : 'Cập nhật chiến dịch' }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.min.js"></script>

    <script>
        $(function () {
            var startDate = "{{isset($campaign->startDate) && $campaign->startDate ? date('Y-m-d',$campaign->startDate) : null}}";
            var endDate = "{{isset($campaign->endDate) && $campaign->endDate ? date('Y-m-d',$campaign->endDate) : null}}";
            var start = _.isEmpty(startDate) ? moment() : moment(startDate);
            var end = _.isEmpty(endDate) ? moment().add(7, 'days') : moment(endDate);

            function cb(start, end) {
                $('#startDate').val(start.unix());
                $('#endDate').val(end.unix());
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                locale: {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Chọn",
                    "cancelLabel": "Hủy",
                    "fromLabel": "Từ",
                    "toLabel": "Đến",
                    "customRangeLabel": "Tùy chọn",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Tháng 1",
                        "Tháng 2",
                        "Tháng 3",
                        "Tháng 4",
                        "Tháng 5",
                        "Tháng 6",
                        "Tháng 7",
                        "Tháng 8",
                        "Tháng 9",
                        "Tháng 10",
                        "Tháng 11",
                        "Tháng 12"
                    ],
                    "firstDay": 1
                },
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                    '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
        });

        function submitForm(form) {

        }

        function checkValue() {
            if ($('#campaign_value_type').val() == 2 && $('#campaign_value').val() > 100) {
                toastError('Giá trị không hợp lệ');
                $('#action').attr('disabled', disabled);
            }
        }

        $(document).ready(function () {
            $('form.Campaign_Form').on('submit', function (e) {
                if ($('#campaign_value_type').val() == 2 && $('#campaign_value').val() > 100) {
                    toastError('Giá trị không hợp lệ');
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endsection
