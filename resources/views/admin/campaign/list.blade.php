@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row form-group">
                            <div class="col-3">
                                <button type="button" onclick="location.href = '{{ route("admin_campaign_new_view") }}'" class="btn btn-info">Thêm mới</button>
                            </div>
                            <div class="col-9">
                                <form action="{{route('admin_campaign_list_view')}}" method="GET">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-4">
                                            <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;text-align: center">
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input class="d-none" id="startDate" name="startDate" value="{{isset($params['startDate']) ? $params['startDate'] : ''}}">
                                            <input class="d-none" id="endDate" name="endDate" value="{{isset($params['endDate']) ? $params['endDate'] : ''}}">
                                        </div>
                                        <div class="col-6 pull-right">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                                <div class="input-group-append">
                                                    <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <div id="postTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="couponTable" class="table table-bordered" style="min-width: 1000px;">
                                    <thead>
                                    <tr>
                                        <th>Ảnh</th>
                                        <th>Tên chiến dịch</th>
                                        <th>Giá trị</th>
                                        <th>Đã dùng</th>
                                        <th>Ngày bắt đầu</th>
                                        <th>Ngày kết thúc</th>
                                        <th>Loại chiến dịch</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($listCampaign))
                                        @foreach($listCampaign as $campaign)
                                            <tr>
                                                <td><img height="30" class="image_preview" src="{{$campaign->image ? $campaign->image : asset('img/placeholder.png')}}" alt="{{$campaign->name}}" title="{{$campaign->name}}"></td>
                                                <td>{{$campaign->name}}</td>
                                                <td>{{Helper::getFormattedNumber($campaign->value)}}{{$campaign->value_type == 1 ? 'đ' : '%'}}</td>
                                                <td>{{$campaign->total_used }}/{{$campaign->total}}</td>
                                                <td>{{date('d/m/Y H:i',$campaign->startDate)}}</td>
                                                <td>{{date('d/m/Y H:i',$campaign->endDate)}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-{{$campaign->type_color()}} btn-sm">
                                                        {{$campaign->type_text()}}
                                                    </button>
                                                </td>
                                                <td align="center">
                                                    <button type="button" class="btn btn-{{$campaign->status == 1 ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$campaign->status == 1 ? 'Hoạt động' : 'Không hoạt động'}}">
                                                        <i class="fa fa-{{$campaign->status == 1 ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_campaign_update_view', ['campaign_id' => $campaign->id])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-campaign_id="{{$campaign->id}}" data-toggle="tooltip" title="Xoá">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <div class="row form-group pull-right">
                                            {{$listCampaign->links()}}
                                        </div>
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="8"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let camp_id = $(this).attr("data-campaign_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_campaign_delete')}}",
                        data: {
                            id: camp_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
        $(function () {
            var startDate = "{{$params['startDate'] ? date('Y-m-d',$params['startDate']) : null}}";
            var endDate = "{{$params['endDate'] ? date('Y-m-d',$params['endDate']) : null}}";
            var start = _.isEmpty(startDate) ? moment().subtract(29, 'days') : moment(startDate);
            var end = _.isEmpty(endDate) ? moment() : moment(endDate);


            function cb(start, end) {
                $('#startDate').val(start.unix());
                $('#endDate').val(end.unix());

                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start.format('d/m/Y'),
                endDate: end.format('d/m/Y'),
                locale: {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Chọn",
                    "cancelLabel": "Hủy",
                    "fromLabel": "Từ",
                    "toLabel": "Đến",
                    "customRangeLabel": "Tùy chọn",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Tháng 1",
                        "Tháng 2",
                        "Tháng 3",
                        "Tháng 4",
                        "Tháng 5",
                        "Tháng 6",
                        "Tháng 7",
                        "Tháng 8",
                        "Tháng 9",
                        "Tháng 10",
                        "Tháng 11",
                        "Tháng 12"
                    ],
                    "firstDay": 1
                },
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                    '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);

        });
    </script>
@endsection

