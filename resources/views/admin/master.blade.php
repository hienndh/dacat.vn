<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{$web_setting['ta_seo_description']}}">
    <meta name="author" content="{{$web_setting['ta_seo_description']}}">
    <meta name="keywords" content=">{{$web_setting['ta_seo_keyword']}}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{$web_setting['ta_favicon'] ?: asset('admin/assets/images/favicon.png')}}">
    <title>Admin Dashboard | {{$web_setting['ta_seo_title']}}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{asset('admin/assets/plugins/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{asset('admin/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/plugins/select2/dist/css/select2.min.css')}}">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

@yield('admin_css')

<!-- Custom CSS -->
    <link href="{{asset('admin/main/css/style.css')}}" rel="stylesheet">

    <!-- CuongDev CSS -->
    <link href="{{asset('admin/main/css/cuongdev.css')}}" rel="stylesheet">

    <!-- You can change the theme colors from here -->
    <link href="{{asset('admin/main/css/colors/blue.css')}}" id="theme" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-sidebar fix-header card-no-border mini-sidebar">

@if(isset($layout) && $layout == "LANDING")
    @yield('admin_main')
@else
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
    @include('admin.layout.header')

    @include('admin.layout.sidebar')

    <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">

            @yield('admin_main')

            @include('admin.layout.footer')

        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
@endif


<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset('admin/assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('admin/assets/plugins/bootstrap/js/popper.min.js')}}"></script>

<script src="{{asset('admin/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('admin/main/js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('admin/main/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('admin/main/js/sidebarmenu.js')}}"></script>
<!--stickey kit -->
<script src="{{asset('admin/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('admin/main/js/custom.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--sparkline JavaScript -->
<script src="{{asset('admin/assets/plugins/sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<!--morris JavaScript -->
<script src="{{asset('admin/assets/plugins/raphael/raphael-min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/morrisjs/morris.min.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('admin/assets/plugins/toast-master/js/jquery.toast.js')}}"></script>
<script src="{{asset('admin/main/js/toastr.js')}}"></script>
<!-- Sweet-Alert  -->
<script src="{{asset('admin/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="{{asset('admin/assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>

<script>
    $('.select2').select2();
</script>

@include('admin.layout.notification')

@yield('admin_script')
</body>
</html>
