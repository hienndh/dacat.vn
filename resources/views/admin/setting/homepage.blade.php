@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Homeage_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">Banner chính</h4>
                            <div class="form-group">
                                <label for="">Hình ảnh</label>
                                @include('admin.template.image_preview', ['input_name' => 'setting[home_banner_1][image]', 'input_id' => 'setting_home_banner_1_image', 'input_image' => ($web_setting['home_banner_1']['image'] ?? null)])
                            </div>

                            <div class="form-group">
                                <label for="">Nội dung</label>
                                <textarea class="form-control mce_editor" name="setting[home_banner_1][text]" id="setting_home_banner_1_text" cols="30" rows="10">{{$web_setting['home_banner_1']['text'] ?? ''}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Nút điều hướng</label>
                                <input type="text" name="setting[home_banner_1][button]" id="setting_home_banner_1_button" class="form-control" placeholder="" value="{{$web_setting['home_banner_1']['button'] ?? ''}}" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="">Link</label>
                                <input type="text" name="setting[home_banner_1][link]" id="setting_home_banner_1_link" class="form-control" placeholder="" value="{{$web_setting['home_banner_1']['link'] ?? '#'}}" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    {{--SẢN PHẨM MỚI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">Danh sách sản phẩm mới</h4>

                            <div class="form-group">
                                <label for="">Tiêu đề</label>
                                <input type="text" name="setting[home_new_product][title]" id="setting_home_new_product_title" class="form-control" placeholder=""
                                       value="{{isset($web_setting['home_new_product']) ? $web_setting['home_new_product']['title'] : 'WHATS NEW'}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="post_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="setting[home_new_product][status]" id="setting_home_new_product_status" {{isset($web_setting['home_new_product']) && isset($web_setting['home_new_product']['status']) && $web_setting['home_new_product']['status'] == 'on' ? 'checked' : ''}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--SẢN PHẨM BÁN CHẠY--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">Danh sách sản phẩm</h4>

                            <div class="form-group">
                                <label for="">Tiêu đề</label>
                                <input type="text" name="setting[home_archive_product][title]" id="setting_home_archive_product_title" class="form-control" placeholder=""
                                       value="{{isset($web_setting['home_archive_product']) ? $web_setting['home_archive_product']['title'] : 'PRODUCTS'}}" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="post_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="setting[home_archive_product][status]" id="setting_home_archive_product_status" {{isset($web_setting['home_archive_product']) && isset($web_setting['home_archive_product']['status']) && $web_setting['home_archive_product']['status'] == 'on' ? 'checked' : ''}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--SẢN PHẨM KHUYẾN MÃI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">Danh sách sản phẩm khuyến mãi</h4>

                            <div class="form-group">
                                <label for="">Tiêu đề</label>
                                <input type="text" name="setting[home_best_sale_product][title]" id="setting_home_best_sale_product_title" class="form-control" placeholder=""
                                       value="{{isset($web_setting['home_best_sale_product']) ? $web_setting['home_best_sale_product']['title'] : 'SALE PRODUCTS'}}" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="post_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="setting[home_best_sale_product][status]" id="setting_home_best_sale_product_status" {{isset($web_setting['home_best_sale_product']) && isset($web_setting['home_best_sale_product']['status']) && $web_setting['home_best_sale_product']['status'] == 'on' ? 'checked' : ''}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--ALBUM INSTAGRAM--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ALBUM INSTAGRAM</h4>

                            <div class="form-group">
                                <label for="">Tiêu đề</label>
                                <input type="text" name="setting[home_instagram_album][title]" id="setting_home_instagram_album_title" class="form-control" placeholder=""
                                       value="{{isset($web_setting['home_instagram_album']) ? $web_setting['home_instagram_album']['title'] : 'INSTAGRAM'}}" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="post_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="setting[home_instagram_album][status]" id="setting_home_instagram_album_status" {{isset($web_setting['home_instagram_album']) && isset($web_setting['home_instagram_album']['status']) && $web_setting['home_instagram_album']['status'] == 'on' ? 'checked' : ''}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">Banner Coupon mặc định</h4>
                            <div class="form-group">
                                <label for="">Hình ảnh</label>
                                @include('admin.template.image_preview', ['input_name' => 'setting[home_default_coupon_banner]', 'input_id' => 'setting_home_default_coupon_banner', 'input_image' => ($web_setting['home_default_coupon_banner'] ?? null)])
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Cập nhật</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@section('admin_css')
    <!-- Color picker plugins css -->
    <link href="{{asset('admin/assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/libs/jquery-asColor.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js')}}"></script>

    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/javascript">
        // Colorpicker
        $(".complex-colorpicker").asColorPicker({
            mode: 'complex'
        });

    </script>
@endsection
