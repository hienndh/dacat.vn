@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Home_Slider_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="card" style="max-width: 1000px;">
                <div class="card-body">
                    <div class="form-group">
                        <button type="button" class="btn btn-info" onclick="addSlide()">Thêm mới</button>
                    </div>

                    <div id="home_slider_block" class="home_slider_block">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Ảnh</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($slider_data))
                                    @foreach($slider_data as $index => $slider_item)
                                        <tr class="setting_{{$slider_meta_key}}_{{$index}}">
                                            <td>
                                                @include('admin.template.image_preview', ['input_name' => 'setting[' . $slider_meta_key . '][' . $index . ']', 'input_id' => 'setting_' . $slider_meta_key . '_' . $index, 'input_image' => ($slider_item ? $slider_item : null)])
                                            </td>
                                            <td width="120px">
                                                <button type="button" class="btn btn-success btn-rounded btn-sm btn__up" data-toggle="tooltip" title="Lên"
                                                        onclick="moveSlideUp('setting_{{$slider_meta_key}}_{{$index}}')">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <button type="button" class="btn btn-success btn-rounded btn-sm btn__down" data-toggle="tooltip" title="Xuống"
                                                        onclick="moveSlideDown('setting_{{$slider_meta_key}}_{{$index}}')">
                                                    <i class="fa fa-caret-down"></i>
                                                </button>
                                                <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-toggle="tooltip" title="Xoá"
                                                        onclick="deleteSlide('setting_{{$slider_meta_key}}_{{$index}}')">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Cập nhật</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@section('admin_css')

@endsection

@section('admin_script')
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/html" id="html_setting_home_slider">
        <tr class="setting_{{$slider_meta_key}}___INDEX__">
            <td>
                @include('admin.template.image_preview', ['input_name' => 'setting[' . $slider_meta_key . '][__INDEX__]', 'input_id' => 'setting_' . $slider_meta_key . '___INDEX__', 'input_image' => null])
            </td>
            <td>
                <button type="button" class="btn btn-success btn-rounded btn-sm btn__up" data-toggle="tooltip" title="Lên"
                        onclick="moveSlideUp('setting_{{$slider_meta_key}}___INDEX__')">
                    <i class="fa fa-caret-up"></i>
                </button>
                <button type="button" class="btn btn-success btn-rounded btn-sm btn__down" data-toggle="tooltip" title="Xuống"
                        onclick="moveSlideDown('setting_{{$slider_meta_key}}___INDEX__')">
                    <i class="fa fa-caret-down"></i>
                </button>
                <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-toggle="tooltip" title="Xoá"
                        onclick="deleteSlide('setting_{{$slider_meta_key}}___INDEX__')">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
    </script>

    <script type="text/javascript">
        function moveSlideUp(slide_class) {
            let moved_slide = $('#home_slider_block table tbody tr.' + slide_class);
            let index = moved_slide.index();
            if (index === 0) {
                return toastError('Slide này đã ở trên cùng');
            }
            $('#home_slider_block table tbody tr').eq(index - 1).before(moved_slide);
            reindexSliderItem();
        }

        function moveSlideDown(slide_class) {
            let moved_slide = $('#home_slider_block table tbody tr.' + slide_class);
            let index = moved_slide.index();
            if (index === ($('#home_slider_block table tbody tr').length - 1)) {
                return toastError('Slide này đã ở dưới cùng');
            }
            $('#home_slider_block table tbody tr').eq(index - 1).after(moved_slide);
            reindexSliderItem();
        }

        function addSlide() {
            let html_setting_home_slider = $('#html_setting_home_slider').html();
            $('#home_slider_block table tbody').append(html_setting_home_slider.replace(/__INDEX__/g, $('#home_slider_block table tbody tr').length));
            reindexSliderItem();
        }

        function deleteSlide(slide_class) {
            $('#home_slider_block table tbody tr.' + slide_class).closest('tr').remove();
            reindexSliderItem();
            // $('form#Home_Slider_Form').submit();
        }

        function reindexSliderItem() {
            $('#home_slider_block table tbody tr').each(function (index, value) {
                $(this).attr('class', 'setting_{{$slider_meta_key}}_' + index);
                $(this).find('input').attr('name', 'setting[{{$slider_meta_key}}][' + index + ']');
                let replace = 'setting_{{$slider_meta_key}}_(\\d+)';
                let re = new RegExp(replace, "g");
                $(this).html($(this).html().replace(re, 'setting_{{$slider_meta_key}}_' + index));
            });
        }
    </script>
@endsection
