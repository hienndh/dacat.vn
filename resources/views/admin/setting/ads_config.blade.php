@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <!-- TAB NAVIGATION -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item active">
                        <a href="#config_banner" class="nav-link active" role="tab" data-toggle="tab">Banner</a>
                    </li>
                    <li class="nav-item">
                        <a href="#config_popup" class="nav-link" role="tab" data-toggle="tab">Popup</a>
                    </li>
                    <li class="nav-item">
                        <a href="#config_form" class="nav-link" role="tab" data-toggle="tab">Form Marketing</a>
                    </li>
                </ul>

                <!-- TAB CONTENT -->
                <div class="tab-content bg-light p-3">
                    <div class="tab-pane active show" id="config_banner">
                        <div class="row">
                            <div class="col-12 col-md-4 col-lg-4">
                                <form id="Top_Banner_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title font-bold">TOP BANNER</h4>
                                            <div class="form-group">
                                                <label for="">Hình ảnh</label>
                                                @include('admin.template.image_preview', ['input_name' => 'setting[top_banner][image]', 'input_id' => 'setting_top_banner_image', 'input_image' => ($web_setting['top_banner']['image'] ?? null)])
                                            </div>

                                            <div class="form-group">
                                                <label for="">Nội dung</label>
                                                <input type="text" name=setting[top_banner][text]" id=setting_top_banner_text" class="form-control"
                                                       placeholder="" value="{{$web_setting['top_banner']['text'] ?? ''}}" autocomplete="off">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Link</label>
                                                <input type="text" name=setting[top_banner][link]" id=setting_top_banner_link" class="form-control"
                                                       placeholder="" value="{{$web_setting['top_banner']['link'] ?? '#'}}" autocomplete="off">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Màu nền</label> <br>
                                                <input type="text" name=setting[top_banner][color]" id=setting_top_banner_color" class="form-control complex-colorpicker"
                                                       placeholder="" value="{{$web_setting['top_banner']['color'] ?? '#000000'}}" autocomplete="off">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Màu chữ</label> <br>
                                                <input type="text" name=setting[top_banner][text_color]" id=setting_top_banner_text_color" class="form-control complex-colorpicker"
                                                       placeholder="" value="{{$web_setting['top_banner']['text_color'] ?? '#ffffff'}}" autocomplete="off">
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info">Cập nhật</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="config_popup">
                        <div class="row">
                            <div class="col-12 col-md-4 col-lg-4">
                                <form id="Popup_Phone_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title font-bold">POPUP SỐ ĐIỆN THOẠI</h4>

                                            <div class="form-group">
                                                <label for="">Hình ảnh</label>
                                                @include('admin.template.image_preview', ['input_name' => 'setting[popup_phone][image]', 'input_id' => 'setting_popup_phone_image', 'input_image' => ($web_setting['popup_phone']['image'] ?? null)])
                                            </div>

                                            <div class="form-group">
                                                <label for="">Script Marketing</label>
                                                <textarea name=setting[popup_phone][script]" id=setting_popup_phone_script" class="form-control" rows="8"
                                                          placeholder="" autocomplete="off">{{$web_setting['popup_phone']['script'] ?? ''}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info">Cập nhật</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="col-12 col-md-4 col-lg-4">
                                <form id="Popup_Email_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title font-bold">POPUP EMAIL</h4>

                                            <div class="form-group">
                                                <label for="">Hình ảnh</label>
                                                @include('admin.template.image_preview', ['input_name' => 'setting[popup_email][image]', 'input_id' => 'setting_popup_email_image', 'input_image' => ($web_setting['popup_email']['image'] ?? null)])
                                            </div>

                                            <div class="form-group">
                                                <label for="">Màu dấu đóng popup</label> <br>
                                                <input type="text" name=setting[popup_email][close_color]" id=setting_popup_email_close_color" class="form-control complex-colorpicker"
                                                       placeholder="" value="{{$web_setting['popup_email']['close_color'] ?? '#ffffff'}}" autocomplete="off">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Script Marketing</label>
                                                <textarea name=setting[popup_email][script]" id=setting_popup_email_script" class="form-control" rows="8"
                                                          placeholder="" autocomplete="off">{{$web_setting['popup_email']['script'] ?? ''}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info">Cập nhật</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="config_form">
                        <div class="row">
                            <div class="col-12 col-md-4 col-lg-4">
                                <form id="Checkout_Form_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title font-bold">Form thanh toán</h4>

                                            <div class="form-group">
                                                <label for="">Script Marketing</label>
                                                <textarea name=setting[checkout_form][script]" id=setting_checkout_form_script" class="form-control" rows="8"
                                                          placeholder="" autocomplete="off">{{$web_setting['checkout_form']['script'] ?? ''}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info">Cập nhật</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4">
                                <form id="Email_Subscriber_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title font-bold">Form Đămg ký nhận tin</h4>

                                            <div class="form-group">
                                                <label for="">Script Marketing</label>
                                                <textarea name=setting[email_subscriber][script]" id=setting_email_subscriber_script" class="form-control" rows="8"
                                                          placeholder="" autocomplete="off">{{$web_setting['email_subscriber']['script'] ?? ''}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info">Cập nhật</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@section('admin_css')
    <!-- Color picker plugins css -->
    <link href="{{asset('admin/assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/libs/jquery-asColor.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js')}}"></script>

    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/javascript">
        // Colorpicker
        // $(".colorpicker").asColorPicker();
        $(".complex-colorpicker").asColorPicker({
            mode: 'complex'
        });
        // $(".gradient-colorpicker").asColorPicker({
        //     mode: 'gradient'
        // });
    </script>
@endsection
