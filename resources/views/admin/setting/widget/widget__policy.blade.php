<div class="card {{$widget_key}}" data-sidebar_key="{{$sidebar_key}}">
    <div class="card-body">
        <h4 class="card-title font-bold">CÁC ĐIỂM NỔI BẬT</h4>
        <div class="form-group">
            <label for="">Tiêu đề widget</label>
            <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_title]" id="{{$widget_key}}_title" class="form-control" autocomplete="off"
                   value="{{$widget_data ? $widget_data['widget_title'] : ''}}">
        </div>
        <div class="form-group">
            <label for="">Ảnh đại diện</label>
            @include('admin.template.image_preview', ['input_name' => 'setting['.$sidebar_key.']['.$widget_key.'][widget_image]', 'input_id' => $sidebar_key.'_'.$widget_key.'_image', 'input_image' => ($widget_data ? $widget_data['widget_image'] : null)])
        </div>

        <div class="form-group">
            <label for="">Nội dung chính sách</label>
            <p>
                Mã Icon có thể được tìm kiếm các giá trị phù hợp ở trang <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">Font Awesome</a> này
            </p>
            <div class="form-group form-inline">
                <label for="">Lấy link ảnh:</label>
                <div class="input-group ml-3">
                    <input type="text" class="form-control" id="widget__policy_image_link" onchange="copy_to_clipboard('widget__policy_image_link')"/>
                    <div class="input-group-append">
                        <button class="btn btn-info btn-sm" type="button" onclick="open_popup_file_manager('widget__policy_image_link', '{{$FM_KEY}}')">
                            <i class="fa fa-image" aria-hidden="true"></i> Chọn ảnh
                        </button>
                    </div>
                </div>
            </div>
            <div id="Policy_Block">
                <div class="table-responsive">
                    <table class="table table-bordered" style="min-width: 600px;max-width: 100%">
                        <thead>
                        <th>
                            <button type="button" class="btn btn-success btn-sm btn__add" data-toggle="tooltip" title="Thêm" onclick="addPolicyContent(this)">
                                <i class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th>Icon</th>
                        <th>Tiêu đề</th>
                        <th>Nội dung</th>
                        </thead>
                        <tbody>
                        @if(isset($widget_data) && isset($widget_data['widget_content']))
                            @foreach($widget_data['widget_content'] as $index => $content_item)
                                <tr>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm btn__delete" data-toggle="tooltip" title="Xoá" onclick="deletePolicyContent(this)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][{{$index}}][icon]" class="form-control form-control-sm"
                                               autocomplete="off" value="{{$content_item['icon'] ? $content_item['icon'] : ''}}">
                                    </td>
                                    <td>
                                        <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][{{$index}}][title]" class="form-control form-control-sm"
                                               autocomplete="off" value="{{$content_item['title'] ? $content_item['title'] : ''}}">
                                    </td>
                                    <td>
                                        <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][{{$index}}][content]" class="form-control form-control-sm"
                                               autocomplete="off" value="{{$content_item['content'] ? $content_item['content'] : ''}}">
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('admin_css')
@endsection

@section($widget_key . '_script')
    <!-- WIDGET POLICY -->
    <script type="text/html" id="html_policy_content">
        <tr>
            <td>
                <button type="button" class="btn btn-danger btn-sm btn__delete" data-toggle="tooltip" title="Xoá" onclick="deletePolicyContent(this)">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            <td>
                <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][][icon]" class="form-control form-control-sm" autocomplete="off" value="">
            </td>
            <td>
                <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][][title]" class="form-control form-control-sm" autocomplete="off" value="">
            </td>
            <td>
                <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][][content]" class="form-control form-control-sm" autocomplete="off" value="">
            </td>
        </tr>
    </script>

    <script type="text/javascript">
        function addPolicyContent() {
            let html_policy_content = $('#html_policy_content').html();
            $('#Policy_Block table tbody').append(html_policy_content);
            reindexPolicyContentTable();
        }

        function deletePolicyContent(element) {
            element.closest('tr').remove();
            reindexPolicyContentTable();
        }

        function reindexPolicyContentTable() {
            let sidebar_key = $('.{{$widget_key}}').data('sidebar_key');
            $('#Policy_Block table tbody tr').each(function (index, value) {
                $(this).find('input[name$="[icon]"]').attr('name', 'setting[' + sidebar_key + '][{{$widget_key}}][widget_content][' + index + '][icon]');
                $(this).find('input[name$="[title]"]').attr('name', 'setting[' + sidebar_key + '][{{$widget_key}}][widget_content][' + index + '][title]');
                $(this).find('input[name$="[content]"]').attr('name', 'setting[' + sidebar_key + '][{{$widget_key}}][widget_content][' + index + '][content]');
            });
        }

        $(document).ready(function () {
            if ($(".colorpicker").length) {
                $(".colorpicker").asColorPicker();
            }
        });
    </script>
@endsection
