<div class="card {{$widget_key}}" data-sidebar_key="{{$sidebar_key}}">
    <div class="card-body">
        <h4 class="card-title font-bold">MENU: {{$widget_data ? $widget_data['widget_title'] : ''}}</h4>
        <div class="form-group">
            <label for="">Tiêu đề widget</label>
            <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_title]" id="{{$widget_key}}_title" class="form-control" autocomplete="off"
                   value="{{$widget_data ? $widget_data['widget_title'] : ''}}">
        </div>

        <div class="form-group">
            <label for="">Chọn menu</label>
            <select class="form-control" name="setting[{{$sidebar_key}}][{{$widget_key}}][menu_id]" id="{{$widget_key}}_menu_id">
                <option value="0">-- Để trống --</option>
                @if(!empty($widget_param))
                    @foreach($widget_param as $menu)
                        <option value="{{$menu['id']}}" {{$widget_data && $widget_data['menu_id'] == $menu['id'] ?  'selected' : ''}}>{{$menu['name']}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
</div>

@section($widget_key . '_script')
    <!-- WIDGET TEXT -->

@endsection
