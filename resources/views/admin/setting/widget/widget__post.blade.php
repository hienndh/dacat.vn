<div class="card {{$widget_key}}" data-sidebar_key="{{$sidebar_key}}">
    <div class="card-body">
        <h4 class="card-title font-bold">DANH SÁCH BÀI VIẾT: {{$widget_data ? $widget_data['widget_title'] : ''}}</h4>
        <div class="form-group">
            <label for="">Tiêu đề widget</label>
            <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_title]" id="{{$widget_key}}_title" class="form-control" autocomplete="off"
                   value="{{$widget_data ? $widget_data['widget_title'] : ''}}">
        </div>

        <div class="form-group">
            <label for="">Số lượng</label>
            <input type="number" name="setting[{{$sidebar_key}}][{{$widget_key}}][post_limit]" id="{{$widget_key}}_post_limit" class="form-control" autocomplete="off"
                   min="3" step="1" value="{{$widget_data ? $widget_data['post_limit'] : 5}}">
        </div>

        <div class="form-group">
            <label for="">Kiểu hiển thị</label>
            <select class="form-control" name="setting[{{$sidebar_key}}][{{$widget_key}}][view_type]" id="{{$widget_key}}_view_type">
                <option value="list_view" {{$widget_data && isset($widget_data['view_type']) && $widget_data['view_type'] == 'list_view' ?  'selected' : ''}}>Dạng danh sách</option>
                <option value="grid_view" {{$widget_data && isset($widget_data['view_type']) && $widget_data['view_type'] == 'grid_view' ?  'selected' : ''}}>Dạng lưới</option>
            </select>
        </div>

        <div class="form-group">
            <label for="">Loại danh sách</label>
            <select class="form-control" name="setting[{{$sidebar_key}}][{{$widget_key}}][list_type]" id="{{$widget_key}}_view_type">
                <option value="most_view" {{$widget_data && isset($widget_data['list_type']) && $widget_data['list_type'] == 'most_view' ?  'selected' : ''}}>Bài viết xem nhiều</option>
                <option value="lastest" {{$widget_data && isset($widget_data['list_type']) && $widget_data['list_type'] == 'lastest' ?  'selected' : ''}}>Bài viết mới nhất</option>
            </select>
        </div>
    </div>
</div>

@section($widget_key . '_script')
    <!-- WIDGET TEXT -->

@endsection
