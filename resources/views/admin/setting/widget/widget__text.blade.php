<div class="card {{$widget_key}}" data-sidebar_key="{{$sidebar_key}}">
    <div class="card-body">
        <h4 class="card-title font-bold">VĂN BẢN: {{$widget_data ? $widget_data['widget_title'] : ''}}</h4>
        <div class="form-group">
            <label for="">Tiêu đề widget</label>
            <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_title]" id="{{$widget_key}}_title" class="form-control" autocomplete="off" value="{{$widget_data ? $widget_data['widget_title'] : ''}}">
        </div>

        <div class="form-group">
            <label for="">Nội dung</label>
            <textarea class="form-control mce_editor" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content]" id="{{$widget_key}}_content">
                {{ isset($widget_data) && isset($widget_data['widget_content']) ? $widget_data['widget_content'] : '' }}
            </textarea>
        </div>
    </div>
</div>

@section($widget_key . '_script')
    <!-- WIDGET TEXT -->

@endsection
