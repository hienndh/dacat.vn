<div class="card {{$widget_key}}" data-sidebar_key="{{$sidebar_key}}">
    <div class="card-body">
        {{--<h4 class="card-title font-bold">ĐĂNG KÝ NHẬN TIN: {{$widget_data ? $widget_data['widget_title'] : 'ĐĂNG KÝ NHẬN TIN'}}</h4>--}}
        <div class="form-group">
            <label for="">Tiêu đề widget</label>
            <input type="text" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_title]" id="{{$widget_key}}_title" class="form-control" autocomplete="off"
                   value="{{$widget_data ? $widget_data['widget_title'] : ''}}">
        </div>

        <div class="form-group">
            <label for="">Nội dung</label>
            <textarea class="form-control {{$widget_key}}_editor" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][content]" id="{{$widget_key}}_content" rows="3">{{ isset($widget_data) && isset($widget_data['widget_content']) ? $widget_data['widget_content']['content']  : '' }}</textarea>
        </div>
        <div class="form-group">
            <label for="">Script Marketing</label>
            <textarea class="form-control" name="setting[{{$sidebar_key}}][{{$widget_key}}][widget_content][script]" id="{{$widget_key}}_script" rows="5">{{ isset($widget_data) && isset($widget_data['widget_content']) ? $widget_data['widget_content']['script'] : '' }}</textarea>
        </div>
    </div>
</div>

@section($widget_key . '_script')
    <!-- WIDGET TEXT -->
    <script type="text/javascript">
        $(document).ready(function () {
            admin_tinymce('{{$widget_key}}_editor', 300);
        });
    </script>
@endsection
