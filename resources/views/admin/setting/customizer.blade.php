@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Customizer_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="card">
                <div class="card-body">
                    <button type="submit" class="btn btn-info mb-3">Cập nhật</button>
                    <div class="vtabs">
                        <ul class="nav nav-tabs tabs-vertical" role="tablist" style="width: auto;">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#product_filter" role="tab" aria-selected="true">
                                    <span class="hidden-sm-up"><i class="ti-bookmark"></i></span>
                                    <span class="hidden-xs-down">Bộ lọc giá sản phẩm</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#seo" role="tab" aria-selected="true">
                                    <span class="hidden-sm-up"><i class="ti-world"></i></span>
                                    <span class="hidden-xs-down">SEO</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#mail_config" role="tab" aria-selected="true">
                                    <span class="hidden-sm-up"><i class="ti-mail"></i></span>
                                    <span class="hidden-xs-down">Cấu hình Mail</span>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active show" id="product_filter" role="tabpanel">
                                <div id="Price_Filter_Block">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" style="min-width: 600px;max-width: 100%">
                                            <thead>
                                            <th>
                                                <button type="button" class="btn btn-success btn-sm btn__add" data-toggle="tooltip" title="Thêm" onclick="addPriceFilter(this)">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </th>
                                            <th>Giá trị</th>
                                            <th>Tiêu đề</th>
                                            </thead>
                                            <tbody>
                                            @if(isset($web_setting['filter_price']))
                                                @foreach($web_setting['filter_price'] as $index => $price_filter_item)
                                                    <tr>
                                                        <td>
                                                            <button type="button" class="btn btn-danger btn-sm btn__delete" data-toggle="tooltip" title="Xoá" onclick="deletePriceFilter(this)">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="setting[filter_price][{{$index}}][value]" class="form-control form-control-sm"
                                                                   autocomplete="off" value="{{$price_filter_item['value'] ? $price_filter_item['value'] : ''}}">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="setting[filter_price][{{$index}}][title]" class="form-control form-control-sm"
                                                                   autocomplete="off" value="{{$price_filter_item['title'] ? $price_filter_item['title'] : ''}}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="seo" role="tabpanel">
                                <div class="form-group">
                                    <label for="">Google Analytics</label>
                                    <textarea class="form-control" name="setting[customizer_google_analytics]" id="setting_customizer_google_analytics" cols="30" rows="10">{{$web_setting['customizer_google_analytics'] ?? ''}}</textarea>
                                </div>
                            </div>
                            <div class="tab-pane" id="mail_config" role="tabpanel">
                                <div class="form-group">
                                    <label for="">Mail</label>
                                    <input type="text" class="form-control" name="setting[ta_mail_username]" id="ta_mail_username" value="{{$web_setting['ta_mail_username'] ?? null}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Mật khẩu</label>
                                    <input type="password" class="form-control" name="setting[ta_mail_password]" id="ta_mail_password" value="{{$web_setting['ta_mail_password'] ?? null}}">
                                </div>
                            </div>
                            <div class="tab-pane" id="blog2" role="tabpanel">

                            </div>
                            <div class="tab-pane" id="blog3" role="tabpanel">

                            </div>
                            <div class="tab-pane" id="blog4" role="tabpanel">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@section('admin_css')

@endsection

@section('admin_script')
    <!-- WIDGET POLICY -->
    <script type="text/html" id="html_price_filter">
        <tr>
            <td>
                <button type="button" class="btn btn-danger btn-sm btn__delete" data-toggle="tooltip" title="Xoá" onclick="deletePriceFilter(this)">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            <td>
                <input type="text" name="setting[filter_price][][value]" class="form-control form-control-sm" autocomplete="off" value="">
            </td>
            <td>
                <input type="text" name="setting[filter_price][][title]" class="form-control form-control-sm" autocomplete="off" value="">
            </td>
        </tr>
    </script>

    <script type="text/javascript">
        function addPriceFilter() {
            let html_price_filter = $('#html_price_filter').html();
            $('#Price_Filter_Block table tbody').append(html_price_filter);
            reindexPriceFilterTable();
        }

        function deletePriceFilter(element) {
            element.closest('tr').remove();
            reindexPriceFilterTable();
        }

        function reindexPriceFilterTable() {
            $('#Price_Filter_Block table tbody tr').each(function (index, value) {
                $(this).find('input[name$="[value]"]').attr('name', 'setting[filter_price][' + index + '][value]');
                $(this).find('input[name$="[title]"]').attr('name', 'setting[filter_price][' + index + '][title]');
            });
        }
    </script>
@endsection
