@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Website_Info_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-4 col-lg-3">
                            <h4 class="card-title font-bold">HÌNH ẢNH THƯƠNG HIỆU</h4>
                            <div class="form-group">
                                <label for="">Logo</label>
                                @include('admin.template.image_preview', ['input_name' => 'setting[ta_logo]', 'input_id' => 'setting_ta_logo', 'input_image' => ($web_setting['ta_logo'] ?? null)])
                            </div>
                            <div class="form-group">
                                <label for="">Logo 2</label>
                                @include('admin.template.image_preview', ['input_name' => 'setting[ta_logo_dark]', 'input_id' => 'setting_ta_logo_dark', 'input_image' => ($web_setting['ta_logo_dark'] ?? null)])
                            </div>
                            <div class="form-group">
                                <label for="">Favicon</label>
                                @include('admin.template.image_preview', ['input_name' => 'setting[ta_favicon]', 'input_id' => 'setting_ta_favicon', 'input_image' => ($web_setting['ta_favicon'] ?? null)])
                            </div>
                            <div class="form-group">
                                <label for="">Hình nền đăng nhập</label>
                                @include('admin.template.image_preview', ['input_name' => 'setting[ta_login_background]', 'input_id' => 'setting_ta_login_background', 'input_image' => ($web_setting['ta_login_background'] ?? null)])
                            </div>
                        </div>

                        <div class="col-12 col-md-4 col-lg-4">
                            <h4 class="card-title font-bold">THÔNG TIN WEBSITE</h4>
                            <div class="form-group">
                                <label for="">SEO Tiêu đề</label>
                                <input type="text" name="setting[ta_seo_title]" id="setting_ta_seo_title" class="form-control" maxlength="70"
                                       placeholder="" value="{{$web_setting['ta_seo_title'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">SEO Mô tả</label>
                                <textarea name="setting[ta_seo_description]" id="setting_ta_seo_description" class="form-control" rows="3"
                                          maxlength="170" placeholder="" autocomplete="off">{{$web_setting['ta_seo_description'] ?? null}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">SEO Từ khóa</label>
                                <input type="text" name="setting[ta_seo_keyword]" id="setting_ta_seo_keyword" class="form-control" data-role="tagsinput"
                                       placeholder="" value="{{$web_setting['ta_seo_keyword'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">SEO Hình ảnh</label>
                                @include('admin.template.image_preview', ['input_name' => 'setting[ta_seo_image]', 'input_id' => 'setting_ta_seo_image', 'input_image' => ($web_setting['ta_seo_image'] ?? null)])
                            </div>
                        </div>

                        <div class="col-12 col-md-4 col-lg-4">
                            <h4 class="card-title font-bold">THÔNG TIN CÔNG TY</h4>
                            <div class="form-group">
                                <label for="">Tên công ty</label>
                                <input type="text" name="setting[ta_company_name]" id="setting_ta_company_name" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_company_name'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Địa chỉ</label>
                                <input type="text" name="setting[ta_company_address]" id="setting_ta_company_address" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_company_address'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Điện thoại</label>
                                <input type="text" name="setting[ta_company_phone]" id="setting_ta_company_phone" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_company_phone'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="setting[ta_company_email]" id="setting_ta_company_email" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_company_email'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Email 2</label>
                                <input type="email" name="setting[ta_company_email_2]" id="setting_ta_company_email_2" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_company_email_2'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Facebook</label>
                                <input type="text" name="setting[ta_social_facebook]" id="setting_ta_social_facebook" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_social_facebook'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Zalo</label>
                                <input type="text" name="setting[ta_social_zalo]" id="setting_ta_social_zalo" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_social_zalo'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Youtube</label>
                                <input type="text" name="setting[ta_social_youtube]" id="setting_ta_social_youtube" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_social_youtube'] ?? null}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Instagram</label>
                                <input type="text" name="setting[ta_social_instagram]" id="setting_ta_social_instagram" class="form-control"
                                       placeholder="" value="{{$web_setting['ta_social_instagram'] ?? null}}" autocomplete="off">
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Cập nhật</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/javascript">

    </script>
@endsection
