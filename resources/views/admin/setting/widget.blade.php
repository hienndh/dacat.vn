@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="form-group form-inline">
                    <div class="input-group">
                        <label for="list_widget">Chọn widget:</label>
                        <select class="form-control ml-3" id="list_widget">
                            @if(!empty($list_widget))
                                @foreach($list_widget as $widget_key => $widget_name)
                                    <option value="{{$widget_key}}">{{$widget_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <select class="form-control ml-3" id="list_sidebar" onchange="checkActiveWidgetOfSidebar()">
                            @if(!empty($list_sidebar))
                                @foreach($list_sidebar as $sidebar_key => $sidebar_name)
                                    <option value="{{$sidebar_key}}">{{$sidebar_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-info btn-sm px-3" type="button" data-toggle="tooltip" title="Thêm" onclick="addWidgetToSidebar()">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div id="paste_widget_block" class="form-group form-inline" style="display: none;">
                    <div class="input-group">
                        <label for="list_widget">Dán Widget:</label>
                        <select class="form-control ml-3" id="list_sidebar_paste">
                            @if(!empty($list_sidebar))
                                @foreach($list_sidebar as $sidebar_key => $sidebar_name)
                                    <option value="{{$sidebar_key}}">{{$sidebar_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-info btn-sm px-3" type="button" data-toggle="tooltip" title="Dán" onclick="pasteWidgetToSidebar()">
                                <i class="fa fa-paste" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            @if(!empty($list_sidebar))
                @foreach($list_sidebar as $sidebar_key => $sidebar_name)
                    <div class="col-12 col-md-sm-6 col-md-6 col-lg-4">
                        <form id="Widget_{{$sidebar_key}}_Form" action="{{route('admin_setting_save')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="setting[{{$sidebar_key}}]" value="">

                            <div id="{{$sidebar_key}}" class="card card_{{$sidebar_key}}">
                                <div class="card-body">
                                    <h4 class="card-title font-bold">{{$sidebar_name}}</h4>

                                    <div id="{{$sidebar_key}}_block" class="sidebar_block">
                                        @if(!empty($list_sidebar_data[$sidebar_key]))
                                            @foreach($list_sidebar_data[$sidebar_key] as $widget_key => $widget_data)
                                                <div class="collapse_block collapse_block_{{$sidebar_key}}_{{$widget_key}}" data-widget_key="{{$widget_key}}">
                                                    <a class="collapse_button collapsed w-100 mb-3" data-toggle="collapse" href="#collapse_{{$sidebar_key}}_{{$widget_key}}" aria-expanded="false" aria-controls="collapse_{{$sidebar_key}}_{{$widget_key}}">
                                                        {{$list_widget[$widget_key]}}

                                                        <div class="collapse_button_group">
                                                            <button type="button" class="btn btn-success btn-rounded btn-sm btn__widget_up" data-toggle="tooltip" title="Lên"
                                                                    onclick="moveWidgetUp('{{$widget_key}}', '{{$sidebar_key}}')">
                                                                <i class="fa fa-caret-up"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-success btn-rounded btn-sm btn__widget_down" data-toggle="tooltip" title="Xuống"
                                                                    onclick="moveWidgetDown('{{$widget_key}}', '{{$sidebar_key}}')">
                                                                <i class="fa fa-caret-down"></i>
                                                            </button>
                                                            @if (!in_array($widget_key, ['widget__text']))
                                                                <button type="button" class="btn btn-info btn-rounded btn-sm btn__copy" data-toggle="tooltip" title="Sao chép"
                                                                        onclick="copyWidgetFromSidebar('{{$widget_key}}', '{{$sidebar_key}}')">
                                                                    <i class="fa fa-copy"></i>
                                                                </button>
                                                            @endif
                                                            <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-toggle="tooltip" title="Xoá"
                                                                    onclick="deleteWidgetFromSidebar('{{$widget_key}}', '{{$sidebar_key}}')">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </div>
                                                    </a>

                                                    <div class="collapse" id="collapse_{{$sidebar_key}}_{{$widget_key}}">
                                                        @include('admin.setting.widget.' . Helper::getWidgetKey($widget_key), ['sidebar_key' => $sidebar_key, 'widget_key' => $widget_key, 'widget_data' => $widget_data, 'widget_param' => $widget_param[$widget_key]])
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info">Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endforeach
            @endif
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    @if(!empty($list_widget))
        @foreach($list_widget as $widget_key => $widget_name)
            <script type="text/html" id="html_{{$widget_key}}">
                <div class="collapse_block collapse_block___SIDEBAR_KEY___{{$widget_key}}" data-widget_key="{{$widget_key}}">
                    <a class="collapse_button collapsed w-100 mb-3" data-toggle="collapse" href="#collapse___SIDEBAR_KEY___{{$widget_key}}" aria-expanded="false" aria-controls="collapse___SIDEBAR_KEY___{{$widget_key}}">
                        {{$widget_name}}
                        <div class="collapse_button_group">
                            <button type="button" class="btn btn-success btn-rounded btn-sm btn__widget_up" data-toggle="tooltip" title="Lên"
                                    onclick="moveWidgetUp('{{$widget_key}}', '__SIDEBAR_KEY__')">
                                <i class="fa fa-caret-up"></i>
                            </button>
                            <button type="button" class="btn btn-success btn-rounded btn-sm btn__widget_down" data-toggle="tooltip" title="Xuống"
                                    onclick="moveWidgetDown('{{$widget_key}}', '__SIDEBAR_KEY__')">
                                <i class="fa fa-caret-down"></i>
                            </button>
                            @if (!in_array($widget_key, ['widget__text']))
                                <button type="button" class="btn btn-info btn-rounded btn-sm btn__copy" data-toggle="tooltip" title="Sao chép"
                                        onclick="copyWidgetFromSidebar('{{$widget_key}}', '__SIDEBAR_KEY__')">
                                    <i class="fa fa-copy"></i>
                                </button>
                            @endif
                            <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-toggle="tooltip" title="Xoá"
                                    onclick="deleteWidgetFromSidebar('{{$widget_key}}', '__SIDEBAR_KEY__')">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>

                    </a>

                    <div class="collapse" id="collapse___SIDEBAR_KEY___{{$widget_key}}">
                        @include('admin.setting.widget.' . Helper::getWidgetKey($widget_key), ['sidebar_key' => '__SIDEBAR_KEY__', 'widget_key' => $widget_key, 'widget_data' => null, 'widget_param' => $widget_param[$widget_key]])
                    </div>
                </div>
            </script>
        @endforeach
    @endif

@endsection

@section('admin_css')

@endsection

@section('admin_script')
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/javascript">
        function addWidgetToSidebar() {
            let selected_widget_key = $('select#list_widget').val();
            let selected_sidebar_key = $('select#list_sidebar').val();

            if (!selected_widget_key) {
                return toastError("Widget này đã được thêm vào khu vực chỉ định!");
            }
            if (!selected_sidebar_key) {
                return toastError("Không tìm thấy khu vực chỉ định!");
            }

            $('#' + selected_sidebar_key + '_block').append($('#html_' + selected_widget_key).html().replace(/__SIDEBAR_KEY__/g, selected_sidebar_key));
            $("select#list_widget option[value='" + selected_widget_key + "']").attr("disabled", true);

            checkActiveWidgetOfSidebar();
        }

        function deleteWidgetFromSidebar(widget_key, sidebar_key) {
            $('#' + sidebar_key + '_block .collapse_block_' + sidebar_key + '_' + widget_key).remove();
            if ($('select#list_sidebar').val() === sidebar_key) {
                $("select#list_widget option[value='" + widget_key + "']").attr("disabled", false);
            }

            checkActiveWidgetOfSidebar();
        }

        var copied_sidebar_widget = null;
        var copied_sidebar_widget_key = null;

        function copyWidgetFromSidebar(widget_key, sidebar_key) {
            copied_sidebar_widget_key = widget_key;
            let re = new RegExp(sidebar_key, "g");
            copied_sidebar_widget = $('#' + sidebar_key + '_block .collapse_block_' + sidebar_key + '_' + widget_key)[0].outerHTML.replace(re, '__SIDEBAR_KEY__');

            $('#paste_widget_block').show();
            $('select#list_sidebar_paste option').attr('disabled', false);
            $("select#list_sidebar_paste option[value='" + sidebar_key + "']").attr("disabled", true);
        }

        function pasteWidgetToSidebar() {
            let sidebar_key = $('#list_sidebar_paste').val();

            if (!sidebar_key) {
                return toastError('Đây là khu vực chứa Widget đã sao chép');
            }

            if (!copied_sidebar_widget) {
                return toastError('Chưa có Widget nào được sao chép');
            }

            if ($('#' + sidebar_key + '_block .collapse_block_' + sidebar_key + '_' + copied_sidebar_widget_key).length > 0) {
                return toastError('Khu vực này đã tồn tại widget được sao chép', 'Hãy xoá widget cũ đi trước khi dán.');
            }

            $('#' + sidebar_key + '_block').append(copied_sidebar_widget.replace(/__SIDEBAR_KEY__/g, sidebar_key));
            $("select#list_widget option[value='" + copied_sidebar_widget_key + "']").attr("disabled", true);

            copied_sidebar_widget = null;
            copied_sidebar_widget_key = null;
            $('select#list_sidebar_paste option').attr('disabled', false);
            $('#paste_widget_block').hide();

            checkActiveWidgetOfSidebar();
        }

        function checkActiveWidgetOfSidebar() {
            let selected_sidebar_key = $('select#list_sidebar').val();
            if (!selected_sidebar_key) {
                toastError('Không tìm thấy khu vực chỉ định!');
            }

            $('select#list_widget option').attr('disabled', false);
            $('#' + selected_sidebar_key + '_block .collapse_block').each(function () {
                let selected_widget_key = $(this).data('widget_key');
                $("select#list_widget option[value='" + selected_widget_key + "']").attr('disabled', true);
            });
        }

        function moveWidgetUp(widget_key, sidebar_key) {
            let moved_widget = $('.collapse_block_' + sidebar_key + '_' + widget_key);
            let index = moved_widget.index();
            if (index === 0) {
                return toastError('Widget này đã ở trên cùng');
            }
            $('#' + sidebar_key + '_block').find('.collapse_block').eq(index - 1).before(moved_widget);
        }

        function moveWidgetDown(widget_key, sidebar_key) {
            let moved_widget = $('.collapse_block_' + sidebar_key + '_' + widget_key);
            let index = moved_widget.index();
            if (index === ($('#' + sidebar_key + '_block .collapse_block').length - 1)) {
                return toastError('Widget này đã ở dưới cùng');
            }
            $('#' + sidebar_key + '_block').find('.collapse_block').eq(index + 1).after(moved_widget);
        }

        $(document).ready(function () {
            checkActiveWidgetOfSidebar();
        });
    </script>

    @if(!empty($list_widget))
        @foreach($list_widget as $widget_key => $widget_name)
            @yield($widget_key . '_script')
        @endforeach
    @endif
@endsection
