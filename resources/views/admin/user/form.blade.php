@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="User_{{ $user_object ? 'Update' : 'Create' }}_Form" action="{{$user_object ? route('admin_user_update') : route('admin_user_create')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($user_object)
                <input type="hidden" name="user[id]" value="{{$user_object['id']}}">
            @endif
            <div class="row">
                <div id="User_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN TÀI KHOẢN</h4>
                            <div class="form-group">
                                <label for="user_last_name">Họ</label>
                                <input type="text" class="form-control" name="user[last_name]" id="user_last_name" placeholder="Nhập họ"
                                       autocomplete="off" value="{{$user_object ? $user_object['last_name'] : null}}" required>
                            </div>
                            <div class="form-group">
                                <label for="user_first_name">Tên</label>
                                <input type="text" class="form-control" name="user[first_name]" id="user_first_name" placeholder="Nhập tên"
                                       autocomplete="off" value="{{$user_object ? $user_object['first_name'] : null}}" required>
                            </div>
                            <div class="form-group">
                                <label for="user_email">Email</label>
                                <input type="email" class="form-control" name="user[email]" id="user_email" placeholder="Nhập email"
                                       autocomplete="off" value="{{$user_object ? $user_object['email'] : null}}" required>
                            </div>
                            @if($user_object)
                                <div class="form-group">
                                    <label for="user_user_name">Tài khoản</label>
                                    <input type="text" class="form-control" id="user_user_name" placeholder="Nhập tên" disabled value="{{$user_object ? $user_object['user_name'] : null}}">
                                </div>
                                <div class="form-group">
                                    <label for="user_password">Mật khẩu</label>
                                    <input type="password" class="form-control" name="user[password]" id="user_password" placeholder="Nhập mật khẩu mới" aria-describedby="helpPassword">
                                    <small id="helpPassword" class="form-text text-muted">Chỉ nhập khi cần đổi mật khẩu mới</small>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="user_user_name">Tài khoản</label>
                                    <input type="text" class="form-control" name="user[user_name]" id="user_user_name" placeholder="Nhập tài khoản"
                                           autocomplete="off" value="{{$user_object ? $user_object['user_name'] : null}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="user_password">Mật khẩu</label>
                                    <input type="password" class="form-control" name="user[password]" id="user_password" placeholder="Nhập mật khẩu" required>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="user_phone">Số điện thoại</label>
                                <input type="text" class="form-control" name="user[phone]" id="user_phone" placeholder="Nhập số điện thoại"
                                       autocomplete="off" value="{{$user_object ? $user_object['phone'] : null}}" required>
                            </div>
                            <div class="form-group">
                                <label for="user_address">Địa chỉ</label>
                                <input type="text" class="form-control" name="user[address]" id="user_address" placeholder="Nhập địa chỉ"
                                       autocomplete="off" value="{{$user_object ? $user_object['address'] : null}}" required>
                            </div>
                            <div class="form-group">
                                <label for="user_address">Quyền hạn</label>
                                <select class="form-control" name="user[role]" id="user_role">
                                    @foreach($list_role as $role_item)
                                        <option value="{{$role_item['id']}}" {{$user_object && in_array($role_item['id'], $user_object['role_ids']) ? 'selected' : ''}}>{{$role_item['display_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="User_Sidebar" class="col-12 col-md-3">
                    {{--ẢNH ĐẠI DIỆN--}}
                    {{--<div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.template.image_preview', ['input_name' => 'user[image]', 'input_id' => 'user_image', 'input_image' => ($user_object ? $user_object['image'] : null)])
                        </div>
                    </div>--}}
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <div class="form-group">
                                <label for="user_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="user[status]" id="user_status" {{$user_object && $user_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info">{{$user_object ? 'Cập nhật' : 'Tạo mới'}}</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
@endsection

@section('admin_script')
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
