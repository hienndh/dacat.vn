@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Page_{{ $page_object ? 'Update' : 'Create' }}_Form" action="{{$page_object ? route('admin_page_update') : route('admin_page_create')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($page_object)
                <input type="hidden" name="page[id]" value="{{$page_object['id']}}">
            @endif
            <div class="row">
                <div id="Page_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN TRANG</h4>
                            <div class="form-group">
                                <label for="page_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="page[title]" id="page_title" aria-describedby="helpPageTitle" placeholder="Nhập tiêu đề trang"
                                       autocomplete="off" value="{{$page_object ? $page_object['title'] : null}}" onkeyup="getSlug('page_title', 'page_slug', 'page')" required>
                                <small id="helpPageTitle" class="form-text text-muted">Tiêu đề trang là bắt buộc</small>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="page_slug_pre">{{url('/')}}/</span>
                                    </div>
                                    <input type="text" class="form-control" name="page[slug]" id="page_slug" aria-describedby="page_slug_pre"
                                           placeholder="Chuỗi cho đường dẫn tĩnh" autocomplete="off" value="{{$page_object ? $page_object['slug'] : null}}" readonly required>
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button" data-toggle="tooltip" title="Tạo slug tự động từ tiêu đề" onclick="getSlug('page_title', 'page_slug', 'page')">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="page_content">Nội dung</label>
                                <ul class="nav nav-tabs customtab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#default_page" role="tab" aria-selected="true">
                                            <span class="hidden-sm-up"><i class="ti-home"></i></span>
                                            <span class="hidden-xs-down">Mặc định</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#landing_page" role="tab" aria-selected="false">
                                            <span class="hidden-sm-up"><i class="ti-user"></i></span>
                                            <span class="hidden-xs-down">Landing Page</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active show" id="default_page" role="tabpanel">
                                        <textarea class="form-control mce_editor" name="page[content]" id="page_content" rows="3">{{$page_object ? $page_object['content'] : null}}</textarea>
                                    </div>
                                    <div class="tab-pane p-20" id="landing_page" role="tabpanel">
                                        <div class="form-group form-inline">
                                            <label for="">Lấy link ảnh:</label>
                                            <div class="input-group ml-3">
                                                <input type="text" class="form-control" id="image_landing" onchange="copy_to_clipboard('image_landing')"/>
                                                <div class="input-group-append">
                                                    <button class="btn btn-info btn-sm" type="button" onclick="open_popup_file_manager('image_landing', '{{$FM_KEY}}')">
                                                        <i class="fa fa-image" aria-hidden="true"></i> Chọn ảnh
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea class="form-control summernote_editor" name="page[content_html]" id="page_content_html" rows="3">{{$page_object ? $page_object['content_html'] : null}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Mô tả ngắn</label>
                                <textarea class="form-control" name="page[description]" id="page_description" rows="3">{{$page_object ? $page_object['description'] : null}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">CẤU HÌNH SEO</h4>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="page_seo_title">SEO Tiêu đề</label>
                                <input type="text" name="page[seo_title]" id="page_seo_title" class="form-control col-9 col-xs-9 col-md-10" maxlength="70"
                                       placeholder="" value="{{$page_object ? $page_object['seo_title'] : null}}" autocomplete="off">
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="page_seo_title">SEO Mô tả</label>
                                <textarea name="page[seo_description]" id="page_seo_description" class="form-control col-9 col-xs-9 col-md-10" rows="3"
                                          maxlength="170" placeholder="" autocomplete="off">{{$page_object ? $page_object['seo_description'] : null}}</textarea>
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="page_seo_title">SEO Từ khóa</label>
                                <div class="col-9 col-xs-9 col-md-10 p-0">
                                    <input type="text" name="page[seo_keyword]" id="page_seo_keyword" class="form-control" data-role="tagsinput"
                                           placeholder="" value="{{$page_object ? $page_object['seo_keyword'] : null}}" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Page_Sidebar" class="col-12 col-md-3">
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <div class="form-group">
                                <label for="page_type">Giao diện:</label>
                                <select class="form-control" name="page[type]" id="page_type">
                                    @if(count($page_type))
                                        @foreach($page_type as $page_type_key => $page_type_value)
                                            <option value="{{$page_type_key}}" {{$page_object && $page_object['type'] == $page_type_key ? 'selected' : ''}}>{{$page_type_value}}</option>
                                        @endforeach
                                    @else
                                        <option value="default">Mặc định</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="page_status">Trạng thái:</label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="page[status]" id="page_status" {{$page_object && $page_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info">{{$page_object ? 'Cập nhật' : 'Đăng bài'}}</button>
                            @if($page_object)
                                <a href="{{route('client_page_detail_view', ['slug' => $page_object['slug']])}}" class="btn btn-success pull-right" target="_blank">Xem</a>
                            @endif
                        </div>
                    </div>
                    {{--ẢNH ĐẠI DIỆN--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.template.image_preview', ['input_name' => 'page[image]', 'input_id' => 'page_image', 'input_image' => ($page_object ? $page_object['image'] : null)])
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <!-- summernotes CSS -->
    <link href="{{asset('admin/assets/plugins/summernote/dist/summernote.css')}}" rel="stylesheet"/>
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script src="{{asset('admin/assets/plugins/summernote/dist/summernote.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.summernote_editor').summernote({
                height: 600,
                minHeight: null,
                maxHeight: null,
                focus: false,
                lang: 'vi-VN',
                toolbar: [
                    ['misc', ['codeview']]
                ],
                callbacks: {
                    onInit: function () {
                        $("div.note-editor button.btn-codeview:not('.active')").click();
                        $(".note-toolbar").hide();
                    }
                }
            });

            $('.note-codable').on('keyup keypress', function () {
                console.log($(this).val());
                $('textarea[name="page[content_html]"]').html($(this).val())
            });
        });
    </script>
@endsection
