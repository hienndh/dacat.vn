@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-info" onclick="location.href = '{{ route("admin_page_new_view") }}'">Thêm mới</button>
                        <div class="pull-right">
                            <form action="{{route('admin_page_list_view')}}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="pageTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="pageTable" class="table table-bordered" style="min-width: 1200px;">
                                    <thead>
                                    <tr>
                                        <th>Ảnh</th>
                                        <th>Tiêu đề</th>
                                        <th>Giao diện</th>
                                        <th>Tác giả</th>
                                        <th width="100px">Trạng thái</th>
                                        <th width="180px">Ngày tạo</th>
                                        <th width="120px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_page))
                                        @foreach($list_page as $page_item)
                                            <tr>
                                                <td><img height="30" class="image_preview" src="{{$page_item['image'] ? $page_item['image'] : asset('img/placeholder.png')}}" alt="{{$page_item['title']}}" title="{{$page_item['title']}}"></td>
                                                <td><a href="{{route('admin_page_update_view', ['page_id' => $page_item['id']])}}" class="text-dark">{{$page_item['title']}}</a></td>
                                                <td>{{ $page_item['type'] ? $page_type[$page_item['type']] : '' }}</td>
                                                <td>{{$page_item->author->first_name}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-{{$page_item['status'] ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$page_item['status'] ? 'Hiện' : 'Ẩn'}}">
                                                        <i class="fa fa-{{$page_item['status'] ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>{{$page_item->getFormattedDate('created_at')}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_page_update_view', ['page_id' => $page_item['id']])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-page_id="{{$page_item['id']}}" data-toggle="tooltip" title="Xoá">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <a href="{{route('client_page_detail_view', ['slug' => $page_item['slug']])}}" class="btn btn-info btn-rounded btn-sm" data-toggle="tooltip" title="Xem" target="_blank">
                                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="7"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                        @if(count($list_page))
                            {{ $list_page->appends($params)->links('admin.layout.pagination', ['list_object' => $list_page]) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let page_id = $(this).attr("data-page_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_page_delete')}}",
                        data: {
                            id: page_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection

