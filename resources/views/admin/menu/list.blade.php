@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form id="Menu_Create_Form" action="{{route('admin_menu_create')}}" method="POST" class="d-inline-block">
                            {{csrf_field()}}
                            <div class="form-group form-inline">
                                <label for="">Tên menu: </label>
                                <div class="input-group ml-3">
                                    <input type="text" name="menu[name]" id="menu_name" class="form-control" autocomplete="off">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-info">Thêm mới</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="pull-right">
                            <form action="{{route('admin_menu_list_view')}}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="pageTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="pageTable" class="table table-bordered" style="min-width: 1200px;">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tên</th>
                                        <th>Vị trí</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_menu))
                                        @foreach($list_menu as $menu_item)
                                            <tr>
                                                <td>{{$menu_item['id']}}</td>
                                                <td><a href="{{route('admin_menu_update_view', ['menu_id' => $menu_item['id']])}}" class="text-dark">{{$menu_item['name']}}</a></td>
                                                <td>{{$menu_item['location'] && isset($menu_location[$menu_item['location']]) ? $menu_location[$menu_item['location']] : ''}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-{{$menu_item['status'] ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$menu_item['status'] ? 'Hiện' : 'Ẩn'}}">
                                                        <i class="fa fa-{{$menu_item['status'] ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_menu_update_view', ['menu_id' => $menu_item['id']])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-menu_id="{{$menu_item['id']}}" data-toggle="tooltip" title="Xoá">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="5"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let menu_id = $(this).attr("data-menu_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_menu_delete')}}",
                        data: {
                            id: menu_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection

