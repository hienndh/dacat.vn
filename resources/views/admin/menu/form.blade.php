@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <form id="Menu_Update_Form" action="{{$menu_object ? route('admin_menu_update') : route('admin_menu_create')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    @if($menu_object)
                        <input type="hidden" name="menu[id]" value="{{$menu_object['id']}}">
                    @endif
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN MENU</h4>
                            <div class="form-group">
                                <label for="menu_name">Tiêu đề</label>
                                <div class="input-group">
                                    <input type="text" name="menu[name]" id="menu_name" class="form-control"
                                           value="{{$menu_object ? $menu_object['name'] : 0}}" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="menu_location">Vị trí</label>
                                <select name="menu[location]" id="menu_location" class="form-control">
                                    <option value="0" {{$menu_object && $menu_object['location'] == '' ? 'selected' : ''}}>-- Để trống --</option>
                                    @foreach($list_menu_location as $menu_location_key => $menu_location_name)
                                        <option value="{{$menu_location_key}}" {{$menu_object && $menu_object['location'] == $menu_location_key ? 'selected' : ''}}>{{$menu_location_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="menu_status">Trạng thái:</label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="menu[status]" id="menu_status" {{$menu_object && $menu_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">{{$menu_object ? 'Cập nhật' : 'Tạo mới'}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <form id="Menu_Item_{{ $menu_item_object ? 'Update' : 'Create' }}_Form" method="post" enctype="multipart/form-data"
                      action="{{$menu_item_object ? route('admin_menu_item_update') : route('admin_menu_item_create')}}">
                    {{csrf_field()}}
                    @if($menu_item_object)
                        <input type="hidden" name="menu_item[id]" value="{{$menu_item_object['id']}}">
                    @endif
                    @if($menu_object)
                        <input type="hidden" name="menu_item[menu_id]" value="{{$menu_item_object ? $menu_item_object['menu_id'] : $menu_object['id']}}">
                    @endif
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">{{ $menu_item_object ? 'CẬP NHẬT CHỈ MỤC' : 'TẠO MỚI CHỈ MỤC' }}</h4>
                            {{--TIÊU ĐỀ--}}
                            <div class="form-group">
                                <label for="menu_item_title">Tiêu đề</label>
                                <input type="text" name="menu_item[title]" id="menu_item_title" class="form-control" autocomplete="off"
                                       value="{{$menu_item_object ? $menu_item_object['title'] : ''}}" required>
                            </div>
                            {{--LOẠI MENU--}}
                            <div class="form-group">
                                <label for="menu_item_type">Loại</label>
                                <select name="menu_item[type]" id="menu_item_type" class="form-control" onchange="changeMenuItemType()">
                                    @foreach($list_menu_item_type as $menu_item_type_key => $menu_item_type_name)
                                        <option value="{{$menu_item_type_key}}" {{$menu_item_object && $menu_item_object['type'] == $menu_item_type_key ? 'selected' : ''}}>{{$menu_item_type_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="menu_item_type_block" class="form-group">
                                @if(count($list_menu_item_type) && $menu_item_object)
                                    @foreach($list_menu_item_type as $menu_item_type_key => $menu_item_type_name)
                                        @if($menu_item_type_key != $menu_item_object['type'])
                                            @continue
                                        @endif
                                        <div class="form-group">
                                            <label for="menu_item_object_id">{{$menu_item_type_name}}</label>
                                            @switch($menu_item_type_key)
                                                @case('custom')
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-globe" aria-hidden="true"></i></span>
                                                    </div>
                                                    <input type="text" name="menu_item[link]" id="menu_item_link" class="form-control"
                                                           value="{{$menu_item_object ? $menu_item_object['link'] : ''}}" autocomplete="off" required>
                                                </div>
                                                @break
                                                @case('post')
                                                @case('page')
                                                @case('product')
                                                <select name="menu_item[object_id]" id="menu_item_object_id" class="form-control custom-select" onchange="changeMenuItemObject(this)">
                                                    @foreach(${'list_' . $menu_item_type_key} as $menu_item_type_object)
                                                        <option value="{{$menu_item_type_object['id']}}">{{$menu_item_type_object['title']}}</option>
                                                    @endforeach
                                                </select>
                                                @break
                                                @case('post_cat')
                                                <select name="menu_item[object_id]" id="menu_item_object_id" class="form-control custom-select" onchange="changeMenuItemObject(this)">
                                                    @include('admin.template.recursive_category', ['list_cat' => ${'list_' . $menu_item_type_key}, 'level' => 0, 'recursive_type' => 'select', 'type' => 'post', 'selected_cat_id' => ($menu_item_object ? $menu_item_object['object_id'] : [])])
                                                </select>
                                                @break
                                                @case('product_cat')
                                                <select name="menu_item[object_id]" id="menu_item_object_id" class="form-control custom-select" onchange="changeMenuItemObject(this)">
                                                    @include('admin.template.recursive_category', ['list_cat' => ${'list_' . $menu_item_type_key}, 'level' => 0, 'recursive_type' => 'select', 'type' => 'product', 'selected_cat_id' => ($menu_item_object ? $menu_item_object['object_id'] : [])])
                                                </select>
                                                @break
                                            @endswitch
                                        </div>
                                    @endforeach
                                @else
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-globe" aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" name="menu_item[link]" id="menu_item_link" class="form-control"
                                               value="{{$menu_item_object ? $menu_item_object['link'] : ''}}" autocomplete="off" required>
                                    </div>
                                @endif
                            </div>
                            {{--MỤC CHA--}}
                            <div class="form-group">
                                <label for="menu_item_parent_id">Menu cha</label>
                                <select class="form-control custom-select" name="menu_item[parent_id]" id="menu_item_parent_id">
                                    <option value="0" {{$menu_item_object && $menu_item_object['parent_id'] == 0 ? 'selected' : ''}}>-- Để trống --</option>
                                    @include('admin.template.recursive_menu_item', ['list_menu_item' => $list_menu_item, 'level' => 0, 'recursive_type' => 'select', 'selected_menu_item_id' => ($menu_item_object ? $menu_item_object['parent_id'] : 0)])
                                </select>
                            </div>
                            {{--THỨ TỰ SẮP XẾP--}}
                            <div class="form-group">
                                <label for="menu_item_sort">Thứ tự</label>
                                <div class="input-group">
                                    <input type="number" name="menu_item[sort]" id="menu_item_sort" class="form-control" min="0" step="1"
                                           value="{{$menu_item_object ? $menu_item_object['sort'] : 0}}" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">{{$menu_item_object ? 'Cập nhật' : 'Tạo mới'}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-12 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title font-bold">CẤU TRÚC MENU</h4>

                        <div class="menu_treeview" id="menu_treeview">
                            <ul class="list_menu_item">
                                @include('admin.template.recursive_menu_item', ['list_menu_item' => $list_menu_item, 'level' => 0, 'recursive_type' => 'tree_view'])
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    {{--    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">--}}
    <style type="text/css">
        .menu_treeview ul {
            padding: 0;
            margin: 0;
            list-style: none;
        }

        .menu_treeview ul li {
            margin: 10px 0px;
            padding: 5px 10px;
            display: block;
            line-height: 1.5;
            background-color: #fff;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            position: relative;
        }

        .menu_treeview ul li button {
            display: none;
        }

        .menu_treeview ul li:hover button {
            display: block;
        }

        .menu_treeview ul li .btn__edit {
            position: absolute;
            top: 50%;
            right: 36px;
            transform: translateY(-50%)
        }

        .menu_treeview ul li .btn__delete {
            position: absolute;
            top: 50%;
            right: 4px;
            transform: translateY(-50%)
        }
    </style>
@endsection

@section('admin_script')
    <!-- wysuhtml5 Plugin JavaScript -->
    {{--<script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>--}}
    {{--<script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>--}}

    @foreach($list_menu_item_type as $menu_item_type_key => $menu_item_type_name)
        <script type="text/html" id="html_type_{{$menu_item_type_key}}">
            <div class="form-group">
                <label for="menu_item_object_id">{{$menu_item_type_name}}</label>
                @switch($menu_item_type_key)
                    @case('custom')
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-globe" aria-hidden="true"></i></span>
                        </div>
                        <input type="text" name="menu_item[link]" id="menu_item_link" class="form-control" value="{{$menu_item_object ? $menu_item_object['link'] : ''}}" required>
                    </div>
                    @break
                    @case('post')
                    @case('page')
                    @case('product')
                    <select name="menu_item[object_id]" id="menu_item_object_id" class="form-control custom-select" onchange="changeMenuItemObject(this)">
                        @foreach(${'list_' . $menu_item_type_key} as $menu_item_type_object)
                            <option value="{{$menu_item_type_object['id']}}">{{$menu_item_type_object['title']}}</option>
                        @endforeach
                    </select>
                    @break
                    @case('post_cat')
                    <select name="menu_item[object_id]" id="menu_item_object_id" class="form-control custom-select" onchange="changeMenuItemObject(this)">
                        @include('admin.template.recursive_category', ['list_cat' => ${'list_' . $menu_item_type_key}, 'level' => 0, 'recursive_type' => 'select', 'type' => 'post', 'selected_cat_id' => ($menu_item_object ? $menu_item_object['object_id'] : [])])
                    </select>
                    @break
                    @case('product_cat')
                    <select name="menu_item[object_id]" id="menu_item_object_id" class="form-control custom-select" onchange="changeMenuItemObject(this)">
                        @include('admin.template.recursive_category', ['list_cat' => ${'list_' . $menu_item_type_key}, 'level' => 0, 'recursive_type' => 'select', 'type' => 'product', 'selected_cat_id' => ($menu_item_object ? $menu_item_object['object_id'] : [])])
                    </select>
                    @break
                @endswitch
            </div>
        </script>
    @endforeach

    <script type="text/javascript">
        function changeMenuItemType() {
            let type_key = $("#menu_item_type").val();
            if (!type_key) {
                toastError("Bạn chưa chọn loại cho menu con");
            }

            let html_type = $("#html_type_" + type_key).html();
            $("#menu_item_type_block").html(html_type);
            if (type_key !== "custom") {
                setTimeout(function () {
                    changeMenuItemObject($("#menu_item_type_block select#menu_item_object_id")[0]);
                }, 100);
            }
        }

        function changeMenuItemObject(selector) {
            let menu_item_title = selector.options[selector.selectedIndex].text;
            $("#menu_item_title").val(menu_item_title.replace("---", "").trim());
        }

        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let menu_item_id = $(this).attr("data-menu_item_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_menu_item_delete')}}",
                        data: {
                            id: menu_item_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection
