@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Post_{{ $account_object ? 'Update' : 'Create' }}_Form" action="{{$account_object ? route('admin_account_update') : route('admin_account_create')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($account_object)
                <input type="hidden" name="account[id]" value="{{$account_object['id']}}">
            @endif
            <div class="row">
                <div id="Post_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN KHÁCH HÀNG</h4>
                            <div class="form-group">
                                <label for="account_full_name">Họ tên</label>
                                <input type="text" class="form-control" name="account[full_name]" id="account_full_name" placeholder="Nhập tên khách hàng"
                                       autocomplete="off" value="{{$account_object ? $account_object['full_name'] : null}}" required>
                            </div>

                            <div class="form-group">
                                <label for="account_email">Email</label>
                                <input type="email" class="form-control" name="account[email]" id="account_email" placeholder="Nhập email khách hàng"
                                       autocomplete="off" value="{{$account_object ? $account_object['email'] : null}}" required>
                            </div>
                            @if($account_object)
                                <div class="form-group">
                                    <label for="account_user_name">Tài khoản</label>
                                    <input type="text" class="form-control" id="account_user_name" placeholder="Nhập tên khách hàng" required disabled value="{{$account_object ? $account_object['user_name'] : null}}">
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="account_user_name">Tài khoản</label>
                                    <input type="text" class="form-control" name="account[user_name]" id="account_user_name" placeholder="Nhập tài khoản khách hàng"
                                           autocomplete="off" value="{{$account_object ? $account_object['user_name'] : null}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="account_password">Mật khẩu</label>
                                    <input type="password" class="form-control" name="account[password]" id="account_password" placeholder="Nhập mật khẩu khách hàng" required>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="account_phone">Số điện thoại</label>
                                <input type="text" class="form-control" name="account[phone]" id="account_phone" placeholder="Nhập số điện thoại khách hàng"
                                       autocomplete="off" value="{{$account_object ? $account_object['phone'] : null}}" required>
                            </div>
                            <div class="form-group">
                                <label for="account_address">Địa chỉ</label>
                                <input type="text" class="form-control" name="account[address]" id="account_address" placeholder="Nhập địa chỉ của khách hàng"
                                       autocomplete="off" value="{{$account_object ? $account_object['address'] : null}}" required>
                            </div>
                            <div class="form-group">
                                <label for="account_address">Giới tính</label>
                                <div class="demo-radio-button">
                                    <input name="account[gender]" type="radio" value="1" id="account_gender_1" {{$account_object && $account_object['gender'] == 1 ? 'checked="true"' : null}}>
                                    <label for="account_gender_1">Nam</label>
                                    <input name="account[gender]" type="radio" value="0" id="account_gender_2" {{$account_object && $account_object['gender'] == 0 ? 'checked="true"' : null}}>
                                    <label for="account_gender_2">Nữ</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Post_Sidebar" class="col-12 col-md-3">
                    {{--ẢNH ĐẠI DIỆN--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.template.image_preview', ['input_name' => 'account[image]', 'input_id' => 'account_image', 'input_image' => ($account_object ? $account_object['image'] : null)])
                        </div>
                    </div>
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <div class="form-group">
                                <label for="account_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="account[status]" id="account_status" {{$account_object && $account_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info">{{$account_object ? 'Cập nhật' : 'Tạo mới'}}</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
@endsection

@section('admin_script')
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
