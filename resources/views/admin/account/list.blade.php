@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-info" onclick="location.href = '{{ route("admin_account_new_view") }}'">Thêm mới</button>
                        <div class="pull-right">
                            <form action="{{route('admin_account_list_view')}}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="postTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="postTable" class="table table-bordered" style="min-width: 1200px;">
                                    <thead>
                                    <tr>
                                        <th>Ảnh đại diện</th>
                                        <th>Tài khoản</th>
                                        <th>Họ và tên</th>
                                        <th>Email</th>
                                        <th>Số điện thoại</th>
                                        <th width="100px">Trạng thái</th>
                                        <th width="180px">Ngày tạo</th>
                                        <th width="120px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_account))
                                        @foreach($list_account as $account)
                                            <tr>
                                                <td><img height="30" class="image_preview" src="{{$account->image ? $account->image : asset('img/placeholder.png')}}" alt="{{$account->user_name}}" title="{{$account->user_name}}"></td>
                                                <td>{{$account->user_name}}</td>
                                                <td>{{$account->full_name}}</td>
                                                <td>{{$account->email}}</td>
                                                <td>{{$account->phone}}</td>
                                                <td align="center">
                                                    <button type="button" class="btn btn-{{$account->status == 1 ? 'success' : 'danger'}} btn-rounded btn-sm btn__update" account_id="{{$account->id}}" data-toggle="tooltip" title="{{$account->status == 1 ? 'Hoạt động' : 'Không hoạt động'}}">
                                                        <i class="fa fa-{{$account->status == 1 ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>{{date('d/m/Y',$account->created_at)}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_account_update_view', ['account_id' => $account->id])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-account_id="{{$account->id}}" data-toggle="tooltip" title="Xoá">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="8"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                        @if(count($list_account))
                            {{ $list_account->appends($params)->links('admin.layout.pagination', ['list_object' => $list_account]) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__update').on("click", function (e) {
                let camp_id = $(this).attr("account_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_account_status')}}",
                        data: {
                            id: camp_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn cập nhật trạng thái?");
            });

            $('.btn__delete').on("click", function (e) {
                let account_id = $(this).attr("data-account_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_account_delete')}}",
                        data: {
                            id: account_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection

