<div id="Pagination" class="dataTables_wrapper">
    <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Hiển thị {{ $list_object->firstItem() }} đến {{ $list_object->lastItem() }} của {{$list_object->total()}} bản ghi</div>
    <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
        @if(!$list_object->onFirstPage())
            <a href="{{ $list_object->previousPageUrl() }}" class="paginate_button previous" aria-controls="dataTable" tabindex="0" id="dataTable_previous">Trước</a>
        @endif
        <span>
            @for($i=1;$i<=$list_object->lastPage();$i++)
                <a href="{{ $list_object->url($i) }}" class="paginate_button {{ $list_object->currentPage() == $i ? 'current' : ''}}" aria-controls="dataTable" tabindex="0">{{$i}}</a>
            @endfor
        </span>
        @if($list_object->hasMorePages())
            <a href="{{ $list_object->nextPageUrl() }}" class="paginate_button next" aria-controls="dataTable" tabindex="0" id="dataTable_next">Sau</a>
        @endif
    </div>
</div>
