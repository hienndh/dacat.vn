<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        @if(isset($breadcrumb['return_back']))
            <a href="{{route($breadcrumb['return_back'])}}{{isset($breadcrumb['query']) ? '?'.http_build_query($breadcrumb['query']) : ''}}" class="badge badge-info" data-toggle="tooltip" title="Quay lại">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
        @endif
        <h3 class="text-themecolor d-inline-block">{{$breadcrumb['main_title']}}</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('admin_dashboard')}}">Trang chủ</a>
            </li>
            @if(isset($breadcrumb['router_list']) && count($breadcrumb['router_list']))
                @foreach($breadcrumb['router_list'] as $index => $route_item)
                    <li class="breadcrumb-item">
                        <a href="{{route($route_item['name'])}}">{{$route_item['title']}}</a>
                    </li>
                @endforeach
            @else
                <li class="breadcrumb-item active">{{$breadcrumb['main_title']}}</li>
            @endif
        </ol>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
