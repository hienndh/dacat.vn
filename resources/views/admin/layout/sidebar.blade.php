<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile" style="background: url({{asset('admin/assets/images/background/user-info.jpg')}}) no-repeat;">
            <!-- User profile image -->
            <div class="profile-img"><img src="{{$web_setting['ta_favicon'] ?: asset('img/placeholder.png')}}" alt="user"/></div>
            <!-- User profile text-->
            <div class="profile-text"><a href="javascript: 0;" class="dropdown-toggle link u-dropdown alert alert-info text-info font-weight-bold" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{ Auth::user()->first_name }} <span class="caret"></span></a>
                <div class="dropdown-menu animated flipInY">
                    <a href="{{route('admin_auth_reset_password_view')}}" class="dropdown-item"><i class="ti-lock"></i> Đổi mật khẩu</a>
                    <div class="dropdown-divider"></div>
                    <a href="javascript: 0;" class="dropdown-item btn__logout"><i class="fa fa-power-off"></i> Đăng xuất</a>
                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                @foreach($admin_sidebar as $menu_item)
                    @switch($menu_item['type'])
                        @case('caption')
                        <li class="nav-small-cap text-center"><strong>{{$menu_item['label']}}</strong></li>
                        @break
                        @case('link')
                        <li>
                            <a href="{{$menu_item['href']}}" aria-expanded="false">
                                {!! $menu_item['icon'] ? '<i class="'.$menu_item['icon'].'"></i>' : '' !!}
                                <span class="hide-menu">{{$menu_item['label']}}</span>
                            </a>
                        </li>
                        @break
                        @case('parent')
                        <li class="{{ strlen($admin_route_uri) && strpos($admin_route_uri, $menu_item['prefix']) !== false ? 'active' : ''}}">
                            <a class="has-arrow " href="{{$menu_item['href']}}" aria-expanded="false">
                                {!! $menu_item['icon'] ? '<i class="'.$menu_item['icon'].'"></i>' : '' !!}
                                <span class="hide-menu">{{$menu_item['label']}}</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                @if(count($menu_item['child']))
                                    @foreach($menu_item['child'] as $menu_child)
                                        <a href="{{$menu_child['href']}}" aria-expanded="false" class="{{strlen($admin_route_uri) && strpos($menu_child['href'], $admin_route_uri) !== false ? 'active' : ''}}">
                                            {!! $menu_child['icon'] ? '<i class="'.$menu_child['icon'].'"></i>' : '' !!}
                                            {{$menu_child['label']}}
                                        </a>
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                        @break
                    @endswitch
                @endforeach
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        <!-- item-->
        {{--<a href="javascript: 0;" class="link" data-toggle="tooltip" title="Cài đặt chung"><i class="ti-settings"></i></a>--}}
        <!-- item-->
        <a href="javascript: 0;" class="link btn__logout" data-toggle="tooltip" title="Đăng xuất"><i class="mdi mdi-power"></i></a>
    </div>
    <!-- End Bottom points-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
