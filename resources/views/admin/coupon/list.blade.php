@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="pull-right">
                            <form action="{{route('admin_coupon_list_view')}}" method="GET">
                                <div class="form-inline">
                                    <div class="form-group mr-2">
                                        <label class="mr-2" for="order_status">Chiến dịch: </label>
                                        <select class="form-control ml-2 select2" data-placeholder="Lọc chiến dịch" name="campaign_id">
                                            @foreach($listCampaign as $campaign)
                                                <option {{(isset($params['campaign_id']) && $params['campaign_id'] == $campaign->id) ? 'selected' : '' }} value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <div id="couponTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="couponTable" class="table table-bordered" style="min-width: 1000px;">
                                    <thead>
                                    <tr>
                                        <th>Mã coupon</th>
                                        <th>Trạng thái</th>
                                        <th>Người sử dụng</th>
                                        <th>Ngày sử dụng</th>
                                        <th>Thời hạn hoạt động</th>
                                        <th>Loại coupon</th>
                                        <th>Chiến dịch</th>
                                        <th>Giá trị</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($listCoupon))
                                        @foreach($listCoupon as $coupon)
                                            <tr>
                                                <td>{{$coupon->code}}</td>
                                                <td align="center">
                                                    <button type="button" class="btn btn-{{$coupon->status == 1 ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$coupon->status == 1 ? 'Hoạt động' : 'Không hoạt động'}}">
                                                        <i class="fa fa-{{$coupon->status == 1 ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>{{isset($coupon->account) ? $coupon->account->user_name : ''}}</td>
                                                <td>{{isset($coupon->used_date) && $coupon->used_date ? date('d/m/Y H:i', $coupon->used_date) : ''}}</td>
                                                <td>{{isset($coupon->campaign->startDate) ? date('d/m/Y H:i',$coupon->campaign->startDate) : ''}} - {{isset($coupon->campaign->endDate) ? date('d/m/Y H:i',$coupon->campaign->endDate) : ''}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-{{$coupon->type_color()}} btn-sm">
                                                        {{$coupon->type_text()}}
                                                    </button>
                                                </td>
                                                <td>{{isset($coupon->campaign->name) ? $coupon->campaign->name : ''}}</td>
                                                <td>{{isset($coupon->campaign) ? Helper::getFormattedNumber($coupon->campaign->value) : ''}}{{isset($coupon->campaign) ? $coupon->campaign->value_type == 1 ? 'đ' : '%' : ''}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="8"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                        @if(count($listCoupon))
                            {{ $listCoupon->appends($params)->links('admin.layout.pagination', ['list_object' => $listCoupon]) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="create-coupon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tạo mới coupon</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="create" action="{{route('admin_coupon_create')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="campaign" class="col-form-label">Chon chiến dịch:</label>
                                <select class="form-control select2"
                                        data-placeholder="Chọn chiến dịch" name="coupon[campaign_id]"
                                        style="width: 100%;">
                                    @foreach($listCampaign as $campaign)
                                        <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="total" class="col-form-label">Số lượng:</label>
                                <input class="form-control" id="total" name="coupon[total]">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" onclick="createMultipleCoupon()" class="btn btn-primary">Tạo mới</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let camp_id = $(this).attr("data-coupon_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_coupon_delete')}}",
                        data: {
                            id: camp_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });

        function createMultipleCoupon() {
            $("#create").submit();
        }
    </script>
@endsection

