@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        {{--<button type="button" class="btn btn-info" onclick="location.href = '{{ route("admin_order_new_view") }}'">Thêm mới</button>--}}
                        <div class="pull-left">
                            <form id="report-status" action="{{route('admin_order_report_status_view')}}" method="GET">
                                <div class="input-group">
                                    <div class="form-inline">
                                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;text-align: center">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                        <input class="d-none" id="from" name="from" value="{{isset($params['from']) ? $params['from'] : ''}}">
                                        <input class="d-none" id="to" name="to" value="{{isset($params['to']) ? $params['to'] : ''}}">
                                    </div>
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="orderTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="orderTable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Số lượng đơn hàng</th>
                                        <th>Tổng phụ</th>
                                        <th>Thành tiền</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($reports))
                                        @foreach($reports as $report)
                                            <tr>
                                                <td>{{$report->total_order}}</td>
                                                <td>{{number_format($report->sum_total_real)}}</td>
                                                <td>{{number_format($report->sum_total)}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-{{$report->status_color()}} btn-sm">
                                                        {{$report->status_text()}}
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="4"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.min.js"></script>

    <script>
        $(function () {
            var startDate = "{{isset($params['from']) ? date('Y-m-d',$params['from']) : null}}";
            var endDate = "{{isset($params['to']) ? date('Y-m-d',$params['to']) : null}}";
            var start = _.isEmpty(startDate) ? moment().subtract(7, 'days') : moment(startDate);
            var end = _.isEmpty(endDate) ? moment() : moment(endDate);

            function cb(start, end) {
                $('#from').val(start.unix());
                $('#to').val(end.unix());
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                locale: {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Chọn",
                    "cancelLabel": "Hủy",
                    "fromLabel": "Từ",
                    "toLabel": "Đến",
                    "customRangeLabel": "Tùy chọn",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Tháng 1",
                        "Tháng 2",
                        "Tháng 3",
                        "Tháng 4",
                        "Tháng 5",
                        "Tháng 6",
                        "Tháng 7",
                        "Tháng 8",
                        "Tháng 9",
                        "Tháng 10",
                        "Tháng 11",
                        "Tháng 12"
                    ],
                    "firstDay": 1
                },
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                    '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
        });
    </script>
@endsection

