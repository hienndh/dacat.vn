<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style>
        @media print {
            @page {
                size: landscape
            }
        }

        body {
            font-family: DejaVu Sans, sans-serif;
        }

        .content {
            width: 100%;
            margin: auto;
        }

        .header {
            display: flex;
            margin-bottom: 90px;
        }

        .col-left, .col-right {
            width: 50%;
        }

        .col-left_content {
            width: 300px;
            text-align: center;
        }

        .col-left_content h2 {
            font-size: 23px;
            margin-bottom: 5px;
            color: #698e3a;
        }

        .col-left_content span {
            font-size: 10px;
            font-style: italic;
        }

        .col-right_content {
            width: 335px;
            float: right;
            text-align: center;
        }

        .col-right_content .time-export {
            color: #a7a7a7;
            font-size: 11px;
        }

        .company-name {
            font-size: 10px;
            font-weight: bold;
        }

        table {
            font-size: 12px;
        }

        table tr th {
            background: #333;
            color: white;
            line-height: 18px;
            text-align: center;
            vertical-align: middle;
        }

        table tr td {
            vertical-align: middle;
            line-height: 18px;
        }

        .text-italic {
            font-style: italic;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-danger {
            color: red;
            font-weight: bold;
        }

        .main-content {
            clear: both;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="content">
    <div class="header">
        <div class="col-left">
            <div class="col-left_content">
                <h2>BÁO CÁO DOANH THU</h2>
                <span class="time">
                    Từ {{date('d/m/Y', $params['from'])}} đến {{date('d/m/Y', $params['to'])}}
                </span>
            </div>
        </div>
        <div class="col-right" style="float: right">
            <div class="col-right_content">
                <h4 class="time-export text-italic">
                    Hà Nội, Ngày {{date('d', time())}} tháng {{date('m', time())}} năm {{date('Y H:i', time())}}
                </h4>
                <div class="logo">
                    <img src="{{url($web_setting['ta_logo']) ?? asset('img/dingo-logo.jpg')}}" height="80" alt="">
                </div>
                <label class="company-name text-italic">DINGO JOINT STOCK COMPANY</label>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="main-content">
        <table style="width:100%;">
            <thead>
            <tr>
                <th>STT</th>
                <th style="width: 25%">Danh mục</th>
                <th style="width: 30%">Sản phẩm</th>
                <th>Số lượng</th>
                <th>Đơn giá</th>
                <th style="width: 20%">Doanh thu</th>
            </tr>
            </thead>
            <tbody>
            @if(count($reports))
                @php
                    $total_all = 0;
                @endphp
                @foreach($reports as $index => $report)
                    @php
                        $total_all += $report->total_money;
                    @endphp
                    <tr>
                        <td class="text-center">{{$index+1}}</td>
                        <td>{{$report->findCateByProduct($report->product_id) ? implode(', ', $report->findCateByProduct($report->product_id)) : '' }}</td>
                        <td>{{$report->product_title}}</td>
                        <td class="text-center">{{Helper::getFormattedNumber($report->total_product)}}</td>
                        <td class="text-right">{{Helper::getFormattedNumber($report->order_product_price)}} VND</td>
                        <td class="text-right">{{Helper::getFormattedNumber($report->total_money)}} VND</td>
                    </tr>
                @endforeach

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-danger" colspan="2">Tổng cộng</td>
                    <td class="text-danger text-right">{{Helper::getFormattedNumber($total_all)}} VND</td>
                </tr>
            @else
                <tr>
                    <td class="text-center" colspan="5"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
</body>
</html>





