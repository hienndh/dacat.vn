@switch($recursive_type)
    @case('select')
    @if(count($list_cat))
        @foreach($list_cat as $cat_item)
            <option value="{{$cat_item['id']}}" {{$cat_item['id'] == $selected_cat_id ? 'selected' : ''}}>{{str_repeat('---', $level)}} {{$cat_item['title']}}</option>
            @include('admin.template.recursive_category', ['list_cat' => $cat_item->childCategory, 'level' => ($level+1), 'recursive_type' => 'select', 'selected_cat_id' => $selected_cat_id])
        @endforeach
    @endif
    @break
    @case('table')
    @if(count($list_cat))
        @foreach($list_cat as $cat_item)
            <tr>
                {{--                    <td>{{$cat_item['id']}}</td>--}}
                <td><img height="30" class="image_preview" src="{{$cat_item['image'] ? $cat_item['image'] : asset('img/placeholder.png')}}" alt="{{$cat_item['title']}}" title="{{$cat_item['title']}}"></td>
                <td><a href="{{route('admin_' . $cat_item['type'] . 'egory_update_view', ['cat_id' => $cat_item['id']])}}" class="text-dark">{{str_repeat('---', $level)}} {{$cat_item['title']}}</a></td>
                <td>{{$cat_item['slug']}}</td>
                <td>
                    <button type="button" class="btn btn-{{$cat_item['status'] ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$cat_item['status'] ? 'Hiện' : 'Ẩn'}}">
                        <i class="fa fa-{{$cat_item['status'] ? 'eye' : 'eye-slash'}}"></i>
                    </button>
                </td>
                <td>
                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                            onclick="window.location.href = '{{route('admin_' . $cat_item['type'] . 'egory_update_view', ['cat_id' => $cat_item['id']])}}'">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-cat_id="{{$cat_item['id']}}" data-toggle="tooltip" title="Xoá">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('client_' . $cat_item['type'] . '_view', ['slug' => $cat_item['slug']])}}" class="btn btn-info btn-rounded btn-sm" data-toggle="tooltip" title="Xem" target="_blank">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            @include('admin.template.recursive_category', ['list_cat' => $cat_item->childCategory, 'level' => ($level+1), 'recursive_type' => 'table'])
        @endforeach
    @endif
    @break
    @case('checkbox')
    @if(count($list_cat))
        @foreach($list_cat as $cat_item)
            <li>
                <input type="checkbox" name="{{$type}}[category][]" id="{{$type}}_cat_{{$cat_item['id']}}" class="filled-in chk-col-light-blue"
                       value="{{$cat_item['id']}}" {{in_array($cat_item['id'], $cat_ids) ? 'checked' : ''}} />
                <label for="{{$type}}_cat_{{$cat_item['id']}}">{{str_repeat('---', $level)}} {{$cat_item['title']}}</label>
            </li>
            @include('admin.template.recursive_category', ['list_cat' => $cat_item->childCategory, 'level' => ($level+1), 'recursive_type' => 'checkbox', 'type' => $type, 'cat_ids' => $cat_ids])
        @endforeach
    @endif
    @break
    @case('setting_checkbox')
    @if(count($list_cat))
        @foreach($list_cat as $cat_item)
            <li>
                <input type="checkbox" name="{{$setting_id}}[]" id="{{$type}}_cat_{{$cat_item['id']}}" class="filled-in chk-col-light-blue"
                       value="{{$cat_item['id']}}" {{in_array($cat_item['id'], $cat_ids) ? 'checked' : ''}} />
                <label for="{{$type}}_cat_{{$cat_item['id']}}">{{str_repeat('---', $level)}} {{$cat_item['title']}}</label>
            </li>
            @include('admin.template.recursive_category', ['list_cat' => $cat_item->childCategory, 'level' => ($level+1), 'recursive_type' => 'checkbox', 'type' => $type, 'cat_ids' => $cat_ids, 'setting_id' => $setting_id])
        @endforeach
    @endif
    @break
@endswitch

