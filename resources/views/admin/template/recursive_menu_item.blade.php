@switch($recursive_type)
    @case('select')
    @if(count($list_menu_item))
        @foreach($list_menu_item as $menu_item)
            <option value="{{$menu_item['id']}}" {{$menu_item['id'] == $selected_menu_item_id ? 'selected' : ''}}>{{str_repeat('---', $level)}} {{$menu_item['title']}}</option>
            @include('admin.template.recursive_menu_item', ['list_menu_item' => $menu_item->childMenuItem, 'level' => ($level+1), 'recursive_type' => 'select', 'selected_menu_item_id' => $selected_menu_item_id])
        @endforeach
    @endif
    @break

    @case('tree_view')
    @if(count($list_menu_item))
        @foreach($list_menu_item as $menu_item)
            <li class="menu_item" data-id="{{$menu_item['id']}}" style="margin-left: {{30*$level}}px">
                {{$menu_item['title']}}
                <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                        onclick="window.location.href = '{{route('admin_menu_update_view', ['menu_id' => $menu_item['menu_id'], 'menu_item_id' => $menu_item['id']])}}'">
                    <i class="fa fa-pencil"></i>
                </button>
                <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-menu_item_id="{{$menu_item['id']}}" data-toggle="tooltip" title="Xoá">
                    <i class="fa fa-trash"></i>
                </button>
            </li>
            @if(count($menu_item->childMenuItem))
                <ul class="list_menu_item_child">
                    @include('admin.template.recursive_menu_item', ['list_menu_item' => $menu_item->childMenuItem, 'level' => ($level+1), 'recursive_type' => 'tree_view'])
                </ul>
            @endif
        @endforeach
    @endif
    @break
@endswitch

