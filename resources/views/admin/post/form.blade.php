@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Post_{{ $post_object ? 'Update' : 'Create' }}_Form" action="{{$post_object ? route('admin_post_update') : route('admin_post_create')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($post_object)
                <input type="hidden" name="post[id]" value="{{$post_object['id']}}">
            @endif
            <div class="row">
                <div id="Post_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN BÀI VIẾT</h4>
                            <div class="form-group">
                                <label for="post_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="post[title]" id="post_title" aria-describedby="helpPostTitle" placeholder="Nhập tiêu đề bài viết"
                                       autocomplete="off" value="{{$post_object ? $post_object['title'] : null}}" onkeyup="getSlug('post_title', 'post_slug', 'post')" required>
                                <small id="helpPostTitle" class="form-text text-muted">Tiêu đề bài viết là bắt buộc</small>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="post_slug_pre">{{url('/bai-viet/')}}/</span>
                                    </div>
                                    <input type="text" class="form-control" name="post[slug]" id="post_slug" aria-describedby="post_slug_pre"
                                           placeholder="Chuỗi cho đường dẫn tĩnh" autocomplete="off" value="{{$post_object ? $post_object['slug'] : null}}" readonly required>
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button" data-toggle="tooltip" title="Tạo slug tự động từ tiêu đề" onclick="getSlug('post_title', 'post_slug', 'post')">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="post_content">Nội dung</label>
                                <textarea class="form-control mce_editor" name="post[content]" id="post_content" rows="3">{{$post_object ? $post_object['content'] : null}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="post_description">Mô tả ngắn</label>
                                <textarea class="form-control" name="post[description]" id="post_description" rows="3">{{$post_object ? $post_object['description'] : null}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">CẤU HÌNH SEO</h4>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="post_seo_title">SEO Tiêu đề</label>
                                <input type="text" name="post[seo_title]" id="post_seo_title" class="form-control col-9 col-xs-9 col-md-10" maxlength="70"
                                       placeholder="" value="{{$post_object ? $post_object['seo_title'] : null}}" autocomplete="off">
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="post_seo_title">SEO Mô tả</label>
                                <textarea name="post[seo_description]" id="post_seo_description" class="form-control col-9 col-xs-9 col-md-10" rows="3"
                                          maxlength="170" placeholder="" autocomplete="off">{{$post_object ? $post_object['seo_description'] : null}}</textarea>
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="post_seo_title">SEO Từ khóa</label>
                                <div class="col-9 col-xs-9 col-md-10 p-0">
                                    <input type="text" name="post[seo_keyword]" id="post_seo_keyword" class="form-control" data-role="tagsinput"
                                           placeholder="" value="{{$post_object ? $post_object['seo_keyword'] : null}}" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Post_Sidebar" class="col-12 col-md-3">
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <div class="form-group">
                                <label for="post_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="post[status]" id="post_status" {{$post_object && $post_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info">{{$post_object ? 'Cập nhật' : 'Đăng bài'}}</button>
                            @if($post_object)
                                <a href="{{route('client_post_detail_view', ['slug' => $post_object['slug']])}}" class="btn btn-success pull-right" target="_blank">Xem</a>
                            @endif
                        </div>
                    </div>
                    {{--ẢNH ĐẠI DIỆN--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.template.image_preview', ['input_name' => 'post[image]', 'input_id' => 'post_image', 'input_image' => ($post_object ? $post_object['image'] : null)])
                        </div>
                    </div>
                    {{--CHUYÊN MỤC BÀI VIẾT--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">CHUYÊN MỤC BÀI VIẾT</h4>
                            <div class="cat_list_wrapper post_cat_list">
                                <ul class="list-style-none">
                                    @include('admin.template.recursive_category', ['list_cat' => $list_cat, 'level' => 0, 'recursive_type' => 'checkbox', 'type' => 'post', 'cat_ids' => ($post_object ? $post_object['cat_ids'] : [])])
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
