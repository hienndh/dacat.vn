@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Category_{{ $cat_object ? 'Update' : 'Create' }}_Form" action="{{$cat_object ? route('admin_category_update') : route('admin_category_create')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="category[type]" value="{{$cat_type}}">
            @if($cat_object)
                <input type="hidden" name="category[id]" value="{{$cat_object['id']}}">
            @endif
            <div class="row">
                <div id="Category_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN {{$cat_type == 'post_cat' ? 'CHUYÊN MỤC': 'DANH MỤC'}}</h4>
                            <div class="form-group">
                                <label for="category_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="category[title]" id="category_title" aria-describedby="helpCategoryTitle"
                                       placeholder="Nhập tiêu đề {{$cat_type == 'post_cat' ? 'chuyên mục bài viết': 'danh mục sản phẩm'}}" autocomplete="off"
                                       onkeyup="getSlug('category_title', 'category_slug', 'category')" value="{{$cat_object ? $cat_object['title'] : null}}" required>
                                <small id="helpCategoryTitle" class="form-text text-muted">Tiêu đề {{$cat_type == 'post_cat' ? 'chuyên mục bài viết': 'danh mục sản phẩm'}} là bắt buộc</small>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="category_slug_pre">{{url($cat_type == 'post_cat' ? '/chuyen-muc/': '/danh-muc/')}}/</span>
                                    </div>
                                    <input type="text" class="form-control" name="category[slug]" id="category_slug" aria-describedby="category_slug_pre"
                                           placeholder="Chuỗi cho đường dẫn tĩnh" autocomplete="off" value="{{$cat_object ? $cat_object['slug'] : null}}" readonly required>
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button" data-toggle="tooltip" title="Tạo slug tự động từ tiêu đề" onclick="getSlug('category_title', 'category_slug', 'category')">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category_content">Nội dung</label>
                                <textarea class="form-control mce_editor" name="category[content]" id="category_content" rows="3">{{$cat_object ? $cat_object['content'] : null}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Mô tả ngắn</label>
                                <textarea class="form-control" name="category[description]" id="category_description" rows="3">{{$cat_object ? $cat_object['description'] : null}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">CẤU HÌNH SEO</h4>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="category_seo_title">SEO Tiêu đề</label>
                                <input type="text" name="category[seo_title]" id="category_seo_title" class="form-control col-9 col-xs-9 col-md-10" placeholder=""
                                       maxlength="70" autocomplete="off" value="{{$cat_object ? $cat_object['seo_title'] : null}}">
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="category_seo_title">SEO Mô tả</label>
                                <textarea name="category[seo_description]" id="category_seo_description" class="form-control col-9 col-xs-9 col-md-10" rows="3" maxlength="170" placeholder="" autocomplete="off">{{$cat_object ? $cat_object['seo_description'] : null}}</textarea>
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-3 col-xs-3 col-md-2 text-left" for="category_seo_title">SEO Từ khóa</label>
                                <div class="col-9 col-xs-9 col-md-10 p-0">
                                    <input type="text" name="category[seo_keyword]" id="category_seo_keyword" class="form-control" data-role="tagsinput"
                                           placeholder="" value="{{$cat_object ? $cat_object['seo_keyword'] : null}}" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Category_Sidebar" class="col-12 col-md-3">
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <div class="form-group">
                                <label for="category_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="category[status]" id="category_status" {{$cat_object && $cat_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info">{{$cat_object ? 'Cập nhật' : 'Tạo mới'}}</button>
                        </div>
                    </div>
                    {{--CHUYÊN MỤC BÀI VIẾT CHA - DANH MỤC SẢN PHẨM CHA--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">{{$cat_type == 'post_cat' ? 'CHUYÊN MỤC' : 'DANH MỤC'}} CHA</h4>
                            <div class="category_list">
                                <select class="form-control custom-select" name="category[parent_id]">
                                    <option value="0" {{$cat_object && $cat_object['parent_id'] == 0 ? 'selected' : ''}}>-- Để trống --</option>
                                    @include('admin.template.recursive_category', ['list_cat' => $list_cat, 'level' => 0, 'recursive_type' => 'select', 'selected_cat_id' => ($cat_object ? $cat_object['parent_id'] : 0)])
                                </select>
                            </div>
                        </div>
                    </div>
                    {{--ẢNH ĐẠI DIỆN--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.template.image_preview', ['input_name' => 'category[image]', 'input_id' => 'category_image', 'input_image' => ($cat_object ? $cat_object['image'] : null)])
                        </div>
                    </div>
                    {{--ẢNH BANNER--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH BANNER</h4>
                            @include('admin.template.image_preview', ['input_name' => 'category[banner]', 'input_id' => 'category_banner', 'input_image' => ($cat_object ? $cat_object['banner'] : null)])
                            <div class="form-group mt-3">
                                <label for="">Link tuỳ chọn của banner</label>
                                <input type="text" name="category[banner_link]" id="category_banner_link" class="form-control" value="{{$cat_object ? $cat_object['banner_link'] : null}}" autocomplete="off">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
