@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="pull-right">
                            <form class="form-inline" action="{{route('admin_repository_list_view')}}" method="GET">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="postTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="postTable" class="table table-bordered" style="min-width: 1200px;">
                                    <thead>
                                    <tr>
                                        <th>SKU</th>
                                        <th>Ảnh</th>
                                        <th>Tiêu đề</th>
                                        <th>Danh mục</th>
                                        <th>Số lượng đã bán</th>
                                        <th>Số lượng còn lại</th>
                                        <th>Tổng số lượng hàng</th>
                                        <th width="120px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_product))
                                        @foreach($list_product as $product_item)
                                            <tr>
                                                <td>{{$product_item['sku']}}</td>
                                                <td><img height="30" class="image_preview" src="{{$product_item['image'] ? $product_item['image'] : asset('img/placeholder.png')}}" alt="{{$product_item['title']}}" title="{{$product_item['title']}}"></td>
                                                <td width="300px"><a href="{{route('admin_repository_update_view', ['product_id' => $product_item['id']])}}" class="text-dark">{{$product_item['title']}}</a></td>
                                                <td width="100px">{{$product_item['list_cat']}}</td>
                                                <td class="text-center">{{$product_item->sold_quantity()}}</td>
                                                <td class="text-center">{{$product_item->remain_quantity()}}</td>
                                                <td class="text-center">{{$product_item->total_quantity()}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_repository_update_view', ['product_id' => $product_item['id']])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="7"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                        @if(count($list_product))
                            {{ $list_product->appends($params)->links('admin.layout.pagination', ['list_object' => $list_product]) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
@endsection

