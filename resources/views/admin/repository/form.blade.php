@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Repository_Update_Form" action="{{route('admin_repository_update')}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="repository[product_id]" value="{{$product_object['id']}}">
            <div class="row">
                <div id="Repository_Content" class="col-12 col-md-8 col-lg-9">
                    {{--NỘI DUNG--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN KHO: {{$product_object ? $product_object['title'] : null}}</h4>

                            {{--THUỘC TÍNH--}}
                            <div class="row">
                                @if(count($list_attr) && $product_object)
                                    @foreach($list_attr as $attr_item)
                                        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xlg-8">
                                            <h4>{{$attr_item->title}}</h4>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <th>{{$attr_item->title}}</th>
                                                    <th>Số lượng đã bán</th>
                                                    <th>Số lượng còn lại</th>
                                                    <th>Tổng số lượng</th>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($attr_item->childAttribute))
                                                        @foreach($attr_item->childAttribute as $attr_child_item)
                                                            @php
                                                                $repo = $product_object->repository($attr_child_item->id);
                                                            @endphp
                                                            <tr class="repo repo_{{$attr_child_item->id}}">
                                                                <td class="align-middle">{{$attr_child_item->title}}</td>
                                                                <td class="align-middle">
                                                                    {{$repo['sold_quantity'] ?? 0}}
                                                                </td>
                                                                <td>
                                                                    <input type="number" name="repository[repo][{{$attr_child_item->id}}][remain_quantity]"
                                                                           class="form-control" min="0" step="1" value="{{$repo['remain_quantity'] ?? 0}}" readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="number" name="repository[repo][{{$attr_child_item->id}}][total_quantity]" class="form-control total_quantity"
                                                                           min="{{$repo['sold_quantity']}}"
                                                                           step="1" value="{{$repo['total_quantity'] ?? 0}}" required
                                                                           data-min_total_quantity="{{$repo['sold_quantity']}}"
                                                                           data-old_total_quantity="{{$repo['total_quantity']}}" data-attr_child_id="{{$attr_child_item->id}}">
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Repository_Sidebar" class="col-12 col-md-4 col-lg-3">
                    {{--Cập nhật--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <button type="submit" class="btn btn-info">Cập nhật</button>
                            @if($product_object)
                                <a href="{{route('admin_product_update_view', ['product_id' => $product_object['id']])}}" class="btn btn-success pull-right" data-toggle="tooltip" title="Xem thông tin sản phẩm" target="_blank">Xem</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
@endsection

@section('admin_script')
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/javascript">
        function updateQuantity(attr_child_id) {
            let totalQuantityObj = $('.repo_' + attr_child_id + ' input[name="repository[repo][' + attr_child_id + '][total_quantity]"]');
            let min_total_quantity = totalQuantityObj.data('min_total_quantity');
            let old_total_quantity = totalQuantityObj.data('old_total_quantity');
            let new_total_quantity = totalQuantityObj.val();
            if (new_total_quantity < min_total_quantity) {
                totalQuantityObj.val(min_total_quantity);
                return false;
            }
            let change_quantity = parseFloat(new_total_quantity - old_total_quantity);
            let remainQuantityObj = $('.repo_' + attr_child_id + ' input[name="repository[repo][' + attr_child_id + '][remain_quantity]"]');
            let remain_quantity = parseFloat(remainQuantityObj.val());
            remainQuantityObj.val(parseFloat(remain_quantity + change_quantity));
            totalQuantityObj.data('old_total_quantity', new_total_quantity);
        }

        $(document).ready(function () {
            $('input.total_quantity').on({
                change: function () {
                    let attr_child_id = $(this).data('attr_child_id');
                    updateQuantity(attr_child_id);
                },
                keyup: function () {
                    let attr_child_id = $(this).data('attr_child_id');
                    updateQuantity(attr_child_id);
                },
                keydown: function () {
                    let attr_child_id = $(this).data('attr_child_id');
                    updateQuantity(attr_child_id);
                }
            });
        });
    </script>
@endsection
