@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if(!$parent_attr_object)
                            <button type="button" class="btn btn-info" onclick="location.href = '{{ route("admin_attribute_new_view") }}'">Thêm mới</button>
                        @else
                            <button type="button" class="btn btn-info"
                                    onclick="location.href = '{{ route("admin_attribute_new_view") }}{{isset($params['parent_id']) ? '?parent_id=' . $params['parent_id'] : ''}}'">Thêm thuộc tính con mới
                            </button>
                        @endif
                        <div class="pull-right">
                            <form action="{{route('admin_attribute_list_view')}}" method="GET">
                                @if(isset($params['parent_id']))
                                    <input type="hidden" name="parent_id" value="{{$params['parent_id']}}">
                                @endif
                                @if(isset($params['status']))
                                    <input type="hidden" name="status" value="{{$params['status']}}">
                                @endif
                                @if(isset($params['type']))
                                    <input type="hidden" name="status" value="{{$params['type']}}">
                                @endif
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="postTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="postTable" class="table table-bordered" style="min-width: 1000px;">
                                    <thead>
                                    <tr>
                                        {{--<th>ID</th>--}}
                                        <th>Ảnh</th>
                                        <th>Tiêu đề</th>
                                        <th>Chuỗi đường dẫn tĩnh</th>
                                        @if(!$parent_attr_object)
                                            <th>Mô tả</th>
                                        @else
                                            <th>Thứ tự</th>
                                        @endif
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_attr))
                                        @foreach($list_attr as $attr_item)
                                            <tr>
                                                {{--                    <td>{{$attr_item['id']}}</td>--}}
                                                <td><img height="30" class="image_preview" src="{{$attr_item['image'] ? $attr_item['image'] : asset('img/placeholder.png')}}" alt="{{$attr_item['title']}}" title="{{$attr_item['title']}}"></td>
                                                <td><a href="{{route('admin_attribute_update_view', ['attr_id' => $attr_item['id']])}}" class="text-dark">{{$attr_item['title']}}</a></td>
                                                <td>{{$attr_item['slug']}}</td>
                                                @if(!$parent_attr_object)
                                                    <td>{{$attr_item['description']}}</td>
                                                @else
                                                    <td>
                                                        {{$attr_item['menu_order']}}
                                                        {{--@if($attr_item['type'] == 'color')
                                                            <span class="attr_color" style="background-color: {{$attr_item['content']}};"></span>
                                                        @else
                                                            {{$attr_item['content']}}
                                                        @endif--}}
                                                    </td>
                                                @endif
                                                <td>
                                                    <button type="button" class="btn btn-{{$attr_item['status'] ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$attr_item['status'] ? 'Hiện' : 'Ẩn'}}">
                                                        <i class="fa fa-{{$attr_item['status'] ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    @if(!$parent_attr_object)
                                                        <button type="button" class="btn btn-info btn-rounded btn-sm" data-toggle="tooltip" title="Xem thuộc tính con"
                                                                onclick="window.location.href = '{{route('admin_attribute_list_view')}}?parent_id={{$attr_item['id']}}'">
                                                            <i class="fa fa-list-ol"></i> Thuộc tính con
                                                        </button>
                                                        <button type="button" class="btn btn-primary btn-rounded btn-sm" data-toggle="tooltip" title="Thêm mới thuộc tính con"
                                                                onclick="location.href = '{{ route("admin_attribute_new_view") }}{{ '?parent_id=' . $attr_item['id'] }}'">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                        |
                                                    @endif
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_attribute_update_view', ['attr_id' => $attr_item['id']])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-attr_id="{{$attr_item['id']}}" data-toggle="tooltip" title="Xoá">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="7"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <style>
        .attr_color {
            width: 30px;
            height: 30px;
            display: block;
            border: 1px solid #ccc;
        }
    </style>
@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let attr_id = $(this).attr("data-attr_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_attribute_delete')}}",
                        data: {
                            id: attr_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection

