@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form id="Attribute_{{ $attr_object ? 'Update' : 'Create' }}_Form" action="{{$attr_object ? route('admin_attribute_update') : route('admin_attribute_create')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="attribute[parent_id]" value="{{$parent_id ? $parent_id : 0}}">
            @if($attr_object)
                <input type="hidden" name="attribute[id]" value="{{$attr_object['id']}}">
            @endif
            <div class="row">
                <div id="Attribute_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN THUỘC TÍNH</h4>
                            <div class="form-group">
                                <label for="attribute_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="attribute[title]" id="attribute_title" aria-describedby="helpAttributeTitle"
                                       placeholder="Nhập tiêu đề thuộc tính" autocomplete="off"
                                       onkeyup="getSlug('attribute_title', 'attribute_slug', 'attribute')" value="{{$attr_object ? $attr_object['title'] : null}}" required>
                                <small id="helpAttributeTitle" class="form-text text-muted">Tiêu đề thuộc tính là bắt buộc</small>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="attribute_slug_pre">{{url('/thuoc-tinh/')}}/</span>
                                    </div>
                                    <input type="text" class="form-control" name="attribute[slug]" id="attribute_slug" aria-describedby="attribute_slug_pre"
                                           placeholder="Chuỗi cho đường dẫn tĩnh" autocomplete="off" value="{{$attr_object ? $attr_object['slug'] : null}}" readonly required>
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button" data-toggle="tooltip" title="Tạo slug tự động từ tiêu đề" onclick="getSlug('attribute_title', 'attribute_slug', 'attribute')">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="attribute_content">Màu sắc</label>
                                <input type="text" class="colorpicker form-control asColorPicker-input" name="attribute[content]" id="attribute_content"
                                       aria-describedby="helpAttributeContent" placeholder="Ví dụ: #000000" autocomplete="off"
                                       value="{{$attr_object ? $attr_object['content'] : '#000000'}}">
                                <small id="helpAttributeContent" class="form-text text-muted">Nếu chọn loại thuộc tính là màu sắc thì sẽ chọn màu ở đây</small>
                            </div>

                            <div class="form-group">
                                <label for="">Mô tả ngắn</label>
                                <textarea class="form-control" name="attribute[description]" id="attribute_description" rows="3">{{$attr_object ? $attr_object['description'] : null}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Attribute_Sidebar" class="col-12 col-md-3">
                    {{--ĐĂNG BÀI--}}
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>
                            <div class="form-group">
                                <label for="attribute_type">Loại:</label>
                                <select class="form-control" name="attribute[type]" id="attribute_type">
                                    @if(count($attr_type))
                                        @foreach($attr_type as $attr_type_key => $attr_type_value)
                                            <option value="{{$attr_type_key}}" {{$attr_object && $attr_object['type'] == $attr_type_key ? 'selected' : ''}}>{{$attr_type_value}}</option>
                                        @endforeach
                                    @else
                                        <option value="text">Văn bản</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="attribute_menu_order">Thứ tự sắp xếp <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="attribute[menu_order]" id="attribute_menu_order" placeholder="Nhập thứ tự sắp xếp"
                                       autocomplete="off" min="0" step="1" value="{{$attr_object ? $attr_object['menu_order'] : 0}}">
                            </div>
                            <div class="form-group">
                                <label for="attribute_status">Trạng thái: </label>
                                <div class="switch">
                                    <label>Ẩn<input type="checkbox" name="attribute[status]" id="attribute_status" {{$attr_object && $attr_object['status'] == 0 ? '' : 'checked'}}><span class="lever"></span>Hiện</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info">{{$attr_object ? 'Cập nhật' : 'Tạo mới'}}</button>
                        </div>
                    </div>
                    {{--ẢNH ĐẠI DIỆN--}}
                    <div class="card d-none">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.template.image_preview', ['input_name' => 'attribute[image]', 'input_id' => 'attribute_image', 'input_image' => ($attr_object ? $attr_object['image'] : null)])
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/libs/jquery-asColor.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".colorpicker").asColorPicker();
        });
    </script>
@endsection
