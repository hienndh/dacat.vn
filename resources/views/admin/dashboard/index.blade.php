@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-center">CHÀO MỪNG BẠN ĐẾN VỚI TRANG QUẢN TRỊ HỆ THỐNG</h3>
                    </div>
                    <div class="col-12 col-md-4">
                        <p>Các tính năng đang hoạt động:</p>
                        <ol>
                            <li>Quản lý đơn hàng <span class="badge badge-info">mới</span></li>
                            <li>Đổi mật khẩu tài khoản <span class="badge badge-info">mới</span></li>
                            <li>Quản lý bài viết, chuyên mục</li>
                            <li>Quản lý trang tĩnh</li>
                            <li>Quản lý sản phẩm, danh mục</li>
                            <li>Quản lý thuộc tính sản phẩm</li>
                            <li>Cài đặt menu</li>
                            <li>Widget bổ sung</li>
                            <li>Slider</li>
                            <li>Thông tin website - SEO</li>
                        </ol>
                    </div>
                    <div class="col-12 col-md-4">
                        <p>Các tính năng đang phát triển:</p>
                        <ol>
                            <li>Quản lý khách hàng</li>
                            <li>Quản lý bình luận</li>
                        </ol>
                    </div>
                    <div class="col-12 col-md-4">
                        <p>Các tính năng dự kiến:</p>
                        <ol>
                            <li>Quản lý dữ liệu Email Marketing</li>
                            <li>Quản lý phân quyền hệ thống</li>
                            <li>Báo cáo, thống kê hệ thống</li>
                            <li>Đa ngôn ngữ</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-group d-none">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-briefcase-check text-info"></i></h2>
                            <h3 class="">2456</h3>
                            <h6 class="card-subtitle">New Projects</h6></div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                            <h3 class="">546</h3>
                            <h6 class="card-subtitle">Pending Project</h6></div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                            <h3 class="">$24561</h3>
                            <h6 class="card-subtitle">Total Cost</h6></div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                            <h3 class="">$30010</h3>
                            <h6 class="card-subtitle">Total Earnings</h6></div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@section('admin_css')

@endsection

@section('admin_script')
    <!-- Chart JS -->
    <script src="{{asset('admin/main/js/dashboard1.js')}}"></script>
@endsection
